<%--
  Created by IntelliJ IDEA.
  User: se-8
  Date: 11/14/2017
  Time: 12:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <fmt:setLocale value="en_US"/>
    <fmt:setBundle basename="module_en" var="module"/>
    <title>Appointment.lk - Checkout</title>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/font-awesome.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-46070462-3"></script>
    <script>
        function check(){
            document.getElementById("total").innerHTML="0";
            if(document.getElementById("serviceCode").value== 2) {
                document.getElementById("call").checked = false;
                document.getElementById("call1").checked = true;
                document.getElementById("call2").checked = false;
            }else if(document.getElementById("serviceCode").value==1) {
                document.getElementById("call1").checked = false;
                document.getElementById("call").checked = true;
                document.getElementById("call2").checked = false;
            }else if(document.getElementById("serviceCode").value==3){
                document.getElementById("call1").checked = false;
                document.getElementById("call").checked = false;
                document.getElementById("call2").checked = true;
            }


        }
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-46070462-3');
    </script>

</head>
<body onload="check();">
<!--Header Section Start-->
<nav class="navbar navbar-default header-section">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/<fmt:message bundle="${module}" key="moduleName" />"><img
                    src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/logo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<fmt:message bundle="${module}" key="moduleName" />/">Home <span
                        class="sr-only">(current)</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/v1/home">Counseling & Psychology</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/home">Fitness & Wellbeing</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/">Entertainment</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/">Professional Services</a></li>
                    </ul>
                </li>
                <li><a href="<fmt:message bundle="${module}" key="moduleName" />/">About Us</a></li>
                <li><a href="<fmt:message bundle="${module}" key="moduleName" />/">Contact Us</a></li>
                <li><a href="<fmt:message bundle="${module}" key="moduleName" />/register" class="btn-register">Register Your Business</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!--Header Section End-->

<!--Customer Details Form Start-->
<div class="container-fluid checkout-panel">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3 class="suggestion-heading suggestion-heading-top">Reservation Details</h3>

                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row reservation-details">
                                <div class="col-lg-3 col-md-4 col-sm-3 hidden-xs">
                                    <div class="serviceProvider-Image lg-round">
                                        <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${professionalCode}.jpg"/>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-8 col-sm-9 col-xs-12 no-padding-xs">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="serviceProvider-Image visible-xs-block pull-left">
                                                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${professionalCode}.jpg"/>
                                            </div>
                                            <div class="serviceProvider-Details pull-left">
                                                <p>${name}</p>
                                                <span>${qualification}</span>
                                                <span>${specialNote}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="availability-details-location">
                                        <h4>${centerName}</h4>
                                    </div>
                                    <table class="session-details-table">
                                        <thead>
                                        <tr>
                                            <th>Session Details</th>
                                            <th>Reservation No.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <span class="day">${weekDay}, </span><span
                                                    class="date">${month} ${day}</span>
                                                <span class="time">${startTime} - ${finishTime}</span>
                                            </td>
                                            <td>
                                                <span class="reservation-number">${appNo}</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="suggestion-heading">Reservation Form</h3>
                    <t:choose>
                        <t:when test="${serviceCode==1}">
                            <t:set var="action" value="/payment-redirect"/>
                            <t:set var="additionalAstroProperty" value=""/>
                            <t:set var="additionalVetProperty" value="hide"/>
                            <t:set var="additionalbeautyProperty" value="hide"/>
                        </t:when>
                        <t:when test="${serviceCode==2}">
                            <t:set var="action" value="/v1/payment-redirect"/>
                            <t:set var="additionalAstroProperty" value="hide"/>
                            <t:set var="additionalVetProperty" value=""/>
                            <t:set var="additionalbeautyProperty" value="hide"/>
                        </t:when>
                        <t:otherwise>
                            <t:set var="action" value="/b/payment-redirect"/>
                            <t:set var="additionalAstroProperty" value="hide"/>
                            <t:set var="additionalVetProperty" value="hide"/>
                            <t:set var="additionalbeautyProperty" value=""/>
                        </t:otherwise>
                    </t:choose>
                    <form method="post" action="<fmt:message bundle="${module}" key="moduleName" />${action}"
                          name="detail_form">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group form-group-sm">
                                            <label>Title(නාමය)</label>
                                            <select class="form-control" name="title">
                                                <option>Mr.</option>
                                                <option>Mrs.</option>
                                                <option>Ms.</option>
                                                <option>Dr.</option>
                                                <option>Rev.</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 col-xs-12">
                                        <div class="form-group form-group-sm">
                                            <label class="required">Name (නම)</label>
                                            <input type="text" class="form-control" name="customerName" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group form-group-sm">
                                            <label class="required">Nationality (ජාතිය)</label>
                                            <select class="form-control" name="nationality"
                                                    onchange="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                    id="nationality" required>
                                                <option selected value="L">Sri Lankan</option>
                                                <option value="F">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                   <%-- <div class="col-sm-6 col-xs-12">
                                        <div class="form-group form-group-sm">
                                            <label class="required">NIC / Passport (ජා.හෑ.අ / ගමන් බල පත්‍රය)</label>
                                            <input type="text" class="form-control" name="nic" required/>
                                        </div>
                                    </div>--%>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group form-group-sm">
                                            <label class="required">Phone(දුරකතන අංකය)</label>

                                            <div class="input-group">
                                                <%--<div class="input-group-addon">+94</div>--%>
                                                <input type="text" class="form-control" name="telNo" id="tele" required
                                                       pattern="[0-9]{10}" maxlength="10"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group form-group-sm">
                                            <label>Email (විද්‍යුත් ලිපිනය)</label>
                                            <input type="email" id="email" class="form-control" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="additional-fields astrology ${additionalAstroProperty}">
                                   <%-- <div class="row">
                                        <div class="col-sm-6 col-xs-12 col-md-6">
                                            <div class="form-group form-group-sm">
                                                <label>Date of Birth (උපන් දිනය)</label>
                                                <input class="form-control ui-datepicker-DOB" type="date"
                                                       name="b_date"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-10 col-md-3">
                                            <div class="form-group form-group-sm">
                                                <label>Birth Time (උපන් වේලාව) </label>
                                                <input type="text" class="form-control" name="b_time"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-2 col-md-3">
                                            <div class="form-group form-group-sm">
                                                <label>Time Interval (කාල අන්තරය)</label>
                                                <select name="timePeriod" class="form-control">
                                                    <option value="AM">AM</option>
                                                    <option value="PM">PM</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Birth Country(උපන් රට)</label>
                                                <select class="form-control" name="b_country">
                                                    <option>Sri Lanka</option>
                                                    <option>India</option>
                                                    <option>Japan</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Birth Town(උපන් නගරය)</label>
                                                <input type="text" class="form-control" name="b_town"/>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <label>Appointment Type</label>

                                            <div class="form-group form-group-sm">
                                                <%--<label class="custom-control custom-radio">
                                                    <input id="visit" type="radio" class="custom-control-input"
                                                           onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                           value="V" name="appointmentType" checked>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">Visit(පෑමිණීම)</span>
                                                </label>--%>
                                                <label class="custom-control custom-radio">
                                                    <input id="call" name="appointmentType" type="radio"
                                                           onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                           class="custom-control-input" value="C">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description"><%--over the phone/skype(දුරකතනය හෝ ස්කය්ප්)--%>Video Consultation</span>
                                                </label>
                                                <%--<label class="custom-control custom-radio">
                                                    <input id="online" name="appointmentType" type="radio"
                                                           onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                           class="custom-control-input" value="O">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">online(අන්තර් ජාලය හරහා)</span>
                                                </label>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="additional-fields veterinary ${additionalVetProperty}">

                                   <%-- <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Pet Name</label>
                                                <input class="form-control" type="text" name="petName"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Pet Type</label>
                                                <select class="form-control" name="petType">
                                                    <option name="dog">Dog</option>
                                                    <option name="cat">Cat</option>
                                                    <option name="cow">Cow</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Reg No.</label>
                                                <input class="form-control" type="text" name="regNo"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Date of Birth(උපන් දිනය)</label>
                                                <input class="form-control ui-datepicker-DOB" type="date" name="dob"/>
                                            </div>
                                        </div>
                                    </div>--%>
                                       <div class="row">
                                           <div class="col-sm-6 col-xs-12">
                                               <label>Appointment Type</label>

                                               <div class="form-group form-group-sm">
                                                   <%--<label class="custom-control custom-radio">
                                                       <input id="visit1" type="radio" class="custom-control-input"
                                                              onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                              value="V" name="appointmentType" checked>
                                                       <span class="custom-control-indicator"></span>
                                                       <span class="custom-control-description">Visit(පෑමිණීම)</span>
                                                   </label>--%>
                                                   <label class="custom-control custom-radio">
                                                       <input id="call1" name="appointmentType" type="radio"
                                                              onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                              class="custom-control-input" value="C">
                                                       <span class="custom-control-indicator"></span>
                                                       <span class="custom-control-description">Video Consultation</span>
                                                   </label>
                                                   <%--<label class="custom-control custom-radio">
                                                       <input id="online1" name="appointmentType" type="radio"
                                                              onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                              class="custom-control-input" value="O">
                                                       <span class="custom-control-indicator"></span>
                                                       <span class="custom-control-description">online(අන්තර් ජාලය හරහා)</span>
                                                   </label>--%>
                                               </div>
                                           </div>
                                       </div>
                                </div>
                               <div class="additional-fields veterinary ${additionalbeautyProperty}">
                                    <%--<div class="row">
                                      iv class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Time Slot</label>
                                                <select name="appointmentTime" class="form-control">
                                                    <t:forEach items="${timeSlot}" begin="0" var="time"
                                                               varStatus="stat">
                                                        <option value="${time}">${time}</option>
                                                    </t:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        &lt;%&ndash;<div class="col-sm-12 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Speciality</label><br>
                                                <select name="specCode" class="form-control">
                                                    <t:forEach items="${specializationList}" begin="0" var="spec" varStatus="stat">
                                                        <option value="${spec.key}">${spec.value}</option>
                                                    </t:forEach>
                                                </select>
                                            </div>
                                        </div>&ndash;%&gt;
                                    </div>
                                    &lt;%&ndash;<div class="row">

                                    </div>&ndash;%&gt;--%>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <label>Appointment Type</label>

                                                <div class="form-group form-group-sm">
                                                    <label class="custom-control custom-radio">
                                                        <input id="call2" name="appointmentType" type="radio"
                                                               onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                               class="custom-control-input" value="C">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">Video Consultation</span>
                                                    </label>
                                                    <%--<label class="custom-control custom-radio">
                                                        <input id="visit2" type="radio" class="custom-control-input"
                                                               onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                               value="V" name="appointmentType" checked>
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">Visit(පෑමිණීම)</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input id="call2" name="appointmentType" type="radio"
                                                               onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                               class="custom-control-input" value="C">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">over the phone/skype(දුරකතනය හෝ ස්කය්ප්)</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input id="online2" name="appointmentType" type="radio"
                                                               onclick="calculate(${professionalCharge},${centerCharge},${serviceCode})"
                                                               class="custom-control-input" value="O">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">online(අන්තර් ජාලය හරහා)</span>
                                                    </label>--%>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-sm-5 col-sm-offset-1 col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h5 class="reservation-cart-total">Total : <strong><span id="total"> </span>.00
                                            LKR</strong></h5>
                                    </div>
                                </div>
                                <div class="row">
                                   <%-- <div class="col-lg-5 col-sm-6 col-xs-12">
                                        <div class="form-group form-group-sm">
                                            <label>Payment Method(ගෙවීමේ ක්‍රමය)</label>

                                            <div class="form-group form-group-sm">
                                                <select class="form-control" name="paymentMode">
                                                    <option value="HSBC">Visa</option>
                                                    <option value="HSBC">Master</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <%--<t:if test="${serviceCode!=3}">--%>
                                    <input type="hidden" name="specCode" value="${specCode}">
                                       <input type="hidden" name="paymentMode" value="FREE">
                                    <%--</t:if>--%>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group form-group-sm">
                                            <div class="checkbox terms-check">
                                                <label>
                                                    <input type="checkbox" value="" class="required" required>
                                                    <strong>I agree to the <a href="#" data-toggle="modal"
                                                                              data-target="#myModal">terms and
                                                        conditions</a></strong>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                                             aria-labelledby="myModalLabel">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Terms and
                                                            Conditions</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        Any of the terms and provisions of Buyer's order which are
                                                        inconsistent with the terms and provisions hereto shall not be
                                                        binding on APPOINTMENT.LK unless APPOINTMENT.LK consents in
                                                        writing and shall not be considered part of
                                                        the Parties’ Agreement as expressed herein.

                                                        Orders accepted by APPOINTMENT.LK can be cancelled only upon
                                                        written consent of APPOINTMENT.LK and after payment by Buyer of
                                                        reasonable costs and expenses for the effort expended thereon.

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default"
                                                                data-dis66miss="modal">Close
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <button class="btn btn-sm btn-warning reservation-confirm">
                                            Confirm Appointment <%--Confirm Payment--%>
                                            <span class="glyphicon glyphicon-copy"
                                                                  aria-hidden="true"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="sessionId" value="${sessionId}">
                        <input type="hidden" name="serviceCode" id="serviceCode" value="${serviceCode}">
                        <input type="hidden" name="astrologerName" id="astrologerName" value="${name}">
                        <input type="hidden" name="centerName" value="${centerName}">
                        <input type="hidden" name="address" value="${address}">
                        <input type="hidden" name="city" value="${city}">
                        <input type="hidden" name="echFee" id="echFee" value="${echFee}">
                        <input type="hidden" name="profNo" id="profNo" value="${profNo}">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}"
                                                                                                      key="moduleName"/>/resources/images/mainSlider/slide1.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Your Emotional Health</h2>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/index"
                               class="btn btn-primary">Make an Appointment <i class="fa fa-arrow-circle-right"
                                                                              aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}"
                                                                                                      key="moduleName"/>/resources/images/mainSlider/slide5.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Your Physical Health</h2>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/index" class="btn btn-primary">Make
                                an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}"
                                                                                                      key="moduleName"/>/resources/images/mainSlider/slide7.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Your Emotional Health</h2>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/index"
                               class="btn btn-primary">Make an Appointment <i class="fa fa-arrow-circle-right"
                                                                              aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Customer Details Form End-->

<!--People Also Searched For Start-->

<div class="container-fluid text-center">
    <div class="row">
        <div class="col-xs-12">
            <%--<h3 class="suggestion-heading">Related Searches</h3>
            <t:choose>
                <t:when test="${serviceCode==2}">
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=SUGATH+PEMACHANDRA">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-01.jpg"/>
                                </div>
                                <p>Dr. Sugath Pemachandra</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=Pavithra+Eshwara">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-03.jpg"/>
                                </div>
                                <p>Dr. Pavithra Eshwara</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=Sugandhika+Gothami">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-02.jpg"/>
                                </div>
                                <p>Dr. Sugandhika Gothami</p>
                            </a>
                        </li>
                    </ul>
                </t:when>
                <t:when test="${serviceCode==1}">
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=Lux+Senanayake">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/13.jpg"/>
                                </div>
                                <p>Mr. Lux Senanayake</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=Kalum+Gunathilaka">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/2.jpg"/>
                                </div>
                                <p>Mr. Kalum Gunathilaka</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=SUNIL+GAMLATH">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/38.jpg"/>
                                </div>
                                <p>Mr. Sunil Gamalath</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=INDIKA+THOTAWATHTHA">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/32.jpg"/>
                                </div>
                                <p>Mr. Indika Thotawaththa</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=ANURADHA+PERERA">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/39.jpg"/>
                                </div>
                                <p>Mr. Anuradha Perera</p>
                            </a>
                        </li>
                    </ul>
                </t:when>
                <t:otherwise>
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/b/search?name=Nilmini+De+Pinto">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/22.jpg"/>
                                </div>
                                <p>Ms. Nilmini De Pinto</p>
                            </a>
                        </li>
                    </ul>
                </t:otherwise>
            </t:choose>--%>
        </div>
    </div>
</div>

<!--People Also Searched For End-->

<!--Registering Section Start-->
<%--<div class="container-fluid home-register-section text-center">
    <div class="row">
        <div class="col-xs-12">
            <h2>Connecting People with Professionals</h2>
            <h3>List your business, accept online bookings and get discovered by new customers.<br>
                Get Registered Today. </h3>
            <form class="form-inline">
                <div class="form-group form-group-lg">
                    <input type="email" class="form-control" placeholder="Email Address" name="email">
                </div>
                <button type="submit" class="btn btn-lg btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>--%>
<!--Registering Section End-->

<!--Advertisement Slider Section Start-->
<div id="Advertisement-Slider" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-01.jpg"
                     alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="images/advertisements/AdSlide-02.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-03.jpg"
                     alt="...">
            </a>
        </div>
    </div>
</div>
<!--Advertisement Slider Section End-->

<!--FooterLinks Section Start-->
<div class="container-fluid footer-links-section">
    <div class="row">
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Services</h4>
            <ul>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/home">Counseling & Psychology</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/home">Fitness & Wellbeing</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/">Entertainment</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/">Professional Services</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Company</h4>
            <ul class="xs-no-bottom-margin">
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Home</a>
                </li>
                <li>
                    <a href="#">Add Your Business</a>
                </li>
                <li>
                    <a href="#">About Us</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4 class="hidden-xs">&nbsp;</h4>
            <ul>
                <li>
                    <a href="#">Terms & Conditions</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Privacy Policy</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Social Media</h4>
            <ul>
                <li>
                    <a href="https://web.facebook.com/eChannelling/">Facebook</a>
                </li>
                <li>
                    <a href="#">Twitter</a>
                </li>
                <li>
                    <a href="#">Google+</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-lg-offset-1 col-xs-12">
            <h4>Appointment.lk</h4>
            <table class="footer-contact-details-table">
                <tbody>
                <tr>
                    <td>Telephone</td>
                    <td>+94 (071)0 225 225</td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>+94 (011)2 370 979</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>No.409,<br>
                        R.A.De.Mel Mawatha,<br>
                        Colombo 3, Sri Lanka
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--FooterLinks Section End-->

<!--Footer Section Start-->
<div class="container-fluid footer-section">
    <div class="row">
        <div class="col-sm-4 col-xs-12 pull-right">
            <p class="power-credits text-center">POWERED BY <img
                    src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mobitel_logo_white.png"/>
            </p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <%-- <p class="design-credits text-center">Design By : <a href="#" target="_blank">BeetleSoft (Pvt) Ltd.</a></p>--%>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p class="text-center copyrights">Copyright © 2017 appointment.lk<br>
                All Rights Reserved.</p>
        </div>
    </div>
</div>
<!--Footer Section End-->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/jquery-ui.min.js"></script>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/bootstrap.min.js"></script>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/ui-js.js"></script>

<script>
    $(document).ready(function () {
        calculate(${professionalCharge}, ${centerCharge}, ${serviceCode});
    });

    /*function calculate(professionalCharge, centerCharge) {
     if (($('select[name=nationality]').val() == 'L') && $('#visit').is(':checked')) {
     $('#total').text(Math.round(Number(professionalCharge + centerCharge) * 0.1));
     $('#total_description').text("Reservation Fee");
     } else if (($('select[name=nationality]').val() == 'L') && $('#call').is(':checked')) {
     $('#total').text(Math.round(Number(professionalCharge * 11 * 0.1)));
     $('#total_description').text("Total Fee");
     } else if (($('select[name=nationality]').val() == 'F') && $('#visit').is(':checked')) {
     $('#total').text(Math.round(Number(professionalCharge + centerCharge) * 2 * 0.1));
     $('#total_description').text("Reservation Fee");
     } else if (($('select[name=nationality]').val() == 'F') && $('#call').is(':checked')) {
     $('#total').text(Math.round(Number(professionalCharge * 22 * 0.1)));
     $('#total_description').text("Total Fee");
     }
     }*/

    function calculate(professionalCharge, centerCharge, serviceCode) {
        if (serviceCode == 1 || serviceCode == 2 || serviceCode == 3) {
            var y = $('#echFee').val();
            if (y > 0) {
                if (($('select[name=nationality]').val() == 'L') && ($('#visit').is(':checked') || $('#visit1').is(':checked') )) {
                    $('#total').text(Math.ceil(Number(y)));
                    $('#total_description').text("Reservation Fee");
                } else if (($('select[name=nationality]').val() == 'L') && ($('#call').is(':checked') || $('#call1').is(':checked') || $('#call2').is(':checked'))) {
                    $('#total').text(/*Math.ceil(Number(professionalCharge) + Number(y))*/0.00);
                    $('#total_description').text("Total Fee");
                } else if (($('select[name=nationality]').val() == 'F') && ($('#visit').is(':checked') || $('#visit1').is(':checked') )) {
                    $('#total').text(Math.ceil(Number(y) * 2));
                    $('#total_description').text("Reservation Fee");
                } else if (($('select[name=nationality]').val() == 'F') && ($('#call').is(':checked') || $('#call1').is(':checked') || $('#call2').is(':checked') )) {
                    $('#total').text(/*Math.ceil((Number(professionalCharge) + Number(y)) * 2)*/0.00);
                    $('#total_description').text("Total Fee");
                }
            } else {
                if (($('select[name=nationality]').val() == 'L') && $('#visit').is(':checked')) {
                    $('#total').text(Math.ceil((Number(professionalCharge) + Number(centerCharge)) * 0.1));
                    $('#total_description').text("Reservation Fee");
                } else if (($('select[name=nationality]').val() == 'L') && $('#call').is(':checked')) {
                    $('#total').text(Math.ceil((Number(professionalCharge)) * 11 * 0.1));
                    $('#total_description').text("Total Fee");
                } else if (($('select[name=nationality]').val() == 'F') && $('#visit').is(':checked')) {
                    $('#total').text(Number(Math.ceil((Number(professionalCharge) + Number(centerCharge)) * 0.1) * 2));
                    $('#total_description').text("Reservation Fee");
                } else if (($('select[name=nationality]').val() == 'F') && $('#call').is(':checked')) {
                    $('#total').text(Number(Math.ceil((Number(professionalCharge)) * 1.1) * 2));
                    $('#total_description').text("Total Fee");
                }
            }
        }
        else {
            $('#total').text(Math.ceil(Number(professionalCharge) + Number(centerCharge) + 99.0));
        }
    }
    //$('select[name=selector]').val()
    //$('#test2').is(':checked');
</script>

</html>
