<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Appointment.lk - Doctor</title>
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Header Section Start-->
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand sr-only" href="#">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/Appointment/index">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="v1/veterinary/landPage">Veterinary</a></li>
                        <li><a href="/Appointment/landPage">Astrology</a></li>
                        <li><a href="#">Doctors</a></li>
                        <li><a href="#">Beauty Parlours</a></li>
                    </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="/Appointment/index#contact-us">Contact Us</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!--Header Section End-->
<!--Body Section Start-->
<div class="container-fluid content-container">
    <div class="row col-md-9 col-xs-12">
        <iframe src="https://www.echannelling.com/EChWebAPI/indexWeb" width="950" height="700">
            alternative content for browsers which do not support iframe.
        </iframe>
    </div>


    <div class="col-md-3 col-xs-12 detailed-sidebar" <%--data-spy="affix"--%> <%--data-offset-top="20"--%>>
        <div class="category-details" style="background-image: url('resources/images/banner-astrology.jpg')">
            <h1>Astrology</h1>
        </div>
        <div id="advertisements-section" class="carousel slide" data-ride="carousel">
    <p class="ad-indication">Advertisement</p>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href="#" target="_blank">
                <img src="resources/images/mobitel_logo.png"/>
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="resources/images/mobitel_logo.png"/>
            </a>
        </div>
    </div>
            </div>
        </div>
</div>


<!--Body Section End-->
<!--Footer Section Start-->
<footer class="footer text-right">
    <div class="container-fluid">
        <div class="row">
            <span class="copyrights pull-left text-left">Copyright © 2017 appointment.lk<br>All Rights Reserved<br><br>
					<span class="design-credits">Design By: <a href="#" target="_blank">BeetleSoft (Pvt) Ltd</a></span>
            </span>
            <img src="/Appointment/resources/images/mobitel_logo_white.png" class="footer-logo pull-right" />
            <p class="footer-logo-support pull-right">Powered by</p>
        </div>
    </div>
</footer>
<!--Footer Section End-->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/Appointment/resources/js/jquery-ui.min.js"></script>
<script src="/Appointment/resources/js/bootstrap.min.js"></script>
<script src="/Appointment/resources/js/ui-js.js"></script>
<script>
    $(document).ready(function () {
        calculate(${professionalCharge}, ${centerCharge});
    });

    function calculate(professionalCharge, centerCharge) {
        /*if (($('select[name=nationality]').val() == 'L') && $('#visit').is(':checked')) {
         $('#total').text(Math.round(Number((professionalCharge + centerCharge) * 0.1)));
         } else if (($('select[name=nationality]').val() == 'L') && $('#call').is(':checked')) {
         $('#total').text(Math.round(Number(professionalCharge * 11 * 0.1)));
         } else if (($('select[name=nationality]').val() == 'F') && $('#visit').is(':checked')) {
         $('#total').text(Math.round(Number(professionalCharge + centerCharge) * 2 * 0.1));
         } else if (($('select[name=nationality]').val() == 'F') && $('#call').is(':checked')) {
         $('#total').text(Math.round(Number(professionalCharge * 22 * 0.1)));
         }*/
        $('#total').text(Math.round(Number((professionalCharge + centerCharge)+ 99.0)));

    }

    function onSubmit() {
        /*var x = checkNicNo();*/
        var y = checkContactNo();
        if(y) {
            return true;
        }
        else {
            return false;
        }
    }

    function checkNicNo () {
        var nic = document.forms.detail_form.nic.value;
        var match = /[0-9]{9}[x|X|v|V]$/.test(nic);

        if (match) {
            return true;
        }
        else {
            alert('please, Enter valid nic no.');
            document.forms.detail_form.nic.value = "";
            document.forms.detail_form.nic.focus();
            return false;
        }
        console.log((/[0-9]{9}[x|X|v|V]$/g).test(nic));
        console.log(b_day);
    }

    function checkContactNo(){

        var mobileNo = document.forms.detail_form.telNo.value;
        console.log(mobileNo);
        if(mobileNo.substr(0,1) == "0" && mobileNo.length == 9){
            alert('Please Check Phone No Again');
            document.forms.Add.cell.focus();
            return false;
        }else if(mobileNo.length >= 9){
            if(mobileNo.length == 9){
                mobileNo = '0'+mobileNo;
            }
            if(mobileNo.substr(0,1)=='0'){
                return true;
            }else{
                alert(' Please Enter a Valid Phone No');
                document.forms.detail_form.telNo.value = "";
                document.forms.detail_form.telNo.focus();
                return false;
            }
        } else{
            alert('Please Enter a Valid Phone No');
            document.forms.detail_form.telNo.value = "";
            document.forms.detail_form.telNo.focus();
            return false;
        }
    }
</script>
</html>