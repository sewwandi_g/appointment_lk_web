<!DOCTYPE html>
<html lang="en">
  <head>
      <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
      <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Appointment.lk - Home</title>
      <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/bootstrap.min.css" />">
      <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/styles.css" />">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<!--Header Section Start-->
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand sr-only" href="#">Brand</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
			<li class="active"><a href="#">Home</a></li>
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="#">Astrology</a></li>
				<li><a href="#">Veterinary</a></li>
				<li><a href="#">Beauty Parlours</a></li>
				<li><a href="#">Architects</a></li>
			  </ul>
			</li>
			<li><a href="#">About Us</a></li>
			<li><a href="#">Contact Us</a></li>
		  </ul>
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="#">Sign In/ Sign Up</a></li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<!--Header Section End-->
	<!--Body Section Start-->
	<div>
		<h3 class="text-center home-main-support-title">Welcome To</h3>
		<h1 class="text-center home-main-title">Appointment.lk</h1>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<p class="introduction-text">Choose the service to make your appointment :</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<a href="<c:url value="/landPage"/>" class="MainCategory-Item">
						<div class="MainCategory-Item-Image astrology" style="background-image:url('resources/images/astrology-icon.png');"></div>
						<p>Astrology</p>
					</a>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<a href="<c:url value="v1/veterinary/landPage"/>" class="MainCategory-Item">
						<div class="MainCategory-Item-Image veterinary" style="background-image:url('resources/images/veterinary-icon.png');"></div>
						<p>Veterinary</p>
					</a>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<a href="#" class="MainCategory-Item">
						<div class="MainCategory-Item-Image beautyParlours" style="background-image:url('resources/images/beauty-parlours-icon.png');"></div>
						<p>Beauty Parlours</p>
					</a>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-12">
					<a href="#" class="MainCategory-Item">
						<div class="MainCategory-Item-Image architects" style="background-image:url('resources/images/architects-icon.png');"></div>
						<p>Architects</p>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!--Body Section End-->
	<!--Footer Section Start-->
	<footer class="footer navbar-fixed-bottom text-right">
		<div class="container-fluid">
			<div class="row">
				<img src="resources/images/mobitel_logo_white.png" class="footer-logo pull-right" />
				<p class="footer-logo-support pull-right">Powered by</p>
			</div>
		</div>
	</footer>
	<!--Footer Section End-->
  </body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
</html>