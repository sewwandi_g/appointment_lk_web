<!DOCTYPE html>
<html lang="en">
<head>
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Appointment.lk - Home</title>
    <%--<link href="css/jquery-ui.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">--%>
    <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/jquery-ui.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/styles.css" />">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--Header Section Start-->
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand sr-only" href="#">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu">
				  <li><a href="v1/veterinary/landPage">Veterinary</a></li>
                        <li><a href="landPage">Astrology</a></li>
				  <li><a href="https://www.echannelling.com/Echannelling/index">Doctors</a></li>
                        <li><a href="#">Beauty Parlours</a></li>
                    </ul>
                </li>
                <li><a href="#about-us">About Us</a></li>
                <li><a href="#contact-us">Contact Us</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!--Header Section End-->
<!--Body Section Start-->
<div>
    <div id="homepage-slider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#homepage-slider" data-slide-to="0" class="active"></li>
            <li data-target="#homepage-slider" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="resources/images/slider/slide-01.jpg" alt="...">
            </div>
            <div class="item">
                <img src="resources/images/slider/slide-02.jpg" alt="...">
            </div>
        </div>
    </div>

    <div class="container ">
        <div class="row">
            <div class="col-xs-12">
                <h2 id="category" class="text-center section-heading">Select a <strong>Category</strong></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="v1/veterinary/landPage" class="MainCategory-Item veterinary">
                    <div class="MainCategory-Item-Image veterinary" style="background-image:url('resources/images/veterinary-icon.png');"></div>
                    <p>Veterinary</p>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="landPage" class="MainCategory-Item astrology">
                    <div class="MainCategory-Item-Image astrology" style="background-image:url('resources/images/astrology-icon.png');"></div>
                    <p>Astrology</p>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="doctorIndex" class="MainCategory-Item doctors">
                    <div class="MainCategory-Item-Image doctors" style="background-image:url('resources/images/doctor-icon.png');"></div>
                    <p>Doctors</p>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="#" class="MainCategory-Item beautyParlours">
                    <div class="MainCategory-Item-Image beautyParlours" style="background-image:url('resources/images/beauty-parlours-icon.png');"></div>
                    <p>Beauty Parlours</p>
                </a>
            </div>
        </div>
    </div>
    <div class="home-section contact-us-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 id="contact-us" class="text-center section-heading">Contact <strong>Us</strong></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <table class="contact-us-table">
                        <tbody>
                        <tr>
                            <td><strong>Telephone  : </strong></td>
                            <td>+94 (071)0 225 225</td>
                        </tr>
                        <tr>
                            <td><strong>Fax  : </strong></td>
                            <td>+94 (011)2 370 979</td>
                        </tr>
                        <tr>
                            <td><strong>Address : </strong></td>
                            <td>appointment.lk,<br>
                                No.409,<br>
                                R.A.De.Mel Mawatha,<br>
                                Colombo 3, Sri Lanka
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-8 col-xs-12">
                    <%--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1665.3970902742965!2d79.8579920866447!3d6.890106524953352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae25bdb7610adf3%3A0x1aadcbd8fd88fd45!2s409%2C+3+R.+A.+De+Mel+Mawatha%2C+Colombo+00500%2C+Sri+Lanka!5e0!3m2!1sen!2s!4v1506926934567" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>--%>
                        <%--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126749.49868042229!2d79.7856069066531!3d6.899951794202304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae25961b6fd3ec7%3A0xe50048647d988506!2seChannelling+PLC!5e0!3m2!1sen!2slk!4v1508739652799" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>--%>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4710.315118405397!2d79.85416002188988!3d6.904122278450506!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9617290b03ee1d78!2sX+Station!5e0!3m2!1sen!2slk!4v1509420444794" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Body Section End-->
<!--Footer Section Start-->
<footer class="footer text-right">
    <div class="container-fluid">
        <div class="row">
				<span class="copyrights pull-left text-left">Copyright © 2017 appointment.lk<br>All Rights Reserved<br><br>
					<span class="design-credits">Design By: <a href="#" target="_blank">BeetleSoft (Pvt) Ltd</a></span>
				</span>
            <img src="/Appointment/resources/images/mobitel_logo_white.png" class="footer-logo pull-right" />
            <p class="footer-logo-support pull-right">Powered by</p>
        </div>
    </div>
</footer>
<!--Footer Section End-->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="resources/js/jquery-ui.min.js"></script>
<script src="resources/js/bootstrap.min.js"></script>
<script src="resources/js/ui-js.js"></script>
</html>