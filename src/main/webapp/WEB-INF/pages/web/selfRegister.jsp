<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <%--<meta name="viewport" content="width=device-width, initial-scale=1">--%>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Appointment.lk - Home</title>
    <style>
        iframe {
            max-width: 100%;
        }
    </style>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            background-color: black;
        }

        * {
            box-sizing: border-box;
        }

        /* Add padding to containers */
        .container {
            padding: 16px;
            background-color: white;
            border-radius: 15px;
            max-width: 1000px;
        }

        /* Full-width input fields */
        input[type=text], input[type=password], input[type=number], input[type=email], select {
            width: 100%;
            padding: 15px;
            margin: 5px 0 5px 0;
            display: inline-block;
            border: none;
            background: #eee;
            border-radius: 10px;
        }

        input[type=text]:focus, input[type=password]:focus, input[type=number]:focus, input[type=email]:focus {
            background-color: #ddd;
            outline: none;
        }

        /* Overwrite default styles of label names */
        .lblnames {
            padding-top: 20px;
        }

        /* Overwrite default styles of hr */
        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* Set a style for the submit button */
        .registerbtn {

            background-color: #4CAF50;
            color: white;
            padding: 16px 20px;
            margin: 8px 0;
            margin-top: 30px;
            border: none;
            cursor: pointer;
            max-width: 250px;
            width: 50%;
            opacity: 0.9;
            border-radius: 10px;
        }

        .registerbtn:hover {
            opacity: 1;
        }

        /* Add a blue text color to links */
        a {
            color: dodgerblue;
        }

        /* Set a grey background color and center the text of the "sign in" section */
        .signin {
            background-color: #f1f1f1;
            text-align: center;
        }
    </style>
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
    <fmt:setLocale value="en_US"/>
    <fmt:setBundle basename="module_en" var="module"/>
    <%--<link href="css/jquery-ui.min.css" rel="stylesheet">
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <link href="css/font-awesome.min.css" rel="stylesheet">
            <link href="css/styles.css" rel="stylesheet">--%>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/font-awesome.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">

        $(document).ready(function () {
            $(".veterinery").click(function () {
                $('#veterinarian1').tab('show');
            });
        });

        $(document).ready(function () {
            $(".astrologer").click(function () {
                $('#astrologers1').tab('show');
            });
        });

        $(document).ready(function () {
            $(".doctor").click(function () {
                $('#doctor1').tab('show');
            });
        });

        $(document).ready(function () {
            $(".beauty").click(function () {
                $('#beauty1').tab('show');
            });
        });
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-46070462-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-46070462-3');
    </script>

</head>
<body>
<!--Header Section Start-->
<nav class="navbar navbar-default header-section">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/<fmt:message bundle="${module}" key="moduleName" />"><img
                    src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/logo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<fmt:message bundle="${module}" key="moduleName" />/">Home <span
                        class="sr-only">(current)</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Services<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/v1/home" class="veterinery">Counseling
                            & Psychology</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/home" class="astrologer">Fitness
                            & Wellbeing</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/"
                               class="doctor">Entertainment</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/" class="beauty">Professional
                            Services</a></li>
                    </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#" class="btn-register">Register Your Business<fmt:message bundle="${module}"
                                                                                        key="moduleName"/></a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!--Header Section End-->

<!--Main Slider Section Start-->
<%--<div id="Main-Slider" class="carousel slide" data-ride="carousel"></div>--%>
<!--Main Slider Section End-->
<%--<t:choose></t:choose>--%>

<!--Content Section Start-->
<%--<div class="content-section"></div>--%>
<!--Content Section End-->

<!--Registering Section Start-->
<%--<div class="container-fluid home-register-section text-center">--%>
<%--<div class="row">--%>
<%--<div class="col-xs-12">--%>
<%--<h2>Connecting People with Professionals</h2>--%>
<%--<h3>List your business, accept online bookings and get discovered by new customers.<br>--%>
<%--Get Registered Today. </h3>--%>
<%--<form class="form-inline">--%>
<%--<div class="form-group form-group-lg">--%>
<%--<input type="email" class="form-control" placeholder="Email Address">--%>
<%--</div>--%>
<%--<button type="submit" class="btn btn-lg btn-success">Submit</button>--%>
<%--</form>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>

<div class="container-fluid home-register-section text-center">
    <form action="<fmt:message bundle="${module}" key="moduleName" />/register" method="POST">
        <div class="container">
            <h1 style="text-align:center">Registration Form</h1>

            <p>Please fill in this form to register.</p>
            <hr>

            <div>

                <div class="col-md-3 lblnames" style="text-align: left;">

                    <label><b>Full Name</b></label>
                </div>
                <div class="col-md-9 ">
                    <input type="text" placeholder="Type your full name here." name="name" required>
                </div>

            </div>


            <div>

                <div class="col-md-3 lblnames" style="text-align: left;">
                    <label><b>Email Address</b></label>
                </div>
                <div class="col-md-9" style="text-align: left;">

                    <input type="email" placeholder="Enter your email address." name="email" required>
                </div>

            </div>


            <div>
                <div class="col-md-3 lblnames" style="text-align: left;">
                    <label><b>Professional Qualification</b></label>
                </div>
                <div class="col-md-9 ">
                    <input type="text" placeholder="State your professional qualifications." name="qualification"
                           required>
                </div>
            </div>


            <div>
                <div class="col-md-3 lblnames" style="text-align: left;">

                    <label><b>National Identity Card</b></label>
                </div>
                <div class="col-md-9 ">
                    <input type="text" placeholder="Enter NIC number here." name="nic" maxlength="12" minlength="10"
                           required>
                </div>
            </div>

            <div>
                <div class="col-md-3 lblnames" style="text-align: left;">
                    <label><b>Permanent Address</b></label>
                </div>
                <div class="col-md-9 ">
                    <input type="text" placeholder="Enter your official address." name="addr" required>
                </div>

            </div>


            <div>
                <div class="col-md-3 lblnames" style="text-align: left;">
                    <label><b>Contact Number</b></label>
                </div>
                <div class="col-md-9" style="text-align: left;">

                    <input type="number" placeholder="Enter your contact number here." name="contact" minlength="9"
                           maxlength="10" required>
                </div>
            </div>

            <div>
                <div class="col-md-3 lblnames" style="text-align: left;">
                    <label for="category"><b>Service Category</b></label>
                </div>
                <div class="col-md-9 " style="text-align: left; margin: 5px 0 5px 0;">
                    <%--<div style="text-align: left;">--%>

                    <select id="category" name="category">
                        <option value="Counselor and Psychology">Counselor and Psychology</option>
                        <option value="Health And Fitness">Health And Fitness</option>
                        <option value="Entertainment">Entertainment</option>
                        <option value="Professional Service">Professional Service</option>
                    </select>
                    <%--</div>--%>
                </div>

            </div>

            <div>
                <div class="col-md-3 lblnames" style="text-align: left;">
                    <label><b>Sub-Category</b></label>
                </div>
                <div class="col-md-9 ">
                    <input type="text" placeholder="State your speciality." name="sub" required>
                </div>
            </div>

            <button type="submit" class="registerbtn">Register Now</button>
        </div>


        <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
    </form>

</div>
<!--Registering Section End-->

<!--Advertisement Slider Section Start-->
<%--<div id="Advertisement-Slider" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-01.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-02.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-03.jpg" alt="...">
            </a>
        </div>
    </div>
</div>--%>
<!--Advertisement Slider Section End-->

<!--FooterLinks Section Start-->
<div class="container-fluid footer-links-section">
    <div class="row">
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Services</h4>
            <ul>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/home" class="veterinery">Counseling
                        & Psychology</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/home" class="astrologer">Fitness &
                        Wellbeing</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/" class="doctor">Entertainment</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/" class="beauty">Professional
                        Services</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Company</h4>
            <ul class="xs-no-bottom-margin">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Add Your Business</a>
                </li>
                <li>
                    <a href="#">About Us</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4 class="hidden-xs">&nbsp;</h4>
            <ul>
                <li>
                    <a href="#">Terms & Conditions</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Privacy Policy</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Social Media</h4>
            <ul>
                <li>
                    <a href="https://web.facebook.com/eChannelling/">Facebook</a>
                </li>
                <li>
                    <a href="#">Twitter</a>
                </li>
                <li>
                    <a href="#">Google+</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-lg-offset-1 col-xs-12">
            <h4>Appointment.lk</h4>
            <table class="footer-contact-details-table">
                <tbody>
                <tr>
                    <td>Telephone</td>
                    <td>+94 (071)0 225 225</td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>+94 (011)2 370 979</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>No.409,<br>
                        R.A.De.Mel Mawatha,<br>
                        Colombo 3, Sri Lanka
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--FooterLinks Section End-->

<!--Footer Section Start-->
<div class="container-fluid footer-section">
    <div class="row">
        <div class="col-sm-4 col-xs-12 pull-right">
            <p class="power-credits text-center">POWERED BY <img
                    src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mobitel_logo_white.png"/>
            </p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <%-- <p class="design-credits text-center">Design By : <a href="#" target="_blank">BeetleSoft (Pvt) Ltd.</a></p>--%>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p class="text-center copyrights">Copyright © 2017 appointment.lk<br>
                All Rights Reserved.</p>
        </div>
    </div>
</div>
<!--Footer Section End-->
</body>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/jquery-ui.min.js"></script>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/bootstrap.min.js"></script>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/ui-js.js"></script>
</html>