<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Appointment.lk - Home</title>
    <style>
        iframe{
            max-width: 100%;
        }
    </style>
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <fmt:setLocale value = "en_US"/>
    <fmt:setBundle basename="module_en" var="module" />
    <%--<link href="css/jquery-ui.min.css" rel="stylesheet">
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <link href="css/font-awesome.min.css" rel="stylesheet">
            <link href="css/styles.css" rel="stylesheet">--%>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/font-awesome.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">

        $(document).ready(function(){
            $(".veterinery").click(function(){
                $('#veterinarian1').tab('show');
            });
        });

        $(document).ready(function(){
            $(".astrologer").click(function(){
                $('#astrologers1').tab('show');
            });
        });

        $(document).ready(function(){
            $(".doctor").click(function(){
                $('#doctor1').tab('show');
            });
        });

        $(document).ready(function(){
            $(".beauty").click(function(){
                $('#beauty1').tab('show');
            });
        });

        $(function () {
            $("#tagsName").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getNamesOfVeterinary", {
                        term: request.term
                    }, response);
                }
            });
        });

        $(function () {
            $("#tagsCenter").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getCentersOfveterinary", {
                        term: request.term
                    }, response);
                }
            });
        });

        $(function () {
            $("#tagsCity").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getCitiesOfveterinary", {
                        term: request.term
                    }, response);
                }
            });
        });

        $(function () {
            $("#beautyName").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getNamesOfbeauticians", {
                        term: request.term
                    }, response);
                }
            });
        });

        $(function () {
            $("#beautyCenter").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getCentersOfbeauticians", {
                        term: request.term
                    }, response);
                }
            });
        });

        $(function () {
            $("#beautyCity").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getCitiesOfbeauticians", {
                        term: request.term
                    }, response);
                }
            });
        });


        $(function () {
            $("#tagAstroName").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getNames", {
                        term: request.term
                    }, response);
                }
            });
        });


        $(function () {
            $("#tagAstroCenter").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getCenters", {
                        term: request.term
                    }, response);
                }
            });
        });

        /*    function GetAllProperties() {
         $.ajax({
         cache: false,
         url: '/allProfessionals',
         type: 'GET',
         contentType: "application/json; charset=utf-8",
         success: function (response) {
         if (response.list.length > 0) {
         console.log(response.list)
         var $data = $('<table id="mytable"  class="table  table-striped"> </table>');
         var header = "<thead><tr><th>Property Name</th><th>Edit</th></tr></thead>";
         $data.append(header);
         $.each(response.list, function (i, row) {
         var $row = $('<tr/>');
         $row.append($('<td/>').html(row.PropertyName));
         $hidden = $(' <input type="hidden" name="hid" value= "' + row.PropertyId + '">');
         $row.append($hidden);
         $editButton = $("<button class='editbtn' id='mybtn'>Edit</button>");
         $row.append($editButton);
         $deleteButton = $("<button class='deletebtn' id='delbtn'>Delete</button>");
         $row.append($deleteButton);
         $data.append($row);
         });
         $("#MyDiv").empty();
         $("#MyDiv").append($data);
         }
         else {

         }
         },
         error: function (r) {
         alert('Error! Please try again.' + r.responseText);
         console.log(r);
         }
         });
         }*/

    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-46070462-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-46070462-3');
    </script>

</head>
<body>
<!--Header Section Start-->
<nav class="navbar navbar-default header-section">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/<fmt:message bundle="${module}" key="moduleName" />"><img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/logo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<fmt:message bundle="${module}" key="moduleName" />/">Home <span class="sr-only">(current)</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Services<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="veterinery">Counseling & Psychology</a></li>
                        <li><a href="#" class="astrologer">Fitness & Wellbeing</a></li>
                        <li><a href="#" class="doctor">Entertainment</a></li>
                        <li><a href="#" class="beauty">Professional Services</a></li>
                    </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="<fmt:message bundle="${module}" key="moduleName" />/register"  class="btn-register">Register Your Business</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!--Header Section End-->

<%--<!--Main Slider Section Start-->
<div id="Main-Slider" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#Main-Slider" data-slide-to="0" class="active"></li>
        <li data-target="#Main-Slider" data-slide-to="1"></li>
        <li data-target="#Main-Slider" data-slide-to="2"></li>
        <li data-target="#Main-Slider" data-slide-to="3"></li>
        <li data-target="#Main-Slider" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide1.png" alt="...">
            <div class="carousel-caption hidden-xs caption-right">
                <h2>Care For Your Emotional Health</h2>
                &lt;%&ndash;<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua.</p>&ndash;%&gt;
                <a href="#" class="btn btn-primary veterinery">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="item">
            <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide5.png" alt="...">
            <div class="carousel-caption hidden-xs caption-left">
                <h2>Care For Your Physical Health</h2>
                &lt;%&ndash;<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua.</p>&ndash;%&gt;
                <a href="#" class="btn btn-primary astrologer">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="item">
            <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide7.png" alt="...">
            <div class="carousel-caption hidden-xs caption-right">
                <h2>Care For Your Emotional Health</h2>
                &lt;%&ndash;<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua.</p>&ndash;%&gt;
                <a href="#" class="btn btn-primary veterinery">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="item">
            <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide6.png" alt="...">
            <div class="carousel-caption hidden-xs caption-right">
                <h2>Care For Your  Physical Health</h2>
                &lt;%&ndash;<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua.</p>&ndash;%&gt;
                <a href="#" class="btn btn-primary astrologer">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="item">
            <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide4.png" alt="...">
            <div class="carousel-caption hidden-xs caption-right">
                <h2>Care For Your  Entertainment</h2>
                &lt;%&ndash;<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    ut labore et dolore magna aliqua.</p>&ndash;%&gt;
                <a href="#" class="btn btn-primary doctor">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>--%>
<!--Main Slider Section End-->
<t:choose>
    <t:when test="${serviceCode=='1'}">
        <t:set value="active" var="activeAstro" />
        <t:set value="" var="activeVet" />
        <t:set value="" var="activebeauty"/>
    </t:when>
    <t:when test="${serviceCode=='2'}">
        <t:set value="active" var="activeVet" />
        <t:set value="" var="activeAstro" />
        <t:set value="" var="activebeauty"/>
    </t:when>
    <t:when test="${veterinarysessions == null && sessions==null}">
        <t:set value="" var="activeVet" />
        <t:set value="" var="activeAstro" />
        <t:set value="active" var="activebeauty"/>
    </t:when>
    <t:otherwise>
        <t:set value="active" var="activeVet" />
        <t:set value="" var="activeAstro" />
        <t:set value="" var="activebeauty"/>
    </t:otherwise>
</t:choose>

<!--Content Section Start-->
<div class="content-section">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="${activeVet}">
            <a href="#veterinarian" aria-controls="veterinarian" id="veterinarian1" role="tab" data-toggle="tab">
                <%-- <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/counselling.png" class="icon-inAct"/>
                 <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/icon-vet-active.png" class="icon-Act"/>--%>
                Counseling/Psychology
            </a>
        </li>
        <li role="presentation" class="${activeAstro}">
            <a href="#astrologers" aria-controls="astrologers" id="astrologers1" role="tab" data-toggle="tab">
                <%--<img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/icon-astro.png" class="icon-inAct"/>
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/icon-astro-active.png" class="icon-Act"/>--%>
                Fitness & Wellbeing
            </a>
        </li>
        <li role="presentation" >
            <a href="#doctors" aria-controls="doctors" id="doctor1" role="tab" data-toggle="tab">
                <%--<img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/icon-doc.png" class="icon-inAct"/>
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/icon-doc-active.png" class="icon-Act"/>--%>
                Entertainment
            </a>
        </li>
        <li role="presentation" class="${activebeauty}">
            <a href="#beauticians" aria-controls="beauticians" id="beauty1" role="tab" data-toggle="tab">
                <%--<img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/icon-beauty.png" class="icon-inAct"/>
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/icon-beauty-active.png" class="icon-Act"/>--%>
                Professional Services
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane ${activeVet}" id="veterinarian">
            <div class="navbar navbar-default navbar-search">
                <div class="col-xs-12">
                    <div class="navbar-form">
                        <form name="vetSearch" action="<fmt:message bundle="${module}" key="moduleName" />/v1/search" method="post">
                            <div class="form-inline">
                                <div class="form-group form-group-sm hidden-md hidden-sm hidden-xs">
                                    <label class="search-label">Search :</label>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-user" aria-hidden="true"></i> Name</label>
                                    <input class="form-control" name="name" id="tagsName"/>
                                </div>
                                <%-- <div class="form-group form-group-sm">
                                     <label><i class="fa fa-map-marker" aria-hidden="true"></i> Centre </label>
                                     <input class="form-control" name="centerName" id="tagsCenter"/>
                                 </div>--%>
                                <%-- <div class="form-group form-group-sm">
                                     <label><i class="fa fa-list" aria-hidden="true"></i> Location</label>
                                     <input class="form-control" name="location" id="tagsCity"/>
                                 </div>--%>
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Date</label>
                                    <input class="form-control ui-datepicker-search" type="datetime"  name="date" />
                                </div>
                                <div class="form-group form-group-sm">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-sm btn-primary">
                                        Search
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>
                    </div>
                </div>
            </div>
            <!--Search Panel Section End-->
            <!--Search Results Section Start-->
            <t:choose>
                <t:when test="${empty veterinarysessions}">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h5 class="text-left search-result-conditions">No Results >></h5>
                            </div>
                        </div>
                    </div>
                </t:when>
                <t:otherwise>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h5 class="text-left search-result-conditions">Search Results >></h5>
                            </div>
                        </div>
                    </div>
                </t:otherwise>
            </t:choose>

            <t:forEach items="${veterinarysessions}" begin="0" var="veterinarycenter" varStatus="stat2">
                <div class="search-result-group">
                    <div class="search-result-heading-wrap text-left">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3 class="search-result-heading">${veterinarycenter.centerName}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row search-result-item-row">
                            <t:forEach items="${veterinarycenter.professionals}" begin="0" var="veterinary" varStatus="stat">
                                <div class="col-md-6 col-xs-12">
                                    <div class="search-result-item">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="no-padding-xs col-sm-2 col-xs-4">
                                                    <div class="serviceProvider-Image">
                                                        <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${veterinary.professionalCode}.jpg"/>
                                                    </div>
                                                </div>
                                                <div class="no-padding-xs text-left col-sm-10 col-xs-8">
                                                    <div class="serviceProvider-Details">
                                                        <p title="${veterinary.firstName}">${veterinary.title} ${veterinary.firstName} ${veterinary.surName}</p>
                                                        <span title="${veterinary.qualification}">${veterinary.qualification}</span>
                                                    </div>
                                                    <button class="btn btn-sm btn-primary btn-availability" type="button" data-toggle="collapse" data-target="#${veterinary.professionalCode}${veterinarycenter.centerCode}" aria-expanded="false" aria-controls="${veterinary.professionalCode}${veterinarycenter.centerCode}">Check Availability</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="collapse availability-details-wrap" id="${veterinary.professionalCode}${veterinarycenter.centerCode}">
                                            <div class="triangle"></div>
                                            <div class="panel">
                                                <div class="panel-body no-padding-xs">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="pull-right">
                                                                    <button class="btn btn-sm btn-default btn-close"
                                                                            type="button" data-toggle="collapse"
                                                                            data-target="#${veterinary.professionalCode}${veterinarycenter.centerCode}"
                                                                            aria-expanded="false"
                                                                            aria-controls="${veterinary.professionalCode}${veterinarycenter.centerCode}"><i
                                                                            class="fa fa-2x fa-times"
                                                                            aria-hidden="true"></i></button>
                                                                </div>
                                                                <div class="user-details pull-left">
                                                                    <div class="serviceProvider-Image pull-left">
                                                                        <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${veterinary.professionalCode}.jpg"/>
                                                                    </div>
                                                                    <div class="serviceProvider-Details pull-left text-left">
                                                                        <p title="${veterinary.firstName}">${veterinary.title} ${veterinary.firstName} ${veterinary.surName}</p>
                                                                        <span title="${veterinary.qualification}">${veterinary.qualification}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability-details-location">
                                                        <h4>${veterinarycenter.centerName} - ${veterinarycenter.city}</h4>
                                                    </div>
                                                    <div class="session-details-table-wrap">
                                                        <table class="session-details-table">
                                                            <thead>
                                                            <tr>
                                                                <th>Session Details</th>
                                                                    <%--<th>Next No. Available</th>--%>
                                                                <th>Speciality</th>
                                                                <th></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <t:forEach items="${veterinary.sessions}" begin="0" var="sessions"
                                                                       varStatus="stat1">
                                                                <form name="" action="<fmt:message bundle="${module}" key="moduleName" />/v1/client-details" method="post">
                                                                    <t:set var="fullDay" value="${sessions.appointmentDate}"/>
                                                                    <t:set var="dateParts" value="${fn:split(fullDay, '-')}"/>
                                                                    <t:set var="year" value="${dateParts[0]}"/>
                                                                    <t:set var="month" value="${dateParts[1]}"/>
                                                                    <t:set var="day" value="${dateParts[2]}"/>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="day">${sessions.date}, </span> <span
                                                                                class="date">${sessions.month} ${day}</span>
                                                                            <span class="time">${sessions.startTime} - ${sessions.finishTime}</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="date">${sessions.speciality}</span>
                                                                        </td>
                                                                        <t:choose>
                                                                            <%--<t:when test="${sessions.availability==false}">--%><t:when test="${sessions.availability == 'AVAILABLE'}">
                                                                            <td>
                                                                                <button class="btn btn-sm btn-primary">${sessions.availability} <i
                                                                                        class="fa fa-bookmark" aria-hidden="true"></i>
                                                                                </button>
                                                                            </td>
                                                                        </t:when>
                                                                            <t:otherwise>
                                                                                <td>
                                                                                    <button class="btn btn-sm btn-primary disabled" disabled>${sessions.availability} <i
                                                                                            class="fa fa-bookmark" aria-hidden="true"></i>
                                                                                    </button>
                                                                                </td>
                                                                            </t:otherwise>
                                                                        </t:choose>
                                                                            <%--<td>
                                                                                <button class="btn btn-sm btn-primary">Book <span
                                                                                        class="hidden-xs">Now</span> <i
                                                                                        class="fa fa-bookmark" aria-hidden="true"></i>
                                                                                </button>
                                                                            </td>--%>
                                                                    </tr>
                                                                    <input type="hidden" name="sessionId" value="${sessions.sessionId}">
                                                                    <input type="hidden" name="speciality" value="${sessions.speciality}">
                                                                    <input type="hidden" name="serviceCode" value="${veterinary.serviceCode}">
                                                                    <input type="hidden" name="qualification" value="${veterinary.qualification}">
                                                                    <input type="hidden" name="specialNote" value="${veterinary.specialNote}">
                                                                    <input type="hidden" name="centerCode" value="${veterinarycenter.centerCode}">
                                                                    <input type="hidden" name="specCode"
                                                                           value="${sessions.specializationId}">
                                                                    <input type="hidden" name="profNo"
                                                                           value="${veterinary.mobNo}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}centerName"
                                                                           name="centerName" value="${sessions.centerName}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}name"
                                                                           name="veterinaryName" value="${veterinary.title} ${veterinary.firstName} ${veterinary.surName}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}startTime"
                                                                           name="startTime" value="${sessions.startTime}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}finishTime"
                                                                           name="finishTime" value="${sessions.finishTime}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}month" name="month"
                                                                           value="${sessions.month}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}appointmentNo"
                                                                           name="appNo" value="${sessions.appointmentNo}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}date"
                                                                           name="date" value="${sessions.date}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}total" name="total"
                                                                           value="${sessions.total}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}address"
                                                                           name="address" value="${veterinarycenter.address}">
                                                                    <input type="hidden" name="centerCharge" value="${sessions.centerCharge}">
                                                                    <input type="hidden" name="professionalCharge"
                                                                           value="${sessions.professionalCharge}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}city" name="city"
                                                                           value="${sessions.city}">
                                                                    <input type="hidden" name="professionalCode" id="astro${stat.index}${stat1.index}professionalCode"
                                                                           value="${veterinary.professionalCode}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}year" name="year"
                                                                           value="${year}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}day" name="day"
                                                                           value="${day}">
                                                                    <input type="hidden" name="weekDay"
                                                                           value="${sessions.date}">
                                                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                                </form>
                                                            </t:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </t:forEach>
                        </div>
                    </div>
                </div>
            </t:forEach>
            <%--serach panel result end--%>
            <%--<h3 class="suggestion-heading">All Professionals</h3>
            <ul class="suggestion-list">
                <t:forEach items="${veterinarysessions}" begin="0" var="allvetcenter">
                    <t:forEach items="${allvetcenter.professionals}" begin="0" var="allvetprofessional">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${allvetprofessional.professionalCode}.jpg"/>
                                </div>
                                <p>${allvetprofessional.title} ${allvetprofessional.firstName} ${allvetprofessional.surName}</p>
                            </a>
                        </li>
                    </t:forEach>
                </t:forEach>
            </ul>--%>
        </div>
        <div role="tabpanel" class="tab-pane ${activeAstro}" id="astrologers">
            <!--Search Panel Section Start-->
            <div class="navbar navbar-default navbar-search">
                <div class="col-xs-12">
                    <div class="navbar-form">
                        <form name="astroSearch" action="<fmt:message bundle="${module}" key="moduleName" />/search" method="post">
                            <div class="form-inline">
                                <div class="form-group form-group-sm hidden-md hidden-sm hidden-xs">
                                    <label class="search-label">Search :</label>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-user" aria-hidden="true"></i> Name</label>
                                    <input class="form-control" name="name" id="tagAstroName"/>
                                </div>
                                <%--<div class="form-group form-group-sm">
                                    <label><i class="fa fa-map-marker" aria-hidden="true"></i> Centre </label>
                                    <input class="form-control" name="centerName" id="tagAstroCenter"/>
                                </div>--%>
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-list" aria-hidden="true"></i> Specialization </label>
                                    <%--<input class="form-control" name="location"/>--%>
                                    <select class="form-control" name="specCode">
                                        <option value="">By Service Type</option>
                                        <%--<option value="1">All</option>--%>
                                        <%--<option value="10">Porondam</option>
                                        <option value="13">Pooja & Shaanthikarma
                                        </option>
                                        <option value="11">Waasthu</option>
                                        <option value="12">Yanthra & Gems</option>--%>
                                    </select>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Date</label>
                                    <input class="form-control ui-datepicker-search" type="datetime" name="date" />
                                </div>
                                <div class="form-group form-group-sm">
                                    <label>&nbsp;</label>
                                    <button class="btn visible-xs-block visible-sm-inline-block visible-md-inline-block visible-lg-inline-block btn-sm btn-primary">
                                        <span class="hidden-sm">Search</span>
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>
                    </div>
                </div>
            </div>
            <!--Search Panel Section End-->

            <!--Search Results Section Start-->
            <t:choose>
                <t:when test="${empty sessions}">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h5 class="text-left search-result-conditions">No Results >></h5>
                            </div>
                        </div>
                    </div>
                </t:when>
                <t:otherwise>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h5 class="text-left search-result-conditions">Search Results >></h5>
                            </div>
                        </div>
                    </div>
                </t:otherwise>
            </t:choose>
            <t:forEach items="${sessions}" begin="0" var="center" varStatus="stat2">
                <div class="search-result-group">
                    <div class="search-result-heading-wrap text-left">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3 class="search-result-heading"><%--Dr. Neville Fernando Teaching Hospital - Malabe (3)--%>${center.centerName}</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">


                        <div class="row search-result-item-row">
                            <t:forEach items="${center.professionals}" begin="0" var="astrologers" varStatus="stat">
                                <div class="col-md-6 col-xs-12">
                                    <div class="search-result-item">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="no-padding-xs col-sm-2 col-xs-4">
                                                    <div class="serviceProvider-Image">
                                                        <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${astrologers.professionalCode}.jpg"/>
                                                    </div>
                                                </div>
                                                <div class="no-padding-xs text-left col-sm-10 col-xs-8">
                                                    <div class="serviceProvider-Details">
                                                        <p title="${astrologers.firstName}">${astrologers.title} ${astrologers.firstName} ${astrologers.surName}</p>
                                                        <span title="${astrologers.qualification}">${astrologers.qualification}</span>
                                                    </div>
                                                    <button class="btn btn-sm btn-primary btn-availability" type="button" data-toggle="collapse" data-target="#${astrologers.professionalCode}${center.centerCode}" aria-expanded="false" aria-controls="${astrologers.professionalCode}${center.centerCode}">Check Availability</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="collapse availability-details-wrap" id="${astrologers.professionalCode}${center.centerCode}">
                                            <div class="triangle"></div>
                                            <div class="panel">
                                                <div class="panel-body no-padding-xs">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="pull-right">
                                                                    <button class="btn btn-sm btn-default btn-close"
                                                                            type="button" data-toggle="collapse"
                                                                            data-target="#${astrologers.professionalCode}${center.centerCode}"
                                                                            aria-expanded="false"
                                                                            aria-controls="${astrologers.professionalCode}${center.centerCode}"><i
                                                                            class="fa fa-2x fa-times"
                                                                            aria-hidden="true"></i></button>
                                                                </div>
                                                                <div class="user-details pull-left">
                                                                    <div class="serviceProvider-Image pull-left">
                                                                        <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${astrologers.professionalCode}.jpg"/>
                                                                    </div>
                                                                    <div class="serviceProvider-Details pull-left text-left">
                                                                        <p title="${astrologers.firstName}">${astrologers.title} ${astrologers.firstName} ${astrologers.surName}</p>
                                                                        <span title="${astrologers.qualification}">${astrologers.qualification}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability-details-location">
                                                        <h4>${center.centerName} - ${center.city}</h4>
                                                    </div>
                                                    <div class="session-details-table-wrap">
                                                        <table class="session-details-table">
                                                            <thead>
                                                            <tr>
                                                                <th>Session Details</th>
                                                                    <%--<th>Next No. Available</th>--%>
                                                                <th>Speciality</th>
                                                                <th></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <t:forEach items="${astrologers.sessions}" begin="0" var="sessions"
                                                                       varStatus="stat1">
                                                                <form name="" action="<fmt:message bundle="${module}" key="moduleName" />/client-details" method="post">
                                                                    <t:set var="fullDay" value="${sessions.appointmentDate}"/>
                                                                    <t:set var="dateParts" value="${fn:split(fullDay, '-')}"/>
                                                                    <t:set var="year" value="${dateParts[0]}"/>
                                                                    <t:set var="month" value="${dateParts[1]}"/>
                                                                    <t:set var="day" value="${dateParts[2]}"/>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="day">${sessions.date}, </span><span
                                                                                class="date">${sessions.month} ${day}</span>
                                                                            <span class="time">${sessions.startTime} - ${sessions.finishTime}</span>
                                                                        </td>
                                                                            <%--<td>
                                                                                <span class="reservation-number">${sessions.appointmentNo}</span>
                                                                            </td>--%>
                                                                        <td>
                                                                                <%--<span class="reservation-number">${sessions.speciality}</span>--%>
                                                                            <span class="date">${sessions.speciality}</span>
                                                                        </td>
                                                                        <t:choose>
                                                                            <%--<t:when test="${sessions.availability==false}">--%><t:when test="${sessions.availability == 'AVAILABLE'}">
                                                                            <td>
                                                                                <button class="btn btn-sm btn-primary">${sessions.availability} <i
                                                                                        class="fa fa-bookmark" aria-hidden="true"></i>
                                                                                </button>
                                                                            </td>
                                                                        </t:when>
                                                                            <t:otherwise>
                                                                                <td>
                                                                                    <button class="btn btn-sm btn-primary disabled" disabled>${sessions.availability} <i
                                                                                            class="fa fa-bookmark" aria-hidden="true"></i>
                                                                                    </button>
                                                                                </td>
                                                                            </t:otherwise>
                                                                        </t:choose>
                                                                    </tr>
                                                                    <input type="hidden" name="sessionId" value="${sessions.sessionId}">
                                                                    <input type="hidden" name="speciality" value="${sessions.speciality}">
                                                                    <input type="hidden" name="serviceCode" value="${astrologers.serviceCode}">
                                                                    <input type="hidden" name="qualification" value="${astrologers.qualification}">
                                                                    <input type="hidden" name="specialNote" value="${astrologers.specialNote}">
                                                                    <input type="hidden" name="centerCode" value="${center.centerCode}">
                                                                    <input type="hidden" name="specCode"
                                                                           value="${sessions.specializationId}">
                                                                    <input type="hidden" name="profNo"
                                                                           value="${astrologers.mobNo}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}centerName"
                                                                           name="centerName" value="${center.centerName}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}name"
                                                                           name="astrologerName" value="${astrologers.title} ${astrologers.firstName} ${astrologers.surName}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}startTime"
                                                                           name="startTime" value="${sessions.startTime}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}finishTime"
                                                                           name="finishTime" value="${sessions.finishTime}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}month" name="month"
                                                                           value="${sessions.month}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}appointmentNo"
                                                                           name="appNo" value="${sessions.appointmentNo}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}date"
                                                                           name="date" value="${sessions.date}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}total" name="total"
                                                                           value="${sessions.total}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}address"
                                                                           name="address" value="${center.address}">
                                                                    <input type="hidden" name="centerCharge" value="${sessions.centerCharge}">
                                                                    <input type="hidden" name="professionalCharge"
                                                                           value="${sessions.professionalCharge}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}city" name="city"
                                                                           value="${sessions.city}">
                                                                    <input type="hidden" name="professionalCode" id="astro${stat.index}${stat1.index}professionalCode"
                                                                           value="${astrologers.professionalCode}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}year" name="year"
                                                                           value="${year}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}day" name="day"
                                                                           value="${day}">
                                                                    <input type="hidden" name="weekDay"
                                                                           value="${sessions.date}">
                                                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                                </form>
                                                            </t:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </t:forEach>
                        </div>

                    </div>

                </div>
            </t:forEach>
            <%-- <h3 class="suggestion-heading">Most Searched</h3>
             <ul class="suggestion-list">
                 <li>
                     <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=Lux+Senanayake">
                         <div class="serviceProvider-Image lg-round">
                             <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/13.jpg"/>
                         </div>
                         <p>Mr. Lux Senanayake</p>
                     </a>
                 </li>
                 <li>
                     <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=Kalum+Gunathilaka">
                         <div class="serviceProvider-Image lg-round">
                             <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/2.jpg"/>
                         </div>
                         <p>Mr. Kalum Gunathilaka</p>
                     </a>
                 </li>
                 <li>
                     <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=NISHANTHA+PERERA">
                         <div class="serviceProvider-Image lg-round">
                             <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/14.jpg"/>
                         </div>
                         <p>Mr. Nishantha Perera</p>
                     </a>
                 </li>
                 <li>
                     <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=INDIKA+THOTAWATHTHA">
                         <div class="serviceProvider-Image lg-round">
                             <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/32.jpg"/>
                         </div>
                         <p>Mr. Indika Thotawaththa</p>
                     </a>
                 </li>
                 <li>
                     <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=RANJITH+OPATHA">
                         <div class="serviceProvider-Image lg-round">
                             <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/37.jpg"/>
                         </div>
                         <p>Mr. Ranjith Opatha</p>
                     </a>
                 </li>
             </ul>--%>
        </div>
        <div role="tabpanel" class="tab-pane" id="doctors">
            <div class="navbar navbar-default navbar-search">
                <div class="col-xs-12">
                    <div class="navbar-form">
                    <jsp:include page="underConstruction.jsp"/>
                    <%--<iframe src="https://www.echannelling.com/EChWebAPI/indexWeb" width="950" height="1000">
                    <%--<iframe src="https://www.echannelling.com/EChWebAPI/indexWeb" width="950" height="1000">
                        alternative content for browsers which do not support iframe.
                    </iframe>--%>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane ${activebeauty}" id="beauticians">
            <div class="navbar navbar-default navbar-search">
                <div class="col-xs-12">
                    <div class="navbar-form">
                        <%--<form name="vetSearch" action="<fmt:message bundle="${module}" key="moduleName" />/b/search" method="post">
                            <div class="form-inline">
                                <div class="form-group form-group-sm hidden-md hidden-sm hidden-xs">
                                    <label class="search-label">Search :</label>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-user" aria-hidden="true"></i> Name</label>
                                    <input class="form-control" name="name" id="beautyName"/>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-list" aria-hidden="true"></i> Specialization </label>
                                    <select class="form-control" name="specCode">
                                        <option value="">By Service Type</option>
                                        <option value="1">Financial Services</option>
                                        <option value="10">Legal</option>
                                        <option value="13">Guide</option>
                                        <option value="11">Life Coaching</option>
                                    </select>
                                </div>
                                &lt;%&ndash;<div class="form-group form-group-sm">
                                    <label><i class="fa fa-map-marker" aria-hidden="true"></i> Centre </label>
                                    <input class="form-control" name="centerName" id="beautyCenter"/>
                                </div>
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-list" aria-hidden="true"></i> Location</label>
                                    <input class="form-control" name="location" id="beautyCity"/>
                                </div>&ndash;%&gt;
                                <div class="form-group form-group-sm">
                                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Date</label>
                                    <input class="form-control ui-datepicker-search" type="datetime"  name="date" />
                                </div>
                                <div class="form-group form-group-sm">
                                    <label>&nbsp;</label>
                                    <button class="btn btn-sm btn-primary">
                                        Search
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        </form>--%>
                            <jsp:include page="underConstruction.jsp"/>
                    </div>
                </div>
            </div>
            <!--Search Panel Section End-->
            <!--Search Results Section Start-->
            <t:choose>
                <t:when test="${empty beautySession}">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h5 class="text-left search-result-conditions"><%--No Results >>--%></h5>
                            </div>
                        </div>
                    </div>
                </t:when>
                <t:otherwise>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <h5 class="text-left search-result-conditions">Search Results >></h5>
                            </div>
                        </div>
                    </div>
                </t:otherwise>
            </t:choose>

            <t:forEach items="${beautySession}" begin="0" var="beautyCenter" varStatus="stat2">
                <div class="search-result-group">
                    <div class="search-result-heading-wrap text-left">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h3 class="search-result-heading">${beautyCenter.centerName}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row search-result-item-row">
                            <t:forEach items="${beautyCenter.professionals}" begin="0" var="beautician" varStatus="stat">
                                <div class="col-md-6 col-xs-12">
                                    <div class="search-result-item">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="no-padding-xs col-sm-2 col-xs-4">
                                                    <div class="serviceProvider-Image">
                                                        <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${beautician.professionalCode}.jpg"/>
                                                    </div>
                                                </div>
                                                <div class="no-padding-xs text-left col-sm-10 col-xs-8">
                                                    <div class="serviceProvider-Details">
                                                        <p title="${beautician.firstName}">${beautician.title} ${beautician.firstName} ${beautician.surName}</p>
                                                        <span title="${beautician.qualification}">${beautician.qualification}</span>
                                                    </div>
                                                    <button class="btn btn-sm btn-primary btn-availability" type="button" data-toggle="collapse" data-target="#${beautician.professionalCode}${beautyCenter.centerCode}" aria-expanded="false" aria-controls="${veterinary.professionalCode}${beautician.centerCode}">Check Availability</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="collapse availability-details-wrap" id="${beautician.professionalCode}${beautyCenter.centerCode}">
                                            <div class="triangle"></div>
                                            <div class="panel">
                                                <div class="panel-body no-padding-xs">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="pull-right">
                                                                    <button class="btn btn-sm btn-default btn-close"
                                                                            type="button" data-toggle="collapse"
                                                                            data-target="#${beautician.professionalCode}${beautyCenter.centerCode}"
                                                                            aria-expanded="false"
                                                                            aria-controls="${beautician.professionalCode}${beautyCenter.centerCode}"><i
                                                                            class="fa fa-2x fa-times"
                                                                            aria-hidden="true"></i></button>
                                                                </div>
                                                                <div class="user-details pull-left">
                                                                    <div class="serviceProvider-Image pull-left">
                                                                        <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/${beautician.professionalCode}.jpg"/>
                                                                    </div>
                                                                    <div class="serviceProvider-Details pull-left text-left">
                                                                        <p title="${beautician.firstName}">${beautician.title} ${beautician.firstName} ${beautician.surName}</p>
                                                                        <span title="${beautician.qualification}">${beautician.qualification}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="availability-details-location">
                                                        <h4>${beautyCenter.centerName} - ${beautyCenter.city}</h4>
                                                    </div>
                                                    <div class="session-details-table-wrap">
                                                        <table class="session-details-table">
                                                            <thead>
                                                            <tr>
                                                                <th>Session Details</th>
                                                                    <%--<th>Next No. Available</th>--%>
                                                                <th>Speciality</th>
                                                                <th></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            <t:forEach items="${beautician.sessions}" begin="0" var="sessions"
                                                                       varStatus="stat1">
                                                                <form name="" action="<fmt:message bundle="${module}" key="moduleName" />/b/client-details" method="post">
                                                                    <t:set var="fullDay" value="${sessions.appointmentDate}"/>
                                                                    <t:set var="dateParts" value="${fn:split(fullDay, '-')}"/>
                                                                    <t:set var="year" value="${dateParts[0]}"/>
                                                                    <t:set var="month" value="${dateParts[1]}"/>
                                                                    <t:set var="day" value="${dateParts[2]}"/>
                                                                    <tr>
                                                                        <td>
                                                                            <span class="day">${sessions.date}, </span> <span
                                                                                class="date">${sessions.month} ${day}</span>
                                                                            <span class="time">${sessions.startTime} - ${sessions.finishTime}</span>
                                                                        </td>
                                                                        <td>
                                                                            <span class="date">${sessions.speciality}</span>
                                                                        </td>
                                                                        <td>
                                                                            <button class="btn btn-sm btn-primary">Book <span
                                                                                    class="hidden-xs">Now</span> <i
                                                                                    class="fa fa-bookmark" aria-hidden="true"></i>
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                    <input type="hidden" name="sessionId" value="${sessions.sessionId}">
                                                                    <input type="hidden" name="speciality" value="${sessions.speciality}">
                                                                    <input type="hidden" name="serviceCode" value="${beautician.serviceCode}">
                                                                    <input type="hidden" name="qualification" value="${beautician.qualification}">
                                                                    <input type="hidden" name="specialNote" value="${beautician.specialNote}">
                                                                    <input type="hidden" name="specCode"
                                                                           value="${sessions.specializationId}">
                                                                    <input type="hidden" name="profNo"
                                                                           value="${beautician.mobNo}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}centerName"
                                                                           name="centerName" value="${sessions.centerName}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}name"
                                                                           name="professionName" value="${beautician.title} ${beautician.firstName} ${beautician.surName}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}startTime"
                                                                           name="startTime" value="${sessions.startTime}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}finishTime"
                                                                           name="finishTime" value="${sessions.finishTime}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}month" name="month"
                                                                           value="${sessions.month}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}appointmentNo"
                                                                           name="appNo" value="${sessions.appointmentNo}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}date"
                                                                           name="date" value="${sessions.date}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}total" name="total"
                                                                           value="${sessions.total}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}address"
                                                                           name="address" value="${sessions.address}">
                                                                    <input type="hidden" name="centerCharge" value="${sessions.centerCharge}">
                                                                    <input type="hidden" name="professionalCharge"
                                                                           value="${sessions.professionalCharge}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}city" name="city"
                                                                           value="${sessions.city}">
                                                                    <input type="hidden" name="professionalCode" id="astro${stat.index}${stat1.index}professionalCode"
                                                                           value="${beautician.professionalCode}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}year" name="year"
                                                                           value="${year}">
                                                                    <input type="hidden" id="astro${stat.index}${stat1.index}day" name="day"
                                                                           value="${day}">
                                                                    <input type="hidden" name="weekDay"
                                                                           value="${sessions.date}">
                                                                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                                </form>
                                                            </t:forEach>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </t:forEach>
                        </div>
                    </div>
                </div>
            </t:forEach>
            <%--serach panel result end--%>
            <%--<h3 class="suggestion-heading">Most Searched</h3>
            <ul class="suggestion-list">
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/b/search?name=Nilmini+De+Pinto">
                        <div class="serviceProvider-Image lg-round">
                            <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/22.jpg"/>
                        </div>
                        <p>Ms. Nilmini De Pinto</p>
                    </a>
                </li>
            </ul>--%>
        </div>
    </div>
</div>
<!--Content Section End-->

<!--Registering Section Start-->
<%--<div class="container-fluid home-register-section text-center">
    <div class="row">
        <div class="col-xs-12">
            <h2>Connecting People with Professionals</h2>
            <h3>List your business, accept online bookings and get discovered by new customers.<br>
                Get Registered Today. </h3>
            <form class="form-inline">
                <div class="form-group form-group-lg">
                    <input type="email" class="form-control" placeholder="Email Address">
                </div>
                <button type="submit" class="btn btn-lg btn-success">Submit</button>
            </form>
        </div>
    </div>
</div>--%>
<!--Registering Section End-->

<!--Advertisement Slider Section Start-->
<%--<div id="Advertisement-Slider" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-01.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-02.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-03.jpg" alt="...">
            </a>
        </div>
    </div>
</div>--%>
<!--Advertisement Slider Section End-->

<!--FooterLinks Section Start-->
<div class="container-fluid footer-links-section">
    <div class="row">
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Services</h4>
            <ul>
                <li>
                    <a href="#" class="veterinery">Counseling & Psychology</a>
                </li>
                <li>
                    <a href="#" class="astrologer">Fitness & Wellbeing</a>
                </li>
                <li>
                    <a href="#" class="doctor">Entertainment</a>
                </li>
                <li>
                    <a href="#" class="beauty">Professional Services</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Company</h4>
            <ul class="xs-no-bottom-margin">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Add Your Business</a>
                </li>
                <li>
                    <a href="#">About Us</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4 class="hidden-xs">&nbsp;</h4>
            <ul>
                <li>
                    <a href="#">Terms & Conditions</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Privacy Policy</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Social Media</h4>
            <ul>
                <li>
                    <a href="https://web.facebook.com/eChannelling/">Facebook</a>
                </li>
                <li>
                    <a href="#">Twitter</a>
                </li>
                <li>
                    <a href="#">Google+</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-lg-offset-1 col-xs-12">
            <h4>Appointment.lk</h4>
            <table class="footer-contact-details-table">
                <tbody>
                <tr>
                    <td>Telephone</td>
                    <td>+94 (071)0 225 225</td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>+94 (011)2 370 979</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>No.409,<br>
                        R.A.De.Mel Mawatha,<br>
                        Colombo 3, Sri Lanka
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--FooterLinks Section End-->

<!--Footer Section Start-->
<div class="container-fluid footer-section">
    <div class="row">
        <div class="col-sm-4 col-xs-12 pull-right">
            <p class="power-credits text-center">POWERED BY <img
                    src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mobitel_logo_white.png"/></p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <%-- <p class="design-credits text-center">Design By : <a href="#" target="_blank">BeetleSoft (Pvt) Ltd.</a></p>--%>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p class="text-center copyrights">Copyright © 2017 appointment.lk<br>
                All Rights Reserved.</p>
        </div>
    </div>
</div>
<!--Footer Section End-->
</body>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/jquery-ui.min.js"></script>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/bootstrap.min.js"></script>
<script src="<fmt:message bundle="${module}" key="moduleName" />/resources/js/ui-js.js"></script>
</html>