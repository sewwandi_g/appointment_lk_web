<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Appointment.lk - Veterinary</title>
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    <%--  <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/styles.css" rel="stylesheet">--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-46070462-3"></script>
    <%--<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->--%>

    <%--<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">--%>
    <%--<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>--%>
    <%--<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>--%>
    <script type="text/javascript">

        $(function () {
            $("#tagsName").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getNamesOfVeterinary", {
                        term: request.term
                    }, response);
                }
            });
        });

        $(function () {
            $("#tagsCenter").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getCentersOfveterinary", {
                        term: request.term
                    }, response);
                }
            });
        });

        $(function () {
            $("#tagsCity").autocomplete({
                source: function (request, response) {
                    $.getJSON("${pageContext.request.contextPath}/getCitiesOfveterinary", {
                        term: request.term
                    }, response);
                }
            });
        });


        function displayFirstSession(astroid){
            //alert("------------------------------------------"+astroid);
            $("#displayName").text($("#astro"+astroid+"0name").val());
            $("#displayNumber").text($('#astro'+astroid+'0appointmentNo').val());
            $("#displayMonth").text($('#astro'+astroid+'0month').val());
            //$("#displayDate").text($('#'+astroid+'1name').val());
            $("#displayYear").text($('#astro'+astroid+'0year').val());
            $("#displayDay").text($('#astro'+astroid+'0day').val());
            $("#displayStartTime").text($('#astro'+astroid+'0startTime').val());
            $("#displayFinishTime").text($('#astro'+astroid+'0finishTime').val());
            $("#displayCenterName").text($('#astro'+astroid+'0centerName').val());
            $("#displayCenterAddress").text($('#astro'+astroid+'0address').val());
            $("#displayDate").text($('#astro'+astroid+'0date').val());
            /*$("#displayTotal").text($('#astro'+astroid+'0total').val());*/

            var profCode = ($('#astro'+astroid+'0professionalCode').val());
            /*$("#finalImage").html("<div class=\"serviceProvider-image\"><img src=\"/Appointment/resources/images/userImages/" + profCode + ".jpg\"/></div>");*/
            $("#finalImage").html("<div class=\"serviceProvider-image\"><img src=\"/Appointment/resources/images/userImages/" + profCode + ".jpg\"/></div>");

            /*$("#finalImage").text($('#astro'+astroid+'0professionalCode').val());*/
        }

    </script>
</head>
<body>
<!--Header Section Start-->
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand sr-only" href="#">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/Appointment/index">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Veterinary</a></li>
                        <li><a href="/Appointment/landPage">Astrology</a></li>
                        <li><a href="doctorIndex">Doctors</a></li>
                        <li><a href="#">Beauty Parlours</a></li>
                    </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="/Appointment/index#contact-us">Contact Us</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<!--Header Section End-->
<!--Body Section Start-->
<div class="container-fluid content-container">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <!--Search Bar Section Start-->
            <div class="row">
                <div class="navbar navbar-default navbar-search">
                    <div class="col-xs-12">
                        <div class="navbar-form navbar-left">
                            <form action="/Appointment/v1/veterinary/search" method="get">
                                <div class="form-inline">
                                    <div class="form-group form-group-sm">
                                        <label class="search-label">Search :</label>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                            </div>
                                            <input class="form-control" name="name"
                                                   placeholder="By Doctor Name" id="tagsName"/>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-addon">
                                                <span class=" glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            </div>
                                            <input class="form-control" name="center"
                                                   placeholder="By Centre" id="tagsCenter"/>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-addon">
                                                <span class=" glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                            </div>
                                            <input class="form-control" name="location" id="tagsCity"
                                                   placeholder="By Location"/>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <div class="input-group input-group-sm">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                            </div>
                                            <input class="form-control ui-datepicker-search" type="text" name="date"
                                                   placeholder="By Date Available"/>
                                        </div>
                                    </div>
                                    <button class="btn btn-sm btn-default">
                                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                                    </button>
                                </div>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Search Bar Section End-->

            <!--Search Results Section Start-->
            <t:choose>
            <t:when test="${not empty veterinarysessions}">
            <h5 class="search-criteria">Results for >> <strong>Searched</strong></h5>


            <div class="panel-group serviceProvider-list serviceProvider-list-veterinary" id="accordion" role="tablist"
                 aria-multiselectable="true">
                <t:forEach items="${veterinarysessions}" begin="0" var="veterinary" varStatus="stat">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="${veterinary.firstName}">
                            <div class="row">
                                <div class="serviceProvider-image-wrap pull-left">
                                    <div class="serviceProvider-image">
                                        <img src="/Appointment/resources/images/userImages/${veterinary.professionalCode}.jpg"/>
                                    </div>
                                </div>
                                <div class="serviceProvider-details-wrap pull-left">
                                    <div class="serviceProvider-details">
                                        <h4 class="panel-title"><%--<span>Mrs.</span>--%>${veterinary.firstName}</h4>
                                        <p>${veterinary.qualification}</p>
                                        <p class="language-list"><%--<strong>Languages : </strong>--%><span>${veterinary.specialNote}</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="providingServices-List-wrap pull-right">
                                    <div class="form-group form-group-sm form-group-reservation form-group-reservation-vet text-right">
                                        <a type="button" class="btn btn-sm btn-danger collapsed" role="button"
                                           data-toggle="collapse" data-parent="#accordion" href="#${stat.index}"
                                           aria-expanded="false" aria-controls="${stat.index}" onclick="displayFirstSession('${stat.index}')">Click for Reservation
                                            <span class="glyphicon glyphicon-copy" aria-hidden="true"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="${stat.index}" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="${veterinary.firstName}">
                            <div class="panel-body">
                                <table class="table table-striped table-hover table-condensed sessionTable">
                                    <thead>
                                    <tr>
                                        <th>Session Details</th>
                                        <th>Location / Centre</th>
                                        <th>Next No. Available</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <t:forEach items="${veterinary.sessions}" begin="0" var="sessions"
                                               varStatus="stat1">
                                        <form name="" action="/Appointment/v1/veterinary/client-details" method="post">
                                            <t:set var="fullDay" value="${sessions.appointmentDate}"/>
                                            <t:set var="dateParts" value="${fn:split(fullDay, '-')}"/>
                                            <t:set var="year" value="${dateParts[0]}"/>
                                            <t:set var="month" value="${dateParts[1]}"/>
                                            <t:set var="day" value="${dateParts[2]}"/>
                                            <tr>
                                                <td>
                                                    <p class="sessionTable-date">${sessions.month} ${day},
                                                        <span>${year} (${sessions.date})</span></p>

                                                    <p class="sessionTable-time">${sessions.startTime}
                                                        - ${sessions.finishTime}</p>
                                                </td>
                                                <td>
                                                    <p class="sessionTable-location">
                                                        <strong>${sessions.centerName}</strong><span>${sessions.address} ${sessions.city}</span>
                                                    </p>
                                                </td>
                                                <td>
                                                    <p class="sessionTable-NextNo">Next Number
                                                        <span>${sessions.appointmentNo}</span></p>
                                                </td>
                                                <t:choose>
                                                    <%--<t:when test="${sessions.availability == true}">--%><t:when test="${sessions.availability == 'AVAILABLE'}">
                                                        <td class="text-right">
                                                            <button class="btn btn-sm btn-info">Book Now <span
                                                                    class="glyphicon glyphicon-copy" aria-hidden="true"></span>
                                                            </button>
                                                        </td>
                                                    </t:when>
                                                    <t:otherwise>
                                                        <td class="text-right">
                                                            <button class="btn btn-sm btn-info disabled" disabled>Book Now <span
                                                                    class="glyphicon glyphicon-copy" aria-hidden="true"></span>
                                                            </button>
                                                        </td>
                                                    </t:otherwise>
                                                </t:choose>
                                            </tr>
                                            <input type="hidden" name="sessionId" value="${sessions.sessionId}">
                                            <input type="hidden" name="speciality" value="${sessions.speciality}">
                                            <input type="hidden" name="serviceCode" value="${veterinary.serviceCode}">
                                            <input type="hidden" name="qualification" value="${veterinary.qualification}">
                                            <input type="hidden" name="specialNote" value="${veterinary.specialNote}">
                                            <input type="hidden" name="specCode"
                                                   value="${sessions.specializationId}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}centerName"
                                                   name="centerName" value="${sessions.centerName}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}name"
                                                   name="veterinaryName" value="${veterinary.firstName}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}startTime"
                                                   name="startTime" value="${sessions.startTime}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}finishTime"
                                                   name="finishTime" value="${sessions.finishTime}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}month" name="month"
                                                   value="${sessions.month}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}appointmentNo"
                                                   name="appNo" value="${sessions.appointmentNo}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}date"
                                                   name="date" value="${sessions.date}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}total" name="total"
                                                   value="${sessions.total}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}address"
                                                   name="address" value="${sessions.address}">
                                            <input type="hidden" name="centerCharge" value="${sessions.centerCharge}">
                                            <input type="hidden" name="professionalCharge"
                                                   value="${sessions.professionalCharge}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}city" name="city"
                                                   value="${sessions.city}">
                                            <input type="hidden" name="professionalCode" id="astro${stat.index}${stat1.index}professionalCode"
                                                   value="${veterinary.professionalCode}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}year" name="year"
                                                   value="${year}">
                                            <input type="hidden" id="astro${stat.index}${stat1.index}day" name="day"
                                                   value="${day}">
                                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        </form>
                                    </t:forEach>
                                    </tbody>
                                </table>
                                <div class="text-right">
                                    <a type="button" class="btn btn-sm btn-default collapsed form-group-reservation-bottom" role="button" data-toggle="collapse" data-parent="#accordion" href="#${stat.index}" aria-expanded="false" aria-controls="${stat.index}"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </t:forEach>
            </div>
            <!--Search Results Section End-->
            </t:when>
                <t:otherwise>
                    <h5 class="search-criteria">No <strong>Result</strong></h5>
                </t:otherwise>
            </t:choose>
        </div>
        <div class="col-md-3 col-xs-12 detailed-sidebar" data-spy="affix" data-offset-top="200">
            <div id="selected-Details" class="collapse">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="reservation-heading">Reservation Request Details</h4>
                        <div class="container-fluid reservation-cart">
                            <div class="row">
                                <div class="serviceProvider-details-wrap">
                                    <div class="serviceProvider-details">
                                        <div class="serviceProvider-image-wrap pull-left">
                                            <%--<div class="serviceProvider-image">--%>
                                                <span id="finalImage"></span><%--<img src="/Appointment/resources/images/userImages/01.jpg"/>--%>
                                            <%--</div>--%>
                                        </div>
											<h4 class="panel-title"><span></span><span id="displayName"></span></h4>
											<%--<p>FMRP [Kalkota] Jotish Shankar</p>
											<p>Languages : <span>Sinhala / English</span></p>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row reservation-details-wrap">
                            <div class="col-xs-12">
                                <p>
										<span class="sessionTable-NextNo">Your Number <span><span id="displayNumber"></span></span></span>
										<span class="sessionTable-date"><span id="displayMonth"></span> <span id="displayDay"></span>, <span><span id="displayYear"></span> (<span id="displayDate"></span>)</span></span>
										<span class="sessionTable-time"><span id="displayStartTime"></span> - <span id="displayFinishTime"></span></span>
										<span class="sessionTable-location"><strong><span id="displayCenterName"></span></strong><span><span id="displayCenterAddress"></span></span></span>
                                </p>
                            </div>
                        </div>
                        <%--<div class="row reservation-details-wrap">
                            <div class="col-xs-12">
									<h5 class="reservation-cart-total">Total Charge <span class="pull-right"><span id="displayTotal"></span> LKR</span></h5>
                            </div>
                        </div>--%>
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <button class="btn btn-sm btn-warning reservation-confirm disabled">Confirm Reservation <span
                                        class="glyphicon glyphicon-copy" aria-hidden="true"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="category-details" style="background-image: url('/Appointment/resources/images/banner-veterinary-1.jpg')">
                <h1>Veterinary</h1>
            </div>
            <div id="advertisements-section" class="carousel slide" data-ride="carousel">
                <p class="ad-indication">Advertisement</p>
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <a href="#" target="_blank">
                            <img src="/Appointment/resources/images/mobitel_logo.png"/>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#" target="_blank">
                            <img src="/Appointment/resources/images/mobitel_logo.png"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Body Section End-->
<!--Footer Section Start-->
<footer class="footer text-right">
    <div class="container-fluid">
        <div class="row">
				<span class="copyrights pull-left text-left">Copyright © 2017 appointment.lk<br>All Rights Reserved<br><br>
					<span class="design-credits">Design By: <a href="#" target="_blank">BeetleSoft (Pvt) Ltd</a></span>
				</span>
            <img src="/Appointment/resources/images/mobitel_logo_white.png" class="footer-logo pull-right"/>

            <p class="footer-logo-support pull-right">Powered by</p>
        </div>
    </div>
</footer>

<!--Footer Section End-->

<script src="/Appointment/resources/js/jquery-ui.min.js"></script>
<script src="/Appointment/resources/js/bootstrap.min.js"></script>
<script src="/Appointment/resources/js/ui-js.js"></script>



  </body>
</html>