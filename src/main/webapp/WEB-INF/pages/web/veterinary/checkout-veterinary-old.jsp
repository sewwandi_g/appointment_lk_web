<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Appointment.lk - Veterinary</title>
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-46070462-3"></script>
</head>
<body>
<!--Header Section Start-->
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand sr-only" href="#">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/Appointment/index">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Veterinary</a></li>
                        <li><a href="/Appointment/landPage">Astrology</a></li>
                        <li><a href="doctorIndex">Doctors</a></li>
                        <li><a href="#">Beauty Parlours</a></li>
                    </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="/Appointment/index#contact-us">Contact Us</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!--Header Section End-->
<!--Body Section Start-->
<div class="container-fluid content-container">
    <div class="row">
        <form method="post" action="/Appointment/v1/veterinary/payment-redirect" name="detail_form">
        <div class="col-md-9 col-xs-12">
            <h2 class="form-guide-text text-primary">Fill the Form</h2>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-7 col-xs-12">
                            <div class="panel panel-primary panel-checkout">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Owner Details</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Title</label>
                                                <select class="form-control">
                                                    <option>Mr.</option>
                                                    <option>Mrs.</option>
                                                    <option>Ms.</option>
                                                    <option>Dr.</option>
                                                    <option>Rev.</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label class="required">Name</label>
                                                    <input type="text" class="form-control" name="customerName"
                                                           required="true"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label class="required">Nationality</label>
                                                    <select class="form-control"
                                                            onchange="calculate(${professionalCharge},${centerCharge})"
                                                            id="nationality" name="nationality" required="true">
                                                        <option selected value="L">Sri Lankan</option>
                                                        <option value="F">Other</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label class="required">NIC / Passport</label>
                                                    <input type="text" class="form-control" name="nic" required="true"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label class="required">Phone</label>
                                                <div class="input-group">
                                                    <%--<div class="input-group-addon">+94</div>--%>
                                                        <input type="text" class="form-control" name="telNo"
                                                               required="true"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Email</label>
                                                        <input type="email" class="form-control" name="email"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5 col-xs-12">
                            <div class="panel panel-primary panel-checkout">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Pet Details</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Pet Name</label>
                                                <input class="form-control" type="text"  name="petName"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label class="required">Pet Type</label>
                                                <select class="form-control" name="petType" required="true">
                                                    <option>Dog</option>
                                                    <option>Cat</option>
                                                    <option>Fish</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Reg No.</label>
                                                <input class="form-control" type="text" name="regNo" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <div class="form-group form-group-sm">
                                                <label>Date of Birth</label>
                                                <input class="form-control ui-datepicker-DOB" type="date" name="b_date" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group form-group-sm">
                                <label>Special Notes</label>
                                <textarea rows="2" class="form-control" name="note"></textarea>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <br>
                            <h5 class="text-primary"><strong>TERMS AND CONDITIONS</strong></h5>
                            <div class="terms-container">
                                <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                    unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                <p>Orders accepted by APPOINTMENT.LK can be cancelled only upon written consent of APPOINTMENT.LK and after payment by Buyer of reasonable costs
                                    and expenses for the effort expended thereon.</p>
                                <div class="collapse" id="TermsFull">
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                    <p>Any of the terms and provisions of Buyer's order which are inconsistent with the terms and provisions hereto shall not be binding on APPOINTMENT.LK
                                        unless APPOINTMENT.LK consents in writing and shall not be considered part of the Parties’ Agreement as expressed herein.</p>
                                </div>
                                <div class="text-right">
                                    <a role="button" data-toggle="collapse" href="#TermsFull" aria-expanded="false" aria-controls="TermsFull"><strong>View All</strong></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group form-group-sm">
                                <div class="checkbox">
                                    <label class="required">
                                            <input type="checkbox" value="" id="check" required="true">
                                        <strong>I agree to the terms and conditions</strong>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="reservation-heading">Reservation Request Details</h4>
                    <div class="container-fluid reservation-cart">
                        <div class="row">
                            <div class="serviceProvider-details-wrap">
                                <div class="serviceProvider-details">
                                    <div class="serviceProvider-image-wrap pull-left">
                                        <div class="serviceProvider-image">
                                            <img src="resources/images/userImages/03.jpg"/>
                                        </div>
                                    </div>
                                    <h4 class="panel-title"><%--<span>Mr.</span>--%>${veterinaryName}</h4>
                                    <p>${qualification}</p>
                                    <p><span>${specialNote}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row reservation-details-wrap">
                        <div class="col-xs-12">
                            <p>
                                <span class="sessionTable-NextNo">Your Number <span>${appNo}</span></span>
                                <span class="sessionTable-date">${month} ${day}, <span>${year} <%--(Thursday)--%></span></span>
                                <span class="sessionTable-time">${startTime} - ${finishTime} (${speciality})</span>
                                <span class="sessionTable-location"><strong>${centerName}</strong><span>${address} ${city}</span></span>
                            </p>
                        </div>
                    </div>
                    <%--<div class="row">
                        <div class="col-xs-12">
                            <p class="reservation-cart-item">Handahan<span class="pull-right">1000.00 LKR</span></p>
                        </div>
                    </div>--%>
                    <div class="row reservation-details-wrap">
                        <div class="col-xs-12">
                            <h5 class="reservation-cart-total">Total <span class="pull-right"> <span id="total"> </span>.00 LKR</span>
                            </h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <br>
                            <div class="form-group form-group-sm">
                                <label>Payment Method</label>
                                <div class="form-group form-group-sm">
                                    <select class="form-control" name="paymentMode" required="true">
                                        <option value="HSBC">Visa</option>
                                        <option value="HSBC">Master</option>
                                        <%--<option>Amex</option>
                                        <option>Sampath Vishwa</option>--%>
                                    </select>
                                    <%--<div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="">
                                            Pencil Booking
                                        </label>
                                    </div>--%>
                                    <%--<label>Appointment Type</label>
                                    <div class="form-group form-group-sm">
                                        <label class="custom-control custom-radio">
                                            <input id="visit" type="radio" class="custom-control-input"
                                                   onclick="calculate(${professionalCharge},${centerCharge})"
                                                   value="V" name="appointmentType" checked>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Visit</span>
                                        </label>--%>
                                        <%--<label class="custom-control custom-radio">
                                            <input id="call" name="appointmentType" type="radio"
                                                   onclick="calculate(${professionalCharge},${centerCharge})"
                                                   class="custom-control-input" value="C">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Call</span>
                                        </label>--%>
                                </div>
                            </div>
                        </div>
                        </div>
                        <input type="hidden" name="sessionId" value="${sessionId}">
                        <input type="hidden" name="serviceCode" value="${serviceCode}">
                        <%--<input type="hidden" name="specCode" value="${specCode}">--%>
                        <div class="col-xs-12">
                            <button class="btn btn-sm btn-warning reservation-confirm" id="reload" onclick="return onSubmit();">Confirm Payment <span class="glyphicon glyphicon-copy" aria-hidden="true"></span></button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
<!--Body Section End-->
<!--Footer Section Start-->
<footer class="footer text-right">
    <div class="container-fluid">
        <div class="row">
            <span class="copyrights pull-left text-left">Copyright © 2017 appointment.lk<br>All Rights Reserved<br><br>
					<span class="design-credits">Design By: <a href="#" target="_blank">BeetleSoft (Pvt) Ltd</a></span>
            </span>
            <img src="/Appointment/resources/images/mobitel_logo_white.png" class="footer-logo pull-right" />
            <p class="footer-logo-support pull-right">Powered by</p>
        </div>
    </div>
</footer>
<!--Footer Section End-->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/Appointment/resources/js/jquery-ui.min.js"></script>
<script src="/Appointment/resources/js/bootstrap.min.js"></script>
<script src="/Appointment/resources/js/ui-js.js"></script>
<script>
    $(document).ready(function () {
        calculate(${professionalCharge}, ${centerCharge});
    });

    function calculate(professionalCharge, centerCharge) {
        /*if (($('select[name=nationality]').val() == 'L') && $('#visit').is(':checked')) {
            $('#total').text(Math.round(Number((professionalCharge + centerCharge) * 0.1)));
        } else if (($('select[name=nationality]').val() == 'L') && $('#call').is(':checked')) {
            $('#total').text(Math.round(Number(professionalCharge * 11 * 0.1)));
        } else if (($('select[name=nationality]').val() == 'F') && $('#visit').is(':checked')) {
            $('#total').text(Math.round(Number(professionalCharge + centerCharge) * 2 * 0.1));
        } else if (($('select[name=nationality]').val() == 'F') && $('#call').is(':checked')) {
            $('#total').text(Math.round(Number(professionalCharge * 22 * 0.1)));
        }*/
        $('#total').text(Math.round(Number((professionalCharge + centerCharge)+ 99.0)));

    }

    function onSubmit() {
        /*var x = checkNicNo();*/
        var y = checkContactNo();
        if(y) {
            return true;
        }
        else {
            return false;
        }
    }

    function checkNicNo () {
        var nic = document.forms.detail_form.nic.value;
        var match = /[0-9]{9}[x|X|v|V]$/.test(nic);

        if (match) {
            return true;
        }
        else {
            alert('please, Enter valid nic no.');
            document.forms.detail_form.nic.value = "";
            document.forms.detail_form.nic.focus();
            return false;
        }
        console.log((/[0-9]{9}[x|X|v|V]$/g).test(nic));
        console.log(b_day);
    }

    function checkContactNo(){

        var mobileNo = document.forms.detail_form.telNo.value;
        console.log(mobileNo);
        if(mobileNo.substr(0,1) == "0" && mobileNo.length == 9){
            alert('Please Check Phone No Again');
            document.forms.Add.cell.focus();
            return false;
        }else if(mobileNo.length >= 9){
            if(mobileNo.length == 9){
                mobileNo = '0'+mobileNo;
            }
            if(mobileNo.substr(0,1)=='0'){
                return true;
            }else{
                alert(' Please Enter a Valid Phone No');
                document.forms.detail_form.telNo.value = "";
                document.forms.detail_form.telNo.focus();
                return false;
            }
        } else{
            alert('Please Enter a Valid Phone No');
            document.forms.detail_form.telNo.value = "";
            document.forms.detail_form.telNo.focus();
            return false;
        }
    }
</script>
</html>