<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <fmt:setLocale value = "en_US"/>
    <fmt:setBundle basename="module_en" var="module" />
    <title>Appointment.lk</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/font-awesome.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-46070462-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-46070462-3');
    </script>
    <style>
        .margin {
            margin-top: 25px;
        }

        input[type=number] {
            height: 45px;
            width: 45px;
            font-size: 25px;
            text-align: center;
            border: 1px solid #000000;
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
    <script>
        function getCodeBoxElement(index) {
            return document.getElementById('codeBox' + index);
        }
        function onKeyUpEvent(index, event) {
            const eventCode = event.which || event.keyCode;
            if (getCodeBoxElement(index).value.length === 1) {
                if (index !== 6) {
                    getCodeBoxElement(index + 1).focus();
                } else {
                    getCodeBoxElement(index).blur();
                }
            }
            if (eventCode === 8 && index !== 1) {
                getCodeBoxElement(index - 1).focus();
            }
        }
        function onFocusEvent(index) {
            for (item = 1; item < index; item++) {
                const currentElement = getCodeBoxElement(item);
                if (!currentElement.value) {
                    currentElement.focus();
                    break;
                }
            }
        }
        function reSendOtp(otpId) {
            var url = "otp-resend/" + otpId;
            $("#otp_resend").prop('disabled', true);
            $("#otp_submit").prop('disabled', true);
            $("#wait").show();
            $.ajax({
                type: 'GET',
                url: "otp-resend/" + otpId,
                success: function (data, textStatus, request) {
                    if ("true" == data) {
                        $("#otp_resend_message").html("<div class='alert alert-success'>Resend Success!</div>").fadeOut(10000);
                        $("#otp_resend").prop('disabled', false);
                        $("#otp_submit").prop('disabled', false);
                        $("#wait").fadeOut(6000);
                        resetCountDown();
                    }
                },
                error: function (request, textStatus, errorThrown) {
                    $("#otp_resend_message").html("<div class='alert alert-danger'>Resend Fail!</div>").fadeOut(10000);
                    $("#otp_resend").prop('disabled', false);
                    $("#otp_submit").prop('disabled', false);
                    $("#wait").fadeOut(6000);

                }
            });
        }

        function onLoad() {
            $("#wait").hide();
        }

        $("#codeBox1").validate({
            rules: {
                codeBox1: {required: true, number: true}
            }
        });
        $("#codeBox2").validate({
            rules: {
                codeBox1: {required: true, number: true}
            }
        });
        $("#codeBox3").validate({
            rules: {
                codeBox1: {required: true, number: true}
            }
        });
        $("#codeBox4").validate({
            rules: {
                codeBox1: {required: true, number: true}
            }
        });
        $("#codeBox5").validate({
            rules: {
                codeBox1: {required: true, number: true}
            }
        });
        $("#codeBox6").validate({
            rules: {
                codeBox1: {required: true, number: true}
            }
        });

    </script>
</head>
<body onload="onLoad();">
<!--Header Section Start-->
<nav class="navbar navbar-default header-section">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/<fmt:message bundle="${module}" key="moduleName" />"><img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/logo.png" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<fmt:message bundle="${module}" key="moduleName" />/">Home <span
                        class="sr-only">(current)</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/index">Counseling & Psychology</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/index">Fitness & Wellbeing</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/index">Entertainment</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/index">Professional Services</a></li>
                    </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="<fmt:message bundle="${module}" key="moduleName" />/register" class="btn-register">Register Your Business</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!--Header Section End-->
<%--<t:choose>
    <t:when test="${serviceCode==1}">
        <t:set var="astroStatus" value="" />
        <t:set var="vetStatus" value="hide" />
    </t:when>
    <t:when test="${serviceCode==2}">
        <t:set var="astroStatus" value="hide" />
        <t:set var="vetStatus" value="" />
    </t:when>
    <t:otherwise>
        <t:set var="astroStatus" value="hide" />
        <t:set var="vetStatus" value="hide" />
    </t:otherwise>
</t:choose>--%>
<!--Customer Details Form Start-->
<div class="container-fluid checkout-panel">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <!--Payment Confirmation Section Start-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <t:choose>
                        <t:when test="${success}">
                            <div class="margin">
                                <div class="row">
                                    <div class="col-md-3">
                                        &nbsp;
                                    </div>
                                    <!-- BEGIN Right Column-->
                                    <div class="col-md-6">
                                        <div>Check your mobile for the OTP</div>
                                        <div align="center">
                                            <div class="row">
                                                <div class="col-md-3">&nbsp;</div>
                                                <div class="col-md-6 col-xs-12">
                                                    <div id="wait" class="alert alert-info">Please Wait!!!</div>
                                                </div>
                                                <div class="col-md-3">&nbsp;</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">&nbsp;</div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div id="otp_resend_message"></div>
                                                </div>
                                                <div class="col-md-3">&nbsp;</div>
                                            </div>
                                            <h4>Enter OTP</h4>

                                            <form action="/otp_confirmation" method="post">

                                                <div id="divOuter" class="form-group">
                                                    <div id="divInner">
                                                        <input id="codeBox1" type="number" name="otp1" maxlength="1"
                                                               onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)"/>
                                                        <input id="codeBox2" type="number" name="otp2" maxlength="1"
                                                               onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)"/>
                                                        <input id="codeBox3" type="number" name="otp3" maxlength="1"
                                                               onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)"/>
                                                        <input id="codeBox4" type="number" name="otp4" maxlength="1"
                                                               onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)"/>
                                                        <input id="codeBox5" type="number" name="otp5" maxlength="1"
                                                               onkeyup="onKeyUpEvent(5, event)" onfocus="onFocusEvent(4)"/>
                                                        <input id="codeBox6" type="number" name="otp6" maxlength="1"
                                                               onkeyup="onKeyUpEvent(6, event)" onfocus="onFocusEvent(4)"/>
                                                    </div>
                                                </div>
                                                <br/><br/>
                                                <input type="hidden" value="${referenceNo}" name="refNo">
                                                <input type="hidden" value="${mobile}" name="mobile">
                                                <input type="hidden" value="${astrologerName}" name="astrologerName">
                                                <input type="hidden" value="${centerName}" name="centerName">
                                                <input type="hidden" value="${address}" name="address">
                                                <input type="hidden" value="${AppDate}" name="AppDate">
                                                <input type="hidden" value="${appTime}" name="appTime">
                                                <input type="hidden" value="${appNo}" name="appNo">
                                                <input type="hidden" value="${customerName}" name="customerName">
                                                <input type="hidden" value="${profNo}" name="profNo">
                                                <input type="hidden" value="${email}" name="email">
                                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                                <button type="submit" class="btn btn-primary" id="otp_submit">SUBMIT >></button>
                                            </form>
                                            <button type="button" onclick="reSendOtp(${referenceNo});" class="btn btn-warning"
                                                    id="otp_resend">
                                                Re-Send OTP
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                        </t:when>
                        <t:otherwise>
                            <div class="margin">
                                <div class="row">
                                    <!-- BEGIN Right Column-->
                                    <div class="col-md-8">
                                        <div align="center">
                                            <div class="row">
                                                <div class="col-md-3">&nbsp;</div>
                                                <div class="col-md-6 col-xs-12">
                                                    <div class="alert alert-danger">Your mobile number (${mobile}) temporary
                                                        has been blocked.<br/>
                                                        Please try it again later.<br/>
                                                        <hr/>
                                                        Thank You.
                                                    </div>
                                                </div>
                                                <div class="col-md-3">&nbsp;</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-xs-3">
                                                </div>
                                                <div class="col-md-4 col-xs-6">
                                                    <a href="appointment.lk" class="btn btn-info" role="button">
                                                        GO BACK TO HOME</a>
                                                </div>
                                                <div class="col-md-4 col-xs-3">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- BEGIN Left Column-->
                                        <%--<jsp:include page="../side-bar.jsp"/>--%>
                                    <!-- END Left Column-->
                                </div>
                            </div>
                        </t:otherwise>
                    </t:choose>
                </div>
            </div>
            <!--Payment Confirmation Section End-->
        </div>
       <div class="col-md-3 col-xs-12">
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide1.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Your Emotional Health</h2>
                            <a href="#" class="btn btn-primary">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide6.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Physical Health</h2>
                            <a href="#" class="btn btn-primary">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide7.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Emotional Health</h2>
                            <a href="#" class="btn btn-primary">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Customer Details Form End-->

<!--People Also Searched For Start-->
<div class="container-fluid text-center">
    <div class="row">
        <div class="col-xs-12">
           <%-- <h3 class="suggestion-heading">Related Searches</h3>
            <t:choose>
                <t:when test="${serviceCode==1}">
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=Lux+Senanayake">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/13.jpg"/>
                                </div>
                                <p>Mr. Lux Senanayake</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=Kalum+Gunathilake">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/2.jpg"/>
                                </div>
                                <p>Mr. Kalum Gunathilake</p>
                            </a>
                        </li>
                    </ul>
                </t:when>
                <t:when test="${serviceCode==2}">
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=SUGATH+PEMACHANDRA">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-01.jpg"/>
                                </div>
                                <p>Dr. Sugath Pemachandra</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=Pavithra+Eshwara">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-03.jpg"/>
                                </div>
                                <p>Dr. Pavithra Eshwara</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=Sugandhika+Gothami">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-02.jpg"/>
                                </div>
                                <p>Dr. Sugandhika Gothami</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=PET+CLINIC">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/17.jpg"/>
                                </div>
                                <p>Dr PET CLINIC</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=CIS+CARE+PET+CLINIC">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/18.jpg"/>
                                </div>
                                <p>CIS CARE PET CLINIC</p>
                            </a>
                        </li>
                    </ul>
                </t:when>
                <t:otherwise>
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/b/search?name=Nilmini+De+Pinto">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/22.jpg"/>
                                </div>
                                <p>Ms. Nilmini De Pinto</p>
                            </a>
                        </li>
                    </ul>
                </t:otherwise>
            </t:choose>--%>
        </div>
    </div>
</div>
<!--People Also Searched For End-->

<!--Registering Section Start-->
<div class="container-fluid home-register-section text-center">
    <div class="row">
        <div class="col-xs-12">
            <h2>Connecting People with Professionals</h2>
            <h3>List your business, accept online bookings and get discovered by new customers.<br>
                Get Registered Today. </h3>
            <form class="form-inline">
                <div class="form-group form-group-lg">
                    <input type="email" class="form-control" placeholder="Email Address">
                </div>
                <button type="submit" class="btn btn-lg btn-success">Submit</button>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </div>
    </div>
</div>
<!--Registering Section End-->

<!--Advertisement Slider Section Start-->
<div id="Advertisement-Slider" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-01.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-02.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-03.jpg" alt="...">
            </a>
        </div>
    </div>
</div>
<!--Advertisement Slider Section End-->

<!--FooterLinks Section Start-->
<div class="container-fluid footer-links-section">
    <div class="row">
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Services</h4>
            <ul>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Counseling & Psychology</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Fitness & Wellbeing</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Entertainment</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Professional Services</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Company</h4>
            <ul class="xs-no-bottom-margin">
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Home</a>
                </li>
                <li>
                    <a href="#">Add Your Business</a>
                </li>
                <li>
                    <a href="#">About Us</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4 class="hidden-xs">&nbsp;</h4>
            <ul>
                <li>
                    <a href="#">Terms & Conditions</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Privacy Policy</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Social Media</h4>
            <ul>
                <li>
                    <a href="#">Facebook</a>
                </li>
                <li>
                    <a href="#">Twitter</a>
                </li>
                <li>
                    <a href="#">Google+</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-lg-offset-1 col-xs-12">
            <h4>Appointment.lk</h4>
            <table class="footer-contact-details-table">
                <tbody>
                <tr>
                    <td>Telephone</td>
                    <td>+94 (071)0 225 225</td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>+94 (011)2 370 979</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>No.409,<br>
                        R.A.De.Mel Mawatha,<br>
                        Colombo 3, Sri Lanka</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--FooterLinks Section End-->

<!--Footer Section Start-->
<div class="container-fluid footer-section">
    <div class="row">
        <div class="col-sm-4 col-xs-12 pull-right">
            <p class="power-credits text-center">POWERED BY <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mobitel_logo_white.png" /></p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p class="design-credits text-center">Design By : <a href="#" target="_blank">BeetleSoft (Pvt) Ltd.</a></p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p class="text-center copyrights">Copyright © 2017 appointment.lk<br>
                All Rights Reserved.</p>
        </div>
    </div>
</div>
<script>
    var countDownDate = new Date().getTime() + 100000;

    function resetCountDown() {
        countDownDate = new Date().getTime() + 100000;
    }
    var x = setInterval(function () {

        var now = new Date().getTime();

        var distance = countDownDate - now;
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("timer").innerHTML = "<div style=\"color:red;\">OTP Expire in " + minutes + "m " + seconds + "s </div>";

        if (distance < 0) {
            document.getElementById("timer").innerHTML = "<div style=\"color:red;\">EXPIRED</div>";
            $("#otp_resend").prop('disabled', true);
            $("#otp_submit").prop('disabled', true);
        }
    }, 1000);
</script>
<!--Footer Section End-->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/ui-js.js"></script>
</html>