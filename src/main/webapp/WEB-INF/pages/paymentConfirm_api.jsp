<%--
  Created by IntelliJ IDEA.
  User: se-8
  Date: 7/13/2017
  Time: 12:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>


    <style type="text/css">body { padding-top: 125px; }</style>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <title></title>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <%--<div align="right">
        <form>
            <input type=button name=print value="Save as" onClick="window.print()">
        </form>
    </div>--%>
    <div class="panel panel-primary">
        <div class="panel-heading">APPOINTMENT DONE</div>
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">Appointment Details</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Reference Number
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${refNo}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Appointment Date
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${AppDate}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Appointment Time
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${appTime}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Appointment No.
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${appNo}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Advance Payment
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${advncePay}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Customer Details</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Customer Name
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${customerName}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            NIC
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${nic}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Payment Date
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${transDate}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Astrologer Details</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Astrologer Name
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${astrologerName}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Astrologer Center Details</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Center Name
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${centerName}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Address
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${address}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                        <div class="col-md-4">
                            Telephone no
                        </div>
                        <div class="col-md-1">
                            :
                        </div>
                        <div class="col-md-5">
                            ${mobileNo}
                        </div>
                        <div class="col-md-1">
                            &nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
