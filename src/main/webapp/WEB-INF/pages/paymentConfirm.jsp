<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <fmt:setLocale value = "en_US"/>
    <fmt:setBundle basename="module_en" var="module" />
    <title>Appointment.lk - Checkout</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/font-awesome.min.css" />">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-46070462-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-46070462-3');
    </script>

</head>
<body>
<!--Header Section Start-->
<nav class="navbar navbar-default header-section">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/<fmt:message bundle="${module}" key="moduleName" />"><img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/logo.png" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="<fmt:message bundle="${module}" key="moduleName" />/">Home <span
                        class="sr-only">(current)</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/index">Counseling & Psychology</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/index">Fitness & Wellbeing</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/index">Entertainment</a></li>
                        <li><a href="<fmt:message bundle="${module}" key="moduleName" />/index">Professional Services</a></li>
                    </ul>
                </li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="<fmt:message bundle="${module}" key="moduleName" />/register" class="btn-register">Register Your Business</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!--Header Section End-->
<t:choose>
    <t:when test="${serviceCode==1}">
        <t:set var="astroStatus" value="" />
        <t:set var="vetStatus" value="hide" />
    </t:when>
    <t:when test="${serviceCode==2}">
        <t:set var="astroStatus" value="hide" />
        <t:set var="vetStatus" value="" />
    </t:when>
    <t:otherwise>
        <t:set var="astroStatus" value="hide" />
        <t:set var="vetStatus" value="hide" />
    </t:otherwise>
</t:choose>
<!--Customer Details Form Start-->
<div class="container-fluid checkout-panel">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <!--Payment Confirmation Section Start-->
            <div class="panel panel-default">
                <div class="panel-body">
                    <h1 class="payment-message text-success text-center"><span><%--Payment--%> Successful !</span></h1>
                    <h1 class="payment-message text-danger text-center hide"><span><%--Payment--%> Failed !</span></h1>
                    <a href="appointment.lk"><< Back to Home</a>
                    <h3 class="confirmation-heading">Reservation Details</h3>
                    <table class="confirmation-detail-table">
                        <tbody>
                        <tr>
                            <td>Name</td>
                            <td>${astrologerName}</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>${centerName} - ${address}</td>
                        </tr>
                        <tr>
                            <td>Date</td>
                            <%--<td>Monday, November 13</td>--%>
                            <td>${AppDate}</td>
                        </tr>
                        <tr>
                            <td>Time</td>
                            <td>${appTime}</td>
                        </tr>
                        <tr>
                            <td>Reservation No.</td>
                            <td>${appNo}</td>
                        </tr>
                        </tbody>
                    </table>
                    <h3 class="confirmation-heading">Your Details</h3>
                    <table class="confirmation-detail-table">
                        <tbody>
                        <%--<tr>
                            <td>Title</td>
                            <td>Mr.</td>
                        </tr>--%>
                        <tr>
                            <td>Name</td>
                            <td>${customerName.toUpperCase()}</td>
                        </tr>
                        <tr>
                            <td>Nationality</td>
                            <td>Sri Lankan</td>
                        </tr>
                        <%--<tr>
                            <td>NIC/Passport</td>
                            <td>${nic}</td>
                        </tr>--%>
                        <tr>
                            <td>Phone</td>
                            <td>${mobileNo}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>${email}</td>
                        </tr>
                        </tbody>

                        <%--<tbody class="additional-fields veterinary ${vetStatus}">
                        <tr>
                            <td>Pet Name</td>
                            <td>${petName}</td>
                        </tr>
                        <tr>
                            <td>Pet Type</td>
                            <td>${petType}</td>
                        </tr>
                        <tr>
                            <td>Reg No.</td>
                            <td>${petRegNo}</td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>${petDob}</td>
                        </tr>
                        </tbody>--%>

                        <%--<tbody class="additional-fields astrology ${astroStatus}">
                        <tr>
                            <td>Date of Birth</td>
                            <td>${bDate}</td>
                        </tr>
                        <tr>
                            <td>Birth Time</td>
                            <td>${bTime}</td>
                        </tr>
                        <tr>
                            <td>Birth Country</td>
                            <td>${bCountry}</td>
                        </tr>
                        <tr>
                            <td>Birth Town</td>
                            <td>${bTown}</td>
                        </tr>
                        </tbody>--%>
                    </table>
                </div>
            </div>
            <!--Payment Confirmation Section End-->
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide1.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Your Emotional Health</h2>
                            <a href="#" class="btn btn-primary">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide6.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Your Physical Health</h2>
                            <a href="#" class="btn btn-primary">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-sidebar" style="background-image: url('<fmt:message bundle="${module}" key="moduleName" />/resources/images/mainSlider/slide7.png')">
                <div class="panel-body">
                    <div class="item">
                        <div class="carousel-caption">
                            <h2>Care For Your Emotional Health</h2>
                            <a href="#" class="btn btn-primary">Make an Appointment <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Customer Details Form End-->

<!--People Also Searched For Start-->
<%--<div class="container-fluid text-center">
    <div class="row">
        <div class="col-xs-12">
            <h3 class="suggestion-heading">Related Searches</h3>
            <t:choose>
                <t:when test="${serviceCode==1}">
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=Lux+Senanayake">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/13.jpg"/>
                                </div>
                                <p>Mr. Lux Senanayake</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/search?name=Kalum+Gunathilake">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/2.jpg"/>
                                </div>
                                <p>Mr. Kalum Gunathilake</p>
                            </a>
                        </li>
                    </ul>
                </t:when>
                <t:when test="${serviceCode==2}">
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=SUGATH+PEMACHANDRA">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-01.jpg"/>
                                </div>
                                <p>Dr. Sugath Pemachandra</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=Pavithra+Eshwara">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-03.jpg"/>
                                </div>
                                <p>Dr. Pavithra Eshwara</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=Sugandhika+Gothami">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/U-02.jpg"/>
                                </div>
                                <p>Dr. Sugandhika Gothami</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=PET+CLINIC">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/17.jpg"/>
                                </div>
                                <p>Dr PET CLINIC</p>
                            </a>
                        </li>
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/v1/veterinary/search?name=CIS+CARE+PET+CLINIC">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/18.jpg"/>
                                </div>
                                <p>CIS CARE PET CLINIC</p>
                            </a>
                        </li>
                    </ul>
                </t:when>
                <t:otherwise>
                    <ul class="suggestion-list">
                        <li>
                            <a href="<fmt:message bundle="${module}" key="moduleName" />/b/search?name=Nilmini+De+Pinto">
                                <div class="serviceProvider-Image lg-round">
                                    <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/serviceProvider/22.jpg"/>
                                </div>
                                <p>Ms. Nilmini De Pinto</p>
                            </a>
                        </li>
                    </ul>
                </t:otherwise>
            </t:choose>
        </div>
    </div>
</div>--%>
<!--People Also Searched For End-->

<!--Registering Section Start-->
<div class="container-fluid home-register-section text-center">
    <div class="row">
        <div class="col-xs-12">
            <h2>Connecting People with Professionals</h2>
            <h3>List your business, accept online bookings and get discovered by new customers.<br>
                Get Registered Today. </h3>
            <form class="form-inline">
                <div class="form-group form-group-lg">
                    <input type="email" class="form-control" placeholder="Email Address">
                </div>
                <button type="submit" class="btn btn-lg btn-success">Submit</button>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </div>
    </div>
</div>
<!--Registering Section End-->

<!--Advertisement Slider Section Start-->
<div id="Advertisement-Slider" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-01.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-02.jpg" alt="...">
            </a>
        </div>
        <div class="item">
            <a href="#" target="_blank">
                <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/advertisements/AdSlide-03.jpg" alt="...">
            </a>
        </div>
    </div>
</div>
<!--Advertisement Slider Section End-->

<!--FooterLinks Section Start-->
<div class="container-fluid footer-links-section">
    <div class="row">
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Services</h4>
            <ul>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Veterinarian</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Astrologers</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Entertainment</a>
                </li>
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Professional Services</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Company</h4>
            <ul class="xs-no-bottom-margin">
                <li>
                    <a href="<fmt:message bundle="${module}" key="moduleName" />/index">Home</a>
                </li>
                <li>
                    <a href="#">Add Your Business</a>
                </li>
                <li>
                    <a href="#">About Us</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4 class="hidden-xs">&nbsp;</h4>
            <ul>
                <li>
                    <a href="#">Terms & Conditions</a>
                </li>
                <li>
                    <a href="#">FAQ</a>
                </li>
                <li>
                    <a href="#">Privacy Policy</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-2 col-sm-3 col-xs-12">
            <h4>Social Media</h4>
            <ul>
                <li>
                    <a href="#">Facebook</a>
                </li>
                <li>
                    <a href="#">Twitter</a>
                </li>
                <li>
                    <a href="#">Google+</a>
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-lg-offset-1 col-xs-12">
            <h4>Appointment.lk</h4>
            <table class="footer-contact-details-table">
                <tbody>
                <tr>
                    <td>Telephone</td>
                    <td>+94 (071)0 225 225</td>
                </tr>
                <tr>
                    <td>Fax</td>
                    <td>+94 (011)2 370 979</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>No.409,<br>
                        R.A.De.Mel Mawatha,<br>
                        Colombo 3, Sri Lanka</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--FooterLinks Section End-->

<!--Footer Section Start-->
<div class="container-fluid footer-section">
    <div class="row">
        <div class="col-sm-4 col-xs-12 pull-right">
            <p class="power-credits text-center">POWERED BY <img src="<fmt:message bundle="${module}" key="moduleName" />/resources/images/mobitel_logo_white.png" /></p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p class="design-credits text-center">Design By : <a href="#" target="_blank">BeetleSoft (Pvt) Ltd.</a></p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <p class="text-center copyrights">Copyright © 2017 appointment.lk<br>
                All Rights Reserved.</p>
        </div>
    </div>
</div>
<!--Footer Section End-->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/ui-js.js"></script>
</html>