<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
      <%@ taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core" %>
    <title>Appointment.lk - Home</title>
      <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/jquery-ui.min.css" />">
      <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/bootstrap.min.css" />">
      <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/styles.css" />">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
  </head>
  <body>
	<!--Header Section Start-->
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand sr-only" href="#">Brand</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
			<li class="active"><a href="index">Home</a></li>
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
			  <ul class="dropdown-menu">
                  <li><a href="v1/veterinary/landPage">Veterinary</a></li>
				<li><a href="landPage">Astrology</a></li>
                  <li><a href="https://www.echannelling.com/Echannelling/index">Doctors</a></li>
				<li><a href="#">Beauty Parlours</a></li>
			  </ul>
			</li>
			<li><a href="#">About Us</a></li>
			<li><a href="#">Contact Us</a></li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<!--Header Section End-->
	<!--Body Section Start-->
	<div>
		<div class="container content-container">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-primary panel-confirmation">
						<div class="panel-heading"><strong>Appointment Done</strong></div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6 col-xs-12">
									<div class="panel panel-info">
										<div class="panel-heading">Appointment Details</div>
										<div class="panel-body">
											<table class="payment-details-table">
												<tbody>
												<tr>
													<td><strong>Reference Number</strong></td>
													<td>${refNo}</td>
												</tr>
												<tr>
													<td><strong>Appointment Date</strong></td>
													<td>${AppDate}</td>
												</tr>
												<tr>
													<td><strong>Appointment Time</strong></td>
													<td>${appTime}</td>
												</tr>
												<tr>
													<td><strong>Appointment Number</strong></td>
													<td>${appNo}</td>
												</tr>
												<tr>
													<td><strong>Advance Payment</strong></td>
													<td> ${advncePay}</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xs-12">
									<div class="panel panel-info">
										<div class="panel-heading">Customer Details</div>
										<div class="panel-body">
											<table class="payment-details-table">
												<tbody>
												<tr>
													<td><strong>Customer Name</strong></td>
													<td>${customerName}</td>
												</tr>
												<tr>
													<td><strong>NIC</strong></td>
													<td>${nic}</td>
												</tr>
												<tr>
													<td><strong>Payment Date</strong></td>
													<td>${transDate}</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-xs-12">
									<div class="panel panel-info">
										<div class="panel-heading">Astrologer Details</div>
										<div class="panel-body">
											<table class="payment-details-table">
												<tbody>
												<tr>
													<td><strong>Astrologer Name</strong></td>
													<td>${astrologerName}</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xs-12">
									<div class="panel panel-info">
										<div class="panel-heading">Astrologer Centre Details</div>
										<div class="panel-body">
											<table class="payment-details-table">
												<tbody>
												<tr>
													<td><strong>Centre Name</strong></td>
													<td>${centerName}</td>
												</tr>
                                                <tr>
													<td><strong>Address</strong></td>
													<td>${address}</td>
												</tr>
                                                <tr>
													<td><strong>Telephone no</strong></td>
													<td>${mobileNo}</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="panel-group text-right">
						<%--<button class="btn btn-sm btn-danger">Cancel</button>--%>
                       <%-- <form>
                            <input type=button name=print value="Print" onClick="window.print()">
                        </form>--%>

                        <button class="btn btn-sm btn-success" onClick="window.print()" name=print>Print</button>
		</div>
	</div>
			</div>
		</div>
	</div>
	<!--Body Section End-->
	<!--Footer Section Start-->
	<footer class="footer text-right">
		<div class="container-fluid">
			<div class="row">
				<span class="copyrights pull-left text-left">Copyright © 2015 appointment.lk<br>All Rights Reserved<br><br>
					<span class="design-credits">Design By: <a href="#" target="_blank">BeetleSoft (Pvt) Ltd</a></span>
				</span>
				<img src="/Appointment/resources/images/mobitel_logo_white.png" class="footer-logo pull-right" />
				<p class="footer-logo-support pull-right">Powered by</p>
			</div>
		</div>
	</footer>
	<!--Footer Section End-->
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="resources/js/jquery-ui.min.js"></script>
  <script src="resources/js/bootstrap.min.js"></script>
  <script src="resources/js/ui-js.js"></script>
</html>