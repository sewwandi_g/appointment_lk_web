var NameSuggestions = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
];


var LocationSuggestions = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
];

$('.btn-close').on({
   click: function () {
       $('.search-result-item .collapse').collapse('hide');
       $('.tab-pane').removeClass('menu-is-open');
   } 
});

$('.btn-availability').on({
    click:function () {
        if($('.tab-pane').hasClass( "menu-is-open" )){
            $('.search-result-item .collapse').collapse('hide');
        }else{
            $('.tab-pane').addClass('menu-is-open');
        }
    }
});

$(document).on({
    click:function (event) {
        if($('.tab-pane').hasClass( "menu-is-open" ) && ($(event.target).hasClass( "availability-details-wrap" ))){
            $('.search-result-item .collapse').collapse('hide');
            $('.tab-pane').removeClass('menu-is-open');
        }
    }
});

$( function() {
    $( ".ui-datepicker-search" ).datepicker({
        dateFormat: "yy-mm-dd",
        minDate : 0
        //minDate: new Date(2020, 10 -1, 20)
    });

    $( ".ui-datepicker-DOB" ).datepicker({
        dateFormat: "yy-mm-dd",
        maxDate: "+1m +1w"
    });

    $( ".ui-autocomplete-names" ).autocomplete({
        source: NameSuggestions
    });

    $( ".ui-autocomplete-locations" ).autocomplete({
        source: LocationSuggestions
    });
} );

