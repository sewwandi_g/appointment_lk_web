package lk.appointment.professional.service;



/**
 * Created by Sewwandi on 10/20/2018.
 */
public interface AgentService {
    public boolean addAgentDetails(lk.appointment.professional.domain.Agent agent);
    public lk.appointment.professional.dto.Agent getAgentNameWithFee(String agentCode);
    public String getTelephone(String refNo);
}
