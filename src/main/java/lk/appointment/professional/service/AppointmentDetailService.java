package lk.appointment.professional.service;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.dto.AppointmentDTO1;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by se-8 on 1/5/2018.
 */
public interface AppointmentDetailService {
    public AppointmentDTO1 getAppointmentDetails(String refNo) throws SQLException;
    public List<Appointment> getSearchAppointment(String refNo,String name,String phoneNo);
    public boolean makeActiveAppointmentCancel(String refNo);
    public List<Appointment> getAllAppointment(String frmDate,String todate);
}
