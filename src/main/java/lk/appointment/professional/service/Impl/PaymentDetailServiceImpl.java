package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.Impl.PaymentChannelDaoImpl;
import lk.appointment.professional.dao.PaymentChannelDao;
import lk.appointment.professional.domain.PaymentGateway;
import lk.appointment.professional.service.PaymentDetailService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by se-7 on 5/16/2017.
 */
@Service
public class PaymentDetailServiceImpl implements PaymentDetailService {
    PaymentChannelDao paymentChannelDao = new PaymentChannelDaoImpl();
    @Override
    public List<PaymentGateway> getPaymentChannel(PaymentGateway paymentGateway) {
        return paymentChannelDao.getPaymentChannel(paymentGateway);
    }
}
