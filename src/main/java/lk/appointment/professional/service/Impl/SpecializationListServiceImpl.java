package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.AstroSpecializationDao;
import lk.appointment.professional.dao.Impl.AstroSpecializationDaoImpl;
import lk.appointment.professional.dao.ResponseServiceDao;
import lk.appointment.professional.domain.Professional;
import lk.appointment.professional.domain.ProfessionalSpecialization;
import lk.appointment.professional.dto.ProfessionalSpecializationList;
import lk.appointment.professional.dto.Specialization;
import lk.appointment.professional.service.SpecializationListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 * Created by se-7 on 6/21/2017.
 */
@Service
public class SpecializationListServiceImpl implements SpecializationListService {


    @Autowired
    private ResponseServiceDao responseServiceDao;

    @Override
    public List<ProfessionalSpecialization> getSpecList(int serviceCode) {
        AstroSpecializationDao astroSpecializationDao = new AstroSpecializationDaoImpl();

         return astroSpecializationDao.getSpecList(serviceCode);
    }

    public List<ProfessionalSpecializationList> getSpecializationAccordingToProfessionals(){
        AstroSpecializationDao astroSpecializationDao = new AstroSpecializationDaoImpl();
        String professionalCode = null;
        List<Specialization> specializations=null;
        Specialization specialization = null;

        List<ProfessionalSpecializationList> ProfessionalList = new ArrayList<ProfessionalSpecializationList>();

        ProfessionalSpecializationList professionalSpecializationList1= null;


        List<ProfessionalSpecializationList> professionalSpecializationLists = astroSpecializationDao.getSpecializationAccordingToProfessionals();


        int length,i = 0;
        length = professionalSpecializationLists.size();


        for (ProfessionalSpecializationList professionalSpecializationList2:professionalSpecializationLists) {
            if (!professionalSpecializationList2.getProfessionalId().equals(professionalCode)){

                if (professionalCode!=null){
                    professionalSpecializationList1.setSpecializationList(specializations);
                    ProfessionalList.add(professionalSpecializationList1);
                }
                professionalSpecializationList1 = new ProfessionalSpecializationList();
                professionalSpecializationList1.setProfessionalId(professionalSpecializationList2.getProfessionalId());


                specializations =  new ArrayList<Specialization>();
            }
            specialization = new Specialization();
            specialization.setSpecializationId(professionalSpecializationList2.getSpecializationId());
            specializations.add(specialization);
            
            professionalCode = professionalSpecializationList2.getProfessionalId();

            i++;
            if (i==length){
                professionalSpecializationList1.setSpecializationList(specializations);
                ProfessionalList.add(professionalSpecializationList1);
            }

        }
        return ProfessionalList;
    }
    @Override
    public Map<Integer, String> getSpecializationOfProfessionals(int serviceCode, int professionalCode) {
        return responseServiceDao.getSpecailitiesOfProfessionals(serviceCode,professionalCode);
    }
}
