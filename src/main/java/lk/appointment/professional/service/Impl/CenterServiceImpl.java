package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.CenterListDao;
import lk.appointment.professional.dao.Impl.CenterListDaoImpl;
import lk.appointment.professional.domain.Center;
import lk.appointment.professional.dto.CenterDTO;
import lk.appointment.professional.service.CenterService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by se-7 on 6/22/2017.
 */
@Service
public class CenterServiceImpl implements CenterService {
    CenterListDao centerListDao = new CenterListDaoImpl();
    @Override
    public List<Center> getCenterList(int serviceCode) {
        return centerListDao.getCenterList(serviceCode);
    }

    @Override
    public CenterDTO getCenterName(String centerCode) {

        return centerListDao.getCenterName(centerCode);
    }
    @Override
    public double getEchFee(String centerCode) {
        return centerListDao.getEchFee(centerCode);
    }
}
