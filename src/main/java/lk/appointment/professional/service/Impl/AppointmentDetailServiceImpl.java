package lk.appointment.professional.service.Impl;
import lk.appointment.professional.dao.AppointmentDetailsDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.dto.AppointmentDTO1;
import lk.appointment.professional.service.AppointmentDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by se-8 on 1/5/2018.
 */
@Service
public class AppointmentDetailServiceImpl implements AppointmentDetailService {

    @Autowired
    private AppointmentDetailsDao appointmentDetailsDao;

    @Autowired
    public AppointmentDetailServiceImpl(AppointmentDetailsDao appointmentDetailsDao) {
        this.appointmentDetailsDao = appointmentDetailsDao;
    }



    public AppointmentDTO1 getAppointmentDetails(String refNo) throws SQLException{
        return appointmentDetailsDao.getAppointmentDetail(refNo);
    }
    public List<Appointment> getSearchAppointment(String refNo,String name,String phoneNo){
        return appointmentDetailsDao.searchAppointment(refNo,name,phoneNo);
    }

    @Override
    public boolean makeActiveAppointmentCancel(String refNo) {
        return appointmentDetailsDao.makeCancelAppointment(refNo);
    }

    @Override
    public List<Appointment> getAllAppointment(String frmDate, String todate) {
        return appointmentDetailsDao.getAllAppointment(frmDate,todate);
    }
}
