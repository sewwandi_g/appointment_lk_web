package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.AgentDetailsDao;
import lk.appointment.professional.dao.BookingServiceDao;
import lk.appointment.professional.dao.Impl.BookingServiceDaoImpl;
import lk.appointment.professional.domain.Agent;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.PaymentRequest;
import lk.appointment.professional.domain.ServiceResponse;
import lk.appointment.professional.enumbers.PaymentType;
import lk.appointment.professional.service.BookingService;
import lk.appointment.professional.service.PaymentService;
import lk.appointment.professional.utilities.Utility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static lk.appointment.professional.utilities.Utility.convertDateFormat;
/**
 * Created by se-7 on 5/16/2017.
 */
@Service
public class BookingServiceImpl implements BookingService {

    private final static Logger LOGGER = Logger.getLogger(BookingServiceImpl.class);

    @Autowired
    private AgentDetailsDao agentDetailsDao;

    private BookingServiceDao bookingServiceDao = new BookingServiceDaoImpl();



    public BookingServiceImpl() {
    }

    @Override
    public Appointment validateTemporyBooking(String key,String PaymentChannel) {
        String paymentCh = PaymentChannel;
        if(!PaymentType.HSBC.toString().equals(PaymentChannel)) {
            paymentCh = agentDetailsDao.getAgentName(PaymentChannel).getAgentName();
        }
        return bookingServiceDao.getTemporyPaymentDetails(key,paymentCh);
    }

    @Override
    public PaymentRequest generatePaymentGatewayRequest(Appointment appointment) {
        PaymentService paymentService = new PaymentServiceImpl();
        PaymentRequest paymentRequest = paymentService.getPaymentRequest(appointment);

        return paymentRequest;
    }

    @Override
    public ServiceResponse confirmTemporaryBooking(Appointment appointment, HttpServletRequest request) {
        ServiceResponse serviceResponse = new ServiceResponse();
        PaymentService paymentService = new PaymentServiceImpl();

        serviceResponse = paymentService.confirmPaidPayment(appointment, request);

        if (serviceResponse.isStatus()) {
            appointment = bookingServiceDao.getTemporyAppointmentDetails((String) serviceResponse.getDataList().iterator().next());

        } else {
            return serviceResponse;
        }

        return serviceResponse;
    }

    @Override
    public Appointment getPaymentDetails(String refNo) {
        BookingServiceDao bookingServiceDao1 = new BookingServiceDaoImpl();
        Appointment appointment= bookingServiceDao1.getPaymentDetails(refNo);
        try {
            appointment.setAppDate(convertDateFormat(appointment.getAppDate()));
        } catch (ParseException e) {
            LOGGER.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<BOOKINGSERVICEIMPL>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        return appointment;
    }

    @Override
    public String getShowFrontEndBookingFee(String total) {
        String bookingFee=Double.toString((Double.parseDouble(total)*0.1));
        return bookingFee.substring(0,bookingFee.indexOf("."));
    }
    @Override
    public List<String> getTimeSlots(String sessionId,String specId) {
        ResourceBundle time_divide = ResourceBundle.getBundle("time_divide");
        String timeDivider = time_divide.getString("time_divider");

        List<String> timeSlots;
        timeSlots =Utility.splitTimePeriod(sessionId);
        List<String> appointTimeSlots = Utility.getAppointmentTimeSlots(sessionId);
        for (String list: appointTimeSlots) {
            String[] parts=list.split("-", 2);
            String time=parts[0];
            String timenew = "";
            if (time.length()==3) {
                timenew = time.substring(0,1)+":"+time.substring(1,3);
            }
            else {
                timenew = time.substring(0,2)+":"+time.substring(2,4);
            }
            String duration = parts[1];
            int noOfSlots= Integer.parseInt(duration)/Integer.parseInt(timeDivider);
            for (int i=0;i<noOfSlots;i++) {
                SimpleDateFormat df = new SimpleDateFormat("H:mm");
                Date d = null;
                try {
                    d = df.parse(timenew);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                cal.add(Calendar.MINUTE, Integer.parseInt(timeDivider)*i);
                String newTime = df.format(cal.getTime());
                if (timeSlots.contains(newTime)) {
                    timeSlots.remove(newTime);
                }
            }
        }
        /*List<String> activeGivenTime = Utility.getActiveAppointmentTime(sessionId);
        String duration=Utility.getSesionSpecDuration(sessionId,specId);
        if (activeGivenTime.size()>0) {
            int noOfTimeSlots =
        }*/
        return timeSlots;
    }

    public Appointment getTempAppointmentDetails(String refNo){
        return bookingServiceDao.getTemporyAppointmentDetails(refNo);
    }

    @Override
    public boolean doCompleteAppointment(Agent agent) {
        boolean success=false;
        agent.setAgentFee(agentDetailsDao.getAgentName(agent.getAgent_code()).getFee());
        boolean updateStatus = bookingServiceDao.doConfirmPayment(agent.getReferance_no());
        boolean updateverification = bookingServiceDao.doConfirmPaymentVerification(agent.getReferance_no());
        if(updateStatus && updateverification){
            success=agentDetailsDao.insertAgentDetails(agent);
        }
        return success;
    }

    @Override
    public boolean confirmOTP(String refNo){
        return bookingServiceDao.ConfirmOTPBooking(refNo);
    }
    @Override
    public Appointment resendOtp(int refNo){
        return bookingServiceDao.resendOTP(refNo);
    }
}
