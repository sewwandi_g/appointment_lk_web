package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.SessionDetailsDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.SessionDetails;
import lk.appointment.professional.service.SessionDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Sewwandi on 7/5/2018.
 */
@Service
public class SessionDetailsServiceImpl implements SessionDetailsService {

    @Autowired
    private SessionDetailsDao sessionDetailsDao;

    @Override
    public SessionDetails getSessionDetails(Appointment appointment) {
        return sessionDetailsDao.getSessionDetails(appointment);
    }
}
