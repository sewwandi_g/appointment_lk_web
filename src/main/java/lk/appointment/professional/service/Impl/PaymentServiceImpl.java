package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.AgentDetailsDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.PaymentRequest;
import lk.appointment.professional.domain.ServiceResponse;
import lk.appointment.professional.dto.PaymentDetail;
import lk.appointment.professional.service.PaymentService;
import lk.appointment.professional.service.client.PaymentClient;
import lk.appointment.professional.service.client.PaymentFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by se-7 on 5/16/2017.
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private AgentDetailsDao agentDetailsDao;

    public PaymentRequest getPaymentRequest(Appointment appointment) {

        PaymentClient paymentClient = PaymentFactory.getInstance(appointment.getPaymentType());
        return paymentClient.getPaymentRequest(appointment);
    }

    @Override
    public ServiceResponse confirmPaidPayment(Appointment paidAppointment, HttpServletRequest request) {
        PaymentClient paymentClient = PaymentFactory.getInstance(paidAppointment.getPaymentType());
        return paymentClient.confirmPaidPayment(request, paidAppointment);
    }

    public PaymentDetail checkBalance(String phoneNumber,double fullAmt,String agentCode){
        String agentName = agentDetailsDao.getAgentName(agentCode).getAgentName();
        PaymentClient paymentClient = PaymentFactory.getInstance(agentName);
        return paymentClient.checkBalance(phoneNumber,fullAmt);
    }
}
