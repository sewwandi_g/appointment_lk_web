package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.AgentDetailsDao;
import lk.appointment.professional.dao.CenterListDao;
import lk.appointment.professional.service.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Sewwandi on 10/20/2018.
 */
@Service
public class AgentServiceImpl implements AgentService {

    private AgentDetailsDao agentDetailsDao;

    @Autowired
    private CenterListDao centerListDao;

    @Autowired
    public AgentServiceImpl(AgentDetailsDao agentDetailsDao) {
        this.agentDetailsDao = agentDetailsDao;
    }


    @Override
    public boolean addAgentDetails(lk.appointment.professional.domain.Agent agent) {
        return agentDetailsDao.insertAgentDetails(agent);
    }

    @Override
    public lk.appointment.professional.dto.Agent getAgentNameWithFee(String agentCode) {
        return agentDetailsDao.getAgentName(agentCode);
    }

    @Override
    public String getTelephone(String refNo){
        return agentDetailsDao.getTele(refNo);
    }
}
