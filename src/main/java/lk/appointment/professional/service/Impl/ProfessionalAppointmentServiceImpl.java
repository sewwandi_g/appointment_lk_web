package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.AgentDetailsDao;
import lk.appointment.professional.dao.MakeAppointTempDao;
import lk.appointment.professional.dao.SessionDetailsDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.SessionDetails;
import lk.appointment.professional.dto.AppointmentDTO;
import lk.appointment.professional.enumbers.PaymentType;
import lk.appointment.professional.service.BookingService;
import lk.appointment.professional.service.ProfessionalAppointmentService;
import lk.appointment.professional.utilities.ChargeUtility;
import lk.appointment.professional.utilities.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by se-7 on 5/15/2017.
 */
@Service
public class ProfessionalAppointmentServiceImpl implements ProfessionalAppointmentService {
    private MakeAppointTempDao makeAppointTempDao;
    private BookingService bookingService;

    @Autowired
    private SessionDetailsDao sessionDetailsDao=null;

    @Autowired
    private AgentDetailsDao agentDetailsDao=null;

    @Autowired
    public ProfessionalAppointmentServiceImpl(MakeAppointTempDao makeAppointTempDao,BookingService bookingService) {
        this.makeAppointTempDao = makeAppointTempDao;
        this.bookingService = bookingService;
    }
    public ProfessionalAppointmentServiceImpl() {
    }

    private static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(ProfessionalAppointmentServiceImpl.class);

    @Override
    public AppointmentDTO doTempAppointment(Appointment appointment) {
        AppointmentDTO appointmentDTO = null;
        SessionDetails sessionDetails =null;

        sessionDetails = sessionDetailsDao.getSessionDetails(appointment);
        if (sessionDetails.getProfessionalCode() != null && ((Integer.parseInt(sessionDetails.getNoOfClient())> Utility.getMaxAppointNo(appointment.getSessionId()) && (Integer.parseInt(sessionDetails.getWebClient()) > Utility.getCurrentWebClientNo(appointment.getSessionId()))))){
            appointment.setCenterFee(sessionDetails.getCenterCharge());
            appointment.setAppDate(sessionDetails.getAppDate().substring(0,10));
            appointment.setProfessionalName(sessionDetails.getAstrologerName());
            appointment.setEchFee((ChargeUtility.calculateEchFee(sessionDetails.getAstroCharge(), sessionDetails.getCenterCharge().substring(0, sessionDetails.getCenterCharge().length() - 3), appointment.getAppointmentType(), appointment.getNationality(),appointment.getServiceCode(),sessionDetails.getCenterCode())));
            LOG.info("IIIIIIIIIIINNNNNNNNNNNFFFFFFFFFFFFFOOOOOOOOOOOOO2222222222222222222222222222222");
            if ("".equals(appointment.getB_date())) {
                appointment.setB_date("0000-00-00");
            }

            if ("".equals(appointment.getB_time())) {
                appointment.setB_time("00:00");
            }
            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<beforeinsert>>>>>>>>>>>>>>>>>>>");

            if(PaymentType.M.toString().equals(appointment.getChannelFrom())){
                double agentFee = agentDetailsDao.getAgentName(appointment.getPaymentType()).getFee();
                if (agentFee > 0) {
                    appointment.setAgentFee(String.valueOf(agentFee));
                }
            }
            Appointment appointment1 = makeAppointTempDao.insertTempAppointment(appointment, sessionDetails);
            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<beforeinsert>>>>>>>>>>>>>>>>>>>");
            appointmentDTO = new AppointmentDTO();
            appointmentDTO.setSecurityKey(appointment1.getSecurityKey());
            appointmentDTO.setPaymentChannel(appointment1.getPaymentMode());
            appointmentDTO.setRefNo(appointment1.getRefNo());
            appointmentDTO.setAppNo(appointment1.getAppNo());
            appointmentDTO.setAppTime(appointment1.getGivenTime());
            appointmentDTO.setAmt(appointment1.getAmount());
            appointmentDTO.setPayDate( DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()));
            return appointmentDTO;

        }
        else {
            appointmentDTO = new AppointmentDTO();
            appointmentDTO.setStatus("Transaction Fail");
            if (sessionDetails.getSessionId() != null) {
                if (Integer.parseInt(sessionDetails.getNoOfClient()) > Utility.getMaxAppointNo(appointment.getSessionId())) {
                    appointmentDTO.setStatusDescription("No of appointments are already filled.");
                } else if (Integer.parseInt(sessionDetails.getWebClient()) > Utility.getCurrentWebClientNo(appointment.getSessionId())) {
                    appointmentDTO.setStatusDescription("No of web appointments are already filled.");
                } else {
                    appointmentDTO.setStatusDescription("Invalid Parameter(s).");
                }
            }
            else {
                appointmentDTO.setStatusDescription("Invalid Parameter(s).");
            }
            return appointmentDTO;
        }
    }

    @Override
    public void confirmAppointment(String securityKey, String paymentChannel) {
        Appointment appointment=bookingService.validateTemporyBooking(securityKey, paymentChannel);


    }
}
