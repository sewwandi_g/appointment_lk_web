package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.RegisterDao;
import lk.appointment.professional.dto.SelfRegister;
import lk.appointment.professional.service.RegisterService;
import lk.appointment.professional.utilities.SendEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by udara on 5/14/2020.
 */
@Service
public class RegisterServiceImpl  implements RegisterService {

    @Autowired
    private RegisterDao registerDao;


    @Override
    public void selfRegister(SelfRegister reg) {


        registerDao.registerDb(reg);
        SendEmail sendEmail = new SendEmail();
        sendEmail.sendEMail("info@echannelling.com", "gayani@echannelling.com", "", "", "SelfRegister", reg.getName()+" "+reg.getNic()+" has successfully registered with us");

    }

    @Override
    public boolean checkNicEmail(SelfRegister reg) {
        return registerDao.alreadyExistNicEmail(reg);
    }
}
