package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.Impl.ResponseServiceDaoImpl;
import lk.appointment.professional.dao.Impl.ValidateReqServiceDaoImpl;
import lk.appointment.professional.dao.ResponseServiceDao;
import lk.appointment.professional.dao.ValidateReqServiceDao;
import lk.appointment.professional.domain.Success;
import lk.appointment.professional.domain.SuccessResponse;
import lk.appointment.professional.service.ValidateReqService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by se-7 on 5/16/2017.
 */
@Service
public class ValidateReqServiceImpl implements ValidateReqService {
    private ValidateReqServiceDao validateReqServiceDao = new ValidateReqServiceDaoImpl();
    @Override
    public boolean isRequestValidate(String key) {
        if (validateReqServiceDao.getKey(key)) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public List<SuccessResponse> checkPaymentSuccess(Success success) {
        ResponseServiceDao responseServiceDao = new ResponseServiceDaoImpl();
        return responseServiceDao.getPaymentData(success);
    }
    @Override
    public boolean ConfirmOTPComparison(String refno,String otp){
        return validateReqServiceDao.getOTP(refno, otp);
    }
}
