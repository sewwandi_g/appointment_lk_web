package lk.appointment.professional.service.Impl;

import lk.appointment.professional.dao.AstroSpecializationDao;
import lk.appointment.professional.dao.Impl.SearchAstrologerDaoImpl;
import lk.appointment.professional.dao.ResponseServiceDao;
import lk.appointment.professional.dao.SearchAstrologerDao;
import lk.appointment.professional.domain.*;
import lk.appointment.professional.dto.*;
import lk.appointment.professional.dto.Professional;
import lk.appointment.professional.service.ProfessionalDetailService;
import lk.appointment.professional.utilities.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static lk.appointment.professional.utilities.Utility.getSortedSessionList;

/**
 * Created by se-7 on 5/15/2017.
 */
@Service
public class ProfessionalDetailServiceImpl implements ProfessionalDetailService {

    @Autowired
    private ResponseServiceDao responseServiceDao;

    @Autowired
    private AstroSpecializationDao astroSpecializationDao;

    @Autowired
    private SearchAstrologerDao searchAstrologerDao1;




    public List<lk.appointment.professional.domain.Professional> getAstrologerData(lk.appointment.professional.domain.Professional professional) {
        SearchAstrologerDao searchAstrologerDao = new SearchAstrologerDaoImpl();
        List<lk.appointment.professional.domain.Professional> professionals = null;
        if (professional.checkSearchValues()) {
            Utility.setAstrologerName(professional);
            professionals = searchAstrologerDao.searchSession(professional);
            /*for (Astrologer astro:astrologers) {
                astrologer.setDay(Utility.getmonth(astro.getAppDate()));
            }*/
            lk.appointment.professional.domain.Professional astro;
            return professionals;
        }
        else {
            return null;
        }
    }

    @Override
    public List<lk.appointment.professional.domain.Professional> getAstrologerList(int serviceCode) {
        SearchAstrologerDao searchAstrologerDao1=new SearchAstrologerDaoImpl();
        return searchAstrologerDao1.getAllAstrologers(serviceCode);
    }

    @Override
    public Professional getAstrologer(int id) {
        SearchAstrologerDao searchAstrologerDao2=new SearchAstrologerDaoImpl();
        return searchAstrologerDao2.getAstrologer(id);
    }

    @Override
    public SpecialNote getAstroSpecNote(int astroId, int centerId) {
        SearchAstrologerDao searchAstrologerDao=new SearchAstrologerDaoImpl();
        return searchAstrologerDao.getAstroSpecNote(astroId, centerId);
    }

    @Override
    public AllocateAstrologer isCheckAstrologerCenter(int astroId, int centerId) {
        SearchAstrologerDao searchAstrologerDao=new SearchAstrologerDaoImpl();
        return searchAstrologerDao.isCheckAstrologerCenter(astroId,centerId);
    }

    @Override
    public List<ProfessionalSpecialization> getSpeciality(int professionalCode) {
        SearchAstrologerDao searchAstrologerDao=new SearchAstrologerDaoImpl();
        return searchAstrologerDao.getSpeciality(professionalCode);
    }

    @Override
    public List<ServiceType> getTermAndConditions(int serviceCode) {
        return responseServiceDao.gettermsAndConditions(serviceCode);
    }

    @Override
    public List<String> getFillName(int serviceCode,String nameLetter) {
        String firstName = "";
        String lastName="";
        List<String> nameList = new ArrayList<String>();
        if (nameLetter.contains("_")) {
            firstName = nameLetter.substring(0,nameLetter.lastIndexOf("_"));
            lastName = nameLetter.substring(nameLetter.lastIndexOf("_")+1);
        }
        else {
            firstName = nameLetter;
            lastName = nameLetter;
        }
        List<Professional> professionalList=responseServiceDao.getFillName(serviceCode, firstName, lastName);
        for (Professional professional:professionalList) {
            nameList.add(professional.getFirstName());
        }
        return nameList;
    }


    @Override
    public List<String> getCenterName(int serviceCode, String nameLetters) {
        List<String> centerList = new ArrayList<String>();
        List<ProfessionalCenter> professionalCenterList = responseServiceDao.getFillCenter(serviceCode, nameLetters);
        for (ProfessionalCenter professionalCenter:professionalCenterList) {
            centerList.add(professionalCenter.getCenterName());
        }
        return centerList;
    }

    @Override
    public List<String> getCities(int serviceCode, String nameLetters) {
        List<String> cityList = new ArrayList<String>();
        List<City> cityList1 = responseServiceDao.getFillCity(serviceCode, nameLetters);
        for (City city:cityList1) {
            cityList.add(city.getCity());
        }
        return cityList;
    }

    @Override
    public List<lk.appointment.professional.domain.Professional> getAstrologerWithSessions(lk.appointment.professional.domain.Professional professional) {
        SearchAstrologerDao searchAstrologerDao = new SearchAstrologerDaoImpl();
        List<lk.appointment.professional.domain.Professional> professionalList = null;
        List<lk.appointment.professional.domain.Professional> astroOnPageList = new ArrayList<lk.appointment.professional.domain.Professional>();
        lk.appointment.professional.domain.Professional professional1 = null;
        Session session;
        Specialization specialization;
        List<Session> sessions = null;
        String astroCode = null;
        String specCode=null;
        int length,i = 0;

        if (!"".equals(professional.getProfessionalName()) && (professional.getProfessionalName() != null)) {
            professional=Utility.setAstrologerName(professional);
        }

        professionalList = searchAstrologerDao.searchSession(professional);
        length = professionalList.size();
        professionalList = getSortedSessionList(professionalList);

            for(lk.appointment.professional.domain.Professional professional2 : professionalList) {

                if (!professional2.getProfessionalCode().equals(astroCode)){

                    if (astroCode!=null){
                        professional1.setSessions(sessions);
                        astroOnPageList.add(professional1);
                    }
                    professional1 = new lk.appointment.professional.domain.Professional();
                    professional1.setFirstName(professional2.getFirstName());
                    professional1.setQualification(professional2.getQualification());
                    professional1.setSpecialNote(professional2.getSpecialNote());
                    professional1.setProfessionalCode(professional2.getProfessionalCode());
                    professional1.setServiceCode(professional2.getServiceCode());
                    professional1.setCenterName(professional2.getCenterName());
                    professional1.setLocation(professional2.getLocation());
                    professional1.setCenterName(professional2.getCenterName());
                    professional1.setCenterCode(professional2.getCenterCode());

                    sessions =  new ArrayList<Session>();
                }

                session = new Session();
                session.setSessionId(professional2.getSessionId());
                session.setAppointmentDate(professional2.getAppDate());
                session.setAppointmentNo(professional2.getAppNo());
                session.setAvailability(professional2.isAvailability());
                session.setStartTime(professional2.getStarttime());
                session.setFinishTime(professional2.getFinishTime());
                session.setNoOfClients(professional2.getNoOfClients());
                session.setWebClientsNo(professional2.getWebClient());
                session.setSpecializationId(professional2.getSpecCode());
                session.setMonth(professional2.getMonth());
                session.setCenterName(professional2.getCenterName());
                session.setAddress(professional2.getAddress());
                session.setSpeciality(professional2.getSpeciality());
                session.setProfessionalCharge(professional2.getProfessionCharge());
                session.setCenterCharge(professional2.getCenterCharge());
                session.setDate(professional2.getDate());
                session.setCity(professional2.getLocation());
                session.setTotal(String.valueOf(Integer.parseInt(professional2.getProfessionCharge()) + Integer.parseInt(professional2.getCenterCharge().substring(0, professional2.getCenterCharge().indexOf('.')))));
                sessions.add(session);


                astroCode = professional2.getProfessionalCode();
                specCode = professional2.getSpecCode();
                i++;
                if (i==length){
                    professional1.setSessions(sessions);
                    astroOnPageList.add(professional1);
                }
            }
        return astroOnPageList;
    }

    @Override
    public List<ProfessionalSpecializationList> getSpecializationList() {
        return null;
    }

    public List<lk.appointment.professional.domain.Professional> getProfessionalList (List<lk.appointment.professional.domain.Professional> professionalList) {
        String sessionId=null;
        lk.appointment.professional.domain.Session session = null;
        Specialization specialization = null;
        List<Specialization> specList1 = null;
        List<lk.appointment.professional.domain.Session> sessions = new ArrayList<Session>();

        for (lk.appointment.professional.domain.Professional  professional3:professionalList) {
            for (lk.appointment.professional.domain.Session sessionList: professional3.getSessions()) {
                if (sessionId==null || !sessionList.getSessionId().equals(sessionId)) {
                    if (sessionId!=null) {
                        session.setSpecializationList(specList1);
                        sessions.add(session);
                    }

                    specialization = new Specialization();
                    specList1 = new ArrayList<Specialization>();
                    session = new lk.appointment.professional.domain.Session();
                    session.setSessionId(sessionList.getSessionId());
                    session.setSessionId(sessionList.getSessionId());
                    session.setStartTime(sessionList.getStartTime());
                    session.setFinishTime(sessionList.getFinishTime());
                    session.setAvailability(sessionList.getAvailability());
                    session.setAppointmentNo(sessionList.getAppointmentNo());
                    session.setAppointmentDate(sessionList.getAppointmentDate());
                    session.setNoOfClients(sessionList.getNoOfClients());
                    session.setWebClientsNo(sessionList.getWebClientsNo());
                    session.setMonth(sessionList.getMonth());
                    session.setTotal(sessionList.getTotal());
                    session.setCenterName(sessionList.getCenterName());
                    session.setAddress(sessionList.getAddress());
                    session.setCity(sessionList.getCity());
                    session.setSpeciality(sessionList.getSpeciality());
                    session.setCenterCharge(sessionList.getCenterCharge());
                    session.setProfessionalCharge(sessionList.getProfessionalCharge());
                    session.setDate(sessionList.getDate());
                    specialization.setSpecializationId(sessionList.getSpecializationId());
                    specialization.setSpecializationName(sessionList.getSpeciality());
                    specList1.add(specialization);
                    sessionId = sessionList.getSessionId();
                }
                else {
                    /*sessionList1.add(sessionList.getSessionId());
                    sessionId = sessionList.getSessionId();*/
                    specialization = new Specialization();
                    specialization.setSpecializationId(sessionList.getSpecializationId());
                    specialization.setSpecializationName(sessionList.getSpeciality());
                    specList1.add(specialization);
                    /*sessions.add(session);*/
                    sessionId = sessionList.getSessionId();
                }
            }

            professional3.setSessions(sessions);
        }
        return professionalList;
    }
    @Override
    public List<Center> getCentersWithAstrologersAndSessions(lk.appointment.professional.domain.Professional professional) {
        if (!"".equals(professional.getProfessionalName()) && (professional.getProfessionalName() != null)) {
            professional=Utility.setAstrologerName(professional);
        }
        return searchAstrologerDao1.SearchSessionWithCenter(professional);
    }
}
