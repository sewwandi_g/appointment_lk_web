package lk.appointment.professional.service.Impl;


import com.ech.mobitel.JSMPPSmsSenderMobitel;
import com.ech.mobitel.SMPPData;
import com.ech.mobitel.SMSConfiguration;
import com.ech.mobitel.SMSData;
import lk.appointment.professional.dao.Impl.SmsDaoImpl;
import lk.appointment.professional.dao.SmsDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.service.SmsService;
import org.springframework.stereotype.Service;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by se-7 on 5/26/2017.
 */
@Service
public class SmsServiceImpl implements SmsService {
    ResourceBundle resourceBundle = ResourceBundle.getBundle("settingsMobitelSMPP");
    private String readSmppIp = resourceBundle.getString("ipAddress");
    SmsDao smsDao = new SmsDaoImpl();
    boolean isSuccess = false;
    public boolean sendViaMobitel (String phone_number, Appointment appointment,String sms_message,String smsProf) {

        SMPPData smppData = new SMPPData();
        smppData.setIpAddress(readSmppIp);
        smppData.setPort(Integer.parseInt(resourceBundle.getString("port")));
        smppData.setSystemId(resourceBundle.getString("systemId"));
        smppData.setPassword(resourceBundle.getString("password"));
        smppData.setSystemType(null);
        smppData.setSourceNo(resourceBundle.getString("sourceNo"));
        smppData.setReceiveTimeout(Integer.parseInt(resourceBundle.getString("receiveTimeout")));
        smppData.setAddrTon(resourceBundle.getString("addr-ton"));
        smppData.setAddrNpi(resourceBundle.getString("addr-npi"));

        int stat=0;


        String refNo = appointment.getRefNo();
        //String sms_message = ""+ appointment.getTitle()+" "+appointment.getFirstName()+" "+appointment.getSurName() +" on "+ appointment.getAppDate() +" at "+ getTimeFormat(appointment.getGivenTime())+". Time allocated is subject to changed. RefNo."+refNo+ "." + "AppNo." +appointment.getAppNo()+ ". "+ "Address." + appointment.getAddress1()+" "+appointment.getAddress2()+" "+appointment.getAddress3()+"," +appointment.getCity()+ ".";

        //String smsProf = "You have an appointment on "+ appointment.getAppDate() +" at "+ getTimeFormat(appointment.getGivenTime())+"";

        try {


            JSMPPSmsSenderMobitel diagSender = SMSConfiguration.getInstance(smppData);

            SMSData smsData = new SMSData();
            smsData.setSmsAddrNpi(Integer.parseInt(resourceBundle.getString("sms-addr-npi")));
            smsData.setSmsAddrTon(Integer.parseInt(resourceBundle.getString("sms-addr-ton")));
            smsData.setSourceNo(resourceBundle.getString("sourceNo"));

            //send customer sms
            smsData.setPhoneNo(phone_number);
            smsData.setMessagePara(sms_message);
            isSuccess = diagSender.sendSms(smsData);

            //send professional sms
            smsData.setPhoneNo(appointment.getMobileNo());
            //smsData.setPhoneNo("0702590710");
            smsData.setMessagePara(smsProf);
            diagSender.sendSms(smsData);

            if (isSuccess) {
                stat = 1;
                Logger.getLogger("server.log").log(Level.WARNING, "\nException:{0}", "Message Send Sucesses For : " + phone_number + "");
            } else {
                stat = 0;
                Logger.getLogger("server.log").log(Level.WARNING, "\nException:{0}", "Message Send Failed For : " + phone_number + " : sending Failed");
            }

        } catch (Exception e) {
            Logger.getLogger("server.log").log(Level.WARNING, "\nException:{0}", e.getMessage());
        }
        return isSuccess;//smsDao.setSmsDetails(stat,refNo);
    }
}
