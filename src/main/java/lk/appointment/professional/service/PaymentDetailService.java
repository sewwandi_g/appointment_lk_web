package lk.appointment.professional.service;

import lk.appointment.professional.domain.PaymentGateway;

import java.util.List;

/**
 * Created by se-7 on 5/16/2017.
 */
public interface PaymentDetailService {
    public List<PaymentGateway> getPaymentChannel(PaymentGateway paymentGateway);
}
