package lk.appointment.professional.service;

import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.SessionDetails;

/**
 * Created by Sewwandi on 7/5/2018.
 */
public interface SessionDetailsService {
    public SessionDetails getSessionDetails(Appointment appointment);
}
