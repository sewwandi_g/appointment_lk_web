package lk.appointment.professional.service.client.Impl;

import com.encryptRefNo.CryptRefSession;
import com.encryptRefNo.EChCryptRefNoRequest;
import lk.appointment.professional.dao.BookingServiceDao;
import lk.appointment.professional.dao.Impl.BookingServiceDaoImpl;
import lk.appointment.professional.domain.*;
import lk.appointment.professional.dto.PaymentDetail;
import lk.appointment.professional.enumbers.PaymentType;
import lk.appointment.professional.service.client.PaymentClient;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by se-7 on 5/16/2017.
 */
@Service
public class HSBCPaymentClientImpl implements PaymentClient {

    BookingServiceDao bookingServiceDao = new BookingServiceDaoImpl();
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("settingsHSBC");
    static final String SECURE_SECRET = resourceBundle.getString("SECURE_SECRET");


    private static ResourceBundle resource = ResourceBundle.getBundle("proxies");
    static final String host = resource.getString("HOSTPORT");

    static final char[] HEX_TABLE = new char[]{
            '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    String hashKeys = new String();
    String hashValues = new String();


    String SHAhashAllFields(Map fields) {

        hashKeys = "";
        hashValues = "";

        // create a list and sort it
        List fieldNames = new ArrayList(fields.keySet());
        Collections.sort(fieldNames);

        // create a buffer for the SHA256 input
        StringBuffer buf = new StringBuffer();

        // iterate through the list and add the remaining field values
        Iterator itr = fieldNames.iterator();
        while (itr.hasNext()) {
            String fieldName = (String) itr.next();
            String fieldValue = (String) fields.get(fieldName);
            hashKeys += fieldName + ", ";
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                buf.append(fieldName + "=" + fieldValue);
                if (itr.hasNext()) {
                    buf.append('&');
                }
            }
        }
        byte[] mac = null;
        try {
            byte []  b = fromHexString(SECURE_SECRET, 0, SECURE_SECRET.length());
            SecretKey key = new SecretKeySpec(b, "HmacSHA256");
            Mac m = Mac.getInstance("HmacSHA256");
            m.init(key);

            m.update(buf.toString().getBytes("ISO-8859-1"));
            mac = m.doFinal();
        } catch(Exception e) {

        }
        String hashValue = hex(mac);
        return hashValue;

    } // end hashAllFields()



    public static byte[] fromHexString(String s, int offset, int length)
    {
        if ((length%2) != 0)
            return null;
        byte[] byteArray = new byte[length/2];
        int j = 0;
        int end = offset+length;
        for (int i = offset; i < end; i += 2)
        {
            int high_nibble = Character.digit(s.charAt(i), 16);
            int low_nibble = Character.digit(s.charAt(i+1), 16);
            if (high_nibble == -1 || low_nibble == -1)
            {
                // illegal format
                return null;
            }
            byteArray[j++] = (byte)(((high_nibble << 4) & 0xf0) | (low_nibble & 0x0f));
        }
        return byteArray;
    }
//  ----------------------------------------------------------------------------

    static String hex(byte[] input) {
        // create a StringBuffer 2x the size of the hash array
        StringBuffer sb = new StringBuffer(input.length * 2);

        // retrieve the byte array data, convert it to hex
        // and add it to the StringBuffer
        for (int i = 0; i < input.length; i++) {
            sb.append(HEX_TABLE[(input[i] >> 4) & 0xf]);
            sb.append(HEX_TABLE[input[i] & 0xf]);
        }
        return sb.toString();
    }

    private static String null2unknown(String in) {
        if (in == null || in.length() == 0) {
            return "No Value Returned";
        } else {
            return in;
        }
    }

    public String replaceSColon(String s, String find, String replace) {

        StringBuffer sb = new StringBuffer();
        sb.append(s);
        int in = 0;
        while (s.indexOf(find) != 0 && s.indexOf(find) != -1) {
            in = s.indexOf(find);

            sb = sb.replace(in, in + 1, replace);
            s = sb.toString();
        }

        return sb.toString().trim();
    }


    @Override
    public PaymentRequest getPaymentRequest(Appointment appointment) {
        Member member = new Member();
        PaymentRequest paymentRequest = new PaymentRequest();
        Map<String, String> paymentData = new HashMap<String, String>();

        try {

            if (appointment.getMember() != null) {
                member = appointment.getMember();
            }

            String linkDetail = "";

            String vrtualpayment_client_url = resourceBundle.getString("virtualPaymentClientURL");

            String vpc_Version = resourceBundle.getString("vpc_Version");
            String vpc_Command = resourceBundle.getString("vpc_Command");
            String vpc_AccessCode = resourceBundle.getString("vpc_AccessCode");
            String vpc_Merchant = resourceBundle.getString("vpc_Merchant");
            String vpc_OrderInfo  =  "";
            String vpc_MerchTxnRef = resourceBundle.getString("vpc_OrderInfo");

            vpc_OrderInfo = appointment.getRefNo();

            String vpc_Locale = resourceBundle.getString("vpc_Locale");
            String vpc_TxSourceSubType = resourceBundle.getString("vpc_TxSourceSubType");

            paymentData.put("vpc_Version", vpc_Version);
            paymentData.put("vpc_Command", vpc_Command);
            paymentData.put("vpc_AccessCode", vpc_AccessCode);
            paymentData.put("vpc_MerchTxnRef", vpc_MerchTxnRef);
            paymentData.put("vpc_Merchant", vpc_Merchant);
            paymentData.put("vpc_OrderInfo", vpc_OrderInfo);

            String theReturnLink = "";


            linkDetail = LinkDetails_Source(appointment.getSeqNo(), appointment.getRefNo(), appointment.getAmount());
            paymentData.put("vpc_Amount", Integer.toString(Double.valueOf(appointment.getAmount()).intValue() * 100));


                theReturnLink = "http://" + host + "/paymentgateways?paymentMode=" + PaymentType.HSBC.toString();


            paymentData.put("vpc_ReturnURL", theReturnLink);
            paymentData.put("vpc_Locale", vpc_Locale);
            paymentData.put("vpc_Currency", "LKR");


            if (SECURE_SECRET != null && SECURE_SECRET.length() > 0) {
                String secureHash = SHAhashAllFields(paymentData);
                paymentData.put("vpc_SecureHash", secureHash);
                paymentData.put("vpc_SecureHashType", "SHA256");
                paymentData.put("para", linkDetail);
            }

            StringBuffer buf = new StringBuffer();
            buf.append(vrtualpayment_client_url).append('?');
            appendQueryFields(buf, paymentData);

            paymentRequest.setUrl(buf.toString());
            paymentRequest.setRedirectOnly(true);
            paymentRequest.setPaymentData(new HashMap<String, String>());

            //LOG.info(paymentRequest);

            return paymentRequest;


            //LOG.info("--------------------HSBCPaymentClientImpl.java-------------------"+paymentRequest.toString());

        }
        catch (Exception e) {
            //LOG.error("Exception in EChAstroChannel/HSBCPaymentClientImpl.java - ", e);
            Logger.getLogger("server.log").log(Level.WARNING,"Exception in HSBCpAYMENTcLIENTiMPL-------"+e.toString());
        }
        return null;
    }

    public String LinkDetails_Source(String seqno,String ref_n, String amount) {
        String encrptedSqNo = "";

        try {
            Random random = new Random();
            Double ran_number = random.nextDouble();


            String ra_number = Double.toString(ran_number);
            CryptRefSession session = new CryptRefSession();

            session.setReferenceNo(ref_n);

            session.setDocno(amount);

            EChCryptRefNoRequest refObj = new EChCryptRefNoRequest();

            encrptedSqNo = refObj.encrypt(session);

        } catch (NumberFormatException e) {
            e.getStackTrace();
        }

        return encrptedSqNo;

    }

    public ServiceResponse confirmPaidPayment(HttpServletRequest request, Appointment paidAppointment) {

        MerchantResponseHSBC merchantResponce = new MerchantResponseHSBC();


        Map fields = new HashMap();
        Enumeration e = request.getParameterNames();

        while (e.hasMoreElements()) {
            String fieldName = (String) e.nextElement();
            String fieldValue = request.getParameter(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                fields.put(fieldName, fieldValue);
            }
        }


        merchantResponce.setVpcTxnSecureHash(null2unknown((String) fields.remove("vpc_SecureHash")));
        fields.remove("paymentMode");

        merchantResponce.setDecryptpNo(null2unknown((String) fields.get("para")));


        merchantResponce.setSecureHash(SHAhashAllFields(fields));


        merchantResponce.setTxnResponseCode(null2unknown((String) fields.get("vpc_TxnResponseCode")));


        EChCryptRefNoRequest refObj = new EChCryptRefNoRequest();

        CryptRefSession de = refObj.decrypt(replaceSColon(merchantResponce.getDecryptpNo(), "#", ";"));

        merchantResponce.setReferenceNo((String) fields.get("vpc_OrderInfo")); // testing

        return isChargingDone(merchantResponce);
    }

    @Override
    public PaymentDetail checkBalance(String phoneNumber, double fullAmt) {
        return null;
    }

    @Override
    public void reverseBill() {
        return;
    }

    private ServiceResponse isChargingDone(MerchantResponseHSBC merchantResponce) {

        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.addData(merchantResponce.getReferenceNo());
        if (merchantResponce.getVpcTxnSecureHash() != null && merchantResponce.getVpcTxnSecureHash().length() > 0 && !merchantResponce.getDecryptpNo().equals(null)) {
            if (SECURE_SECRET != null && SECURE_SECRET.length() > 0 && (merchantResponce.getTxnResponseCode() != null || merchantResponce.getTxnResponseCode() != "No Value Returned")) {

                String errMessage = "";

                if (merchantResponce.getTxnResponseCode().equalsIgnoreCase("0")) {
                    serviceResponse.setStatus(true);

                    if ((bookingServiceDao.isValidResponceHSBC(merchantResponce.getReferenceNo())) && bookingServiceDao.doConfirmPayment(merchantResponce.getReferenceNo())) {
                        return serviceResponse;
                    }

                } else {

                    serviceResponse.setStatus(false);
                    char input = merchantResponce.getTxnResponseCode().charAt(0);


                    switch (input) {

                        case '3':
                        case '6':
                        case '7':
                            errMessage = "Ref No: " + merchantResponce.getReferenceNo() + "Transaction Unsuccessful, Please Try Again ......";
                            serviceResponse.addError(errMessage);
                            return serviceResponse;

                        default:
                            errMessage = "Ref No: " + merchantResponce.getReferenceNo() + "Transaction Rejected, Please Contact Your Bank ...";
                            serviceResponse.addError(errMessage);
                            return serviceResponse;
                    }


                }

                //  } // remove if part due to unmatched hash code 24/01/2017

            }

        } else {
            serviceResponse.setStatus(false);
        }

        return serviceResponse;
    }

    public void appendQueryFields(StringBuffer buf, Map fields) {

        // create a list
        List fieldNames = new ArrayList(fields.keySet());
        Iterator itr = fieldNames.iterator();

        // move through the list and create a series of URL key/value pairs
        while (itr.hasNext()) {
            String fieldName = (String) itr.next();
            String fieldValue = (String) fields.get(fieldName);

            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                // append the URL parameters
                buf.append(URLEncoder.encode(fieldName));
                buf.append('=');
                buf.append(URLEncoder.encode(fieldValue));
            }

            // add a '&' to the end if we have more fields coming.
            if (itr.hasNext()) {
                buf.append('&');
            }
        }

    }
}
