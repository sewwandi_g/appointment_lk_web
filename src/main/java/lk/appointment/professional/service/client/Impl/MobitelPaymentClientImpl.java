package lk.appointment.professional.service.client.Impl;

//import example.MobitelChargingClient;

//import mypackage.WsResult;


import com.ech.mobitel.astro.v1.*;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.PaymentRequest;
import lk.appointment.professional.domain.ServiceResponse;
import lk.appointment.professional.dto.PaymentDetail;
import lk.appointment.professional.service.client.PaymentClient;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * Created by Sewwandi on 10/8/2018.
 */





@Service
public class MobitelPaymentClientImpl implements PaymentClient {

    PaymentDetail paymentDetail = new PaymentDetail();

    private MCAccMgrLocator locator ;

    @Override
    public PaymentRequest getPaymentRequest(Appointment appointment)  {
        MCAccMngr service;
        String serviceId="APPM";
        String contentId="";
        ChgResult result;
        PaymentRequest paymentRequest=null;
        Map<String,String> paymentData=null;
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<appointment.getAmount() - payment done>>>>>>>>>>>>>>>>>>"+appointment.getAmount());
        int amt = (int)(Double.parseDouble(appointment.getAmount())*100);

        ChgRequest request = new ChgRequest(amt,contentId,appointment.getTelNo(),serviceId,appointment.getRefNo());
        try {
            locator = new MCAccMgrLocator();
            service = locator.getMCAccMngrPort();
            Logger.getLogger("server.log").log(Level.INFO,"----------------service---------------------------" + service.toString());
            try {
                result=service.chargeFromMSISDN(request,"echuser","ecH@123");
                if (result != null) {
                    paymentData=new HashMap<>();
                    paymentRequest=new PaymentRequest();
                    paymentData.put("contentId",result.getContentId());
                    paymentData.put("msisdn",result.getMsisdn());
                    paymentData.put("resultCode",Integer.toString(result.getResultCode()));
                    paymentData.put("resultDesc",result.getResultDesc());
                    paymentData.put("transactionId",result.getTransactionId());
                    paymentRequest.setPaymentData(paymentData);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return paymentRequest;
    }

    @Override
    public ServiceResponse confirmPaidPayment(HttpServletRequest request, Appointment paidAppointment) {
        return null;
    }

    @Override
    public PaymentDetail checkBalance(String phoneNumber, double fullAmt) {
        String connType = "POS";
        boolean sufficientMoney = true;
        double cusBalance = 0;
        locator = new MCAccMgrLocator();
        MCAccMngr service;
        try {
            service = locator.getMCAccMngrPort();
            Logger.getLogger("server.log").log(Level.INFO,"----------------fullAmt---------------------------" + fullAmt);
            Logger.getLogger("server.log").log(Level.INFO,"----------------service---------------------------" + service.toString());
            ((MCAccMgrSoapBindingStub) service).setUsername("echuser");
            ((MCAccMgrSoapBindingStub) service).setPassword("ecH@123");

            BalanceResult balanceQuery = service.checkPrepaidBalance(phoneNumber);
            InfoResult infoResult = service.msisdnInfo(phoneNumber);

            Logger.getLogger("server.log").log(Level.INFO, "-----------------------getResultCode-------------------" + infoResult.getResultCode());
            Logger.getLogger("server.log").log(Level.INFO, "-----------------------getResultDesc-------------------" + infoResult.getResultDesc());
            Logger.getLogger("server.log").log(Level.INFO, "-----------------------getMsisdn-------------------" + infoResult.getMsisdn());
            Logger.getLogger("server.log").log(Level.INFO, "-----------------------getConnectionType-------------------" + infoResult.getConnectionType());

            if(1000==infoResult.getResultCode()) {
                connType = infoResult.getConnectionType();
            }

            if ("POS".equals(connType) && fullAmt > 0) {
                sufficientMoney = true;
            } else {
                Logger.getLogger("server.log").log(Level.INFO, "-----------------------getBalance-------------------" + balanceQuery.getBalance());
                Logger.getLogger("server.log").log(Level.INFO, "-----------------------getResultDesc-------------------" + balanceQuery.getResultDesc());
                Logger.getLogger("server.log").log(Level.INFO, "-----------------------getMsisdn-------------------" + balanceQuery.getMsisdn());
                Logger.getLogger("server.log").log(Level.INFO, "-----------------------getResultCode-------------------" + balanceQuery.getResultCode());
                cusBalance = Double.parseDouble(balanceQuery.getBalance())/100;
               double newFullAmt = fullAmt+50.0;
                if (cusBalance >= newFullAmt && newFullAmt > 0) {
                    sufficientMoney = true;
                } else {
                    sufficientMoney = false;
                }
            }

            if (sufficientMoney) {
                paymentDetail.setErrorCode(paymentDetail.SUCCESS);
                paymentDetail.setPhoneNumber(phoneNumber);
                paymentDetail.setBalance(cusBalance);
                paymentDetail.setConnType(connType);


            } else {
                paymentDetail.setErrorCode(paymentDetail.FAILED);
                paymentDetail.setPhoneNumber(phoneNumber);
                paymentDetail.setBalance(cusBalance);
                paymentDetail.setConnType(connType);
            }
        } catch (ServiceException e) {
            System.out.println(e);
        } catch (RemoteException e) {
            System.out.println(e);
        }
        return paymentDetail;
    }

    @Override
    public void reverseBill() {

    }
}


