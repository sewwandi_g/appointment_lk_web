package lk.appointment.professional.service.client;

import lk.appointment.professional.enumbers.PaymentType;
import lk.appointment.professional.service.client.Impl.HSBCPaymentClientImpl;
import lk.appointment.professional.service.client.Impl.MobitelPaymentClientImpl;
import org.springframework.stereotype.Service;

/**
 * Created by se-7 on 5/16/2017.
 */
@Service
public class PaymentFactory {
    public static PaymentClient getInstance(String gatewayName) {
        if (PaymentType.HSBC.toString().equals(gatewayName)) {
            return (PaymentClient) new HSBCPaymentClientImpl();
        }else if (PaymentType.M.toString().equals(gatewayName)) {
            return (PaymentClient) new MobitelPaymentClientImpl();
        }else {
            return null;
        }
    }
}
