package lk.appointment.professional.service;

import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.dto.AppointmentDTO;
/**
 * Created by se-7 on 5/15/2017.
 */
public interface ProfessionalAppointmentService {
    public AppointmentDTO doTempAppointment(Appointment appointment);
    void confirmAppointment(String securityKey,String paymentChannel);
}
