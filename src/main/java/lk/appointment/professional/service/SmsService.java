package lk.appointment.professional.service;

import lk.appointment.professional.domain.Appointment;

/**
 * Created by se-7 on 5/26/2017.
 */
public interface SmsService {
    public boolean sendViaMobitel (String phone_number, Appointment appointment1,String sms_message,String smsProf);
}
