package lk.appointment.professional.service;

import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.PaymentRequest;
import lk.appointment.professional.domain.ServiceResponse;
import lk.appointment.professional.dto.PaymentDetail;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by se-7 on 5/16/2017.
 */
public interface PaymentService {
    public PaymentRequest getPaymentRequest(Appointment appointment);
    public ServiceResponse confirmPaidPayment(Appointment appointment, HttpServletRequest request);
    public PaymentDetail checkBalance(String phoneNumber,double fullAmt,String agent);
}
