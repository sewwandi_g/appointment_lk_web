package lk.appointment.professional.service;

import lk.appointment.professional.domain.*;
import lk.appointment.professional.dto.Professional;
import lk.appointment.professional.dto.ProfessionalSpecializationList;

import java.util.List;

/**
 * Created by se-7 on 5/15/2017.
 */
public interface ProfessionalDetailService {
    List<lk.appointment.professional.domain.Professional> getAstrologerData(lk.appointment.professional.domain.Professional professional) ;

    public List<lk.appointment.professional.domain.Professional> getAstrologerList(int serviceCode);

    public Professional getAstrologer(int id);

    public SpecialNote getAstroSpecNote(int astroId,int centerId);

    public AllocateAstrologer isCheckAstrologerCenter(int astroId,int centerId);

    public List<ProfessionalSpecialization> getSpeciality(int professionalCode);

    public List<ServiceType> getTermAndConditions(int serviceCode);

    public List<String> getFillName(int serviceCode,String nameLetters);

    public List<String> getCenterName(int serviceCode,String nameLetters);

    public List<String> getCities(int serviceCode,String nameLetters);

    public List<lk.appointment.professional.domain.Professional> getAstrologerWithSessions(lk.appointment.professional.domain.Professional professional);

    public List<Center> getCentersWithAstrologersAndSessions(lk.appointment.professional.domain.Professional professional);

    public List<ProfessionalSpecializationList> getSpecializationList();
}
