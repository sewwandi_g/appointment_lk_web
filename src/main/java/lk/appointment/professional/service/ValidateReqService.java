package lk.appointment.professional.service;

import lk.appointment.professional.domain.Success;
import lk.appointment.professional.domain.SuccessResponse;

import java.util.List;

/**
 * Created by se-7 on 5/16/2017.
 */
public interface ValidateReqService {
    public boolean isRequestValidate(String key);

    public List<SuccessResponse> checkPaymentSuccess(Success success);
    public boolean ConfirmOTPComparison(String refNo,String otp);
}
