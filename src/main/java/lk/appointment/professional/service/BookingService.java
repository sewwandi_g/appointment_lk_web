package lk.appointment.professional.service;

import lk.appointment.professional.domain.Agent;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.PaymentRequest;
import lk.appointment.professional.domain.ServiceResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
/**
 * Created by se-7 on 5/16/2017.
 */
public interface BookingService {

    public Appointment validateTemporyBooking(String key,String PaymentChannel);

    public PaymentRequest generatePaymentGatewayRequest(Appointment appointment);

    public ServiceResponse confirmTemporaryBooking(Appointment appointment, HttpServletRequest request);

    public Appointment getPaymentDetails(String refNo);

    public String getShowFrontEndBookingFee(String total);

    public List<String> getTimeSlots(String sessionId,String specId);

    public Appointment getTempAppointmentDetails(String refNo);

    public boolean doCompleteAppointment(Agent agent);

    public boolean confirmOTP(String refNo);

    public Appointment resendOtp(int refNo);

}
