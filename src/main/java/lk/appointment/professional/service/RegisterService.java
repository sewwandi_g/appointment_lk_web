package lk.appointment.professional.service;

import lk.appointment.professional.dto.SelfRegister;

/**
 * Created by udara on 5/14/2020.
 */
public interface RegisterService {

    public void selfRegister(SelfRegister reg);
    public boolean checkNicEmail(SelfRegister reg);
}
