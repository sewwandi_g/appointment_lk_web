package lk.appointment.professional.service;

import lk.appointment.professional.domain.ProfessionalSpecialization;
import lk.appointment.professional.dto.ProfessionalSpecializationList;

import java.util.List;
import java.util.Map;
/**
 * Created by se-7 on 6/21/2017.
 */
public interface SpecializationListService {

    public List<ProfessionalSpecialization> getSpecList(int serviceCode);

    public List<ProfessionalSpecializationList> getSpecializationAccordingToProfessionals();

    public Map<Integer,String> getSpecializationOfProfessionals(int serviceCode,int professionalCode);
}
