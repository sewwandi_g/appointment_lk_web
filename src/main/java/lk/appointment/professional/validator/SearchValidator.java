package lk.appointment.professional.validator;
import lk.appointment.professional.domain.Professional;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Created by Kavindu on 6/21/2018.
 */
@Component
public class SearchValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Professional.class.equals(aClass);
    }
    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "serviceCode", "user.serviceCode.empty");

        Professional professional = (Professional) o;
        if (Integer.parseInt(professional.getServiceCode())<1) {
            errors.reject("invalid service code");
        }
        if (!"".equals(professional.getSpecCode()) &&  Integer.parseInt(professional.getSpecCode())<1) {
            errors.reject("invalid spec code");
        }
        if (!"".equals(professional.getDate()) && isValidFormat("yyyy-mm-dd",professional.getDate())) {
            errors.reject("Invalid date format");
        }
        if (!"".equals(professional.getCenterName()) && validateString(professional.getCenterName())){
            errors.reject("Invalid center name format");
        }
        if (!"".equals(professional.getProfessionalName()) && validateString(professional.getProfessionalName())){
            errors.reject("Invalid professional name format");
        }
    }

    private boolean isValidFormat(String format, String value) {
        LocalDateTime ldt = null;
        DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(format, Locale.US);

        try {
            ldt = LocalDateTime.parse(value, fomatter);
            String result = ldt.format(fomatter);
            return result.equals(value);
        } catch (DateTimeParseException e) {
            try {
                LocalDate ld = LocalDate.parse(value, fomatter);
                String result = ld.format(fomatter);
                return result.equals(value);
            } catch (DateTimeParseException exp) {
                try {
                    LocalTime lt = LocalTime.parse(value, fomatter);
                    String result = lt.format(fomatter);
                    return result.equals(value);
                } catch (DateTimeParseException e2) {
                    // Debugging purposes
                    //e2.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean validateString(String inString){
        String REGEX = "[a-zA-Z_. ]";
        boolean ok=true;
        Pattern pattern = Pattern.compile(REGEX);
        Matcher m = pattern.matcher(inString);
        if (m.find()) {
            ok = false;
        }
        return ok;
    }

    private boolean validateNumber(String inString){
        String REGEX = "[0-9]";
        boolean ok=false;
        Pattern pattern = Pattern.compile(REGEX);
        Matcher m = pattern.matcher(inString);
        if (!m.find()) {
            ok = true;
        }
        return ok;
    }


}
