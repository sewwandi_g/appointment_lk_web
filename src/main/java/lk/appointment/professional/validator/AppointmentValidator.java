package lk.appointment.professional.validator;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.Professional;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;

import java.lang.annotation.Annotation;
/**
 * Created by Kavindu on 6/25/2018.
 */
@Component
public class AppointmentValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return Appointment.class.equals(aClass);
    }
    @Override
    public void validate(Object o, Errors errors) {

    }
}
