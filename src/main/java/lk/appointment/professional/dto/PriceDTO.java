package lk.appointment.professional.dto;
import lombok.Data;

import java.math.BigDecimal;
/**
 * Created by se-8 on 1/5/2018.
 */
@Data
public class PriceDTO {
    private BigDecimal ProfessionalFee;
    private BigDecimal centerFee;
    private BigDecimal echFee;
    private BigDecimal agentFee;
}
