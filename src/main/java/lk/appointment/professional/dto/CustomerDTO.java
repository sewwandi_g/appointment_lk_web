package lk.appointment.professional.dto;
import lombok.Data;
/**
 * Created by se-8 on 1/5/2018.
 */
@Data
public class CustomerDTO {
    private String customerName;
    private String nic;
    private String email;
    private String nationality;
    private BirthDetailDTO birthDetail;
}
