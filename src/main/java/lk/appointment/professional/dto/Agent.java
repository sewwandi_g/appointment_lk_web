package lk.appointment.professional.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Sewwandi on 10/25/2018.
 */
@Getter
@Setter
public class Agent {
    private String agentName;
    private double fee;
}

