package lk.appointment.professional.dto;
import lombok.Data;
/**
 * Created by se-8 on 1/5/2018.
 */
@Data
public class CenterDTO {
    private String centerName;
    private String address;
    private String city;
    private String tel;
}
