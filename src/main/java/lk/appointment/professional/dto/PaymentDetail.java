package lk.appointment.professional.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Sewwandi on 10/8/2018.
 */
@Getter
@Setter
public class PaymentDetail {
    private String connType = "POST";
    private double balance= 0;
    private String phoneNumber = "";
    private int errorCode = -1;
    public static final int SUCCESS = 1;
    public static final int FAILED = -1;
}
