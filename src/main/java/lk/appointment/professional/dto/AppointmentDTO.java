package lk.appointment.professional.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
/**
 * Created by se-8 on 1/4/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AppointmentDTO {
    private String refNo;
    private String appNo;
    private String appTime;
    private String amt;
    private String payDate;

    public String getAmt() {
        return amt;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    private String securityKey;
    private String paymentChannel;

    public String getAppTime() {
        return appTime;
    }

    public void setAppTime(String appTime) {
        this.appTime = appTime;
    }

    private String status;
    private String statusDescription;
    private String channelSource;


    public String getAppNo() {
        return appNo;
    }
    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }
    public String getRefNo() {
        return refNo;
    }
    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }
    public String getSecurityKey() {
        return securityKey;
    }
    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }
    public String getPaymentChannel() {
        return paymentChannel;
    }
    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getStatusDescription() {
        return statusDescription;
    }
    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
    public String getChannelSource() {
        return channelSource;
    }
    public void setChannelSource(String channelSource) {
        this.channelSource = channelSource;
    }
}
