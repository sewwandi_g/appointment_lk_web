package lk.appointment.professional.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by se-8 on 7/11/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Professional {
    private String title;
    private String firstName;
    private String lastName;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
