package lk.appointment.professional.dto;

/**
 * Created by se-8 on 10/2/2017.
 */
public class Specialization {
    private String specializationId;
    private String SpecializationName;

    public String getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(String specializationId) {
        this.specializationId = specializationId;
    }

    public String getSpecializationName() {
        return SpecializationName;
    }

    public void setSpecializationName(String specializationName) {
        SpecializationName = specializationName;
    }
}
