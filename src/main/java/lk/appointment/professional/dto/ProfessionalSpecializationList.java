package lk.appointment.professional.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by se-8 on 10/2/2017.
 */
public class ProfessionalSpecializationList {
    private String professionalId;
    private String specializationId;
    private List<Specialization> specializationList = new ArrayList<Specialization>();

    public String getProfessionalId() {
        return professionalId;
    }

    public void setProfessionalId(String professionalId) {
        this.professionalId = professionalId;
    }

    public List<Specialization> getSpecializationList() {
        return specializationList;
    }

    public void setSpecializationList(List<Specialization> specializationList) {
        this.specializationList = specializationList;
    }

    public String getSpecializationId() {
        return specializationId;
    }

    public void setSpecializationId(String specializationId) {
        this.specializationId = specializationId;
    }
}
