package lk.appointment.professional.dto;
import lombok.Data;
/**
 * Created by se-8 on 1/5/2018.
 */
@Data
public class BirthDetailDTO {
    private String birthDate;
    private String birthTime;
    private String birthCountry;
    private String birthTown;
}
