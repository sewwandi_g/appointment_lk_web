package lk.appointment.professional.dto;

/**
 * Created by se-8 on 9/22/2017.
 */
public class ProfessionalCenter {
    private String centerName;

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }
}
