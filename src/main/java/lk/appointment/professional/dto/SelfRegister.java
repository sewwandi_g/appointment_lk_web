package lk.appointment.professional.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by udara on 5/14/2020.
 */
@Getter
@Setter
public class SelfRegister {

    private String name;
    private String email;
    private String category;
    private String qualification;
    private String sub;
    private String nic;
    private String addr;
    private String contact;

}
