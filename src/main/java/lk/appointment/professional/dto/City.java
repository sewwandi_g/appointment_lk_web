package lk.appointment.professional.dto;

/**
 * Created by se-8 on 10/3/2017.
 */
public class City {
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
