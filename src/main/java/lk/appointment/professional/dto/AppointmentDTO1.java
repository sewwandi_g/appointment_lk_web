package lk.appointment.professional.dto;
import lombok.Data;
/**
 * Created by se-8 on 1/5/2018.
 */
@Data
public class AppointmentDTO1 {
    private String refNo;
    private String appNo;
    private String roomNo;
    private String appDate;
    private String givenTime;
    private String status;
    private String appointmentType;
    private String paymentType;
    private String channelFrom;
    private PriceDTO price;
    private CustomerDTO customer;
    private CenterDTO center;
}
