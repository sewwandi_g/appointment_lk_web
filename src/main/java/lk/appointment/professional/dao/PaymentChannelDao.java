package lk.appointment.professional.dao;

import lk.appointment.professional.domain.PaymentGateway;

import java.util.List;

/**
 * Created by se-7 on 5/16/2017.
 */
public interface PaymentChannelDao {
    public List<PaymentGateway> getPaymentChannel(PaymentGateway paymentGateway);
}
