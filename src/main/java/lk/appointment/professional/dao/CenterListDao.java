package lk.appointment.professional.dao;

import lk.appointment.professional.domain.Center;
import lk.appointment.professional.dto.CenterDTO;

import java.util.List;

/**
 * Created by se-7 on 6/22/2017.
 */
public interface CenterListDao {
    public List<Center> getCenterList(int serviceCode);
    public CenterDTO getCenterName(String centerCode);
    public double getEchFee(String centerCode);
}
