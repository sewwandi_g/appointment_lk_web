package lk.appointment.professional.dao;

/**
 * Created by se-7 on 5/16/2017.
 */
public interface ValidateReqServiceDao {
    public boolean getKey(String key);

    public boolean getOTP(String refNo, String otp);
}
