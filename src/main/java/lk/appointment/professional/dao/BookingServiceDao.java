package lk.appointment.professional.dao;

import lk.appointment.professional.domain.Appointment;

/**
 * Created by se-7 on 5/16/2017.
 */
public interface BookingServiceDao {

    public Appointment getTemporyPaymentDetails(String key,String PaymentChannel);

    public boolean isValidResponceHSBC(String referenceNo);

    public Appointment getPaymentDetails(String refNo);

    public boolean doConfirmPayment(String referenceNo);

    public boolean doConfirmPaymentVerification(String referenceNo);

    public Appointment getTemporyAppointmentDetails(String refNo);

    public boolean ConfirmOTPBooking(String referenceNo);

    public Appointment resendOTP(int refNo);

}
