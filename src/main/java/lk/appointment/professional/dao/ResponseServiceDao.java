package lk.appointment.professional.dao;

import lk.appointment.professional.domain.ServiceType;
import lk.appointment.professional.domain.Success;
import lk.appointment.professional.domain.SuccessResponse;
import lk.appointment.professional.dto.City;
import lk.appointment.professional.dto.Professional;
import lk.appointment.professional.dto.ProfessionalCenter;

import java.util.List;
import java.util.Map;
/**
 * Created by se-7 on 5/16/2017.
 */
public interface ResponseServiceDao {

    public List<SuccessResponse> getPaymentData(Success success);
    public List<ServiceType> gettermsAndConditions(int serviceCode);
    public List<Professional> getFillName(int serviceCode,String firstName,String lastName);
    public List<ProfessionalCenter> getFillCenter(int serviceCode,String centerName);
    public List<City> getFillCity(int serviceCode,String cityName);
    public Map<Integer,String> getSpecailitiesOfProfessionals(int serviceCode,int professionalCode);

}
