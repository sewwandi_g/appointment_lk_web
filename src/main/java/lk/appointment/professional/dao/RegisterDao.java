package lk.appointment.professional.dao;

import lk.appointment.professional.dto.SelfRegister;

/**
 * Created by udara on 5/14/2020.
 */
public interface RegisterDao {

    public void registerDb(SelfRegister reg);
    public boolean alreadyExistNicEmail(SelfRegister reg);
}
