package lk.appointment.professional.dao;

import lk.appointment.professional.domain.ProfessionalSpecialization;
import lk.appointment.professional.dto.ProfessionalSpecializationList;

import java.util.List;

/**
 * Created by se-7 on 6/21/2017.
 */
public interface AstroSpecializationDao {

    public List<ProfessionalSpecialization> getSpecList(int serviceCode);

    public List<ProfessionalSpecializationList> getSpecializationAccordingToProfessionals();

}
