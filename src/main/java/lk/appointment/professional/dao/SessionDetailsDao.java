package lk.appointment.professional.dao;

import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.SessionDetails;

/**
 * Created by se-7 on 5/17/2017.
 */
public interface SessionDetailsDao {
    public SessionDetails getSessionDetails(Appointment appointment);
}
