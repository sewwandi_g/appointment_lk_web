package lk.appointment.professional.dao;

import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.SessionDetails;

/**
 * Created by se-7 on 5/15/2017.
 */
public interface MakeAppointTempDao {
    public Appointment insertTempAppointment(Appointment appointment,SessionDetails sessionDetails);
}
