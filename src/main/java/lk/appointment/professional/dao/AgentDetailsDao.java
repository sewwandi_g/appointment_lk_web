package lk.appointment.professional.dao;

import lk.appointment.professional.domain.Agent;

/**
 * Created by Sewwandi on 9/25/2018.
 */

public interface AgentDetailsDao {
    public boolean insertAgentDetails(Agent agent);
    public lk.appointment.professional.dto.Agent getAgentName(String agentCode);
    public String getTele(String refNo);
}
