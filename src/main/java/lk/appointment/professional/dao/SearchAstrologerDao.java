package lk.appointment.professional.dao;

import lk.appointment.professional.domain.*;

import java.util.List;

/**
 * Created by se-7 on 5/15/2017.
 */
public interface SearchAstrologerDao {
    public List<Professional> searchSession(Professional professional);
    public List<Professional> getAstrologerCenterList(Professional professional);
    public List<Professional> getAllAstrologers(int serviceCode);
    public lk.appointment.professional.dto.Professional getAstrologer(int id);
    public SpecialNote getAstroSpecNote(int astroId, int centerId);
    public AllocateAstrologer isCheckAstrologerCenter(int astroId, int centerId);
    public List<ProfessionalSpecialization> getSpeciality(int professionalCode);
    public List<Center> SearchSessionWithCenter(Professional professional);
}
