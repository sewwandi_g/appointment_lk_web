package lk.appointment.professional.dao;

import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.dto.AppointmentDTO1;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by se-8 on 1/5/2018.
 */
public interface AppointmentDetailsDao {
    public AppointmentDTO1 getAppointmentDetail(String refNo) throws SQLException;
    public List<Appointment> searchAppointment(String refNo,String name,String phoneNo);
    public boolean makeCancelAppointment(String refNo);
    public List<Appointment> getAllAppointment(String frmDate , String toDate);
}
