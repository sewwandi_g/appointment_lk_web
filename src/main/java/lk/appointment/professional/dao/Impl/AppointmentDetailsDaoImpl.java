package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.AgentDetailsDao;
import lk.appointment.professional.dao.AppointmentDetailsDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.dto.*;
import lk.appointment.professional.utilities.Utility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static lk.appointment.professional.utilities.DBUtility.getDBConnection;

/**
 * Created by se-8 on 1/5/2018.
 */
@Repository
public class AppointmentDetailsDaoImpl implements AppointmentDetailsDao {

    @Autowired
    private AgentDetailsDao agentDetailsDao;

    private static final Logger LOG = Logger.getLogger(AppointmentDetailsDaoImpl.class);

    @Override
    public AppointmentDTO1 getAppointmentDetail(String refNo) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        AppointmentDTO1 appointment = null;
        CenterDTO center = null;
        CustomerDTO customer = null;
        PriceDTO price = null;
        BirthDetailDTO birthDetail = null;

        String query = "SELECT sa.REF_NO,sa.CUSTOMER_NAME,sa.ROOM_NO,sa.GIVEN_TIME,sa.APPOINTMENT_TYPE,sa.APP_NO,sa.APP_DATE,sa.CHANNEL_FROM, " +
                "sa.STATUS,sa.PAYMENT_TYPE,sa.CENTER_FEE,sa.PROFESSIONAL_FEE,sa.AGENT_FEE,sa.ECH_FEE,sa.EMAIL,sa.NIN,sa.NATIONALITY," +
                "abd.DOB,abd.TIME,abd.COUNTRY,abd.TOWN,sc.CENTER_NAME,sc.ADDRESS1,sc.ADDRESS2,sc.ADDRESS3,sc.CITY,sc.TEL " +
                "FROM service_appointment sa INNER JOIN astro_birth_detail abd ON sa.REF_NO=abd.REF_NO INNER JOIN service_center sc " +
                "ON sa.CENTER_CODE=sc.CENTER_CODE WHERE sa.REF_NO=?";
        /*try {*/
        con = getDBConnection();
        ps = con.prepareStatement(query);
        ps.setString(1, refNo);
        rs = ps.executeQuery();
        if (rs.next()) {
            appointment = new AppointmentDTO1();
            center = new CenterDTO();
            customer = new CustomerDTO();
            birthDetail = new BirthDetailDTO();
            price = new PriceDTO();
            appointment.setRefNo(rs.getString("REF_NO"));
            appointment.setAppNo(rs.getString("APP_NO"));
            appointment.setAppDate(rs.getString("APP_DATE"));
            appointment.setAppointmentType(rs.getString("APPOINTMENT_TYPE"));
            appointment.setChannelFrom(rs.getString("CHANNEL_FROM"));
            if ("1".equals(rs.getString("STATUS"))) {
                appointment.setStatus("Active");
            } else {
                appointment.setStatus("Inactive");
            }
            appointment.setGivenTime(rs.getString("GIVEN_TIME"));
            appointment.setRoomNo(rs.getString("ROOM_NO"));
            appointment.setPaymentType(rs.getString("PAYMENT_TYPE"));

            center.setCenterName(rs.getString("CENTER_NAME"));
            center.setCity(rs.getString("CITY"));
            center.setAddress(rs.getString("ADDRESS1") + " " + rs.getString("ADDRESS2") + " " + rs.getString("ADDRESS3"));
            center.setCenterName(rs.getString("CENTER_NAME"));
            center.setTel(rs.getString("TEL"));

            customer.setCustomerName(rs.getString("CUSTOMER_NAME"));
            customer.setEmail(rs.getString("EMAIL"));
            customer.setNationality(rs.getString("NATIONALITY"));
            customer.setNic(rs.getString("NIN"));

            price.setProfessionalFee(rs.getBigDecimal("PROFESSIONAL_FEE"));
            price.setCenterFee(rs.getBigDecimal("CENTER_FEE"));
            price.setEchFee(rs.getBigDecimal("ECH_FEE"));
            price.setAgentFee(rs.getBigDecimal("AGENT_FEE"));

            birthDetail.setBirthCountry(rs.getString("COUNTRY"));
            birthDetail.setBirthTown(rs.getString("TOWN"));
            birthDetail.setBirthDate(rs.getString("DOB"));
            birthDetail.setBirthTime(rs.getString("TIME"));


            customer.setBirthDetail(birthDetail);
            appointment.setCustomer(customer);
            appointment.setPrice(price);
            appointment.setCenter(center);
        }
        rs.close();
        /*}catch (SQLException ex) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java>>>>>>>>>>>>>>>>>>>>>>"+ex.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java`````````````````"+e.getMessage());
            }*/

      /*  }*/
        return appointment;
    }

    @Override
    public List<Appointment> searchAppointment(String refNo, String name,String phoneNo) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String name1 = null;
        String refNo1=null;
        if (!("".equals(name))) {
            name1 = "%" + name + "%";
        }
        if(!("".equals(refNo))){
            refNo1 = "%"+refNo;
        }
        if(!("".equals(phoneNo))){
            name1 = "%" + phoneNo + "%";
        }
        Appointment appointment = null;
        List<Appointment> appointments = new ArrayList<Appointment>();


        String query = "SELECT sa.REF_NO,sa.CUSTOMER_NAME,sa.ROOM_NO,sa.GIVEN_TIME,sa.SERVICE_GIVEN,sa.APPOINTMENT_TYPE,sa.APP_NO,sa.APP_DATE,sa.CHANNEL_FROM, " +
                "sa.STATUS,sa.PAYMENT_TYPE,sa.CENTER_FEE,sa.PROFESSIONAL_FEE,sa.AGENT_FEE,sa.ECH_FEE,sa.EMAIL,sa.NIN,sa.NATIONALITY,spv.AMOUNT,sp.TITLE,sp.FIRST_NAME,sp.SURNAME,spv.PAYMENTCHANNEL," +
                "sc.CENTER_NAME,sc.ADDRESS1,sc.ADDRESS2,sc.ADDRESS3,sc.CITY,sc.TEL,avp.telephone,avp.txn_date,avp.user_id,sc.SERVICE_CODE " +
                "FROM service_appointment sa INNER JOIN agent_via_phone avp ON avp.referance_no = sa.REF_NO INNER JOIN service_center sc " +
                "ON sa.CENTER_CODE=sc.CENTER_CODE INNER JOIN service_professional sp ON sp.PROFESSIONAL_CODE = sa.PROFESSIONAL_CODE INNER JOIN service_payment_veryfication spv on spv.REFNO=sa.REF_NO WHERE (sa.REF_NO LIKE '" + refNo1 + "' OR sa.CUSTOMER_NAME LIKE '" + name1 + "') " +
                "UNION " +
                "SELECT sa.REF_NO,sa.CUSTOMER_NAME,sa.ROOM_NO,sa.GIVEN_TIME,sa.SERVICE_GIVEN,sa.APPOINTMENT_TYPE,sa.APP_NO,sa.APP_DATE,sa.CHANNEL_FROM, " +
                "sa.STATUS,sa.PAYMENT_TYPE,sa.CENTER_FEE,sa.PROFESSIONAL_FEE,sa.AGENT_FEE,sa.ECH_FEE,sa.EMAIL,sa.NIN,sa.NATIONALITY,spv.AMOUNT,sp.TITLE,sp.FIRST_NAME,sp.SURNAME,spv.PAYMENTCHANNEL," +
                "sc.CENTER_NAME,sc.ADDRESS1,sc.ADDRESS2,sc.ADDRESS3,sc.CITY,sc.TEL,avp.telephone,avp.txn_date,avp.user_id,sc.SERVICE_CODE " +
                "FROM service_appointment_bckup sa INNER JOIN agent_via_phone avp ON avp.referance_no = sa.REF_NO INNER JOIN service_center sc " +
                "ON sa.CENTER_CODE=sc.CENTER_CODE INNER JOIN service_professional sp ON sp.PROFESSIONAL_CODE = sa.PROFESSIONAL_CODE INNER JOIN service_payment_veryfication spv on spv.REFNO=sa.REF_NO WHERE (sa.REF_NO LIKE '" + refNo1 + "' OR sa.CUSTOMER_NAME LIKE '" + name1 + "') " +
                "UNION " +
                "SELECT sa.REF_NO,sa.CUSTOMER_NAME,sa.ROOM_NO,sa.GIVEN_TIME,sa.SERVICE_GIVEN,sa.APPOINTMENT_TYPE,sa.APP_NO," +
                "sa.APP_DATE,sa.CHANNEL_FROM, sa.STATUS," +
                "sa.PAYMENT_TYPE,sa.CENTER_FEE,sa.PROFESSIONAL_FEE,sa.AGENT_FEE,sa.ECH_FEE," +
                "sa.EMAIL,sa.NIN,sa.NATIONALITY,spv.AMOUNT,sp.TITLE,sp.FIRST_NAME,sp.SURNAME," +
                "spv.PAYMENTCHANNEL,sc.CENTER_NAME,sc.ADDRESS1,sc.ADDRESS2,sc.ADDRESS3,sc.CITY," +
                "sc.TEL,'',sa.TRANSACTION_DATE,COALESCE(sa.TXN_USER,'W'),sc.SERVICE_CODE FROM service_appointment sa " +
                "INNER JOIN service_center sc " +
                "ON sa.CENTER_CODE=sc.CENTER_CODE INNER JOIN service_professional sp " +
                "ON sp.PROFESSIONAL_CODE = sa.PROFESSIONAL_CODE INNER JOIN service_payment_veryfication spv on " +
                "spv.REFNO=sa.REF_NO and sa.REF_NO not in(select referance_no from agent_via_phone) WHERE (sa.REF_NO LIKE '" + refNo1 + "' OR sa.CUSTOMER_NAME LIKE '" + name1 + "') " +
                "UNION " +
                "SELECT sa.REF_NO,sa.CUSTOMER_NAME,sa.ROOM_NO,sa.GIVEN_TIME,sa.SERVICE_GIVEN,sa.APPOINTMENT_TYPE,sa.APP_NO," +
                "sa.APP_DATE,sa.CHANNEL_FROM, sa.STATUS," +
                "sa.PAYMENT_TYPE,sa.CENTER_FEE,sa.PROFESSIONAL_FEE,sa.AGENT_FEE,sa.ECH_FEE," +
                "sa.EMAIL,sa.NIN,sa.NATIONALITY,spv.AMOUNT,sp.TITLE,sp.FIRST_NAME,sp.SURNAME," +
                "spv.PAYMENTCHANNEL,sc.CENTER_NAME,sc.ADDRESS1,sc.ADDRESS2,sc.ADDRESS3,sc.CITY," +
                "sc.TEL,'',sa.TRANSACTION_DATE,COALESCE(sa.TXN_USER,'W'),sc.SERVICE_CODE FROM service_appointment_bckup sa " +
                "INNER JOIN service_center sc " +
                "ON sa.CENTER_CODE=sc.CENTER_CODE INNER JOIN service_professional sp " +
                "ON sp.PROFESSIONAL_CODE = sa.PROFESSIONAL_CODE INNER JOIN service_payment_veryfication spv on " +
                "spv.REFNO=sa.REF_NO and sa.REF_NO not in(select referance_no from agent_via_phone) WHERE (sa.REF_NO LIKE '" + refNo1 +"' OR sa.CUSTOMER_NAME LIKE '" + name1 + "') "+
                "order by APP_DATE DESC,STATUS ASC";
        System.out.println("###################  searchAppointment  ###########################"+query);
        try {
            con = getDBConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            String date;
            int len = 0;
            while (rs.next()) {
                appointment = new Appointment();
                appointment.setCustomerName(rs.getString("CUSTOMER_NAME"));
                appointment.setRefNo(rs.getString("REF_NO"));
                appointment.setAppNo(rs.getString("APP_NO"));
                appointment.setGivenTime(Utility.convertTimeFormat(rs.getString("GIVEN_TIME")));
                appointment.setAppDate(rs.getString("APP_DATE"));
                appointment.setAppointmentType(rs.getString("APPOINTMENT_TYPE"));
                appointment.setChannelFrom(rs.getString("CHANNEL_FROM"));
                appointment.setCenterName(rs.getString("CENTER_NAME"));
                appointment.setAddress1(rs.getString("ADDRESS1"));
                appointment.setAddress2(rs.getString("ADDRESS2"));
                appointment.setAddress3(rs.getString("ADDRESS3"));
                appointment.setCity(rs.getString("CITY"));
                appointment.setTelNo(rs.getString("TEL"));
                appointment.setAmount(rs.getString("AMOUNT"));
                appointment.setStatus(rs.getString("STATUS"));
                date = rs.getString("txn_date");
                len = date.length();
                appointment.setTransDate(date.substring(0, len - 5));
                appointment.setMobileNo(rs.getString("telephone"));
                appointment.setRemarks(rs.getString("user_id"));
                appointment.setProfessionalName(rs.getString("TITLE") + " " + rs.getString("FIRST_NAME") + " " + rs.getString("SURNAME"));
                appointment.setCenterFee(rs.getString("CENTER_FEE"));
                appointment.setEchFee(rs.getString("ECH_FEE"));
                appointment.setAstroFee(rs.getString("PROFESSIONAL_FEE"));
                appointment.setServiceCode(rs.getString("SERVICE_CODE"));
                appointment.setServiceGiven(rs.getInt("SERVICE_GIVEN"));

                String payType = agentDetailsDao.getAgentName(rs.getString("PAYMENT_TYPE")).getAgentName();

                if (payType != null) {
                    appointment.setPaymentType(payType);
                } else {
                    appointment.setPaymentType(rs.getString("PAYMENT_TYPE"));
                }
                appointments.add(appointment);
            }
            rs.close();
        } catch (SQLException ex) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java>>>>>>>>>>>>>>>>>>>>>>" + ex.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java`````````````````" + e.getMessage());
            }
        }
        return appointments;
    }

    @Override
    public boolean makeCancelAppointment(String refNo) {
        Connection con = null;
        PreparedStatement ps = null;
        boolean isUpdate = false;
        String updateQry = "update service_appointment set STATUS = ? where REF_NO=?";
        try {
            con = getDBConnection();
            ps = con.prepareStatement(updateQry);
            ps.setString(1, "3");
            ps.setString(2, refNo);
            if (Utility.isActiveAppointment(refNo)) {
                int rows = ps.executeUpdate();
                if(rows>0) {
                    isUpdate = true;
                }
            }


        } catch (Exception e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in AppointmentDetailsDaoImpl.java------cancelAppointment>>>>>>>>>>>>>>>>>>>>>>>>>>" + e.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                LOG.error("``````````````````Exception in AppointmentDetailsDaoImpl.java```````````````" + e.getMessage());
            }
        }
        return isUpdate;
    }

    @Override
    public List<Appointment> getAllAppointment(String frmDate , String toDate) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;

        List<Appointment> appointments = new ArrayList<Appointment>();


        String query = "SELECT * FROM ech_astro.service_appointment where APP_DATE BETWEEN '"+frmDate+"'AND '"+toDate+"'; ";

        try {
            con = getDBConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            Appointment appointment =null;
            while (rs.next()) {
                appointment = new Appointment();
                appointment.setCustomerName(rs.getString("CUSTOMER_NAME"));
                appointment.setRefNo(rs.getString("REF_NO"));
                appointment.setAppNo(rs.getString("APP_NO"));
                appointment.setGivenTime(Utility.convertTimeFormat(rs.getString("GIVEN_TIME")));
                appointment.setAppDate(rs.getString("APP_DATE"));
                appointment.setAppointmentType(rs.getString("APPOINTMENT_TYPE"));
                appointment.setChannelFrom(rs.getString("CHANNEL_FROM"));
                appointment.setCenterName(rs.getString("CENTER_NAME"));
                appointment.setAddress1(rs.getString("ADDRESS1"));
                appointment.setAddress2(rs.getString("ADDRESS2"));
                appointment.setAddress3(rs.getString("ADDRESS3"));
                appointment.setCity(rs.getString("CITY"));
                appointment.setTelNo(rs.getString("TEL"));
                appointment.setAmount(rs.getString("AMOUNT"));
                appointment.setStatus(rs.getString("STATUS"));
                appointment.setTransDate( rs.getString("txn_date"));
                appointment.setMobileNo(rs.getString("telephone"));
                appointment.setRemarks(rs.getString("user_id"));
                appointment.setProfessionalName(rs.getString("TITLE") + " " + rs.getString("FIRST_NAME") + " " + rs.getString("SURNAME"));
                appointment.setCenterFee(rs.getString("CENTER_FEE"));
                appointment.setEchFee(rs.getString("ECH_FEE"));
                appointment.setAstroFee(rs.getString("PROFESSIONAL_FEE"));
                appointment.setServiceCode(rs.getString("SERVICE_CODE"));
                appointment.setPaymentType(rs.getString("PAYMENT_TYPE"));

                appointments.add(appointment);
            }
            rs.close();
        } catch (SQLException ex) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java>>>>>>>>>>>>>>>>>>>>>>" + ex.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            } catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java`````````````````" + e.getMessage());
            }
        }
        return appointments;
    }
}
