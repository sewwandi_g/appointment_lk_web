package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.SmsDao;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;

import static lk.appointment.professional.utilities.DBUtility.getDBConnection;

/**
 * Created by se-7 on 5/26/2017.
 */
@Repository
public class SmsDaoImpl implements SmsDao {
    private static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(SmsDaoImpl.class);

    public boolean setSmsDetails (int status,String RefNo) {
        Connection con = null;
        PreparedStatement ps = null;
        boolean isUpdate = false;
        String insertQry = "update service_payment_veryfication set SMS = ? where REFNO =?";
        try {
            con = getDBConnection();
            ps = con.prepareStatement(insertQry);
            ps.setInt(1, status);
            ps.setString(2,RefNo);
            int noOfRows = ps.executeUpdate();
            if (noOfRows == 1){
                isUpdate = true;
            }
        }
        catch (Exception e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in SmsDaoImpl.java>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        return isUpdate;
    }
}
