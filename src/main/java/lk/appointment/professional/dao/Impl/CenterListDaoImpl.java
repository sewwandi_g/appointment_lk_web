package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.CenterListDao;
import lk.appointment.professional.domain.Center;
import lk.appointment.professional.dto.CenterDTO;
import lk.appointment.professional.utilities.DBUtility;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by se-7 on 6/22/2017.
 */
@Repository
public class CenterListDaoImpl implements CenterListDao{

    private static final Logger LOG = Logger.getLogger(CenterListDaoImpl.class);
    @Override
    public List<Center> getCenterList(int serviceCode) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        Center center = null;
        List<Center> centers = new ArrayList<Center>();

        String Qry = "SELECT CENTER_CODE,CENTER_NAME,SHORT_CODE,ADDRESS1,ADDRESS2,ADDRESS3,CITY FROM service_center where STATUS=? and SERVICE_CODE=? ORDER BY CENTER_NAME";
        try {
            con= DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1,"1");
            ps.setInt(2,serviceCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                center = new Center();
                center.setCenterCode(rs.getString("CENTER_CODE"));
                center.setCenterName(rs.getString("CENTER_NAME"));
                center.setShortCode(rs.getString("SHORT_CODE"));
                center.setAddress(rs.getString("ADDRESS1")+" "+rs.getString("ADDRESS2")+" "+rs.getString("ADDRESS3"));
                center.setCity(rs.getString("CITY"));
                centers.add(center);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<EXCEPTION INNNNNNNNNNN CenterListDaoImpl>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception inNNNNNNNNN CenterListDaoImpl.java```````````````"+e.getMessage());
            }
        }
        return centers;
    }

    @Override
    public CenterDTO getCenterName(String centerCode) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        CenterDTO center = null;

        String Qry = "SELECT CENTER_NAME,ADDRESS1,ADDRESS2,ADDRESS3,CITY,TEL FROM service_center where STATUS=? and CENTER_CODE=?";
        try {
            con= DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1,"1");
            ps.setInt(2, Integer.parseInt(centerCode));
            rs = ps.executeQuery();
            while (rs.next()) {
                center = new CenterDTO();
                center.setCenterName(rs.getString("CENTER_NAME"));
                center.setAddress(rs.getString("ADDRESS1")+" "+rs.getString("ADDRESS2")+" "+rs.getString("ADDRESS3"));
                center.setCity(rs.getString("CITY"));
                center.setTel("TEL");
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<EXCEPTION INNNNNNNNNNN CenterListDaoImpl>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception inNNNNNNNNN CenterListDaoImpl.java```````````````"+e.getMessage());
            }
        }

        return center;
    }

    @Override
    public double getEchFee(String centerCode) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        double value = 0.0;

        String Qry = "SELECT DESCRIPTION FROM ech_astro.p_numbers where PARA_NUMBER=?";
        try {
            con= DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1,centerCode);
            rs = ps.executeQuery();
            if (rs.next()) {
              value = Double.parseDouble(rs.getString("DESCRIPTION"));
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<EXCEPTION INNNNNNNNNNN getEchFee>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception inNNNNNNNNN getEchFee.java```````````````"+e.getMessage());
            }
        }

        return value;
    }

}
