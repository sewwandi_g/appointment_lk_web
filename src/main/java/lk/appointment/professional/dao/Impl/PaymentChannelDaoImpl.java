package lk.appointment.professional.dao.Impl;


import lk.appointment.professional.dao.PaymentChannelDao;
import lk.appointment.professional.domain.PaymentGateway;
import lk.appointment.professional.utilities.JsonResponse;
import lk.appointment.professional.utilities.DBUtility;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by se-7 on 5/16/2017.
 */
@Repository
public class PaymentChannelDaoImpl implements PaymentChannelDao {
    final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(PaymentChannelDaoImpl.class);
    private JsonResponse jsonResponse = new JsonResponse();
    public List<PaymentGateway> getPaymentChannel(PaymentGateway paymentGateway) {
        Connection con = null;
        PreparedStatement ps = null;
        PaymentGateway paymentGateway1 = null;
        List<PaymentGateway> paymentGatewayArrayList = new ArrayList<PaymentGateway>();
        ResultSet rs;
        boolean success= false;
        String selctQry = "select CODE, DESCRIPTION, BANK_CODE from service_payment_gateway WHERE STATUS = ?";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selctQry);
            ps.setString(1,"1");
            rs = ps.executeQuery();
            while (rs.next()) {
                paymentGateway1 = new PaymentGateway();
                success = true;
                paymentGateway1.setCode(rs.getString("CODE"));
                paymentGateway1.setDescription(rs.getString("DESCRIPTION"));
                paymentGateway1.setBankCode(rs.getString("BANK_CODE"));

                paymentGatewayArrayList.add(paymentGateway1);
            }
        }
        catch (SQLException ex){
            LOG.error("SQLEXCEPTION in PaymentChannelDaoImpl.java------------------------"+ex.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````"+e.getMessage());
            }
        }
        return paymentGatewayArrayList;
    }
}
