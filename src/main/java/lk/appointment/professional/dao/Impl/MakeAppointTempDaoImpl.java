package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.MakeAppointTempDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.SessionDetails;
import lk.appointment.professional.enumbers.ChargeType;
import lk.appointment.professional.utilities.ChargeUtility;
import lk.appointment.professional.utilities.DBUtility;
import lk.appointment.professional.utilities.Utility;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by se-7 on 5/15/2017.
 */
@Repository
public class MakeAppointTempDaoImpl implements MakeAppointTempDao {
    final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(MakeAppointTempDaoImpl.class);
    @Override
    public Appointment insertTempAppointment(Appointment appointment,SessionDetails sessionDetails) {
        Connection con = null;
        PreparedStatement ps = null;
        boolean success = false;
        String seqQuery="";
        ResultSet rs;
        int seq;
        String refNo="";
        String centerCharge= "0.00";
        String professionalCharge="0.00";
        if ((ChargeType.V.toString()).equals(appointment.getAppointmentType()) || Integer.parseInt(appointment.getServiceCode())==2) {
            centerCharge = sessionDetails.getCenterCharge();
        }

        if (Integer.parseInt(appointment.getServiceCode())==1) {

            if ((ChargeType.F.toString()).equals(appointment.getNationality())) {
                professionalCharge = Double.toString(Double.parseDouble(sessionDetails.getAstroCharge()) * 2);
                centerCharge = Double.toString(Double.parseDouble(centerCharge) * 2);
            } else {
                professionalCharge = sessionDetails.getAstroCharge();
                centerCharge = sessionDetails.getCenterCharge();
            }
        }
        else {
            professionalCharge = sessionDetails.getAstroCharge();
        }

        String echFee = appointment.getEchFee();
        try {
            con = DBUtility.getDBConnection();
            String Qry = "SELECT MAX(REF_NO) FROM service_appointment";
            try {

                ps = con.prepareStatement(Qry);
                rs = ps.executeQuery();
                if (rs.next()) {
                    seq = rs.getInt(1) + 1;
                    if (Integer.toString(seq).length() == 1) {
                        refNo = "0000000" + seq;
                    } else if (Integer.toString(seq).length() == 2) {
                        refNo = "000000" + seq;
                    } else if (Integer.toString(seq).length() == 3) {
                        refNo = "00000" + seq;
                    } else if (Integer.toString(seq).length() == 4) {
                        refNo = "0000" + seq;
                    } else if (Integer.toString(seq).length() == 5) {
                        refNo = "000" + seq;
                    } else if (Integer.toString(seq).length() == 6) {
                        refNo = "00" + seq;
                    } else if (Integer.toString(seq).length() == 7) {
                        refNo = "0" + seq;
                    } else if (Integer.toString(seq).length() == 8) {
                        refNo = "" + seq;
                    }
                } else {
                    refNo = "00000001";
                }
                appointment.setRefNo(refNo);
            } catch (SQLException e) {
                LOG.error("SeqGenerateException in MakeAppointTempDaoImpl.java--------------" + e.toString());
            } finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }

                    /*if (con != null) {
                        con.close();
                    }*/
                } catch (SQLException e) {
                    LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````" + e.getMessage());
                }
            }

            LOG.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<IIIIIIIIIIIINNNNNNNNNNNNNNNNNNNNNFFFFFFFFFFFFFFOOOOOOOOOOOOOOOOOOOOO333333333333333333333333333333333333>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

            String givenTime = "";
            if (!"3".equals(appointment.getServiceCode())) {
                String Query1 = "select PROFESSIONAL_CODE from service_appointment where SESSION_ID =? limit 1/*and status =?*/";
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<Query1 >>>>>>>>>>>>>>>>>>>>>>>>>>>" + Query1 + "<<<<<<<<<<<<<<>>>>>>>>>>>>>" + appointment.getSessionId());
                try {
                    ps = con.prepareStatement(Query1);
                    ps.setString(1, appointment.getSessionId());
                    /*ps.setString(2,"1");*/
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        givenTime = Utility.getGivenTime(appointment.getSessionId());
                    } else {
                        givenTime = sessionDetails.getStartTime();
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error("-----------Exception in Insert Query(EChPay/MakeAppointTwempDao.java)------------" + ex.toString());
                } finally {
                    try {
                        if (ps != null) {
                            ps.close();
                        }

                        /*if (con != null) {
                            con.close();
                        }*/
                    } catch (SQLException e) {
                        LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````" + e.getMessage());
                    }
                }
            } else {
                givenTime = appointment.getGivenTime().replace(":", "").trim();
            }

            /*if(!PaymentType.M.toString().equals(appointment.getChannelFrom())){
                if ("1".equals(appointment.getServiceCode())) {
                    String insertBirthDetail = "INSERT INTO astro_birth_detail(REF_NO, DOB, TIME, COUNTRY, TOWN, NOTE) VALUES (?,?,?,?,?,?)";
                    try {
                        ps = con.prepareStatement(insertBirthDetail);
                        ps.setString(1, appointment.getRefNo());
                        ps.setString(2, appointment.getB_date());
                        ps.setString(3, appointment.getB_time());
                        ps.setString(4, appointment.getB_country());
                        ps.setString(5, appointment.getB_town());
                        ps.setString(6, appointment.getNote());
                        int effectRows = ps.executeUpdate();
                        if (effectRows > 0) {
                            LOG.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SUCCESSFULLY INSERTEDDDDDDDDDDDDDDDDDDDDDDDDD>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        }
                    } catch (SQLException ex) {
                        LOG.error("-----------Exception in Insert Query(EChPay/MakeAppointTwempDao.java->astro_birth_detail)------------" + ex.toString());
                    } finally {
                        try {
                            if (ps != null) {
                                ps.close();
                            }
                            *//*if (con != null) {
                                con.close();
                            }*//*
                        } catch (SQLException e) {
                            LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````" + e.getMessage());
                        }
                    }
                } else if ("2".equals(appointment.getServiceCode())) {
                    String insertPetDetail = "insert into vet_pet_detail(REF_NO,PET_NAME,PET_TYPE,PET_DOB,REG_NO) values (?,?,?,?,?)";

                    try {
                        ps = con.prepareStatement(insertPetDetail);
                        ps.setString(1, appointment.getRefNo());
                        ps.setString(2, appointment.getPet().getPetName());
                        ps.setString(3, appointment.getPet().getPetType());
                        ps.setString(4, appointment.getPet().getDob());
                        ps.setString(5, appointment.getPet().getRegNo());

                        int effectRows = ps.executeUpdate();
                        if (effectRows > 0) {
                            LOG.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Successffulllyy inserted to the vet_pet_detail>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                        }
                    } catch (Exception e) {
                        LOG.error("-----------Exception in Insert Query(EChPay/MakeAppointTwempDao.java)vet_pet_detail------------" + e.toString());
                    } finally {
                        try {
                            if (ps != null) {
                                ps.close();
                            }
                            *//*if (con != null) {
                                con.close();
                            }*//*
                        } catch (SQLException e) {
                            LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````" + e.getMessage());
                        }
                    }
                }
            }*/

            Utility.deleteOlderStatus2Appointment();


            String tempQuery = "insert into service_appointment (CENTER_CODE,PROFESSIONAL_CODE,APP_DATE,APP_NO,REF_NO,CUSTOMER_NAME,ROOM_NO,GIVEN_TIME,SESSION_ID,INS_BRANCHCODE,PAYMENT_TYPE,APPOINTMENT_TYPE,PAYMENT_DETAILS,\n" +
                    "AMOUNT,CENTER_FEE,PROFESSIONAL_FEE,AGENT_FEE,CENTER_VAT,PROFESSIONAL_VAT,SPEC_CODE,NIN,NATIONALITY,STATUS,CHANNEL_FROM,EMAIL,ECH_FEE) " +
                    "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            LOG.info("--------------Insert query EChAstroChannel-MakeAppointTempDaoImpl.java---->>>>> "+tempQuery);

            ps = con.prepareStatement(tempQuery);
            ps.setString(1,sessionDetails.getCenterCode());
            ps.setString(2,sessionDetails.getProfessionalCode());
            ps.setString(3,sessionDetails.getAppDate());
            ps.setInt(4, Utility.getNextAppointNo(appointment.getSessionId()));
            ps.setString(5, appointment.getRefNo());
            ps.setString(6,appointment.getCustomerName() + "-" + appointment.getTelNo());
            ps.setString(7,sessionDetails.getRoomNo());
            ps.setString(8,givenTime);
            ps.setString(9,appointment.getSessionId());
            ps.setString(10,"");
            ps.setString(11,appointment.getPaymentType());
            ps.setString(12,appointment.getAppointmentType());
            ps.setString(13,appointment.getPaymentMode());
            ps.setString(14, ChargeUtility.calculateTotal(sessionDetails.getAstroCharge(), sessionDetails.getCenterCharge().substring(0, sessionDetails.getCenterCharge().length() - 3), echFee,appointment.getAppointmentType(),appointment.getNationality(),appointment.getServiceCode()));
            ps.setString(15,centerCharge);
            ps.setString(16,professionalCharge);
            ps.setString(17,appointment.getAgentFee());
            ps.setString(18,"0.00");
            ps.setString(19,"0.00");
            ps.setString(20,appointment.getSpecId());
            ps.setString(21,appointment.getNic());
            ps.setString(22,appointment.getNationality());
            ps.setString(23,"2");
            ps.setString(24,appointment.getChannelFrom());
            ps.setString(25,appointment.getEmail());
            ps.setString(26,echFee);
            int noOfRows = ps.executeUpdate();
            LOG.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<LLLOOOGGGINSERTAPPOINTMENTRT>>>>>>>>"+noOfRows);
            if (noOfRows == 1) {
                success = true;
                appointment.setGivenTime(Utility.getTimeFormat(givenTime));
                appointment.setAppNo(Integer.toString(Utility.getNextSuccessAppointNo(appointment.getSessionId())));
                appointment.setAmount(ChargeUtility.calculatepaymentTotal(appointment.getServiceCode(), sessionDetails.getAstroCharge(), sessionDetails.getCenterCharge(), appointment.getEchFee(), appointment.getAppointmentType(), appointment.getNationality(),sessionDetails.getCenterCode()));
            }
        }catch (Exception e) {
            Logger.getLogger("server.log").log(Level.WARNING,"Exception in EChAstroChannel.MakeAppointTempDao.java---"+e.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````"+e.getMessage());
            }
        }

        /*if (success) {
            String securityKey = Utility.generateKey(appointment);
            String Query ="insert into service_payment_veryfication (REFNO, AMOUNT, PAYMENT_MODE , STATUS ,PAYMENTCHANNEL,SECURITY_KEY) values(?,?,?,?,?,?) ";

            try{
                con = DBUtility.getDBConnection();
                ps = con.prepareStatement(Query);
                ps.setString(1, appointment.getRefNo());
                ps.setString(2, ChargeUtility.calculatepaymentTotal(appointment.getServiceCode(), sessionDetails.getAstroCharge(), sessionDetails.getCenterCharge(), appointment.getEchFee(), appointment.getAppointmentType(), appointment.getNationality(),sessionDetails.getCenterCode()));
                ps.setString(3, appointment.getPaymentType());
                ps.setString(4, "0");
                ps.setString(5, appointment.getPaymentMode());
                ps.setString(6, securityKey);

                int noOfRows = ps.executeUpdate();

                if (noOfRows>0) {
                    appointment.setSecurityKey(securityKey);
                }
            }catch (Exception e) {
                LOG.error("-----------Exception in Insert Query(EChPay/MakeAppointTwempDao.java)service_payment_veryfication------------" + e.toString());
            }
            finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }

                    if (con != null) {
                        con.close();
                    }
                }
                catch (SQLException e) {
                    LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````"+e.getMessage());
                }
            }
        }
*/
        if(success){
            String securityKey = new Utility().generateOTP(appointment.getTelNo());
            String Query ="insert into OTPGeneration (refNo,mobile_no,OTP,expire_time,status,resend_count) values(?,?,?,?,?,?) ";

            try{
                con = DBUtility.getDBConnection();
                ps = con.prepareStatement(Query);
                ps.setString(1, appointment.getRefNo());
                ps.setString(2, appointment.getTelNo());
                ps.setString(3, securityKey);
                ps.setString(4, "100000");
                ps.setString(5, "1");
                ps.setString(6, "0");

                int noOfRows = ps.executeUpdate();

                if (noOfRows>0) {
                    appointment.setSecurityKey(securityKey);
                }
            }catch (Exception e) {
                LOG.error("-----------Exception in Insert Query(EChPay/MakeAppointTwempDao.java)service_payment_veryfication------------" + e.toString());
            }
            finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }

                    if (con != null) {
                        con.close();
                    }
                }
                catch (SQLException e) {
                    LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````"+e.getMessage());
                }
            }
        }

        return appointment;
    }
}
