package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.ResponseServiceDao;
import lk.appointment.professional.domain.ServiceType;
import lk.appointment.professional.domain.Success;
import lk.appointment.professional.domain.SuccessResponse;
import lk.appointment.professional.dto.City;
import lk.appointment.professional.dto.Professional;
import lk.appointment.professional.dto.ProfessionalCenter;
import lk.appointment.professional.utilities.DBUtility;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by se-7 on 5/16/2017.
 */
@Repository
public class ResponseServiceDaoImpl implements ResponseServiceDao {

    final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(ResponseServiceDaoImpl.class);

    @Override
    public List<SuccessResponse> getPaymentData(Success success) {
        Connection con = null;
        PreparedStatement ps = null;
        SuccessResponse successResponse = new SuccessResponse();
        List<SuccessResponse> successResponses=new ArrayList<SuccessResponse>();
        ResultSet rs;
        String Query = "select amount,payment_mode,status,Txn_Date from astro_payment_veryfication where refNo = ?";
        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Query);
            ps.setString(1,success.getRefNo());
            rs = ps.executeQuery();
            if (rs.next()) {
                successResponse.setAmount(rs.getString("amount"));
                successResponse.setPayment_mode(rs.getString("payment_mode"));
                successResponse.setStatus(rs.getString("status"));
                successResponse.setTxn_Date(rs.getString("Txn_Date"));
                successResponses.add(successResponse);
            }
            rs.close();
        }
        catch(SQLException ex) {
            LOG.error("---------SQLException in EXXCCEEPPTTIIOONNNNNNNNNNNNN/ResponseServiceDaoIMPl.java/getPaymentData--------------"+ex.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EXXCCEEPPTTIIOONNNNNNNNNNNNN/ResponseServiceDaoIMPl.java```````````````"+e.getMessage());
            }
        }
        return successResponses;
    }


    @Override
    public List<ServiceType> gettermsAndConditions(int serviceCode) {
        Connection con = null;
        PreparedStatement ps = null;
        ServiceType serviceType= null;
        List<ServiceType>  serviceTypes = new ArrayList<ServiceType>();
        ResultSet rs;
        String Query = "select SERVICE_CODE,SERVICE_NAME,DESCRIPTION,STATUS,TERMS_AND_CONDITIONS from service_type where SERVICE_CODE = ?";
        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Query);
            ps.setInt(1, serviceCode);
            rs = ps.executeQuery();
            if (rs.next()) {
                serviceType = new ServiceType();
                serviceType.setTermsAndConditions(rs.getString("TERMS_AND_CONDITIONS"));
                serviceType.setServiceName(rs.getString("SERVICE_NAME"));
                serviceType.setServiceType(rs.getString("SERVICE_CODE"));
                serviceTypes.add(serviceType);
            }
            rs.close();
        }
        catch(SQLException ex) {
            LOG.error("---------SQLException in EXXCCEEPPTTIIOONNNNNNNNNNNNN/ResponseServiceDaoIMPl.java/gettermsAndConditions--------------"+ex.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EXXCCEEPPTTIIOONNNNNNNNNNNNN/ResponseServiceDaoIMPl.java```````````````"+e.getMessage());
            }
        }
        return serviceTypes;
    }

    @Override
    public List<Professional> getFillName(int serviceCode,String firstName,String lastName) {
        Connection con = null;
        PreparedStatement ps = null;
        Professional professional=null;
        List<Professional> professionalList= new ArrayList<Professional>();
        ResultSet rs;


        /*String Qry = "select  DISTINCT(FIRST_NAME),CONCAT_WS(' ', service_professional.FIRST_NAME, service_professional.SURNAME) AS NAME from service_professional INNER JOIN service_professional_center,service_center where service_professional.STATUS=? AND service_professional.PROFESSIONAL_CODE=service_professional_center.PROFESSIONAL_CODE AND service_professional_center.CENTER_CODE = service_center.CENTER_CODE AND service_center.SERVICE_CODE=? AND service_professional.FIRST_NAME LIKE '%"+firstName+"%' OR service_professional.SURNAME LIKE '%"+lastName+"%' ORDER BY FIRST_NAME";*/

        String Qry = "select  DISTINCT(FIRST_NAME),CONCAT_WS(' ', service_professional.FIRST_NAME, service_professional.SURNAME) AS NAME  from service_professional INNER JOIN service_professional_center ON service_professional.PROFESSIONAL_CODE=service_professional_center.PROFESSIONAL_CODE AND  service_professional.STATUS=? INNER JOIN service_center ON service_professional_center.CENTER_CODE = service_center.CENTER_CODE AND service_center.SERVICE_CODE=? where service_professional.FIRST_NAME LIKE '%"+firstName+"%' OR service_professional.SURNAME LIKE '%"+lastName+"%' ORDER BY FIRST_NAME";

        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setInt(1, 1);
            ps.setInt(2, serviceCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                professional =  new Professional();
                professional.setFirstName(rs.getString("NAME"));
                professionalList.add(professional);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------SQLException in EXXCCEEPPTTIIOONNNNNNNNNNNNN ResponseServiceDaoIMPl.java/getFillName--------------"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EXXCCEEPPTTIIOONNNNNNNNNNNNN```````````````"+e.getMessage());
            }
        }
        return professionalList;
    }

    @Override
    public List<ProfessionalCenter> getFillCenter(int serviceCode, String centerName) {
        Connection con = null;
        PreparedStatement ps = null;
        ProfessionalCenter professionalCenter=null;
        List<ProfessionalCenter> professionalCenterList = new ArrayList<ProfessionalCenter>();
        ResultSet rs;
        String Qry = "SELECT CENTER_NAME FROM service_center WHERE SERVICE_CODE='"+serviceCode+"' AND STATUS=? AND CENTER_NAME LIKE '%"+centerName+"%'";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setInt(1, 1);
            rs = ps.executeQuery();
            while (rs.next()) {
                professionalCenter = new ProfessionalCenter();
                professionalCenter.setCenterName(rs.getString("CENTER_NAME"));
                professionalCenterList.add(professionalCenter);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------EXXCCEEPPTTIIOONNNNNNNNNNNNN--------------"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````EXXCCEEPPTTIIOONNEXXCCEEPPTTIIOONNNNNNNNNNNNN```````````````"+e.getMessage());
            }
        }
        return professionalCenterList;
    }


    @Override
    public List<City> getFillCity(int serviceCode, String cityName) {
        Connection con = null;
        PreparedStatement ps = null;
        City city = null;
        List<City> cityList = new ArrayList<City>();
        ResultSet rs;
        String Qry = "SELECT CITY FROM service_center WHERE SERVICE_CODE=? AND STATUS=? AND CITY LIKE '%"+cityName+"%'";

        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setInt(1, serviceCode);
            ps.setInt(2, 1);
            rs = ps.executeQuery();
            while (rs.next()) {
                city = new City();
                city.setCity(rs.getString("CITY"));
                cityList.add(city);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------EXXCCEEPPTTIIOONNNNNNNNNNNNN--------------"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````EXXCCEEPPTTIIOONNEXXCCEEPPTTIIOONNNNNNNNNNNNN```````````````"+e.getMessage());
            }
        }
        return cityList;
    }
    @Override
    public Map<Integer, String> getSpecailitiesOfProfessionals(int serviceCode,int professionalCode) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        Map<Integer, String> specList = new LinkedHashMap<>();
        String query = "select SPEC_CODE,DESCRIPTION from service_professional_spec inner join service_specialization on " +
                " SPEC_CODE=PROFESSIONAL_SPEC_CODE and SERVICE_CODE=? AND STATUS=? AND PROFESSIONAL_CODE=?";

        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(query);
            ps.setInt(1,serviceCode);
            ps.setInt(2,1);
            ps.setInt(3,professionalCode);
            rs=ps.executeQuery();
            while (rs.next()) {
                specList.put(rs.getInt("SPEC_CODE"),rs.getString("DESCRIPTION"));
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------SQLLL_EXXCCEEPPTTIIOONNNNNNNNNNNNN @ RESPONSESERVICEdAO--KAVINDUU------------"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````EXXCCEEPPTTIIOONNEXXCCEEPPTTIIOONNNNNNNNNNNNN IN CONNECTION CLOSE RESPONSESERVICEDAO - KAVINDU ```````````````"+e.getMessage());
            }
        }
        return specList;
    }
}
