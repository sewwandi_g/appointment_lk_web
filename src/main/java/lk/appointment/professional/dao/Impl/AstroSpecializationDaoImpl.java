package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.AstroSpecializationDao;
import lk.appointment.professional.domain.ProfessionalSpecialization;
import lk.appointment.professional.dto.ProfessionalSpecializationList;
import lk.appointment.professional.dto.Specialization;
import lk.appointment.professional.utilities.DBUtility;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by se-7 on 6/21/2017.
 */
@Repository
public class AstroSpecializationDaoImpl implements AstroSpecializationDao {

    private Logger LOG = Logger.getLogger(AstroSpecializationDaoImpl.class);
    @Override
    public List<ProfessionalSpecialization> getSpecList(int serviceCode) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ProfessionalSpecialization professionalSpecialization = null;
        List<ProfessionalSpecialization> astroSpecList = new ArrayList<ProfessionalSpecialization>();
        String selectQry = "SELECT PROFESSIONAL_SPEC_CODE,DESCRIPTION FROM service_specialization WHERE STATUS=? and SERVICE_CODE=? ORDER BY DESCRIPTION";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selectQry);
            ps.setString(1,"1");
            ps.setInt(2, serviceCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                professionalSpecialization = new ProfessionalSpecialization();
                professionalSpecialization.setSpecCode(rs.getString("PROFESSIONAL_SPEC_CODE"));
                professionalSpecialization.setDescription(rs.getString("DESCRIPTION"));

                astroSpecList.add(professionalSpecialization);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<???????????????????????--Exception in AstroSpecializationDaoImpl>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try{
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            }
            catch (SQLException x) {
                LOG.error("<<<<<<<<<<<<<<<<<<<<<<???????????????????????--Exception in AstroSpecializationDaoImpl>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+x.toString());
            }
        }
        return astroSpecList;
    }

    @Override
    public List<ProfessionalSpecializationList> getSpecializationAccordingToProfessionals() {
        List<ProfessionalSpecializationList> professionalSpecializationLists = new ArrayList<ProfessionalSpecializationList>();
        ProfessionalSpecializationList professionalSpecializationList=null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String professionalTempId="";
        String query = "select PROFESSIONAL_CODE,SPEC_CODE from service_professional_spec ORDER BY service_professional_spec.PROFESSIONAL_CODE ASC";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                professionalSpecializationList = new ProfessionalSpecializationList();
                String professionalId = rs.getString("PROFESSIONAL_CODE");
                /*professionalSpecializationList.setSpecializationList();*/
                professionalSpecializationList.setProfessionalId(professionalId);
                professionalSpecializationList.setSpecializationId(rs.getString("SPEC_CODE"));

                professionalSpecializationLists.add(professionalSpecializationList);

            }



        }
        catch (SQLException e) {

        }
        finally {
            try{
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
            }
            catch (SQLException x) {
                LOG.error("<<<<<<<<<<<<<<<<<<<<<<???????????????????????--Exception in AstroSpecializationDaoImpl>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+x.toString());
            }
        }
        return professionalSpecializationLists;
    }
}
