package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.AgentDetailsDao;
import lk.appointment.professional.domain.Agent;
import lk.appointment.professional.utilities.DBUtility;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Sewwandi on 9/25/2018.
 */
@Repository
public class AgentDetailsDaoImpl implements AgentDetailsDao {
    final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(AgentDetailsDaoImpl.class);
    @Override
    public boolean insertAgentDetails(Agent agent) {
        Connection con = null;
        boolean success = false;
        PreparedStatement ps = null;
        String insertQuery = "insert into agent_via_phone (referance_no,agent_code,payment_status,user_id,txn_type,telephone,totalAmt,Agent_Fee)values (?,?,?,?,?,?,?,?)";
        LOG.info("--------------Insert query EChAstroChannel-insertAgentDetails---->>>>> "+insertQuery);

        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(insertQuery);
            ps.setString(1,agent.getReferance_no());
            ps.setString(2,agent.getAgent_code());
            ps.setString(3, "1");
            ps.setString(4,agent.getUser_id());
            ps.setString(5, agent.getTxn_type());
            ps.setInt(6,agent.getTelephone());
            ps.setDouble(7, agent.getTotAmt());
            ps.setDouble(8,agent.getAgentFee());


            int noOfRows = ps.executeUpdate();
            LOG.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<LLLOOOGGGINSERTAgent>>>>>>>>"+noOfRows);
            if (noOfRows == 1) {
                success = true;
            }
        }catch (Exception e) {
            Logger.getLogger("server.log").log(Level.WARNING,"Exception in EChAstroChannel.AgentDetailsDao.java---"+e.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EChPay/MakeAppointTwempDao.java```````````````"+e.getMessage());
            }
        }
            return success;
        }

    @Override
    public lk.appointment.professional.dto.Agent getAgentName(String agentCode) {
        ResultSet rs;
        Connection con=null;
        PreparedStatement ps = null;
        lk.appointment.professional.dto.Agent agent = new lk.appointment.professional.dto.Agent();

        String Qry = "SELECT agent_name,Fee FROM agents WHERE agent_code=? AND status=?";
        try {

            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1, agentCode);
            ps.setInt(2,1);
            rs = ps.executeQuery();
            if (rs.next()) {
                agent.setAgentName(rs.getString("agent_name"));
                agent.setFee(rs.getDouble("Fee"));

            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel.AgentDetailsDaoImpl.getAgentName>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("`````````<<<<<>>><><><<><>>>`````````Exception in EChAstroChannel```<><<<<<<<<>>>>>>>>>>````````````"+e.getMessage());
            }
        }
        return agent;
    }

    @Override
    public String getTele(String refNo){

        ResultSet rs;
        Connection con=null;
        PreparedStatement ps = null;
        String telephone = null;

        String Qry = "SELECT telephone FROM agent_via_phone WHERE referance_no=? AND payment_status=?";
        try {

            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1, refNo);
            ps.setString(2, "1");
            rs = ps.executeQuery();
            if (rs.next()) {
                telephone = rs.getString("telephone");
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel.AgentDetailsDaoImpl.getAgentName>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("`````````<<<<<>>><><><<><>>>`````````Exception in EChAstroChannel```<><<<<<<<<>>>>>>>>>>````````````"+e.getMessage());
            }
        }
        return telephone;
    }
}
