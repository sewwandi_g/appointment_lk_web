package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.RegisterDao;
import lk.appointment.professional.dto.SelfRegister;
import lk.appointment.professional.utilities.DBUtility;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by udara on 5/14/2020.
 */
@Repository
public class RegisterDaoImpl implements RegisterDao {
    final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(PaymentChannelDaoImpl.class);


    @Override
    public void registerDb(SelfRegister reg) {
        Connection con = null;
        PreparedStatement ps = null;
        boolean success = false;
        String seqQuery="";
        ResultSet rs;
        int seq;

        try{
            con = DBUtility.getDBConnection();

            seqQuery="INSERT INTO self_reg (full_name,email,service_category,professional_qualification,sub,nic,address,contact_no,Txn_date)\n" +
                    "VALUES (?,?,?,?,?,?,?,?,?);";
            ps = con.prepareStatement(seqQuery);
            ps.setString(1,reg.getName());
            ps.setString(2,reg.getEmail());
            ps.setString(3,reg.getCategory());
            ps.setString(4, reg.getQualification());
            ps.setString(5, reg.getSub());
            ps.setString(6,reg.getNic());
            ps.setString(7,reg.getAddr());
            ps.setString(8,reg.getContact());
            ps.setString(9, String.valueOf(System.currentTimeMillis()));

            int noOfRows = ps.executeUpdate();

            if(noOfRows==1)
            {
                System.out.println("success");
//                return true;
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {

            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
               e.printStackTrace();
            }

        }
//        return false;
    }

    @Override
    public boolean alreadyExistNicEmail(SelfRegister reg) {
            Connection con = null;
            PreparedStatement ps = null;

            ResultSet rs;
            boolean success= false;
            String selctQry = "SELECT * FROM ech_astro.self_reg where email=? or nic=?";
            try {
                con = DBUtility.getDBConnection();
                ps = con.prepareStatement(selctQry);
                ps.setString(1,reg.getEmail());
                ps.setString(2,reg.getNic());
                rs = ps.executeQuery();
                if (rs.next()) {
                    return true;
                }

            }
            catch (SQLException ex){
                LOG.error("SQLEXCEPTION in Register------------------------"+ex.toString());
            }
            finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }

                    if (con != null) {
                        con.close();
                    }
                }
                catch (SQLException e) {
                    LOG.error("``````````````````Exception in Registration```````````````"+e.getMessage());
                }
            }
        return false;
        }


}
