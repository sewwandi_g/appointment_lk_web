package lk.appointment.professional.dao.Impl;


import lk.appointment.professional.dao.SearchAstrologerDao;
import lk.appointment.professional.domain.*;
import lk.appointment.professional.utilities.Utility;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static lk.appointment.professional.utilities.DBUtility.getDBConnection;
import static lk.appointment.professional.utilities.Utility.convertTimeFormat;
/**
 * Created by se-7 on 5/15/2017.
 */
@Repository
public class SearchAstrologerDaoImpl implements SearchAstrologerDao {
    private static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(SearchAstrologerDaoImpl.class);
    @Override
    public List<Professional> searchSession(Professional professional) {

        Connection con = null;
        PreparedStatement ps = null;

        List<Professional> professionals = new ArrayList<Professional>();
        Professional astro;
        ResultSet rs;
        String astrologerSurName;
        String astrologerFirstName;
        String date;
        String centerCode = "";
        String astrologerCode = "";
        String SearchQry  = "";
        String queryPart="";
        String sortQry = "";

        String getToday = Utility.getToday();
        int i = 0;
        String qry1="";
        String qry2="";
        String qry3="";
        String qry4="";
        String qry5="";
        String qry6="";
        String and = "AND";
        String dateQuery = "service_session.APP_DATE>='"+getToday+"' AND";

        if (!"".equals(professional.getSpecCode()) && professional.getSpecCode() != null) {
            qry1 = "service_session_spec.SPEC_CODE='"+ professional.getSpecCode()+"'";
            queryPart = qry1;
            i++;
        }
        if (!"".equals(professional.getProfessionalName()) && professional.getProfessionalName() != null) {
            if (professional.getSurName()!=null) {
                qry2 = "service_professional.FIRST_NAME LIKE '%"+ professional.getProfessionalName()+"%' AND service_professional.SURNAME LIKE '%"+ professional.getSurName()+"%'";
            }
            else {
                qry2 = "service_professional.FIRST_NAME LIKE '%"+ professional.getProfessionalName()+"%'";
            }
            if (i!=0) {
                queryPart = queryPart+" "+and+" "+qry2;
            }
            else {
                queryPart = qry2;
            }
            i++;
        }
        if (!"".equals(professional.getDate()) && professional.getDate() != null) {
            qry3 = "service_session.APP_DATE='"+ professional.getDate()+"'";
            if (i!=0) {
                queryPart = queryPart+" "+and+" "+qry3;
            }
            else {
                queryPart = qry3;
            }
            i++;
        }
        if (!"".equals(professional.getCenterName()) && professional.getCenterName() != null) {
            qry4 = "service_center.CENTER_NAME LIKE '%"+ professional.getCenterName()+"%'";
            if (i!=0) {
                queryPart = queryPart+" "+and+" "+qry4;
            }
            else {
                queryPart = qry4;
            }
            i++;
        }
        if (!"".equals(professional.getServiceCode()) && professional.getServiceCode() != null) {
            qry5 = "service_center.SERVICE_CODE='"+ professional.getServiceCode()+"'";
            if (i!=0) {
                queryPart = queryPart+" "+and+" "+qry5;
            }
            else {
                queryPart = qry5;
            }
            i++;
        }

        if ("2".equals(professional.getServiceCode())){
            if (!"".equals(professional.getLocation()) && professional.getLocation() !=null) {
                qry6 = "service_center.CITY like '%"+professional.getLocation()+"%'";
                if (i!=0) {
                    queryPart = queryPart+" "+and+" "+qry6;
                }
                else {
                    queryPart = qry6;
                }
                i++;
            }
        }

        if (!"".equals(professional.getDate()) && professional.getDate() != null) {
            sortQry = "ORDER BY service_session.APP_DATE,service_session.START_TIME";
        }
        else if (!"".equals(professional.getCenterName()) && professional.getCenterName() != null) {
            sortQry = "ORDER BY service_center.CENTER_NAME";
        }
        else {
            sortQry = "ORDER BY service_session.APP_DATE,service_session.START_TIME,service_professional.FIRST_NAME";
        }




        SearchQry = "SELECT service_professional.PROFESSIONAL_CODE,service_professional.TITLE,service_professional.FIRST_NAME,service_professional.SURNAME,service_professional.MOBILENUMBER,service_session.SESSION_ID,\n" +
                "service_session.ROOM_NO,service_session.CENTER_CODE,service_session.APP_DATE,service_session.START_TIME,service_session_spec.SPEC_CODE,\n" +
                "service_session.CENTER_CHARGE,service_session.FINISH_TIME,service_center.CENTER_NAME,service_center.CITY,service_center.ADDRESS1,service_session.WEB_CLIENTS,service_session.SERVICE_VAT,\n" +
                "service_session.NO_OF_CLIENTS,service_session.DAY,service_session_spec.ASTRO_CHARGE,service_professional.QUALIFICATION,service_professional.SPECIAL_NOTE,service_specialization.DESCRIPTION FROM service_professional INNER JOIN service_session on service_professional.PROFESSIONAL_CODE = service_session.PROFESSIONAL_CODE \n" +
                "INNER JOIN service_center ON service_center.CENTER_CODE= service_session.CENTER_CODE INNER JOIN service_session_spec ON service_session.SESSION_ID=service_session_spec.SESSION_ID INNER JOIN service_specialization ON service_session_spec.SPEC_CODE=service_specialization.PROFESSIONAL_SPEC_CODE WHERE "+dateQuery+" "+queryPart +" "+sortQry+"";

        LOG.info(SearchQry);

        try{
            con = getDBConnection();

            ps = con.prepareStatement(SearchQry);

            rs = ps.executeQuery();

            while (rs.next()) {
                astro = new Professional();
                astro.setProfessionalCode(rs.getString("PROFESSIONAL_CODE"));
                astro.setTitle(rs.getString("TITLE"));
                astro.setFirstName(rs.getString("TITLE") + " " + rs.getString("FIRST_NAME") + " " + rs.getString("SURNAME"));
                astro.setSurName(rs.getString("SURNAME"));
                astro.setMobNo(rs.getString("MOBILENUMBER"));
                astro.setFinishTime(Utility.getTimeFormat(rs.getString("FINISH_TIME")));
                astro.setDate(rs.getString("day"));
                astro.setCenterCode(rs.getString("CENTER_CODE"));
                astro.setSpecCode(rs.getString("SPEC_CODE"));
                String appDate = rs.getString("APP_DATE");
                astro.setAppDate(appDate);
                astro.setMonth(Utility.getmonth(appDate));
                astro.setStarttime(Utility.getTimeFormat(rs.getString("START_TIME")));
                astro.setCenterCharge(rs.getString("CENTER_CHARGE"));
                astro.setProfessionVat(rs.getString("SERVICE_VAT"));
                astro.setNoOfClients(rs.getString("NO_OF_CLIENTS"));
                astro.setRoomNo(rs.getString("ROOM_NO"));
                astro.setWebClient(rs.getString("WEB_CLIENTS"));
                astro.setCenterName(rs.getString("CENTER_NAME"));
                astro.setAddress(rs.getString("ADDRESS1"));
                astro.setLocation(rs.getString("CITY"));
                astro.setProfessionCharge(rs.getString("ASTRO_CHARGE"));
                astro.setSessionId(String.valueOf(rs.getInt("SESSION_ID")));
                astro.setQualification(rs.getString("QUALIFICATION"));
                astro.setSpecialNote(rs.getString("SPECIAL_NOTE"));
                astro.setSpeciality(rs.getString("DESCRIPTION"));
                astro.setServiceCode(professional.getServiceCode());
                astro.setAppNo(Integer.toString(Utility.getNextSuccessAppointNo(Integer.toString(rs.getInt("SESSION_ID")))));
                if ("AVAILABLE".equals(Utility.getWebAvailability(Integer.toString(rs.getInt("SESSION_ID")))) && (Utility.compareDate(Utility.getToday(),astro.getAppDate()))){
                    if (Utility.checkSameDate(Utility.getToday(), appDate)) {
                        if ((Utility.compareTime(Utility.getTimeFormat(rs.getString("FINISH_TIME")),Utility.getCurrentTime()))){
                            astro.setAvailability("AVAILABLE");
                        }
                    }
                    else {
                        astro.setAvailability("AVAILABLE");
                    }
                }else{
                    astro.setAvailability(Utility.getWebAvailability(Integer.toString(rs.getInt("SESSION_ID"))));
                }
                professionals.add(astro);
            }
            rs.close();
        }
        catch (SQLException ex) {
            LOG.error("``````````````````Exception in EChAstroChannel```````````````"+ex.getMessage());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EChAstroChannel```````````````"+e.getMessage());
            }
        }
        return professionals;
    }
    @Override
    public List<Center> SearchSessionWithCenter(Professional professional) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs=null;
        int centerCode=0;
        int professionalCode =0;

        String SearchQry  = "";
        String queryPart="";
        String sortQry = "";

        String getToday = Utility.getToday();
        int i = 0;
        String qry1="";
        String qry2="";
        String qry3="";
        String qry4="";
        String qry5="";
        String qry6="";
        String and = "AND";
        String dateQuery = "service_session.APP_DATE>='"+getToday+"' AND";

        if (!"".equals(professional.getSpecCode()) && professional.getSpecCode() != null) {
            qry1 = "service_session_spec.SPEC_CODE='"+ professional.getSpecCode()+"'";
            queryPart = qry1;
            i++;
        }
        if (!"".equals(professional.getProfessionalName()) && professional.getProfessionalName() != null) {
            if (professional.getSurName()!=null) {
                qry2 = "service_professional.FIRST_NAME LIKE '%"+ professional.getProfessionalName()+"%' AND service_professional.SURNAME LIKE '%"+ professional.getSurName()+"%'";
            }
            else {
                qry2 = "service_professional.FIRST_NAME LIKE '%"+ professional.getProfessionalName()+"%'";
            }
            if (i!=0) {
                queryPart = queryPart+" "+and+" "+qry2;
            }
            else {
                queryPart = qry2;
            }
            i++;
        }
        if (!"".equals(professional.getDate()) && professional.getDate() != null) {
            qry3 = "service_session.APP_DATE='"+ professional.getDate()+"'";
            if (i!=0) {
                queryPart = queryPart+" "+and+" "+qry3;
            }
            else {
                queryPart = qry3;
            }
            i++;
        }
        if (!"".equals(professional.getCenterName()) && professional.getCenterName() != null) {
            qry4 = "service_center.CENTER_NAME LIKE '%"+ professional.getCenterName()+"%'";
            if (i!=0) {
                queryPart = queryPart+" "+and+" "+qry4;
            }
            else {
                queryPart = qry4;
            }
            i++;
        }
        if (!"".equals(professional.getServiceCode()) && professional.getServiceCode() != null) {
            qry5 = "service_center.SERVICE_CODE='"+ professional.getServiceCode()+"'";
            if (i!=0) {
                queryPart = queryPart+" "+and+" "+qry5;
            }
            else {
                queryPart = qry5;
            }
            i++;
        }

        if ("2".equals(professional.getServiceCode())){
            if (!"".equals(professional.getLocation()) && professional.getLocation() !=null) {
                qry6 = "service_center.CITY like '%"+professional.getLocation()+"%'";
                if (i!=0) {
                    queryPart = queryPart+" "+and+" "+qry6;
                }
                else {
                    queryPart = qry6;
                }
                i++;
            }
        }

        sortQry = "ORDER BY service_center.center_code,service_professional.PROFESSIONAL_CODE,service_session.APP_DATE";

        List<Center> centerList = new LinkedList<>();
        List<Professional> professionals=null;
        List<Session> sessions =null;
        Center center=null;
        Professional professional1=null;
        Session session;

        String searchQuery = "SELECT service_professional.PROFESSIONAL_CODE,service_professional.TITLE,service_professional.FIRST_NAME,service_professional.SURNAME,service_professional.MOBILENUMBER,service_session.SESSION_ID,\n" +
                "service_session.ROOM_NO,service_session.CENTER_CODE,service_session.APP_DATE,service_session.START_TIME,service_session_spec.SPEC_CODE,\n" +
                "service_session.CENTER_CHARGE,service_session.FINISH_TIME,service_session.TIME_INTERVAL,service_center.CENTER_NAME,service_center.CITY,service_center.ADDRESS1,service_center.ADDRESS2,service_center.ADDRESS3,service_center.TEL,service_session.WEB_CLIENTS,service_session.SERVICE_VAT,\n" +
                "service_session.NO_OF_CLIENTS,service_session.DAY,service_session_spec.ASTRO_CHARGE,service_professional.QUALIFICATION,service_professional.SPECIAL_NOTE,service_specialization.DESCRIPTION FROM service_professional INNER JOIN service_session on service_professional.PROFESSIONAL_CODE = service_session.PROFESSIONAL_CODE \n" +
                "INNER JOIN service_center ON service_center.CENTER_CODE= service_session.CENTER_CODE INNER JOIN service_session_spec ON service_session.SESSION_ID=service_session_spec.SESSION_ID INNER JOIN service_specialization ON service_session_spec.SPEC_CODE=service_specialization.PROFESSIONAL_SPEC_CODE WHERE "+dateQuery+" "+queryPart +" "+sortQry+"";

        LOG.info(searchQuery);

        try {
            con = getDBConnection();
            ps = con.prepareStatement(searchQuery);
            rs = ps.executeQuery();

            while (rs.next()) {
                if (centerCode == 0 || centerCode != rs.getInt("CENTER_CODE")) {

                    if (center != null) {
                        professional1.setSessions(sessions);
                        center.setProfessionals(professionals);
                        centerList.add(center);
                    }
                    professionals=new ArrayList<>();
                    center = new Center();
                    centerCode = rs.getInt("CENTER_CODE");
                    center.setCenterCode(Integer.toString(centerCode));
                    center.setCenterName(rs.getString("CENTER_NAME"));
                    center.setCity(rs.getString("CITY"));
                    center.setAddress(rs.getString("ADDRESS1")+" "+rs.getString("ADDRESS2")+ " " + rs.getString("ADDRESS3"));
                    center.setContactNo(rs.getString("TEL"));
                }
                if (professionalCode==0 || professionalCode != rs.getInt("PROFESSIONAL_CODE")) {

                    if (professional1 != null) {
                        professional1.setSessions(sessions);
                        center.setProfessionals(professionals);
                    }
                    /*professionals=new ArrayList<>();*/
                    professional1 = new Professional();
                    professionalCode = rs.getInt("PROFESSIONAL_CODE");
                    professional1.setProfessionalCode(Integer.toString(professionalCode));
                    professional1.setTitle(rs.getString("TITLE"));
                    professional1.setFirstName(rs.getString("FIRST_NAME"));
                    professional1.setSurName(rs.getString("SURNAME"));
                    professional1.setMobNo(rs.getString("MOBILENUMBER"));
                    professional1.setQualification(rs.getString("QUALIFICATION"));
                    professional1.setSpecialNote(rs.getString("SPECIAL_NOTE"));
                    professionals.add(professional1);
                    sessions =new ArrayList<>();
                }

                session = new lk.appointment.professional.domain.Session();
                session.setSessionId(rs.getString("SESSION_ID"));
                session.setSpeciality(rs.getString("DESCRIPTION"));
                session.setStartTime(convertTimeFormat(rs.getString("START_TIME")));
                session.setAppointmentDate(rs.getString("APP_DATE"));
                session.setFinishTime(convertTimeFormat(rs.getString("FINISH_TIME")));
                session.setGivenTime(Utility.convertTimeFormat(Utility.getGivenTime(rs.getString("SESSION_ID"))));
                if ("AVAILABLE".equals(Utility.getWebAvailability(Integer.toString(rs.getInt("SESSION_ID")))) && (Utility.compareDate(Utility.getToday(), session.getAppointmentDate()))){
                    if (Utility.checkSameDate(Utility.getToday(), session.getAppointmentDate())) {
                        if ((Utility.compareTime(Utility.getTimeFormat(rs.getString("FINISH_TIME")),Utility.getCurrentTime()))){
                            session.setAvailability("AVAILABLE");
                        }else{
                            session.setAvailability("CLOSED");
                        }
                    }
                    else {
                        session.setAvailability("AVAILABLE");
                    }
                }else{
                    //web quata full
                    session.setAvailability(Utility.getWebAvailability(Integer.toString(rs.getInt("SESSION_ID"))));
                }
                session.setAppointmentNo(Integer.toString(Utility.getNextSuccessAppointNo(Integer.toString(rs.getInt("SESSION_ID")))));
                session.setNoOfClients(rs.getString("NO_OF_CLIENTS"));
                session.setWebClientsNo(rs.getString("WEB_CLIENTS"));
                session.setMonth(Utility.getmonth(rs.getString("APP_DATE")));
                session.setSpecializationId(rs.getString("SPEC_CODE"));
                session.setCenterCharge(rs.getString("CENTER_CHARGE"));
                session.setProfessionalCharge(rs.getString("ASTRO_CHARGE")+".00");
                session.setTotal(String.valueOf(Integer.parseInt(session.getProfessionalCharge().substring(0, session.getProfessionalCharge().indexOf('.'))) + Integer.parseInt(session.getCenterCharge().substring(0, session.getCenterCharge().indexOf('.')))));
                session.setDate(rs.getString("DAY"));

                sessions.add(session);
            }

        }
        catch (SQLException e) {
            LOG.error("``````````````````Exception in EChAstroChannel Connection close>>>>>><<<<><><>``````````````"+e.getMessage());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
                if (center != null) {
                    professional1.setSessions(sessions);
                    center.setProfessionals(professionals);
                    centerList.add(center);
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EChAstroChannel Connection close>>>>>><<<<><><>``````````````"+e.getMessage());
            }
        }
        return centerList;
    }
    public List<Professional> getAstrologerCenterList(Professional professional) {

        List<Professional> professionals = new ArrayList<Professional>();
        Professional astro;
        ResultSet rs;
        String centerCode = "";
        Connection con = null;
        PreparedStatement ps = null;

        /*if (astrologer.getCenterCode() != null && !astrologer.getCenterCode().isEmpty()){
            centerCode = " and ac.CENTER_CODE = '" + astrologer.getCenterCode() +"'";
        }*/

        String searchQuery = "select service_professional.PROFESSIONAL_CODE,service_professional.TITLE,service_professional.FIRST_NAME,service_professional.SURNAME,service_professional.MOBILENUMBER, " +
                "service_session.day,service_session.SESSION_ID, service_session.CENTER_CODE,service_center.CENTER_NAME,service_center.ADDRESS1,service_center.CITY," +
                "service_session.APP_DATE, service_session.START_TIME,service_session.FINISH_TIME,service_session.CENTER_CHARGE,service_session.SERVICE_VAT," +
                "service_session.NO_OF_CLIENTS,service_session.ROOM_NO,service_session.WEB_CLIENTS from service_center,service_professional " +
                "inner join service_session on service_professional.PROFESSIONAL_CODE=service_session.PROFESSIONAL_CODE where " +
                "service_session.CENTER_CODE = service_center.CENTER_CODE and service_center.CENTER_CODE = ? ORDER BY service_professional.FIRST_NAME ASC";
        /*String searchQuery = "select CENTER_CODE form service_centerAS where CENTER_CODE=?";*/

        try{
            con = getDBConnection();
            ps = con.prepareStatement(searchQuery);
            ps.setString(1, professional.getCenterCode());
            rs = ps.executeQuery();
            while (rs.next()) {
                astro = new Professional();
                astro.setProfessionalCode(rs.getString("PROFESSIONAL_CODE"));
                astro.setTitle(rs.getString("TITLE"));
                astro.setFirstName(rs.getString("FIRST_NAME"));
                astro.setSurName(rs.getString("SURNAME"));
                astro.setMobNo(rs.getString("MOBILENUMBER"));
                astro.setFinishTime(Utility.getTimeFormat(rs.getString("FINISH_TIME")));
                astro.setDate(rs.getString("day"));
                astro.setCenterCode(rs.getString("CENTER_CODE"));
                astro.setAppDate(rs.getString("APP_DATE").substring(0,10));
                astro.setStarttime(Utility.getTimeFormat(rs.getString("START_TIME")));
                astro.setCenterCharge(rs.getString("CENTER_CHARGE"));
                astro.setProfessionVat(rs.getString("SERVICE_VAT"));
                astro.setNoOfClients(rs.getString("NO_OF_CLIENTS"));
                astro.setRoomNo(rs.getString("ROOM_NO"));
                astro.setWebClient(rs.getString("WEB_CLIENTS"));
                astro.setCenterName(rs.getString("CENTER_NAME"));
                astro.setAddress(rs.getString("ADDRESS1"));
                astro.setLocation(rs.getString("CITY"));
                astro.setSessionId(String.valueOf(rs.getInt("SESSION_ID")));
                astro.setAppNo(Integer.toString(Utility.getMaxAppointNo(Integer.toString(rs.getInt("SESSION_ID")))));
                astro.setAvailability(Utility.getWebAvailability(Integer.toString(rs.getInt("SESSION_ID"))));

                professionals.add(astro);
            }
            rs.close();
        }
        catch (SQLException ex) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ex.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in EChAstroChannel```````````````"+e.getMessage());
            }
        }
        return professionals;
    }

    @Override
    public List<Professional> getAllAstrologers(int serviceCode) {
        List<Professional> professionals = new ArrayList<Professional>();
        Professional astro;
        ResultSet rs;
        Connection con = null;
        PreparedStatement ps = null;

        String Qry = "select DISTINCT(service_professional.PROFESSIONAL_CODE),service_professional.TITLE,service_professional.FIRST_NAME,service_professional.QUALIFICATION,service_professional.IMAGE_URL,service_professional.SPECIAL_NOTE," +
                "service_professional.SURNAME,service_professional.STATUS,service_professional.QUALIFICATION,service_professional.IMAGE_URL,service_professional.SPECIAL_NOTE from service_professional_center INNER JOIN service_professional,service_center " +
                "WHERE service_professional_center.PROFESSIONAL_CODE = service_professional.PROFESSIONAL_CODE and " +
                "service_professional_center.CENTER_CODE = service_center.CENTER_CODE AND service_professional.STATUS=? " +
                "AND service_center.SERVICE_CODE=? ORDER BY service_professional.FIRST_NAME";

        try {
            con = getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setInt(1,1);
            ps.setInt(2, serviceCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                astro = new Professional();
                astro.setProfessionalCode(rs.getString("PROFESSIONAL_CODE"));
                astro.setTitle(rs.getString("TITLE"));
                astro.setFirstName(rs.getString("FIRST_NAME"));
                astro.setSurName(rs.getString("SURNAME"));
                astro.setAvailability(String.valueOf(rs.getBoolean("STATUS")));
                astro.setQualification(rs.getString("QUALIFICATION"));
                astro.setImgUrl(rs.getString("IMAGE_URL"));
                astro.setSpecialNote(rs.getString("SPECIAL_NOTE"));
                professionals.add(astro);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("`````````<<<<<>>><><><<><>>>`````````Exception in EChAstroChannel```<><<<<<<<<>>>>>>>>>>````````````"+e.getMessage());
            }
        }
        return professionals;
    }

    @Override
    public lk.appointment.professional.dto.Professional getAstrologer(int id) {
        List<lk.appointment.professional.dto.Professional> professional = new ArrayList<lk.appointment.professional.dto.Professional>();
        lk.appointment.professional.dto.Professional professional1 = new lk.appointment.professional.dto.Professional();;
        ResultSet rs;
        Connection con=null;
        PreparedStatement ps = null;

        String Qry = "SELECT TITLE,FIRST_NAME,SURNAME FROM service_professional where STATUS=? and PROFESSIONAL_CODE=?";
        LOG.info(Qry);
        try {
            con = getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1,"1");
            ps.setInt(2, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                professional1.setTitle(rs.getString("TITLE"));
                professional1.setFirstName(rs.getString("FIRST_NAME"));
                professional1.setLastName(rs.getString("SURNAME"));

            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel.SearchAstrologerDaoImpl.getAstrologer>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("`````````<<<<<>>><><><<><>>>`````````Exception in EChAstroChannel```<><<<<<<<<>>>>>>>>>>````````````"+e.getMessage());
            }
        }
        return professional1;
    }

    @Override
    public SpecialNote getAstroSpecNote(int astroId, int centerId) {
        ResultSet rs;
        Connection con=null;
        PreparedStatement ps = null;
        SpecialNote specialNote = new SpecialNote();

        String Qry = "SELECT NOTE,NOTE_ID,PROFESSIONAL_CODE,CENTER_ID FROM service_professional_note WHERE PROFESSIONAL_CODE=? AND CENTER_ID=?";
        try {

            con = getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setInt(1,astroId);
            ps.setInt(2,centerId);
            rs = ps.executeQuery();
            if (rs.next()) {
                specialNote.setNoteId(rs.getString("NOTE_ID"));
                specialNote.setProfessionalCode(rs.getString("PROFESSIONAL_CODE"));
                specialNote.setCenterId(rs.getString("CENTER_ID"));
                specialNote.setNote(rs.getString("NOTE"));
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel.SearchAstrologerDaoImpl.getAstroSpecNote>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("`````````<<<<<>>><><><<><>>>`````````Exception in EChAstroChannel```<><<<<<<<<>>>>>>>>>>````````````"+e.getMessage());
            }
        }
        return specialNote;
    }

    @Override
    public AllocateAstrologer isCheckAstrologerCenter(int astroId, int centerId) {
        AllocateAstrologer allocateAstrologer=null;
        ResultSet rs;
        Connection con = null;
        PreparedStatement ps = null;
        String Qry = "SELECT * FROM astro_astrologer_center WHERE PROFESSIONAL_CODE=? AND CENTER_CODE=? AND STATUS=?";
        try {

            con = getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setInt(1,astroId);
            ps.setInt(2,centerId);
            ps.setInt(3,1);
            rs = ps.executeQuery();
            if (rs.next()) {
                allocateAstrologer=new AllocateAstrologer();
                allocateAstrologer.setAstrologerId(rs.getInt("PROFESSIONAL_CODE"));
                allocateAstrologer.setCenterId(rs.getInt("CENTER_CODE"));
                allocateAstrologer.setDescription("Requesed astrologer has been allocated for requested center");
                allocateAstrologer.setCheck(true);
            }
            else {
                allocateAstrologer=new AllocateAstrologer();
                allocateAstrologer.setAstrologerId(astroId);
                allocateAstrologer.setCenterId(centerId);
                allocateAstrologer.setDescription("Requesed astrologer has not been allocated for requested center");
                allocateAstrologer.setCheck(false);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel.SearchAstrologerDaoImpl.getAstroSpecNote>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("`````````<<<<<>>><><><<><>>>`````````Exception in EChAstroChannel```<><<<<<<<<>>>>>>>>>>````````````"+e.getMessage());
            }
        }
        return allocateAstrologer;
    }

    @Override
    public List<ProfessionalSpecialization> getSpeciality(int professionalCode) {
        ProfessionalSpecialization professionalSpecialization = null;
        List<ProfessionalSpecialization> professionalSpecializations = new LinkedList<ProfessionalSpecialization>();
        ResultSet rs;
        Connection con = null;
        PreparedStatement ps = null;
        String Qry = "select DESCRIPTION,SPEC_CODE,PROFESSIONAL_CODE from service_professional_spec INNER JOIN service_specialization WHERE PROFESSIONAL_CODE=? AND STATUS=? AND service_professional_spec.SPEC_CODE=service_specialization.PROFESSIONAL_SPEC_CODE";

        try {
            con = getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setInt(1,professionalCode);
            ps.setInt(2,1);
            rs = ps.executeQuery();
            while (rs.next()) {
                professionalSpecialization =new ProfessionalSpecialization();
                professionalSpecialization.setDescription(rs.getString("DESCRIPTION"));
                professionalSpecialization.setSpecCode(rs.getString("SPEC_CODE"));;
                professionalSpecialization.setProfessionalCode(rs.getString("PROFESSIONAL_CODE"));
                professionalSpecializations.add(professionalSpecialization);
            }
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel.SearchAstrologerDaoImpl<><<<<<<<<>>>>>>>>>>````````````"+e.getMessage());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("`````````<<<<<>>><><><<><>>>`````````Exception in EChAstroChannel```<><<<<<<<<>>>>>>>>>>````````````"+e.getMessage());
            }
        }
        return professionalSpecializations;
    }





    /*if ("".equals(astrologer.getProfessionalCode()) && "".equals(astrologer.getCenterCode()) && "".equals(astrologer.getDate())) {
        queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
    }
    else if ("".equals(astrologer.getCenterCode()) && "".equals(astrologer.getDate())) {
        if ("0".equals(astrologer.getProfessionalCode())) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_professional.PROFESSIONAL_CODE='" +astrologer.getProfessionalCode()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
    }
    else if ("".equals(astrologer.getProfessionalCode()) && "".equals(astrologer.getDate())) {
        if ("0".equals(astrologer.getCenterCode())) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_center.CENTER_CODE = '" +astrologer.getCenterCode()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
    }
    else if ("".equals(astrologer.getProfessionalCode()) && "".equals(astrologer.getCenterCode())) {
        queryPart ="AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='" +astrologer.getDate()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
    } else if ("".equals(astrologer.getDate())) {
        if ("0".equals(astrologer.getCenterCode()) && !"0".equals(astrologer.getProfessionalCode())) {
            queryPart ="AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_professional.PROFESSIONAL_CODE='" +astrologer.getProfessionalCode()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else if ("0".equals(astrologer.getProfessionalCode()) && !"0".equals(astrologer.getCenterCode())) {
            queryPart ="AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_center.CENTER_CODE = '" +astrologer.getCenterCode()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else if ("0".equals(astrologer.getCenterCode()) && "0".equals(astrologer.getProfessionalCode())) {
            queryPart ="AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else {
            queryPart ="AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_professional.PROFESSIONAL_CODE='" +astrologer.getProfessionalCode()+"' AND service_center.CENTER_CODE = '" +astrologer.getCenterCode()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
    }
    else if ("".equals(astrologer.getCenterCode())) {
        if ("0".equals(astrologer.getProfessionalCode())) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_professional.PROFESSIONAL_CODE = '" +astrologer.getProfessionalCode()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
    }
    else if ("".equals(astrologer.getProfessionalCode())) {
        if ("0".equals(astrologer.getCenterCode())) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_center.CENTER_CODE = '"+astrologer.getCenterCode()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
    }
    else {
        queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_professional.PROFESSIONAL_CODE = '" +astrologer.getProfessionalCode()+"' AND service_center.CENTER_CODE = '"+astrologer.getCenterCode()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
    }*/



    /*if (astrologer.getProfessionalName()==null && astrologer.getCenterName() ==null && astrologer.getDate() == null) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else if (astrologer.getCenterName() ==null && astrologer.getDate() == null) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_professional.FIRST_NAME LIKE '%"+astrologer.getProfessionalName()+"%' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else if (astrologer.getProfessionalName() == null && astrologer.getDate()==null) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_center.CENTER_NAME LIKE '%"+astrologer.getCenterName()+"%' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else if (astrologer.getProfessionalName()==null && astrologer.getCenterName() ==null) {
            queryPart ="AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else if (astrologer.getDate() == null) {
            queryPart ="AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE>='"+getToday+"' AND service_professional.FIRST_NAME LIKE '%"+astrologer.getProfessionalName()+"%' AND service_center.CENTER_NAME LIKE '%"+astrologer.getCenterName()+"%' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else if (astrologer.getCenterName() == null) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_professional.FIRST_NAME LIKE '%"+astrologer.getProfessionalName()+"%' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else if (astrologer.getProfessionalName() == null) {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_center.CENTER_NAME LIKE '%"+astrologer.getCenterName()+"%' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }
        else {
            queryPart = "AND service_session_spec.SPEC_CODE='"+astrologer.getSpecCode()+"' WHERE service_session.APP_DATE='"+astrologer.getDate()+"' AND service_professional.FIRST_NAME LIKE '%"+astrologer.getProfessionalName()+"%' AND service_center.CENTER_NAME LIKE '%"+astrologer.getCenterName()+"%' AND service_center.SERVICE_CODE='"+astrologer.getServiceCode()+"'";
        }*/
}
