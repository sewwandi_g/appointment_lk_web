package lk.appointment.professional.dao.Impl;


import lk.appointment.professional.dao.BookingServiceDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.Pet;
import lk.appointment.professional.utilities.Utility;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static lk.appointment.professional.utilities.DBUtility.getDBConnection;
import static lk.appointment.professional.utilities.Utility.getFinalAppointNo;
import static lk.appointment.professional.utilities.Utility.getSessionId;

/**
 * Created by se-7 on 5/16/2017.
 */
@Component
public class BookingServiceDaoImpl implements BookingServiceDao {
    private static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(BookingServiceDaoImpl.class);

    @Override
    public Appointment getPaymentDetails(String refNo) {
        Connection con = getDBConnection();
        PreparedStatement ps = null;
        Appointment appointment = new Appointment();
        ResultSet rs;

        String selectQuery = "select aa.CENTER_CODE,epg.TXN_DATE,epg.AMOUNT,aa.APP_NO,aa.GIVEN_TIME,aa.PROFESSIONAL_CODE,aa.APP_DATE,aa.CUSTOMER_NAME,aa.ROOM_NO,aa.PAYMENT_TYPE,aa.NIN,aa.EMAIL,aa.NATIONALITY,aa.CHANNEL_FROM,sp.DESCRIPTION from \n" +
                "service_appointment aa inner join service_payment_veryfication epg on aa.REF_NO = epg.REFNO inner join service_specialization sp on sp.PROFESSIONAL_SPEC_CODE = aa.SPEC_CODE where REF_NO = ?";

        try {
            ps = con.prepareStatement(selectQuery);
            ps.setString(1,refNo);
            rs = ps.executeQuery();
            if (rs.next()) {
                appointment.setCenterCode(rs.getString("CENTER_CODE"));
                appointment.setTransDate(rs.getString("TXN_DATE"));
                appointment.setAppNo(rs.getString("APP_NO"));
                appointment.setGivenTime(rs.getString("GIVEN_TIME"));
                appointment.setProfessionalCode(rs.getString("PROFESSIONAL_CODE"));
                appointment.setAppDate(rs.getString("APP_DATE"));
                appointment.setCustomerName(rs.getString("CUSTOMER_NAME"));
                appointment.setRoomNo(rs.getString("ROOM_NO"));
                appointment.setPaymentType(rs.getString("PAYMENT_TYPE"));
                appointment.setAmount(rs.getString("AMOUNT"));
                appointment.setNic(rs.getString("NIN"));
                appointment.setEmail(rs.getString("EMAIL"));
                appointment.setNationality(rs.getString("NATIONALITY"));
                appointment.setChannelFrom(rs.getString("CHANNEL_FROM"));
                appointment.setRefNo(refNo);
                appointment.setSpecId(rs.getString("DESCRIPTION"));
            }
            rs.close();
        }catch (SQLException ex) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java>>>>>>>>>>>>>>>>>>>>>>"+ex.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                /*if (con != null) {
                    con.close();
                }*/
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java`````````````````"+e.getMessage());
            }

        }

        String selectQuery1 = "select TITLE,FIRST_NAME,SURNAME,MOBILENUMBER from service_professional where PROFESSIONAL_CODE =?";

        try {
            ps = con.prepareStatement(selectQuery1);
            ps.setString(1,appointment.getProfessionalCode());
            rs = ps.executeQuery();
            if (rs.next()) {
                appointment.setTitle(rs.getString("TITLE"));
                appointment.setFirstName(rs.getString("FIRST_NAME"));
                appointment.setSurName(rs.getString("SURNAME"));
                appointment.setMobileNo(rs.getString("MOBILENUMBER"));
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                /*if (con != null) {
                    con.close();
                }*/
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }

        }

        String selectQuery2 = "select CENTER_NAME,ADDRESS1,ADDRESS2,ADDRESS3,CITY,SERVICE_CODE from service_center where CENTER_CODE=?";

        try{

            ps = con.prepareStatement(selectQuery2);
            ps.setString(1,appointment.getCenterCode());
            rs = ps.executeQuery();
            if (rs.next()) {
                appointment.setCenterName(rs.getString("CENTER_NAME"));
                appointment.setAddress1(rs.getString("ADDRESS1"));
                appointment.setAddress2(rs.getString("ADDRESS2"));
                appointment.setAddress3(rs.getString("ADDRESS3"));
                appointment.setCity(rs.getString("CITY"));
                appointment.setServiceCode(rs.getString("SERVICE_CODE"));
            }
            rs.close();
        }
        catch (SQLException e) {
            Logger.getLogger("server.log").log(Level.WARNING,"SQLException in BookingServiceDaoImpl.java--------------"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                /*if (con != null) {
                    con.close();
                }*/
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }

        String selectQuery3 = "select AMOUNT from service_payment_veryfication where REFNO = ?";

        try{
            ps = con.prepareStatement(selectQuery3);
            ps.setString(1,refNo);
            rs = ps.executeQuery();
            if (rs.next()) {
                appointment.setEchFee(rs.getString("AMOUNT"));
            }
            rs.close();
        }
        catch(SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java>>>>>>>>>>>>>>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }

        if ("1".equals(appointment.getServiceCode())) {
            String q1="SELECT * FROM astro_birth_detail where ref_no=?";
            try {
                con = getDBConnection();
                ps = con.prepareStatement(q1);
                ps.setString(1,refNo);
                rs = ps.executeQuery();
                if (rs.next()) {
                    appointment.setB_date(rs.getString("DOB"));
                    appointment.setB_time(rs.getString("TIME"));
                    appointment.setB_country(rs.getString("COUNTRY"));
                    appointment.setB_town(rs.getString("TOWN"));
                }
                rs.close();
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
            finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }

                    if (con != null) {
                        con.close();
                    }
                }
                catch (SQLException e) {
                    LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
                }
            }
        } else if ("2".equals(appointment.getServiceCode())) {
            String q2 = "SELECT PET_NAME,PET_TYPE,PET_DOB,REG_NO FROM vet_pet_detail where REF_NO=?";
            Pet pet =null;
            try {
                con = getDBConnection();
                ps = con.prepareStatement(q2);
                ps.setString(1,refNo);
                rs = ps.executeQuery();
                if (rs.next()) {
                    pet = new Pet();
                    pet.setPetName((rs.getString("PET_NAME")));
                    pet.setPetType(rs.getString("PET_TYPE"));
                    pet.setDob(rs.getString("PET_DOB"));
                    pet.setRegNo(rs.getString("REG_NO"));
                    appointment.setPet(pet);
                }
                rs.close();
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
            finally {
                try {
                    if (ps != null) {
                        ps.close();
                    }

                    if (con != null) {
                        con.close();
                    }
                }
                catch (SQLException e) {
                    LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
                }
            }
        }
        return appointment;
    }

    @Override
    public Appointment getTemporyPaymentDetails(String key,String paymentChannel) {
        Connection con = null;
        PreparedStatement ps = null;
        Appointment appointment = null;
        ResultSet rs;

        String selectQuery = "select refNo,amount,payment_mode from service_payment_veryfication where status=? AND  SECURITY_KEY = ?";
        con = getDBConnection();
        try {

            ps = con.prepareStatement(selectQuery);
            ps.setString(1,"0");
            ps.setString(2,key);
            rs = ps.executeQuery();
            if (rs.next()) {
                appointment = new Appointment();
                appointment.setRefNo(rs.getString("refNo"));
                appointment.setPaymentMode(Utility.getPaymentMode(rs.getString("refNo")));
                appointment.setAmount(rs.getString("amount"));
                appointment.setPaymentType(paymentChannel);
            }
            rs.close();
        }catch (Exception ex) {
            LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+ex.getMessage());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                /*if (con != null) {
                    con.close();
                }*/
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }

        String selectQry1 = "select service_code,AMOUNT from service_appointment sa INNER join service_center sc where sa.CENTER_CODE=sc.CENTER_CODE and sa.REF_NO=?";
        try {
            ps = con.prepareStatement(selectQry1);
            ps.setString(1,appointment.getRefNo());
            rs = ps.executeQuery();
            if (rs.next()) {
                appointment.setServiceCode(rs.getString("service_code"));
                appointment.setChannelFrom(paymentChannel);
            }
            rs.close();
        }catch (Exception ex) {
            Logger.getLogger("server.log").log(Level.WARNING,"SQLException in BookingServiceDaoImpl.java....>>getTemporyPaymentDetailsXXXX--------------"+ex.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }
        return appointment;
    }

    @Override
    public boolean isValidResponceHSBC(String referenceNo) {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        String checkRef="";
        ResultSet rs = null;
        boolean isValid = false;

        String getdetails = "select PAYMENTCHANNEL from service_payment_veryfication where REFNO = ?";

        try {
            con = getDBConnection();
            ps = con.prepareStatement(getdetails);
            ps.setString(1,referenceNo);

            rs = ps.executeQuery();

            if (rs.next()) {
                isValid = true;
            }
            rs.close();
            if (isValid) {
                String updteQry = "update service_payment_veryfication set status = '1' where REFNO = ?";
                ps1= con.prepareStatement(updteQry);
                ps1.setString(1,referenceNo);
                int rows = ps1.executeUpdate();
                if (rows > 0) {
                    isValid = true;
                }
                else {
                    isValid = false;
                }
                rs.close();
            }
        } catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java.---isValidResponceHSBCCCCCCCCCC>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }
       return isValid;
    }

    @Override
    public boolean doConfirmPaymentVerification(String referenceNo){
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        ResultSet rs = null;
        boolean isValid = false;

        String getdetails = "select PAYMENTCHANNEL from service_payment_veryfication where REFNO = ?";

        try {
            con = getDBConnection();
            ps = con.prepareStatement(getdetails);
            ps.setString(1,referenceNo);
            rs = ps.executeQuery();

            if (rs.next()) {
                String updteQry = "update service_payment_veryfication set status = '1' where REFNO = ?";
                ps1= con.prepareStatement(updteQry);
                ps1.setString(1,referenceNo);
                int rows = ps1.executeUpdate();
                if (rows > 0) {
                    isValid = true;
                }
                else {
                    isValid = false;
                }
                rs.close();
            }
        } catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java.---doConfirmPaymentVerification>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }
        return isValid;
    }

    /*@Override
    public String getHSBCSequence() {

        ResultSet rs;
        String Sequence = "";
        try {
            con.connectToDB();
            String seqQury = "select 'APP'||lpad(SEQ_ASTRO_HSBC.nextval,9,0) from dual";
            rs = con.query(seqQury);
            if (rs.next()) {
                Sequence = rs.getString(1);

            }
            rs.close();
        }
        catch (Exception e) {
            Logger.getLogger("server.log").log(Level.WARNING,"SQLException in BookingServiceDaoImpl.java--------------"+e.toString());
        }
        finally {
            con.flushStmtRs();
        }
        return Sequence;
    }*/


    @Override
    public boolean doConfirmPayment(String referenceNo) {
        Connection con = null;
        PreparedStatement ps = null;
        boolean isUpdate =  false;
        String updateQry = "update service_appointment set STATUS = ? ,APP_NO = ? where REF_NO=?";
        try {
            con = getDBConnection();
            ps = con.prepareStatement(updateQry);
            ps.setString(1,"1");
            ps.setString(2,getFinalAppointNo(getSessionId(referenceNo)));
            ps.setString(3,referenceNo);

            if (!Utility.isActiveAppointment(referenceNo)) {
                int rows = ps.executeUpdate();
                isUpdate =true;
            }


        }
        catch (Exception e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java------doConfirmPaymentttTTTTTTTTTT>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }
        return isUpdate;
    }

    @Override
    public Appointment getTemporyAppointmentDetails(String refNo) {
        Appointment appointment = new Appointment();
        Connection con = null;
        PreparedStatement ps = null;

        String Qry = "select ac.CENTER_NAME ,aas.TITLE, aas.FIRST_NAME, aas.SURNAME, aa.APP_DATE,aa.GIVEN_TIME,aa.APP_NO,aa.CUSTOMER_NAME,aa.ROOM_NO,aa.SESSION_ID,aa.AMOUNT,aa.CHANNEL_FROM,aa.NIN,aa.TRANSACTION_DATE from service_appointment aa inner join service_center ac\n" +
                "on aa.CENTER_CODE = ac.CENTER_CODE inner join service_professional aas on aa.PROFESSIONAL_CODE = aas.PROFESSIONAL_CODE and REF_NO = ?";
        ResultSet rs;
        System.out.println("<<<<<<<<<<<<<<<<<<bookingserviceDao"+Qry);
        try {
            con = getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1,refNo);

            rs = ps.executeQuery();

            if (rs.next()) {
                appointment.setCustomerName(rs.getString("CUSTOMER_NAME"));
                appointment.setAppNo(rs.getString("APP_NO"));
                appointment.setAppDate(rs.getString("APP_DATE"));
                appointment.setRoomNo(rs.getString("ROOM_NO"));
                appointment.setSessionId(rs.getString("SESSION_ID"));
                appointment.setAmount(rs.getString("AMOUNT"));
                appointment.setProfessionalName(rs.getString("FIRST_NAME") + rs.getString("SURNAME"));
                appointment.setTitle(rs.getString("TITLE"));
                appointment.setCenterName(rs.getString("CENTER_NAME"));
                appointment.setChannelFrom(rs.getString("CHANNEL_FROM"));
                appointment.setGivenTime(rs.getString("GIVEN_TIME"));
                appointment.setNic(rs.getString("NIN"));
                appointment.setTransDate(rs.getString("TRANSACTION_DATE"));
            }
            rs.close();
        }
        catch (Exception e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java-----getTemporyAppointmentDetailsSSSSSSSSS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }
        return appointment;
    }

    @Override
    public  boolean ConfirmOTPBooking( String referenceNo ){

        Connection con = null;
        PreparedStatement ps = null;
        boolean isUpdate =  false;
        String updateQry = "update service_appointment set STATUS = ? ,APP_NO = ? where REF_NO=?";
        try {
            con = getDBConnection();
            ps = con.prepareStatement(updateQry);
            ps.setString(1,"1");
            ps.setString(2,getFinalAppointNo(getSessionId(referenceNo)));
            ps.setString(3,referenceNo);

            if (!Utility.isActiveAppointment(referenceNo)) {
                int rows = ps.executeUpdate();
                isUpdate =true;
            }


        }
        catch (Exception e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java------doConfirmOTPT>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }
        return isUpdate;
    }

    public  Appointment resendOTP(int refNo){
        Connection con = null;
        PreparedStatement ps = null;
        String mobile=null;
        ResultSet rs;
        Appointment appointment=new Appointment();
        int resendCount=0;
        String Qry = "select mobile_no,resend_count from OTPGeneration where CAST(refNo AS UNSIGNED)=?";
        //System.out.println("<<<<<<<<<<<<<<<<<<bookingserviceDaoresendOTPSSSSSSSSS"+Qry);
        try {
            con = getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setInt(1, refNo);

            rs = ps.executeQuery();

            if (rs.next()) {
                mobile=rs.getString("mobile_no");
                resendCount=rs.getInt("resend_count");
            }
            rs.close();
        }
        catch (Exception e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java-----resendOTPSSSSSSSSS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }

        String otp = new Utility().generateOTP(mobile);
        if(otp!=null){
            resendCount=resendCount+1;
        }
        appointment.setRefNo(String.valueOf(refNo));
        appointment.setTelNo(mobile);
        appointment.setSecurityKey(otp);
        String updateQry = "update OTPGeneration set resend_count = ? ,OTP = ? where CAST(refNo AS UNSIGNED)=?";
        try {
            con = getDBConnection();
            ps = con.prepareStatement(updateQry);
            ps.setInt(1,resendCount);
            ps.setString(2,otp);
            ps.setInt(3, refNo);

            int rows = ps.executeUpdate();

        }
        catch (Exception e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SQLException in BookingServiceDaoImpl.java-----resend222222222222222222222>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }
        return appointment;
    }

}
