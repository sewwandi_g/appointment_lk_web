package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.SessionDetailsDao;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.SessionDetails;
import org.springframework.stereotype.Repository;

import java.sql.*;

import static lk.appointment.professional.utilities.DBUtility.getDBConnection;

/**
 * Created by se-7 on 5/17/2017.
 */
@Repository
public class SessionDetailsDaoImpl implements SessionDetailsDao {
    private static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(SessionDetailsDaoImpl.class);
    @Override
    public SessionDetails getSessionDetails(Appointment appointment) {
        Connection con = null;
        PreparedStatement ps = null;
        SessionDetails sessionDetails = new SessionDetails();
        ResultSet rs = null;
        String qry = "";
        if ("1".equals(appointment.getServiceCode()) || "3".equals(appointment.getServiceCode())) {
            qry=" and service_session_spec.SPEC_CODE="+appointment.getSpecId()+" ";
        }
        else {
            qry="";
        }
        String Qry = "select service_session.APP_DATE,service_session.PROFESSIONAL_CODE,service_session.CENTER_CODE,service_session.DAY," +
                "service_professional.TITLE,service_professional.FIRST_NAME, service_professional.SURNAME,service_session.START_TIME," +
                "service_session.FINISH_TIME,service_session.NO_OF_CLIENTS,service_session.ROOM_NO,service_session.CENTER_CHARGE," +
                "service_session.TIME_INTERVAL,service_session.WEB_CLIENTS, service_session.ADVANCED_CH_DATE,service_session_spec.ASTRO_CHARGE " +
                "from service_session inner join service_professional on service_session.PROFESSIONAL_CODE = service_professional.PROFESSIONAL_CODE " +
                "INNER JOIN service_session_spec ON service_session.SESSION_ID=service_session_spec.SESSION_ID "+qry+" " +
                "where service_session.SESSION_ID =?";
        try{
            con = getDBConnection();
            ps = con.prepareStatement(Qry);
            /*ps.setString(1,appointment.getSpecId());*/
            ps.setString(1,appointment.getSessionId());
            rs = ps.executeQuery();
            if (rs.next()) {
                sessionDetails.setAstrologerName(rs.getString("TITLE") + " "+ rs.getString("FIRST_NAME") +" "+rs.getString("SURNAME"));
                sessionDetails.setAppDate(rs.getString("APP_DATE"));
                sessionDetails.setProfessionalCode(rs.getString("PROFESSIONAL_CODE"));
                sessionDetails.setCenterCode(rs.getString("CENTER_CODE"));
                sessionDetails.setAstroCharge(rs.getString("ASTRO_CHARGE"));
                sessionDetails.setDay(rs.getString("DAY"));
                sessionDetails.setStartTime(rs.getString("START_TIME"));
                sessionDetails.setFinishTime(rs.getString("FINISH_TIME"));
                sessionDetails.setNoOfClient(rs.getString("NO_OF_CLIENTS"));
                sessionDetails.setRoomNo(rs.getString("ROOM_NO"));
                sessionDetails.setCenterCharge(rs.getString("CENTER_CHARGE"));
                sessionDetails.setTimeInterval(rs.getString("TIME_INTERVAL"));
                sessionDetails.setWebClient(rs.getString("WEB_CLIENTS"));
                sessionDetails.setAdvanceChDate(rs.getString("ADVANCED_CH_DATE"));
            }

            LOG.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<IIIIIIIIIIIINNNNNNNNNNNNNNNNNNNNNFFFFFFFFFFFFFFOOOOOOOOOOOOOOOOOOOOO>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<Exception in SessionDetailsDaoImpl.java>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    rs.close();
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in SessionDetailsDaoImpl```````````````"+e.getMessage());
            }
        }
        return sessionDetails;
    }
}
