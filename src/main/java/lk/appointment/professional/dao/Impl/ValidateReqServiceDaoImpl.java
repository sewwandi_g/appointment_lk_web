package lk.appointment.professional.dao.Impl;

import lk.appointment.professional.dao.ValidateReqServiceDao;
import lk.appointment.professional.utilities.DBUtility;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by se-7 on 5/16/2017.
 */
@Repository
public class ValidateReqServiceDaoImpl implements ValidateReqServiceDao {
    final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(ValidateReqServiceDaoImpl.class);

    @Override
    public boolean getKey(String key) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        boolean isCompare=false;
        String slctQury = "select amount from service_payment_veryfication where SECURITY_KEY = ?";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(slctQury);
            ps.setString(1,key);
            rs = ps.executeQuery();
            if (rs.next()) {
                isCompare=true;
            }
            rs.close();
        }catch (Exception e) {
            LOG.error("-------------EChPay/ValidateReqServiceDaoImpl.java-----------"+e.toString());
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in SessionDetailsDaoImpl```````````````"+e.getMessage());
            }
        }
        return isCompare;
    }
    @Override
    public boolean getOTP(String refNO,String otp){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        boolean isCompare=false;
        String slctQury = "select OTP from OTPGeneration where refNo = ?";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(slctQury);
            ps.setString(1,refNO);
            rs = ps.executeQuery();
            if (rs.next()) {
                if(otp.equals(rs.getString("OTP"))) {
                    isCompare=true;
                }

            }
            rs.close();
        }catch (Exception e) {
            LOG.error("-------------EChPay/ValidateReqServiceDaoImpl.java-----------"+e.toString());
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in SessionDetailsDaoImpl```````````````"+e.getMessage());
            }
        }
        return isCompare;
    }
}
