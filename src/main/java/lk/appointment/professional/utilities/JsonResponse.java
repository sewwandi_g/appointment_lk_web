package lk.appointment.professional.utilities;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by se-7 on 5/15/2017.
 */
public class JsonResponse {
    boolean status = true;
    int code= 200;
    HttpStatus message = HttpStatus.OK;
    Map<String,Object> data = new HashMap<String, Object>();

    public void addData(String key,Object value) {
        this.data.put(key,value);
    }

    public Map<String,Object> getData() {
        return data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public HttpStatus getMessage() {
        return message;
    }
    public void setMessage(HttpStatus message) {
        this.message = message;
    }


    @Override
    public String toString() {
        return "JsonResponse{" +
                "status=" + status +
                ", code=" + code +
                ", message=" + message +
                ", data=" + data +
                '}';
    }
}
