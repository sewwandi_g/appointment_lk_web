package lk.appointment.professional.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Created by se-7 on 6/7/2017.
 */
public class DBUtility {
    private static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(DBUtility.class);
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("DBproperty");
    static final String JDBC_DRIVER = resourceBundle.getString("JDBC_DRIVER");
    static final String DB_URL = resourceBundle.getString("DB_URL");
    static final String USERNAME = resourceBundle.getString("USERNAME");
    static final String PASSWORD = resourceBundle.getString("PASSWORD");


    public static Connection getDBConnection() {

        Connection dbConnection = null;

        try {

            Class.forName(JDBC_DRIVER);

        } catch (ClassNotFoundException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<exception innnnnnnnnn Classs driverrrrrrrrrrrrrrr>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+e.toString());


        }

        try {

            dbConnection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
            return dbConnection;

        } catch (SQLException e) {

            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<exception innnnnnnnnn db Connectionnnnnnnnnn>>>>>>>>>>>>>>>>>> "+e.toString());

        }

        return dbConnection;

    }
}
