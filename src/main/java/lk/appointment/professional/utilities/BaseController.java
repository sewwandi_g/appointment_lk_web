package lk.appointment.professional.utilities;

/**
 * Created by se-7 on 5/15/2017.
 */
public class BaseController {
    private JsonResponse jsonResponse = new JsonResponse();

    public void addJsonData(String key, Object value) {
        jsonResponse.addData(key, value);
    }

    public JsonResponse getJsonResponse() {
        return jsonResponse;
    }

    @Override
    public String toString() {
        return "BaseController{" +
                "jsonResponse=" + jsonResponse +
                '}';
    }
}
