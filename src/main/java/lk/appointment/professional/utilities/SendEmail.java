package lk.appointment.professional.utilities;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeBodyPart;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Created by udara on 5/19/2020.
 */

/*import javax.mail.*;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Properties;*/

public class SendEmail {
/*    private String SMTP_SERVER = "smtp server";
    private String USERNAME = "info";
    private String PASSWORD = "ech!nf0";

    private String EMAIL_FROM = "info@echannelling.com";
    private String EMAIL_TO = "udara@echannelling.com";
    private String EMAIL_TO_CC = "";

    private String EMAIL_SUBJECT = "Test Send Email via SMTP";
    private String EMAIL_TEXT = "Hello Java Mail \n ABC123";

    public void send(SelfRegister reg){


        EMAIL_TO = reg.getEmail();

        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", "dnsmail.echannelling.com"); //optional, defined in SMTPTransport //203.143.20.118
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "25"); // default port 25
        prop.put("mail.transport.protocol", "smtp");

        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);

        try {

            // from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

            // to
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));

            // cc
            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(EMAIL_TO_CC, false));

            // subject
            msg.setSubject(EMAIL_SUBJECT);

            // content
            msg.setText(EMAIL_TEXT);

            msg.setSentDate(new Date());

            // Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

            // connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);

            // send
            t.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Response: " + t.getLastServerResponse());

            t.close();

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }*/


    public boolean sendEMail(String from, String to, String cc, String bcc, String subject,String mail) {
        boolean sucess = false;
//        EChHospital.Utilities tf = new EChHospital.Utilities();
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", "mail.echannelling.com");
        props.put("mail.smtp.auth", "true");

        Authenticator auth = new SMTPAuthenticator();
        // Get a Session object
        Session sessions= Session.getInstance(props,auth);
        //javax.mail.Session sessions = javax.mail.Session.getInstance(props, auth);
        sessions.setDebug(false);
        // construct the message
        Message msg = new MimeMessage(sessions);
        try {
            msg.setFrom(new InternetAddress(from));

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc, false));
            msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc, false));
            msg.setSubject(subject);
            MimeMultipart mp = new MimeMultipart();

            mp.setSubType("related");
            MimeBodyPart mbp1 = new MimeBodyPart();

            mbp1.setContent(mail, "text/html");
            mbp1.setHeader("X-Mailer", "eChanelling Details");
            MimeBodyPart mbp2 = new MimeBodyPart();
            MimeBodyPart mbp3 = new MimeBodyPart();

            mp.addBodyPart(mbp1);
            msg.setContent(mp);

            msg.setSentDate(new java.util.Date());
            sucess = true;
        } catch (MessagingException e) {
            e.printStackTrace();//To change body of catch statement use File | Settings | File Templates.
            Logger.getLogger("server.log").log(Level.WARNING, "\nException:{0}", e.getMessage());
            sucess = false;
        }

        try {

            Transport.send(msg);
            sucess = true;
        } catch (MessagingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

            e.printStackTrace();//To change body of catch statement use File | Settings | File Templates.
            Logger.getLogger("server.log").log(Level.WARNING, "\nException:{0}", e.getMessage());
            sucess = false;
        }

        return sucess;
    }

    private class SMTPAuthenticator extends Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            String username = "info";
            String password = "ech!nf0";
            return new PasswordAuthentication(username, password);
        }
    }


}

