package lk.appointment.professional.utilities;

import lk.appointment.professional.dao.CenterListDao;
import lk.appointment.professional.dao.Impl.CenterListDaoImpl;
import lk.appointment.professional.enumbers.ChargeType;
import org.apache.log4j.Logger;


/**
 * Created by se-8 on 7/21/2017.
 */
public class ChargeUtility {
    private static final Logger LOG = Logger.getLogger(ChargeUtility.class);

    private  static CenterListDao centerDAO =  new CenterListDaoImpl();
    public static String calculateEchFee(String astroCharge,String centerCharge,String appointType,String NationType,String serviceCode,String centerCode) {
        double echFee = 0;
        if (Integer.parseInt(serviceCode)==1 || Integer.parseInt(serviceCode)==2) {
            double fee = centerDAO.getEchFee(centerCode);
            if(fee>0){
                echFee = fee;
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<echFee>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+echFee);
                if ((ChargeType.V.toString()).equals(appointType) && ((ChargeType.L.toString()).equals(NationType))) {
                    echFee = echFee;
                } else if ((ChargeType.V.toString()).equals(appointType) && ((ChargeType.F.toString()).equals(NationType))) {
                    echFee = (echFee * 2);
                } else if (((ChargeType.C.toString()).equals(appointType) || ChargeType.O.toString().equals(appointType)) && ((ChargeType.L.toString()).equals(NationType))) {
                    echFee = echFee+Double.parseDouble(astroCharge);
                } else if (((ChargeType.C.toString()).equals(appointType) || ChargeType.O.toString().equals(appointType)) && ((ChargeType.F.toString()).equals(NationType))) {
                    echFee = (echFee+Double.parseDouble(astroCharge)) * 2;
                }
            }else {
                if ((ChargeType.V.toString()).equals(appointType) && ((ChargeType.L.toString()).equals(NationType))) {
                    echFee = Math.ceil((Double.parseDouble(astroCharge) + Double.parseDouble(centerCharge)) * 0.1);
                } else if ((ChargeType.V.toString()).equals(appointType) && ((ChargeType.F.toString()).equals(NationType))) {
                    echFee = Math.ceil((Double.parseDouble(astroCharge) + Double.parseDouble(centerCharge)) * 0.1) * 2;
                } else if (((ChargeType.C.toString()).equals(appointType) || ChargeType.O.toString().equals(appointType)) && ((ChargeType.L.toString()).equals(NationType))) {
                    echFee = Math.ceil((Double.parseDouble(astroCharge)) * 0.1);
                } else if (((ChargeType.C.toString()).equals(appointType) || ChargeType.O.toString().equals(appointType)) && ((ChargeType.F.toString()).equals(NationType))) {
                    echFee = Math.ceil((Double.parseDouble(astroCharge) * 0.1) )* 2;
                }
            }
        }
        else {
            echFee = 99.0;
        }
        return Double.toString(echFee).substring(0, Double.toString(echFee).indexOf('.'));
    }

    public static String calculateTotal(String astroCharge,String centerCharge,String echFee,String appointType,String NationType,String serviceCode) {
        double grandTotal = 0;
        if (Integer.parseInt(serviceCode)==2) {
            grandTotal = Double.parseDouble(astroCharge) + Double.parseDouble(centerCharge) + Double.parseDouble(echFee);
        }




        else {
            if (((ChargeType.V.toString()).equals(appointType) || (ChargeType.C.toString()).equals(appointType) || (ChargeType.O.toString()).equals(appointType)) && ((ChargeType.L.toString()).equals(NationType))) {
                grandTotal = Double.parseDouble(astroCharge) + Double.parseDouble(centerCharge) + Double.parseDouble(echFee);
            }
            else if (((ChargeType.V.toString()).equals(appointType) || (ChargeType.C.toString()).equals(appointType) || (ChargeType.O.toString()).equals(appointType)) && ((ChargeType.F.toString()).equals(NationType))) {
                grandTotal = (Double.parseDouble(astroCharge) + Double.parseDouble(centerCharge) + Double.parseDouble(echFee))*2;
            }
            /*else if (((ChargeType.C.toString()).equals(appointType) || (ChargeType.O.toString()).equals(appointType)) && ((ChargeType.L.toString()).equals(NationType))) {
                grandTotal = Double.parseDouble(astroCharge) + Double.parseDouble(echFee);
            }
            else if (((ChargeType.C.toString()).equals(appointType) || (ChargeType.O.toString()).equals(appointType)) && ((ChargeType.F.toString()).equals(NationType))) {
                grandTotal = Double.parseDouble(astroCharge)*2 + Double.parseDouble(echFee);
            }*/

        }
        return Double.toString(grandTotal);
    }

    public static String calculatepaymentTotal(String serviceCode,String astroCharge,String centerCharge,String echFee,String appointType,String NationType,String centerCode) {
        double charge = 0;
        if (Integer.parseInt(serviceCode)==2 || Integer.parseInt(serviceCode)==3) {
            charge = Double.parseDouble(astroCharge) + Double.parseDouble(centerCharge) + Double.parseDouble(echFee);
        }
        else if (Integer.parseInt(serviceCode)==1) {
            double echFee1 = centerDAO.getEchFee(centerCode);
            if(echFee1>0){

                if ((ChargeType.V.toString()).equals(appointType) && ((ChargeType.L.toString()).equals(NationType))) {
                    charge = echFee1;
                } else if ((ChargeType.V.toString()).equals(appointType) && ((ChargeType.F.toString()).equals(NationType))) {
                    charge = (echFee1 * 2);
                } else if (((ChargeType.C.toString()).equals(appointType) || ChargeType.O.toString().equals(appointType)) && ((ChargeType.L.toString()).equals(NationType))) {
                    charge = echFee1+Double.parseDouble(astroCharge);
                } else if (((ChargeType.C.toString()).equals(appointType) || ChargeType.O.toString().equals(appointType)) && ((ChargeType.F.toString()).equals(NationType))) {
                    charge = (echFee1+Double.parseDouble(astroCharge)) * 2;
                }
            }else {
                if ((ChargeType.V.toString()).equals(appointType) && ((ChargeType.L.toString()).equals(NationType))) {
                    charge = Math.ceil((Double.parseDouble(astroCharge) + Double.parseDouble(centerCharge)) * 0.1);
                } else if ((ChargeType.V.toString()).equals(appointType) && ((ChargeType.F.toString()).equals(NationType))) {
                    charge = Math.ceil((Double.parseDouble(astroCharge) + Double.parseDouble(centerCharge)) * 0.1) * 2;
                } else if (((ChargeType.C.toString()).equals(appointType) || (ChargeType.O.toString()).equals(appointType)) && ((ChargeType.L.toString()).equals(NationType))) {
                    charge = Math.ceil((Double.parseDouble(astroCharge)) + ((Double.parseDouble(astroCharge)) * 0.1));
                } else if (((ChargeType.C.toString()).equals(appointType) || (ChargeType.O.toString()).equals(appointType)) && ((ChargeType.F.toString()).equals(NationType))) {
                    charge = Math.ceil(((Double.parseDouble(astroCharge)) + (Double.parseDouble(astroCharge)) * 0.1)) * 2;
                }
            }
        }
        return Double.toString(charge).substring(0, Double.toString(charge).indexOf('.'));
    }
}
