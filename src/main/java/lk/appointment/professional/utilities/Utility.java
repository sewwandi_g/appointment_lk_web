package lk.appointment.professional.utilities;

import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.Professional;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by se-7 on 5/15/2017.
 */
public class Utility {
    private static org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(Utility.class);

    public static Professional setAstrologerName(Professional professional) {
        boolean isCheck = false;
        if (professional.getProfessionalName().contains(" ")) {
            String firstName = professional.getProfessionalName().substring(0, professional.getProfessionalName().indexOf(' '));
            String lastName = professional.getProfessionalName().substring(professional.getProfessionalName().indexOf(' ') + 1);
            professional.setProfessionalName(firstName);
            professional.setSurName(lastName);
            if (!professional.getSurName().isEmpty()) {
                isCheck = true;
            }
        }
        return professional;
    }

    public static String getmonth(String date) {
        int month = Integer.parseInt(date.substring(5,7));
        String monthString = new DateFormatSymbols().getMonths()[month-1];
        return monthString;
    }

    public static boolean validNIN(String nid) {
        int lengthNID = nid.length();
        boolean valid = false;
        nid.replace('_', ' ');
        if(lengthNID == 10)
        {
            char charAt9 = nid.charAt(9);
            if(String.valueOf(charAt9).equals("V") || String.valueOf(charAt9).equals("v") || String.valueOf(charAt9).equals("X") || String.valueOf(charAt9).equals("x"))
                valid = true;
        }
        if(lengthNID == 11)
        {
            char charAt9 = nid.charAt(9);
            char charAt10 = nid.charAt(10);
            if(String.valueOf(charAt9).equals(" ") && (String.valueOf(charAt10).equals("V") || String.valueOf(charAt10).equals("v") || String.valueOf(charAt10).equals("X") || String.valueOf(charAt10).equals("x")))
                valid = true;
        }
        return valid;
    }

    public static int getNextAppointNo(String sessionId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        int nextAppNo=0;

        String selectQuery = "select max(APP_NO) as APP_NO from service_appointment aa where aa.SESSION_ID =?";
        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selectQuery);
            ps.setString(1,sessionId);
            rs = ps.executeQuery();

            while(rs.next()) {
                nextAppNo = rs.getInt("APP_NO");
            }
            nextAppNo = nextAppNo+1;
            rs.close();
        }
        catch(Exception e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<Exception in EChAstroChannel.Utility.java.getNextAppointNo()----------------->>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return nextAppNo;
    }

    public static int getMaxAppointNo(String sessionId){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        int MaxAppNo=0;

        String selectQuery = "select max(APP_NO) as APP_NO from service_appointment aa where aa.SESSION_ID = ? and STATUS = ?";
        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selectQuery);
            ps.setString(1,sessionId);
            ps.setString(2,"1");
            rs = ps.executeQuery();
            while(rs.next()) {
                MaxAppNo = rs.getInt("APP_NO");
            }
            rs.close();
        }
        catch(Exception e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.java.getMaxAppointNo(String sessionId)----------------->>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return MaxAppNo;
    }

    public static int getNextSuccessAppointNo(String sessionId){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        int MaxAppNo=0;

        String selectQuery = "select max(APP_NO) as APP_NO from service_appointment aa where aa.SESSION_ID = ? and STATUS = ?";
        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selectQuery);
            ps.setString(1,sessionId);
            ps.setString(2,"1");
            rs = ps.executeQuery();
            while(rs.next()) {
                MaxAppNo = rs.getInt("APP_NO")+1;
            }
            rs.close();
        }
        catch(Exception e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.java.getMaxAppointNo(String sessionId)----------------->>>>>>>>>>"+e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return MaxAppNo;
    }

    public static int getSessioNoOfClients(String sessionId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String Qry = "select NO_OF_CLIENTS from service_session where session_id = ?";
        int noOFClient=0;
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1,sessionId);
            rs = ps.executeQuery();
            while(rs.next()) {
                noOFClient = rs.getInt("NO_OF_CLIENTS");
            }
            rs.close();
        }
        catch(Exception e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.java.getSessioNoOfClients(String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return noOFClient;
    }

    public static String getGivenTime(String sessionId) {
        Connection con = null;
        int starttime=0;
        PreparedStatement ps = null;
        String selectQuery = "select START_TIME,TIME_INTERVAL from service_session where SESSION_ID = ?";
        int timeInterval = 0;
        String givenTime = "";
        ResultSet rs;
        String maxGivenTime;

        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selectQuery);
            ps.setString(1,sessionId);
            rs = ps.executeQuery();

            while(rs.next()) {
                starttime =rs.getInt("START_TIME");
                timeInterval = rs.getInt("TIME_INTERVAL");
            }
            rs.close();
            maxGivenTime = getMaxGivenTime(sessionId);
            if (maxGivenTime.length() == 3) {
                maxGivenTime =  "0" + maxGivenTime;
            }

            if (Integer.parseInt(maxGivenTime.substring(2, 4)) + timeInterval == 60) {
                int hour = Integer.parseInt(maxGivenTime.substring(0, 2)) + 1;
                givenTime = hour + "00";
            }
            else if (Integer.parseInt(maxGivenTime.substring(2, 4)) + timeInterval > 60) {
                int hour = Integer.parseInt(maxGivenTime.substring(0, 2)) + 1;
                int minute = timeInterval - (60 - Integer.parseInt(maxGivenTime.substring(2, 4)));
                if (Integer.toString(minute).length() == 1) {
                    givenTime =hour+"0"+minute;
                }
                else {
                    givenTime =hour+""+minute;
                }
            }
            else {
                int hour = Integer.parseInt(maxGivenTime.substring(0, 2));
                int minute = Integer.parseInt(maxGivenTime.substring(2, 4)) + timeInterval;
                givenTime = hour +""+minute;
            }

        }
        catch(Exception e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.java.getGivenTime(String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return givenTime;
    }

    public static String getMaxGivenTime(String sessionId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String maxGivenTime = "";
        String selectQuery = "select max(GIVEN_TIME) as givenTime from service_appointment where SESSION_ID = ? and STATUS =?";

        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selectQuery);
            ps.setString(1,sessionId);
            ps.setString(2,"1");
            rs = ps.executeQuery();

            if (rs.next()) {
                maxGivenTime = rs.getString("givenTime");
            }
            rs.close();
            if (maxGivenTime == null){
                maxGivenTime = getStartTime(sessionId);
            }
        }
        catch (Exception e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.java.getMaxGivenTime(String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return  maxGivenTime;
    }

    public static String getStartTime(String sessionId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String startTime="";
        String slctQry = "select START_TIME from service_session where SESSION_ID = ?";

        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(slctQry);
            ps.setString(1,sessionId);
            rs= ps.executeQuery();
            if (rs.next()) {
                startTime = rs.getString("START_TIME");
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.java.getStartTime(String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in BookingServiceDaoImpl.java```````````````"+e.getMessage());
            }
        }
        return startTime;
    }

    public static String getCurrentTime(){
        ResultSet rs1;
        String time="";
        PreparedStatement ps1 = null;
        Connection con=null;
        String currentTime = "SELECT CURRENT_TIME()";
        try {
            con = DBUtility.getDBConnection();
            ps1=con.prepareStatement(currentTime);
            rs1 = ps1.executeQuery();
            if (rs1.next()) {
                time=rs1.getString(1);
            }
            rs1.close();
        }
        catch (SQLException e) {
            LOG.error("------<<<>><<<<>>>>>---Exception in EChAstroChannel.Utility.getCurrentTime()----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps1 != null) {
                    ps1.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return time.substring(0,5);
    }


    public static String generateKey(Appointment appointment) {
        StringBuffer hexString = new StringBuffer();
        String securityKey = "EChPay";
        String keyValue = securityKey+appointment.getRefNo()+"x"+appointment.getAmount()+"x"+appointment.getCustomerName()+"x"+appointment.getCenterFee();
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.generateKey(Appointment appointment)----------------->>>>>>>>>>" + e.toString());
        }
        byte[] hash = md.digest(keyValue.getBytes(StandardCharsets.UTF_8));

        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public String generateOTP(String mobile) {
        String otp=null;
        if (!isBlockedNo(mobile)) {
            otp= String.valueOf(getOtp(6));
        }
        return otp;
    }
    private boolean isBlockedNo(String mobileNo) {
        boolean isBlocked=true;
        if (getOTPNumberCount(mobileNo) < 6) {
            isBlocked=false;
        }
        return isBlocked;
    }
    private char[] getOtp(int length) {
        String numbers = "0123456789";
        Random rndm_method = new Random();
        char[] otp = new char[length];
        for (int i = 0; i < length; i++) {
            otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return otp;
    }


    public int getOTPNumberCount(String mobileNo) {
        Connection con=null ;
        String qry = "select count(*) from OTPGeneration where MOBILE_NO='"+mobileNo+"'  and  TXN_DATE<sysdate() and TXN_DATE>sysdate()-1/24";
        ResultSet rs=null;
        int count=0;
        try {
            con = DBUtility.getDBConnection();
            Statement stmt = con.createStatement() ;
            rs = stmt.executeQuery(qry);
            if (rs.next()) {
                count = rs.getInt(1);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("<<<<<<<<<<<getOTPNumberCount(String mobileNo)>>>>>>>>>>>>", e);
        }
        finally {
            try {
                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return count;
    }


    public static int getMaxWebClient(String sessionId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        int MaxwebClient=0;
        String selectQuery = "select WEB_CLIENTS from service_session where session_id = ?";

        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selectQuery);
            ps.setString(1,sessionId);
            rs = ps.executeQuery();
            while(rs.next()) {
                MaxwebClient = rs.getInt("WEB_CLIENTS");
            }
            rs.close();
        }
        catch(Exception e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getMaxWebClient(String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return MaxwebClient;
    }

    public static int getCurrentWebClientNo(String sessionId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        int currentWebNo=0;
        String selectQuery = "select count(PAYMENT_TYPE) as WEB_NO from service_appointment where SESSION_ID = ? and (CHANNEL_FROM = ? || CHANNEL_FROM = ?) AND STATUS=?";

        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(selectQuery);
            ps.setString(1,sessionId);
            ps.setString(2,"W");
            ps.setString(3,"M");
            ps.setString(4,"1");
            rs = ps.executeQuery();
            while(rs.next()) {
                currentWebNo = rs.getInt("WEB_NO");
            }
            rs.close();
        }
        catch(Exception e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getCurrentWebClientNo(String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return currentWebNo;
    }

    public static String getWebAvailability(String sessionId) {
        String availability = "";


        if(getSessioNoOfClients(sessionId)==0) {
            availability = "HOLIDAY";
        }else {
            if ((getCurrentWebClientNo(sessionId) < getMaxWebClient(sessionId))) {
                if (getNextAppointNo(sessionId) <= getSessioNoOfClients(sessionId)) {
                    availability = "AVAILABLE";
                }else{
                    availability = "    FULL    ";
                }
            } else {
                availability = "WEB_FULL";
            }
        }
        return availability;
    }

    public static String getTimeFormat(String time) {
        String formatTime = "";
        if (!time.equals("0")) {
            if (time.length() == 3){
                time = "0" + time;
            }
            String hour = time.substring(0,2);
            formatTime = hour + ":" +time.substring(time.length()-2,time.length());
        }
        return formatTime;
    }

    public static String getDateFormat(String date) {
        String newDate = date.substring(0,11);
        return newDate;
    }

    public static String getFinalAppointNo (String sessionId) {
        String appNo = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String Qry = "select max(APP_NO) from service_appointment where SESSION_ID =? and status =?";

        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1,sessionId);
            ps.setString(2,"1");
            rs = ps.executeQuery();
            if (rs.next()) {
                appNo = Integer.toString((rs.getInt(1)+1));
            }
            else {
                appNo = getStartTime(sessionId);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getFinalAppointNo (String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return appNo;
    }

    public static String getSessionId (String refNo) {
        String sessionId = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String Qry = "select SESSION_ID from service_appointment where REF_NO= ?";
        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1, refNo);
            rs = ps.executeQuery();
            if (rs.next()) {
                sessionId = rs.getString(1);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getFinalAppointNo (String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return sessionId;
    }


    public static boolean isActiveAppointment (String refNo) {
        boolean isSuccess = false;
        int status = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String Qry = "select STATUS from service_appointment where REF_NO= ?";
        try{
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            ps.setString(1, refNo);
            rs = ps.executeQuery();
            if (rs.next()) {
                status = rs.getInt("STATUS");
            }
            rs.close();
            if (status == 1) {
                isSuccess =true;
            }
            else {
                isSuccess=false;
            }
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.isActiveAppointment----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return isSuccess;
    }

    public static boolean compareTime(String finishTime,String currentTime) {
        boolean status = false;
        String fth1 = finishTime.substring(0,2);
        String ftm1 = finishTime.substring(3,5);
        String cth2 = currentTime.substring(0,2);
        String ctm2 = currentTime.substring(3,5);
        if (Integer.parseInt(fth1) > (Integer.parseInt(cth2))) {
            status = true;
        }
        return status;
    }

    public static boolean compareDate(String today, String appDate) {
        boolean isCompare=false;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dtoday;
        Date dappDate;
        try {
            dtoday = df.parse(today);
            dappDate = df.parse(appDate);

            if (dtoday.before(dappDate)) {
                isCompare = true;
            }
            else if (dtoday.compareTo(dappDate)==0) {
                isCompare = true;
            }
        } catch (ParseException e) {
            LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
        }
        return isCompare;
    }

    public static boolean checkSameDate(String today, String appDate) {
        boolean isCompare=false;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date dtoday;
        Date dappDate;
        try {
            dtoday = df.parse(today);
            dappDate = df.parse(appDate);

            if (dtoday.compareTo(dappDate)==0) {
                isCompare = true;
            }
        } catch (ParseException e) {
            LOG.error("``````````````<<<<>>>>><<<<<<<<<<<>>>>>>>>>>>>>>>>````Exception in Utility.java``````<<<<<<<<<<<<<<<<>>>>>>>>>>>>`````````"+e.getMessage());
        }
        return isCompare;
    }


    public static String getToday() {
        Connection con = null;
        String date = "";
        PreparedStatement ps = null;
        ResultSet rs;
        String Qry = "SELECT CURDATE()";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(Qry);
            rs = ps.executeQuery();
            while (rs.next()) {
                date = rs.getString(1);
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getFinalAppointNo (String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return date;
    }

    public static List<Professional> getSortedSessionList(List<Professional> sessionList) {
        Collections.sort(sessionList, new Comparator<Professional>() {
            public int compare(Professional s1, Professional s2) {
                return s1.getProfessionalCode().compareToIgnoreCase(s2.getProfessionalCode());
                //return s1.getCenterCode().compareToIgnoreCase(s2.getCenterCode());
            }
        });
        return sessionList;
    }

    public static boolean deleteOlderStatus2Appointment() {
        Connection con = null;
        String date = "";
        PreparedStatement ps = null;
        boolean isDelete = false;
        String deleteQry = "delete from ech_astro.service_appointment where STATUS=? and TRANSACTION_DATE<=(SELECT ADDTIME(NOW(), '-0 0:15:00'))";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(deleteQry);
            ps.setInt(1,2);
            int rowCount = ps.executeUpdate();
            if (rowCount > 0) {
                isDelete = true;
            }
        }
        catch (Exception e) {
            LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return isDelete;
    }

    public static String convertDateFormat(String actualDate) throws ParseException {
        SimpleDateFormat month_date = new SimpleDateFormat(" MMMMMMMM dd", Locale.ENGLISH);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        /*String actualDate = "2016-03-20";*/

        Date date = sdf.parse(actualDate);
        DateFormat format2=new SimpleDateFormat("EEEE");
        String finalDay=format2.format(date);

        String month_name = month_date.format(date);
        return finalDay+", "+month_name;
    }

    public static String convertTimeFormat(String timeShort){
        SimpleDateFormat formatShort = new SimpleDateFormat("hhmm", Locale.US);
        //String timeShort = "006";
        if(timeShort.length()==3){
            timeShort = "0"+timeShort;
        }
        String time= null;
        try {
            time = formatShort.format(formatShort.parse(timeShort));
        } catch (ParseException e) {
            LOG.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<EEXXCCEEEPPtion ParseException convertTimeFormat Utility>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.toString());
        }
        String hour=time.substring(0,2);
        String minute = time.substring(2,4);
        if (Integer.parseInt(timeShort.substring(0,2))>=12){
            time=hour+":"+minute+" PM";
        }
        else {
            time=hour+":"+minute+" AM";
        }
        //System.out.println(time);
        return time;
    }

    public static List<String> splitTimePeriod(String sessionId) {
        ResourceBundle time_divide = ResourceBundle.getBundle("time_divide");
        String timeDivider = time_divide.getString("time_divider");
        String timeinfo = getSessionTimeDetails(sessionId);
        String[] parts=timeinfo.split("-", 3);
        String startTime=parts[0];
        String endTime = parts[1];
        /*int timeIntervel = Integer.parseInt(parts[2]);*/
        int timeIntervel = Integer.parseInt(timeDivider);
        List<String> list = new ArrayList<String>();

        int starthour= Integer.parseInt(startTime.substring(0, 2));
        Integer startminute = Integer.parseInt(startTime.substring(2, 4));

        int endhour= Integer.parseInt(endTime.substring(0, 2));
        int endminute = Integer.parseInt(endTime.substring(2, 4));


        if (startminute.toString().length()==1) {
            list.add(starthour+":"+"0"+startminute);
        }
        else {
            list.add(starthour+":"+startminute);
        }

        while (starthour<=endhour) {
            if (starthour==endhour ) {
                while (endminute >= (startminute + timeIntervel)) {
                    startminute = (startminute + timeIntervel);
                    if (startminute.toString().length()==1) {
                        list.add(starthour+":"+"0"+startminute);
                    }
                    else {
                        list.add(starthour+":"+startminute);
                    }

                }
                break;
            }
            else if (endhour > starthour) {
                while ((startminute + timeIntervel) <=60 && (starthour!=endhour)) {
                    startminute +=timeIntervel;
                    if (startminute == 60) {
                        String fminute ="00";
                        startminute=0;
                        starthour +=1;
                        list.add(starthour+":"+fminute);
                    }
                    else {
                        if (startminute.toString().length()==1) {
                            list.add(starthour+":"+"0"+startminute);
                        }else {
                            list.add(starthour+":"+startminute);
                        }
                    }
                }
                if (starthour!=endhour) {
                    starthour +=1;
                    startminute = startminute - 60;
                }
            }

        }
        return list;
    }

    public static String getSessionTimeDetails(String sessionId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String timeinfo="";
        String query = "select START_TIME,FINISH_TIME,TIME_INTERVAL from service_session where SESSION_ID ='"+sessionId+"'";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(query);
            /*ps.setString(1, sessionId);*/
            rs=ps.executeQuery(query);
            if (rs.next()) {
                timeinfo = rs.getString("START_TIME")+"-"+rs.getString("FINISH_TIME")+"-"+rs.getString("TIME_INTERVAL");
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getFinalAppointNo (String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return timeinfo;
    }

    public static List<String> getAppointmentTimeSlots(String sessiionId) {
        List<String> timeList = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        String query = "select GIVEN_TIME,duration from service_appointment INNER JOIN service_session_spec " +
                " ON service_appointment.SESSION_ID=service_session_spec.SESSION_ID AND " +
                " service_appointment.SPEC_CODE=service_session_spec.SPEC_CODE where service_appointment.SESSION_ID=? and STATUS=? ";
        String timeinfo="";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(query);
            ps.setString(1,sessiionId);
            ps.setInt(2, 1);
            rs = ps.executeQuery();
            while (rs.next()) {
                timeList.add(rs.getString("GIVEN_TIME")+"-"+rs.getString("duration"));
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getAppointmentTimeSlots (String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return timeList;
    }


    public static List<String> getActiveAppointmentTime(String sessionId) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs;
        List<String> time = new ArrayList<>();
        String qry = "SELECT GIVEN_TIME FROM service_appointment WHERE SESSION_ID=? and STATUS=?";
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(qry);
            ps.setString(1,sessionId);
            ps.setInt(2, 1);
            rs = ps.executeQuery();
            while (rs.next()) {
                time.add(rs.getString("GIVEN_TIME"));
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getAppointmentTimeSlots (String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return time;
    }

    public static String getSesionSpecDuration(String sessionId, String spec_id) {
        String query = "SELECT duration FROM service_session_spec WHERE SESSION_ID=? and SPEC_CODE=?";
        Connection con = null;
        PreparedStatement ps = null;
        String duration="";
        ResultSet rs;
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(query);
            ps.setString(1,sessionId);
            ps.setString(2, spec_id);
            rs = ps.executeQuery();
            while (rs.next()) {
                duration = rs.getString("duration");
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getAppointmentTimeSlots (String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return duration;
    }

    public static String getPaymentMode(String refNo) {
        String query = "select CHANNEL_FROM from service_appointment WHERE REF_NO=?";
        Connection con = null;
        PreparedStatement ps = null;
        String channelFrom="";
        ResultSet rs;
        try {
            con = DBUtility.getDBConnection();
            ps = con.prepareStatement(query);
            ps.setString(1,refNo);
            rs = ps.executeQuery();
            while (rs.next()) {
                channelFrom = rs.getString("CHANNEL_FROM");
            }
            rs.close();
        }
        catch (SQLException e) {
            LOG.error("---------Exception in EChAstroChannel.Utility.getAppointmentTimeSlots (String sessionId)----------------->>>>>>>>>>" + e.toString());
        }
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }

                if (con != null) {
                    con.close();
                }
            }
            catch (SQLException e) {
                LOG.error("``````````````````Exception in Utility.java```````````````"+e.getMessage());
            }
        }
        return channelFrom;
    }
    public static String isMatch(String input){
        String sanitized="";

        if(input!=null) {
            String REGEX = "^\\w+( \\w+)*(@\\w+)*(.\\w+)*[-\\. ]*$";
            Pattern pattern = Pattern.compile(REGEX);
            Matcher m = pattern.matcher(input);

            if (m.find()) {
                sanitized = m.group();
            }
        }
        return sanitized;
    }

}
