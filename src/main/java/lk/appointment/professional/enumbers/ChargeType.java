package lk.appointment.professional.enumbers;

/**
 * Created by se-8 on 7/21/2017.
 */
public enum ChargeType {
    V,
    C,
    L,
    F,
    O
}
