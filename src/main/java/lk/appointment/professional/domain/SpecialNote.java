package lk.appointment.professional.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by se-8 on 7/11/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpecialNote {
    private String noteId;
    private String professionalCode;
    private String centerId;
    private String note;

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public String getProfessionalCode() {
        return professionalCode;
    }

    public void setProfessionalCode(String professionalCode) {
        this.professionalCode = professionalCode;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
