package lk.appointment.professional.domain;
/**
 * Created by se-8 on 1/8/2018.
 */
public class ErrorInfo {
    public final String url;
    public String errorId;
    public ErrorInfo(String url, String errorId) {
        this.url = url;
        this.errorId = errorId;
    }
}
