package lk.appointment.professional.domain;

/**
 * Created by se-7 on 5/17/2017.
 */
public class MerchantResponseHSBC {
    private String runNumber;    //hsbc
    private String decryptpNo;    //hsbc
    private String secureHash; //hsbc         hashKey
    private String vpcTxnSecureHash; //hsbc
    private String txnResponseCode;
    private String referenceNo;


    public String getRunNumber() {
        return runNumber;
    }

    public void setRunNumber(String runNumber) {
        this.runNumber = runNumber;
    }

    public String getSecureHash() {
        return secureHash;
    }

    public void setSecureHash(String secureHash) {
        this.secureHash = secureHash;
    }

    public String getDecryptpNo() {
        return decryptpNo;
    }

    public void setDecryptpNo(String decryptpNo) {
        this.decryptpNo = decryptpNo;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getTxnResponseCode() {
        return txnResponseCode;
    }

    public void setTxnResponseCode(String txnResponseCode) {
        this.txnResponseCode = txnResponseCode;
    }

    public String getVpcTxnSecureHash() {
        return vpcTxnSecureHash;
    }

    public void setVpcTxnSecureHash(String vpcTxnSecureHash) {
        this.vpcTxnSecureHash = vpcTxnSecureHash;
    }
}
