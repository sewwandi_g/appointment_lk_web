package lk.appointment.professional.domain;

/**
 * Created by se-8 on 7/17/2017.
 */
public class AllocateAstrologer {

    private int astrologerId;
    private int centerId;
    private String description;
    private boolean isCheck;


    public int getAstrologerId() {
        return astrologerId;
    }

    public void setAstrologerId(int astrologerId) {
        this.astrologerId = astrologerId;
    }

    public int getCenterId() {
        return centerId;
    }

    public void setCenterId(int centerId) {
        this.centerId = centerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }
}
