package lk.appointment.professional.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lk.appointment.professional.dto.Specialization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by se-7 on 5/15/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Professional implements Serializable {
    private String title;
    private String firstName;
    private String surName;//For search session
    private String Speciality;//For search session
    private String date;//For search session
    private String centerName;
    private String centerCode;
    private String description;
    private String starttime;
    private String address;
    private String specCode;
    private String appDate;
    private String appNo;
    private String centerCharge;
    private String professionVat;
    private String professionCharge;
    private String noOfClients;
    private String roomNo;
    private String webClient;
    private String ProfessionalName;
    private String professionalCode;
    private String mobNo;
    private String day;
    private String finishTime;
    private String timeInterval;
    private String location;
    private String sessionId;
    @JsonIgnore
    private String availability;
    private String serviceCode;
    private String qualification;
    private String imgUrl;
    private String specialNote;
    private String month;
    /*private  List<String> specializationList = new ArrayList<String>();*/

    private List<Session> sessions = new ArrayList<Session>();
    private List<Specialization> specializations= new ArrayList<Specialization>();

    /*public List<String> getSpecializationList() {
        return specializationList;
    }

    public void setSpecializationList(List<String> specializationList) {
        this.specializationList = specializationList;
    }*/

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public boolean checkSearchValues() {
        boolean isSuccess = true;
        if ("".equals(ProfessionalName) && "".equals(specCode) && "".equals(date) && "".equals(centerName)) {
            isSuccess=false;
        }
        return isSuccess;
    }


    public String isAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getSpeciality() {
        return Speciality;
    }

    public void setSpeciality(String speciality) {
        Speciality = speciality;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSpecCode() {
        return specCode;
    }

    public void setSpecCode(String specCode) {
        this.specCode = specCode;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getAppNo() {
        return appNo;
    }

    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }

    public String getCenterCharge() {
        return centerCharge;
    }

    public void setCenterCharge(String centerCharge) {
        this.centerCharge = centerCharge;
    }

    public String getProfessionVat() {
        return professionVat;
    }

    public void setProfessionVat(String professionVat) {
        this.professionVat = professionVat;
    }

    public String getProfessionCharge() {
        return professionCharge;
    }

    public void setProfessionCharge(String professionCharge) {
        this.professionCharge = professionCharge;
    }

    public String getNoOfClients() {
        return noOfClients;
    }

    public void setNoOfClients(String noOfClients) {
        this.noOfClients = noOfClients;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getWebClient() {
        return webClient;
    }

    public void setWebClient(String webClient) {
        this.webClient = webClient;
    }

    public String getProfessionalName() {
        return ProfessionalName;
    }

    public void setProfessionalName(String professionalName) {
        ProfessionalName = professionalName;
    }

    public String getProfessionalCode() {
        return professionalCode;
    }

    public void setProfessionalCode(String professionalCode) {
        this.professionalCode = professionalCode;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String city) {
        this.location = city;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getSpecialNote() {
        return specialNote;
    }

    public void setSpecialNote(String specialNote) {
        this.specialNote = specialNote;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public List<Specialization> getSpecializations() {
        return specializations;
    }

    public void setSpecializations(List<Specialization> specializations) {
        this.specializations = specializations;
    }

    @Override
    public String toString() {
        return "Astrologer{" +
                "title='" + title + '\'' +
                ", firstName='" + firstName + '\'' +
                ", surName='" + surName + '\'' +
                ", Speciality='" + Speciality + '\'' +
                ", date='" + date + '\'' +
                ", centerName='" + centerName + '\'' +
                ", centerCode='" + centerCode + '\'' +
                ", description='" + description + '\'' +
                ", starttime='" + starttime + '\'' +
                ", address='" + address + '\'' +
                ", specCode='" + specCode + '\'' +
                ", appDate='" + appDate + '\'' +
                ", appNo='" + appNo + '\'' +
                ", centerCharge='" + centerCharge + '\'' +
                ", professionVat='" + professionVat + '\'' +
                ", professionCharge='" + professionCharge + '\'' +
                ", noOfClients='" + noOfClients + '\'' +
                ", roomNo='" + roomNo + '\'' +
                ", webClient='" + webClient + '\'' +
                ", ProfessionalName='" + ProfessionalName + '\'' +
                ", professionalCode='" + professionalCode + '\'' +
                ", mobNo='" + mobNo + '\'' +
                ", day='" + day + '\'' +
                ", finishTime='" + finishTime + '\'' +
                ", timeInterval='" + timeInterval + '\'' +
                ", location='" + location + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", availability=" + availability +
                ", serviceCode='" + serviceCode + '\'' +
                ", qualification='" + qualification + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", specialNote='" + specialNote + '\'' +
                ", month='" + month + '\'' +
                ", sessions=" + sessions +
                '}';
    }
}
