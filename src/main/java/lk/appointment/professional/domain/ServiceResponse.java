package lk.appointment.professional.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by se-7 on 5/17/2017.
 */
public class ServiceResponse {
    private boolean status=false;

    private List<Object> dataList;

    private List<String> errors;


    public List<Object> getDataList() {
        return dataList;
    }

    public void setDataList(List<Object> dataList) {
        this.dataList = dataList;
    }

    public void addData(Object data) {
        if(dataList == null){
            dataList = new ArrayList<Object>();
        }
        this.dataList.add(data);
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void addError(String error) {
        if(errors == null){
            errors = new ArrayList<String>();
        }
        this.errors.add(error);
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
