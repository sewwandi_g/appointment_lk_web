package lk.appointment.professional.domain;
/**
 * Created by se-8 on 1/3/2018.
 */
public class Search {
    private String centerName;
    private String date;
    private String professionalName;
    private String serviceCode;
    private String specCode;
    public String getCenterName() {
        return centerName;
    }
    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getProfessionalName() {
        return professionalName;
    }
    public void setProfessionalName(String professionalName) {
        this.professionalName = professionalName;
    }
    public String getServiceCode() {
        return serviceCode;
    }
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }
    public String getSpecCode() {
        return specCode;
    }
    public void setSpecCode(String specCode) {
        this.specCode = specCode;
    }
}
