package lk.appointment.professional.domain;

/**
 * Created by se-7 on 5/15/2017.
 */
public class Payment {
    private String key;
    private String refNo;
    private String amount;
    private String paymentChannel;
    private String status;

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "key='" + key + '\'' +
                ", amount='" + amount + '\'' +
                ", paymentChannel='" + paymentChannel + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
