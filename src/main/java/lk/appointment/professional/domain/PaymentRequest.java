package lk.appointment.professional.domain;

import java.util.Map;

/**
 * Created by se-7 on 5/16/2017.
 */
public class PaymentRequest {
    String url;
    boolean redirectOnly;
    Map<String, String> paymentData;
    String key;
    String refNo;

    public PaymentRequest(String url, boolean redirectOnly, Map<String, String> paymentData, String key, String refNo) {
        this.url = url;
        this.redirectOnly = redirectOnly;
        this.paymentData = paymentData;
        this.key = key;
        this.refNo = refNo;
    }

    public PaymentRequest() {
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isRedirectOnly() {
        return redirectOnly;
    }

    public void setRedirectOnly(boolean redirectOnly) {
        this.redirectOnly = redirectOnly;
    }

    public Map<String, String> getPaymentData() {
        return paymentData;
    }

    public void setPaymentData(Map<String, String> paymentData) {
        this.paymentData = paymentData;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "PaymentRequest{" +
                "url='" + url + '\'' +
                ", redirectOnly=" + redirectOnly +
                ", paymentData=" + paymentData +
                ", key='" + key + '\'' +
                ", refNo='" + refNo + '\'' +
                '}';
    }
}
