package lk.appointment.professional.domain;

/**
 * Created by se-7 on 5/16/2017.
 */
public class Success {
    private String refNo;

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    @Override
    public String toString() {
        return "Success{" +
                "refNo='" + refNo + '\'' +
                '}';
    }
}
