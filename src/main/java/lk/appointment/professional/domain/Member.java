package lk.appointment.professional.domain;

import java.io.Serializable;

/**
 * Created by se-7 on 5/17/2017.
 */
public class Member implements Serializable {
    private String title;
    private String fistName;
    private String language;
    private String lastName;
    private String memberId;
    private String nationality;
    private String nic;
    private String email;
    private String mobile;
    private String other;
    private String ipAddress;
    private String fax;
    private String address;
    private String cardCollect;
    private String password;
    private boolean smsAlert;
    private String expDate;
    private boolean smsAlertEcl;
    private String active;
    private String deliveryAddress;
    private String pindeliveryType;
    private String memberReference;
    private String dateofBirth;
    private String remarks;
    private String cardNo;
    private String salesOwner;
    private String trnType;
    private String paymentMode;
    private String joiningFee;
    private String annualFee;
    private String postalFee;
    private String localFee;
    private String totalFee;
    private String foreignFee;
    private String country;
    private String status;
    private String pin;
    private boolean local;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAnnualFee() {
        return annualFee;
    }

    public void setAnnualFee(String annualFee) {
        this.annualFee = annualFee;
    }

    public String getForeignFee() {
        return foreignFee;
    }

    public void setForeignFee(String foreignFee) {
        this.foreignFee = foreignFee;
    }

    public String getJoiningFee() {
        return joiningFee;
    }

    public void setJoiningFee(String joiningFee) {
        this.joiningFee = joiningFee;
    }

    public String getLocalFee() {
        return localFee;
    }

    public void setLocalFee(String localFee) {
        this.localFee = localFee;
    }

    public String getPostalFee() {
        return postalFee;
    }

    public void setPostalFee(String postalFee) {
        this.postalFee = postalFee;
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public String getPindeliveryType() {
        return pindeliveryType;
    }

    public void setPindeliveryType(String pindeliveryType) {
        this.pindeliveryType = pindeliveryType;
    }

    public String getTrnType() {
        return trnType;
    }

    public void setTrnType(String trnType) {
        this.trnType = trnType;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getSalesOwner() {
        return salesOwner;
    }

    public void setSalesOwner(String salesOwner) {
        this.salesOwner = salesOwner;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getDateofBirth() {
        return dateofBirth;
    }

    public void setDateofBirth(String dateofBirth) {
        this.dateofBirth = dateofBirth;
    }

    public String getMemberReference() {
        return memberReference;
    }

    public void setMemberReference(String memberReference) {
        this.memberReference = memberReference;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCardCollect() {
        return cardCollect;
    }

    public void setCardCollect(String cardCollect) {
        this.cardCollect = cardCollect;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public boolean isSmsAlert() {
        return smsAlert;
    }

    public void setSmsAlert(boolean smsAlert) {
        this.smsAlert = smsAlert;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSmsAlertEcl() {
        return smsAlertEcl;
    }

    public void setSmsAlertEcl(boolean smsAlertEcl) {
        this.smsAlertEcl = smsAlertEcl;
    }

    public Member() {

    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
