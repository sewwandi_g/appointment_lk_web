package lk.appointment.professional.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by se-7 on 6/22/2017.
 */
@Getter
@Setter
public class Center {
    private String centerCode;
    private String centerName;
    @JsonIgnore
    private String shortCode;
    private String address;
    private String city;
    private String contactNo;

    private List<Professional> professionals= new ArrayList<Professional>();


    @Override
    public String toString() {
        return "Center{" +
                "centerCode='" + centerCode + '\'' +
                ", centerName='" + centerName + '\'' +
                ", shortCode='" + shortCode + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
