package lk.appointment.professional.domain;

/**
 * Created by se-7 on 5/16/2017.
 */
public class SuccessResponse {
    private String Txn_Date;
    private String status;
    private String payment_mode;
    private String amount;

    public String getTxn_Date() {
        return Txn_Date;
    }

    public void setTxn_Date(String txn_Date) {
        Txn_Date = txn_Date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "SuccessResponse{" +
                "Txn_Date='" + Txn_Date + '\'' +
                ", status='" + status + '\'' +
                ", payment_mode='" + payment_mode + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
