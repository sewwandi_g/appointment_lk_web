package lk.appointment.professional.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by se-7 on 5/15/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Appointment {
    private String echFee;
    private String professionalName;
    private String transDate;
    private String centerCode;
    private String centerName;
    private String professionalCode;
    private String appDate;
    private String appNo;
    private String refNo;
    private String customerName;
    private String roomNo;
    private String givenTime;
    private String sessionId;
    private String insBranchCode;
    private String paymentType;
    private String paymentDetails;
    private String amount;
    private String centerFee;
    private String astroFee;
    private String agentFee;
    private String centerVat;
    private String astroVat;
    private String nic;
    private String nationality;
    private String appointmentType;
    private String status;
    private String channelFrom;
    private String remarks;
    private String securityKey;
    private Object paymentChannels;
    private String seqNo;
    private String title;
    private String telNo;
    private String email;
    private String paymentMode;
    private String firstName;
    private String surName;
    private String mobileNo;
    private String address1;
    private String address2;
    private String address3;
    private String city;
    private String specId;
    private String b_date;
    private String b_time;
    private String b_country;
    private String b_town;
    private String note;
    private String serviceCode;
    private Pet pet;

    public int getServiceGiven() {
        return serviceGiven;
    }

    public void setServiceGiven(int serviceGiven) {
        this.serviceGiven = serviceGiven;
    }

    private int serviceGiven;

    public Pet getPet() {
        return pet;
    }
    public void setPet(Pet pet) {
        this.pet = pet;
    }
    public String getB_date() {
        return b_date;
    }

    public void setB_date(String b_date) {
        this.b_date = b_date;
    }

    public String getB_time() {
        return b_time;
    }

    public void setB_time(String b_time) {
        this.b_time = b_time;
    }

    public String getB_country() {
        return b_country;
    }

    public void setB_country(String b_country) {
        this.b_country = b_country;
    }

    public String getB_town() {
        return b_town;
    }

    public void setB_town(String b_town) {
        this.b_town = b_town;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getSpecId() {
        return specId;
    }

    public void setSpecId(String specId) {
        this.specId = specId;
    }

    public String getEchFee() {
        return echFee;
    }

    public void setEchFee(String echFee) {
        this.echFee = echFee;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getProfessionalName() {
        return professionalName;
    }

    public void setProfessionalName(String professionalName) {
        this.professionalName = professionalName;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTelNo() {
        return telNo;
    }

    public void setTelNo(String telNo) {
        this.telNo = telNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    private Member member;

    public Object getPaymentChannels() {
        return paymentChannels;
    }

    public void setPaymentChannels(Object paymentChannels) {
        this.paymentChannels = paymentChannels;
    }

    public String getSecurityKey() {
        return securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }


    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getProfessionalCode() {
        return professionalCode;
    }

    public void setProfessionalCode(String professionalCode) {
        this.professionalCode = professionalCode;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getAppNo() {
        return appNo;
    }

    public void setAppNo(String appNo) {
        this.appNo = appNo;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getGivenTime() {
        return givenTime;
    }

    public void setGivenTime(String givenTime) {
        this.givenTime = givenTime;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getInsBranchCode() {
        return insBranchCode;
    }

    public void setInsBranchCode(String insBranchCode) {
        this.insBranchCode = insBranchCode;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(String paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCenterFee() {
        return centerFee;
    }

    public void setCenterFee(String centerFee) {
        this.centerFee = centerFee;
    }

    public String getAstroFee() {
        return astroFee;
    }

    public void setAstroFee(String astroFee) {
        this.astroFee = astroFee;
    }

    public String getAgentFee() {
        return agentFee;
    }

    public void setAgentFee(String agentFee) {
        this.agentFee = agentFee;
    }

    public String getCenterVat() {
        return centerVat;
    }

    public void setCenterVat(String centerVat) {
        this.centerVat = centerVat;
    }

    public String getAstroVat() {
        return astroVat;
    }

    public void setAstroVat(String astroVat) {
        this.astroVat = astroVat;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChannelFrom() {
        return channelFrom;
    }

    public void setChannelFrom(String channelFrom) {
        this.channelFrom = channelFrom;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }


}
