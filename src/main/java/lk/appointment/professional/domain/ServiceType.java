package lk.appointment.professional.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by se-8 on 9/21/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceType {
    private String serviceType;
    private String ServiceName;
    private String description;
    /*private boolean status;*/
    private String termsAndConditions;
    private String regDate;


    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

/*    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }*/

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }
}
