package lk.appointment.professional.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lk.appointment.professional.dto.Specialization;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by se-8 on 9/26/2017.
 */
@Getter
@Setter
public class Session {

    private String appointmentNo;

    private  String appointmentDate;

    @JsonIgnore
    private String date;

    private  String  sessionId;

    private  String specializationId;

    private String startTime;

    private String finishTime;

    private  String noOfClients;

    private String webClientsNo;

    @JsonIgnore
    private String month;

    @JsonIgnore
    private String total;

    @JsonIgnore
    private String centerName;

    @JsonIgnore
    private String address;

    @JsonIgnore
    private String city;

    private String speciality;

    private String centerCharge;

    private String professionalCharge;

    private String availability;
    private String givenTime;

    @JsonIgnore
    private List<Specialization> specializationList = new ArrayList<Specialization>();

    @Override
    public String toString() {
        return "Session{" +
                "appointmentNo='" + appointmentNo + '\'' +
                ", appointmentDate='" + appointmentDate + '\'' +
                ", date='" + date + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", specializationId='" + specializationId + '\'' +
                ", startTime='" + startTime + '\'' +
                ", finishTime='" + finishTime + '\'' +
                ", noOfClients='" + noOfClients + '\'' +
                ", webClientsNo='" + webClientsNo + '\'' +
                ", month='" + month + '\'' +
                ", total='" + total + '\'' +
                ", centerName='" + centerName + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", speciality='" + speciality + '\'' +
                ", centerCharge='" + centerCharge + '\'' +
                ", professionalCharge='" + professionalCharge + '\'' +
                ", availability=" + availability +
                ", givenTime='" + givenTime + '\'' +
                ", specializationList=" + specializationList +
                '}';
    }
}
