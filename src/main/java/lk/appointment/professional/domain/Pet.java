package lk.appointment.professional.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by se-8 on 11/28/2017.
 */
@Getter
@Setter
public class Pet {
    private String refNo;
    private String petName;
    private String petType;
    private String regNo;
    private String dob;
}
