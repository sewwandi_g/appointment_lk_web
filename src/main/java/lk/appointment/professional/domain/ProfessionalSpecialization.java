package lk.appointment.professional.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by se-7 on 6/21/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfessionalSpecialization {
    private String professionalCode;
    private String specCode;
    private String description;

    public String getSpecCode() {
        return specCode;
    }

    public void setSpecCode(String astroCode) {
        this.specCode = astroCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfessionalCode() {
        return professionalCode;
    }

    public void setProfessionalCode(String professionalCode) {
        this.professionalCode = professionalCode;
    }

    @Override
    public String toString() {
        return "AstroSpecialization{" +
                "astroCode='" + specCode + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
