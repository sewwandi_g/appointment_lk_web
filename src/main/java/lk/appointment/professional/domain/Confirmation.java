package lk.appointment.professional.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Sewwandi on 10/19/2018.
 */
@Getter
@Setter
@JsonInclude
public class Confirmation {
    private String refNO;
    private String totAmt;
    private String key;
    private String phoneNumber;
    private String paymentChannel;
    private String userId;

    @Override
    public String toString() {
        return "Confirmation{" +
                "refNO='" + refNO + '\'' +
                ", totAmt='" + totAmt + '\'' +
                ", key='" + key + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", paymentChannel='" + paymentChannel + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
