package lk.appointment.professional.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Sewwandi on 10/19/2018.
 */
@Setter
@Getter
public class Agent {
    private String agent_code;
    private int telephone;
    private String user_id;
    private String txn_type;
    private String referance_no;
    private double totAmt;
    private double agentFee;
}
