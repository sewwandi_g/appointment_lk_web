package lk.appointment.professional.domain;

/**
 * Created by se-7 on 5/17/2017.
 */
public class SessionDetails {

    private String sessionId;
    private String astrologerName;
    private String app;
    private String appDate;
    private String professionalCode;
    private String day;
    private String startTime;
    private String finishTime;
    private String noOfClient;
    private String roomNo;
    private String astroCharge;
    private String centerCharge;
    private String centerCode;
    private String status;
    private String timeInterval;
    private String webClient;
    private String advanceChDate;
    private String specCode;
    private String regDate;
    private String modifyDate;
    private String repeat;
    private String echFee;

    public String getEchFee() {
        return echFee;
    }

    public void setEchFee(String echFee) {
        this.echFee = echFee;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAstrologerName() {
        return astrologerName;
    }

    public void setAstrologerName(String astrologerName) {
        this.astrologerName = astrologerName;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getProfessionalCode() {
        return professionalCode;
    }

    public void setProfessionalCode(String professionalCode) {
        this.professionalCode = professionalCode;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getNoOfClient() {
        return noOfClient;
    }

    public void setNoOfClient(String noOfClient) {
        this.noOfClient = noOfClient;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getAstroCharge() {
        return astroCharge;
    }

    public void setAstroCharge(String astroCharge) {
        this.astroCharge = astroCharge;
    }

    public String getCenterCharge() {
        return centerCharge;
    }

    public void setCenterCharge(String centerCharge) {
        this.centerCharge = centerCharge;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getWebClient() {
        return webClient;
    }

    public void setWebClient(String webClient) {
        this.webClient = webClient;
    }

    public String getAdvanceChDate() {
        return advanceChDate;
    }

    public void setAdvanceChDate(String advanceChDate) {
        this.advanceChDate = advanceChDate;
    }

    public String getSpecCode() {
        return specCode;
    }

    public void setSpecCode(String specCode) {
        this.specCode = specCode;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    @Override
    public String toString() {
        return "SessionDetails{" +
                "sessionId='" + sessionId + '\'' +
                ", astrologerName='" + astrologerName + '\'' +
                ", app='" + app + '\'' +
                ", appDate='" + appDate + '\'' +
                ", professionalCode='" + professionalCode + '\'' +
                ", day='" + day + '\'' +
                ", startTime='" + startTime + '\'' +
                ", finishTime='" + finishTime + '\'' +
                ", noOfClient='" + noOfClient + '\'' +
                ", roomNo='" + roomNo + '\'' +
                ", astroCharge='" + astroCharge + '\'' +
                ", centerCharge='" + centerCharge + '\'' +
                ", centerCode='" + centerCode + '\'' +
                ", status='" + status + '\'' +
                ", timeInterval='" + timeInterval + '\'' +
                ", webClient='" + webClient + '\'' +
                ", advanceChDate='" + advanceChDate + '\'' +
                ", specCode='" + specCode + '\'' +
                ", regDate='" + regDate + '\'' +
                ", modifyDate='" + modifyDate + '\'' +
                ", repeat='" + repeat + '\'' +
                ", echFee='" + echFee + '\'' +
                '}';
    }
}
