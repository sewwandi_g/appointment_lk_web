package lk.appointment.professional.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Sewwandi on 10/17/2018.
 */
@JsonInclude
@Getter
@Setter
public class BalanceInquiry {
    private String phoneNumber;
    private double fullAmt;
    private String agent;

    @Override
    public String toString() {
        return "BalanceInquiry{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", fullAmt=" + fullAmt +
                ", agent='" + agent + '\'' +
                '}';
    }
}
