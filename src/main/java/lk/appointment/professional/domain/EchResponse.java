package lk.appointment.professional.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Sewwandi on 10/30/2018.
 */
@Getter
@Setter
public class EchResponse {
    private String value;
}
