package lk.appointment.professional.domain;

/**
 * Created by se-7 on 5/16/2017.
 */
public class PaymentGateway {
    private String code;
    private String description;
    private String bankCode;




    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public String toString() {
        return "PaymentGateway{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", bankCode='" + bankCode + '\'' +
                '}';
    }
}
