package lk.appointment.professional.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by se-8 on 10/27/2017.
 */
@Controller
public class DoctorHomeController {
    @RequestMapping(value = "/doctorIndex")
    public String getDoctorIndex() {
        return "web/doctor/category-doctor";
    }
}
