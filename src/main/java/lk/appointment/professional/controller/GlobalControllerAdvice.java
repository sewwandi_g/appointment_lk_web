package lk.appointment.professional.controller;
import lk.appointment.professional.domain.ErrorInfo;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.UUID;




@ControllerAdvice
public class GlobalControllerAdvice {


    private static final Logger LOG = Logger.getLogger(GlobalControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    public ErrorInfo handleAllErrors(HttpServletRequest req,Exception ex ){
        UUID uniqueKey = UUID.randomUUID();
        LOG.error("<<<<<<<<<<<<<<>>>>>+"+uniqueKey+"+>>>>>>>>>>>>>>>>>>>Exception IN APPOINTMENT.LK>>><><><><><><><>>>>>><<<<>>>>>>>>>>>>>>>>>>>>>>>>><<<>>"+ex);
        return new ErrorInfo(req.getRequestURL().toString(),uniqueKey.toString());
    }


    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public ErrorInfo handleNullExceptions(HttpServletRequest req,NullPointerException nx){
        UUID uniqueKey = UUID.randomUUID();
        LOG.error("<<<<<<<<<<<<<<>>"+uniqueKey+">>>>>>>>>>>>>>>>>>>>>>NullPointerException IN APPOINTMENT.LK>>><><><><><><><>>>>>><<<<>>>>>>>>>>>>>>>>>>>>>>>>>"+nx);
        return new ErrorInfo(req.getRequestURL().toString(),uniqueKey.toString());
    }


    @ExceptionHandler(SQLException.class)
    @ResponseBody
    ErrorInfo handleSQLException(HttpServletRequest req,SQLException sx) {
        UUID uniqueKey = UUID.randomUUID();
        LOG.error("<<<<<<<<<<<<<<"+uniqueKey+">>>>>>>>>>>>>>>>>>>>>>>>SQLException IN APPOINTMENT.LK>>><><<<<><><><><><>>>>>><<<<>>>>>>>>>>>>>>>>>>>>>>>>>"+sx);
        return new ErrorInfo(req.getRequestURL().toString(),uniqueKey.toString());
    }

}
