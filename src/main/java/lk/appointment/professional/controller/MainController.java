package lk.appointment.professional.controller;

import lk.appointment.professional.domain.*;
import lk.appointment.professional.dto.AppointmentDTO;
import lk.appointment.professional.dto.AppointmentDTO1;
import lk.appointment.professional.service.*;
import lk.appointment.professional.utilities.BaseController;
import lk.appointment.professional.utilities.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

import static lk.appointment.professional.utilities.Utility.getTimeFormat;

/**
 * Created by se-8 on 1/3/2018.
 */
@RestController
@RequestMapping("v2")
public class MainController {

    @Autowired
    private ProfessionalDetailService professionalDetailService;

    @Autowired
    private PaymentDetailService paymentDetailService;

    @Autowired
    private ProfessionalAppointmentService professionalAppointmentService;

    @Autowired
    private AppointmentDetailService appointmentDetailService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private AgentService agentService;

    @Autowired
    private CenterService centerService;


    @RequestMapping(value = "/sessions", method = RequestMethod.GET,headers = "Accept=application/json", produces = {"application/json"})
    public JsonResponse getsessions(@RequestParam(value = "serviceCode", defaultValue="1") String serviceCode) {
        Professional professional = new Professional();
        professional.setServiceCode(serviceCode);
        BaseController controller = new BaseController();
        List<Center> centerList = professionalDetailService.getCentersWithAstrologersAndSessions(professional);
        if (centerList != null && !centerList.isEmpty()) {
            controller.addJsonData("center", centerList);
            controller.getJsonResponse().setStatus(true);
        }
        else {
            controller.getJsonResponse().setStatus(false);
            controller.getJsonResponse().setCode(HttpStatus.EXPECTATION_FAILED.value());
            controller.getJsonResponse().setMessage(HttpStatus.EXPECTATION_FAILED);
            controller.addJsonData("sessions" , centerList);
        }
        return controller.getJsonResponse();
    }

    @RequestMapping(value = "/appointments", method = RequestMethod.POST)
    public JsonResponse saveTemporaryAppointment(@RequestBody Appointment appointment,ModelMap model) {
        AppointmentDTO tempAppointment = professionalAppointmentService.doTempAppointment(appointment);
        BaseController baseController = new BaseController();
        if (tempAppointment.getSecurityKey() != null) {
            baseController.addJsonData("appointment",tempAppointment);
            return baseController.getJsonResponse();
        }
        else {
            AppointmentDTO appointmentDTO = new AppointmentDTO();
            appointmentDTO.setStatus(tempAppointment.getStatus());
            appointmentDTO.setStatusDescription(tempAppointment.getStatusDescription());
            baseController.addJsonData("appointment", appointmentDTO);
            return baseController.getJsonResponse();
        }
    }


    @RequestMapping(value = "/getPaymentMode",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getPaymentMode() {
        PaymentGateway paymentGateway = new PaymentGateway();
        BaseController baseController = new BaseController();
        List<PaymentGateway> paymentGatewaysList = paymentDetailService.getPaymentChannel(paymentGateway);
        if (paymentGatewaysList != null){
            baseController.addJsonData("paymentGateways", paymentGatewaysList);
        }
        else {
            baseController.getJsonResponse().setStatus(false);
        }
        return baseController.getJsonResponse();
    }

    /*@RequestMapping(value = "/error")
    public JsonResponse returnAppointmentError(HttpServletRequest request,ModelMap map) {
        AppointmentDTO tempAppointment = new AppointmentDTO();
        tempAppointment.setStatus(request.getParameter("status"));
        tempAppointment.setStatusDescription(request.getParameter("statusDescription"));
        BaseController baseController = new BaseController();
        baseController.addJsonData("appointment", tempAppointment);
        return baseController.getJsonResponse();
    }*/

    @RequestMapping(value = "/appointments/{refNo}",method = RequestMethod.GET)
    public JsonResponse getAppointmentDetails(@PathVariable("refNo") String refNo) throws SQLException {
        AppointmentDTO1 appointment = appointmentDetailService.getAppointmentDetails(refNo);
        BaseController baseController = new BaseController();
        baseController.addJsonData("appointment",appointment);
        return baseController.getJsonResponse();
    }
/*
    @RequestMapping(value = "/confirm/{securityKey}/{paymentChannel}",method = RequestMethod.POST)
    public void complete(@PathVariable("securityKey") String securityKey,@PathVariable("paymentChannel") String paymentChannel) {
        Appointment app =bookingService.validateTemporyBooking(securityKey, paymentChannel);
    }*/
    @RequestMapping(value = "/doComplete", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse doComplete(@RequestBody Agent agent) {
        BaseController baseController = new BaseController();
        boolean isSmsSuccessForCus=false;
        boolean status = bookingService.doCompleteAppointment(agent);
        Appointment appointment1 = bookingService.getPaymentDetails(agent.getReferance_no());
        if(status && appointment1!=null){
            String sms_message = ""+ appointment1.getTitle()+" "+appointment1.getFirstName()+" "+appointment1.getSurName() +" on "+ appointment1.getAppDate() +" at "+ getTimeFormat(appointment1.getGivenTime())+". Time allocated is subject to changed. RefNo."+appointment1.getRefNo()+ "." + "AppNo." +appointment1.getAppNo()+ ". "+ "Address." + appointment1.getAddress1()+" "+appointment1.getAddress2()+" "+appointment1.getAddress3()+"," +appointment1.getCity()+ ".";
            String smsProf = "You have an appointment on "+ appointment1.getAppDate() +" at "+ getTimeFormat(appointment1.getGivenTime())+"";
            isSmsSuccessForCus = smsService.sendViaMobitel(Integer.toString(agent.getTelephone()), appointment1,sms_message,smsProf);
        }
        baseController.addJsonData("status",status);
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "/echFee/{centerCode}", method = RequestMethod.GET)
    @ResponseBody
    public EchResponse getEchFee(@PathVariable("centerCode") String centerCode) {
        EchResponse response = new EchResponse();
        response.setValue(String.valueOf(centerService.getEchFee(centerCode)));
        return response;
    }

    @RequestMapping(value = "/searchAppointment", method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getAppointmentDetails(@RequestParam("refNo")String refNo,@RequestParam("name")String name,@RequestParam("phoneNumber") String phoneNo) {
        BaseController baseController = new BaseController();
        List<Appointment>appointmentList = appointmentDetailService.getSearchAppointment(refNo,name,phoneNo);
        if ((appointmentList != null) && !(appointmentList.isEmpty())) {
            baseController.addJsonData("app_list",appointmentList);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "/cancelActiveAppointment/{refNo}", method = RequestMethod.GET)
    @ResponseBody
    public EchResponse cancelAppointment(@PathVariable("refNo") String refNo) {
        EchResponse response = new EchResponse();
        boolean cancelStatus =appointmentDetailService.makeActiveAppointmentCancel(refNo);
        Appointment appointment1 = bookingService.getPaymentDetails(refNo);
        String tele = agentService.getTelephone(refNo);
        if(cancelStatus){
            String sms_message = ""+ appointment1.getTitle()+" "+appointment1.getFirstName()+" "+appointment1.getSurName() +" on "+ appointment1.getAppDate() +" at "+ getTimeFormat(appointment1.getGivenTime())+". is cancelled. RefNo."+refNo+ "." + "AppNo." +appointment1.getAppNo()+ ". "+ "Address." + appointment1.getAddress1()+" "+appointment1.getAddress2()+" "+appointment1.getAddress3()+"," +appointment1.getCity()+ ".";
            String smsProf = "Your appointment is cancelled on "+ appointment1.getAppDate() +" at "+ getTimeFormat(appointment1.getGivenTime())+"";
            boolean isSmsSuccessForCus = smsService.sendViaMobitel(tele, appointment1,sms_message,smsProf);
            boolean isSmsSuccessForProf = smsService.sendViaMobitel(appointment1.getMobileNo(), appointment1,sms_message,smsProf);
        }
        response.setValue(String.valueOf(cancelStatus));
        return response;
    }
    @RequestMapping(value = "/reSendSMS/{refNo}", method = RequestMethod.GET)
    @ResponseBody
    public EchResponse resendSMS(@PathVariable("refNo") String refNo) {
        boolean sucess = false;
        EchResponse response = new EchResponse();
        Appointment appointment1 = bookingService.getPaymentDetails(refNo);
        String tele = agentService.getTelephone(refNo);
        if(tele!=null && appointment1 !=null){
            String sms_message = ""+ appointment1.getTitle()+" "+appointment1.getFirstName()+" "+appointment1.getSurName() +" on "+ appointment1.getAppDate() +" at "+ getTimeFormat(appointment1.getGivenTime())+". Time allocated is subject to changed. RefNo."+refNo+ "." + "AppNo." +appointment1.getAppNo()+ ". "+ "Address." + appointment1.getAddress1()+" "+appointment1.getAddress2()+" "+appointment1.getAddress3()+"," +appointment1.getCity()+ ".";
            String smsProf = "You have an appointment on "+ appointment1.getAppDate() +" at "+ getTimeFormat(appointment1.getGivenTime())+"";
            boolean isSmsSuccessForCus = smsService.sendViaMobitel(tele, appointment1,sms_message,smsProf);
            boolean isSmsSuccessForProf = smsService.sendViaMobitel(appointment1.getMobileNo(), appointment1,sms_message,smsProf);
            if(isSmsSuccessForCus && isSmsSuccessForProf){
                sucess = true;
            }
            //smsService.sendViaMobitel(tele, appointment1)
            response.setValue(String.valueOf(sucess));
        }
        return response;
    }

   /* @RequestMapping(value = "/getAllAppointment/{frmdate}/todate/{todate}")
    @ResponseBody
    public JsonResponse getAllAppointmentDetails(@PathVariable("frmdate") String frmdate ,@PathVariable("todate") String todate){


        List<Appointment> appList = appointmentDetailService.getAllAppointment(frmdate,todate);
        BaseController controller = new BaseController();

        if (appList != null && !appList.isEmpty()) {
            controller.addJsonData("center", appList);
            controller.getJsonResponse().setCode(HttpStatus.OK.value());
            controller.getJsonResponse().setMessage(HttpStatus.OK);
            controller.getJsonResponse().setStatus(true);
        }
        else {
            controller.getJsonResponse().setStatus(false);
            controller.getJsonResponse().setCode(HttpStatus.NO_CONTENT.value());
            controller.getJsonResponse().setMessage(HttpStatus.NO_CONTENT);
            controller.addJsonData("center" , appList);
        }
        return controller.getJsonResponse();*//*List<ProfessionalSpecializationList> professionalSpecializationLists = specializationListService.getSpecializationAccordingToProfessionals();*//*

    }*/
}
