package lk.appointment.professional.controller;
import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.Center;
import lk.appointment.professional.domain.Professional;
import lk.appointment.professional.dto.AppointmentDTO;
import lk.appointment.professional.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
/**
 * Created by se-8 on 12/7/2017.
 */
@Controller
@RequestMapping("/b")
public class beauticianController {

    @Autowired
    private ProfessionalDetailService professionalDetailService;

    @Autowired
    private ProfessionalAppointmentService professionalAppointmentService;

    @Autowired
    private SpecializationListService specializationListService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private SmsService smsService;

    @RequestMapping(value = "/search", method = {RequestMethod.POST,RequestMethod.GET})
    public  String getSearchResults (Model model, String date, String name, String centerName, String specCode){
        Professional professional = new Professional();
        professional.setDate(date);
        professional.setProfessionalName(name);
        professional.setCenterName(centerName);
        professional.setSpecCode(specCode);
        professional.setServiceCode("3");
        List<Center> centerList = professionalDetailService.getCentersWithAstrologersAndSessions(professional);
        model.addAttribute("beautySession", centerList);
        return "web/category";
    }

    @RequestMapping(value = "/client-details", method = RequestMethod.POST)
    public String getClientDetails(Model model, String sessionId, String specCode, String professionName, String month, String startTime, String finishTime, String appNo
            ,String centerName,String city,String address, String total,String professionalCode,String year,String day,String speciality,String centerCharge, String professionalCharge,String serviceCode
            ,String qualification,String specialNote,String weekDay) {

        model.addAttribute("sessionId",sessionId);
        model.addAttribute("specCode",specCode);
        model.addAttribute("name",professionName);
        model.addAttribute("month",month);
        model.addAttribute("startTime",startTime);
        model.addAttribute("finishTime",finishTime);
        model.addAttribute("appNo",appNo);
        model.addAttribute("centerName",centerName);
        model.addAttribute("city",city);
        model.addAttribute("address",address);
        /*model.addAttribute("total",bookingService.getShowFrontEndBookingFee(total));*/
        model.addAttribute("professionalCode",professionalCode);
        model.addAttribute("year",year);
        model.addAttribute("day",day);
        model.addAttribute("speciality",speciality);
        model.addAttribute("centerCharge",centerCharge);
        model.addAttribute("professionalCharge",professionalCharge);
        model.addAttribute("serviceCode","3");
        model.addAttribute("qualification", qualification);
        model.addAttribute("specialNote", specialNote);
        model.addAttribute("weekDay", weekDay);
        model.addAttribute("specializationList",specializationListService.getSpecializationOfProfessionals(3,Integer.parseInt(professionalCode)));
        model.addAttribute("timeSlot",bookingService.getTimeSlots(sessionId,specCode));
        return "web/checkout";
    }

    @RequestMapping(value = "/payment-redirect", method = RequestMethod.POST)
    public String getPaymentLink(Model model, String title, String customerName, String telNo, String email,
                                 String nic, String paymentType,String paymentMode,String nationality,String channelFrom,
                                 String sessionId,String dob, String petName,String petType,String regNo,String appointmentType,
                                 String note,String specCode,String serviceCode,String appointmentTime,String astrologerName,String centerName,String address,String city){

        Appointment appointment = new Appointment();

        appointment.setTitle(title);
        appointment.setCustomerName(customerName);
        appointment.setTelNo(telNo);
        appointment.setEmail(email);
        appointment.setNic(nic);
        appointment.setPaymentType("W");
        appointment.setPaymentMode(paymentMode);
        appointment.setNationality(nationality);
        appointment.setChannelFrom("W");
        appointment.setSessionId(sessionId);
        appointment.setGivenTime(appointmentTime);
        /*appointment.setB_date(b_date);
        appointment.setB_time(b_time);
        appointment.setB_country(b_country);
        appointment.setB_town(b_town);*/
        appointment.setAppointmentType(appointmentType);
        appointment.setNote(note);
        appointment.setSpecId(specCode);
        appointment.setServiceCode(serviceCode);
        AppointmentDTO tempInsertSuccess = professionalAppointmentService.doTempAppointment(appointment);
        model.addAttribute("referenceNo",appointment.getRefNo());
        model.addAttribute("key",appointment.getSecurityKey());
        model.addAttribute("PaymentChannel",appointment.getPaymentMode());
        /*if (tempInsertSuccess.getSecurityKey() != null) {
            return "redirect:/payNow";
        }
        else {
            return "FailPage";
        }*/


        if (tempInsertSuccess.getSecurityKey() != null) {
            String sms_message = "Your OTP Number is "+tempInsertSuccess.getSecurityKey();
            boolean success = smsService.sendViaMobitel(appointment.getTelNo(),appointment,sms_message,"");
            model.addAttribute("mobile",appointment.getTelNo());
            model.addAttribute("astrologerName",astrologerName);
            model.addAttribute("centerName",centerName);
            model.addAttribute("address",address+" "+city);
            model.addAttribute("AppDate",appointment.getAppDate());
            model.addAttribute("appTime",appointment.getGivenTime());
            model.addAttribute("appNo",appointment.getAppNo());
            model.addAttribute("customerName",appointment.getCustomerName());
            model.addAttribute("nic",appointment.getNic());
            model.addAttribute("customerNo",appointment.getTelNo());
            model.addAttribute("email",appointment.getEmail());
            model.addAttribute("success",success);
            return "otpGeneration";
        }
        else {
            return "FailPage";
        }
    }
}
