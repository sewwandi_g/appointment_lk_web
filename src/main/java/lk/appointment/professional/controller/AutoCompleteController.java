package lk.appointment.professional.controller;

import lk.appointment.professional.service.ProfessionalDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by se-8 on 9/27/2017.
 */
@Controller
public class AutoCompleteController {

    @Autowired
    private ProfessionalDetailService professionalDetailService;

    @RequestMapping(value = "/getNames",
            method = RequestMethod.GET,
            headers="Accept=*/*")
    public @ResponseBody
    List<String> getNameList(@RequestParam("term") String term,ModelMap model) {
        int serviceCode = 1;
        List<String> ProfessionalList = professionalDetailService.getFillName(serviceCode,term);
        model.addAttribute("search",ProfessionalList);
        return ProfessionalList;
    }

  /*  @RequestMapping(value = "/allProfessionals",
            method = RequestMethod.GET,
          )
    public @ResponseBody
    List<Center> getAllNameList(@RequestParam("term") String term,ModelMap model) {
        Professional professional =new Professional();
        professional.setServiceCode(term);
        professional.setDate("");
        professional.setProfessionalName("");
        professional.setCenterName("");
        professional.setSpecCode("");
        List<Center> ProfessionalList = professionalDetailService.getCentersWithAstrologersAndSessions(professional);
        model.addAttribute("professionals",ProfessionalList);
        return ProfessionalList;
    }*/

    @RequestMapping(value = "/getCenters",
            method = RequestMethod.GET,
            headers="Accept=*/*")
    public @ResponseBody
    List<String> getCenterList(@RequestParam("term") String term,ModelMap model) {
        int serviceCode = 1;
        List<String> ProfessionalList = professionalDetailService.getCenterName(serviceCode, term);
        model.addAttribute("search",ProfessionalList);
        return ProfessionalList;
    }

    @RequestMapping(value = "/getCentersOfveterinary",
            method = RequestMethod.GET,
            headers="Accept=*/*")
    public @ResponseBody
    List<String> getCenterListOfVeterinary(@RequestParam("term") String term,ModelMap model) {
        int serviceCode = 2;
        List<String> ProfessionalList = professionalDetailService.getCenterName(serviceCode, term);
        model.addAttribute("search",ProfessionalList);
        return ProfessionalList;
    }


    @RequestMapping(value = "/getNamesOfVeterinary",
            method = RequestMethod.GET,
            headers="Accept=*/*")
    public @ResponseBody
    List<String> getNameListOfveterinary(@RequestParam("term") String term,ModelMap model) {
        int serviceCode = 2;
        List<String> ProfessionalList = professionalDetailService.getFillName(serviceCode,term);
        model.addAttribute("search",ProfessionalList);
        return ProfessionalList;
    }

    @RequestMapping(value = "/getCitiesOfveterinary",
            method = RequestMethod.GET,
            headers="Accept=*/*")
    public @ResponseBody
    List<String> getCityListOfveterinary(@RequestParam("term") String term,ModelMap model) {
        int serviceCode = 2;
        List<String> ProfessionalList = professionalDetailService.getCities(serviceCode,term);
        model.addAttribute("search",ProfessionalList);
        return ProfessionalList;
    }

    @RequestMapping(value = "/getCentersOfbeauticians",
            method = RequestMethod.GET,
            headers="Accept=*/*")
    public @ResponseBody
    List<String> getCenterListOfbeauticians(@RequestParam("term") String term,ModelMap model) {
        int serviceCode = 3;
        List<String> ProfessionalList = professionalDetailService.getCenterName(serviceCode, term);
        model.addAttribute("search",ProfessionalList);
        return ProfessionalList;
    }

    @RequestMapping(value = "/getNamesOfbeauticians",
            method = RequestMethod.GET,
            headers="Accept=*/*")
    public @ResponseBody
    List<String> getNameListOfbeauticians(@RequestParam("term") String term,ModelMap model) {
        int serviceCode = 3;
        List<String> ProfessionalList = professionalDetailService.getFillName(serviceCode,term);
        model.addAttribute("search",ProfessionalList);
        return ProfessionalList;
    }

    @RequestMapping(value = "/getCitiesOfbeauticians",
            method = RequestMethod.GET,
            headers="Accept=*/*")
    public @ResponseBody
    List<String> getCityListOfbeauticians(@RequestParam("term") String term,ModelMap model) {
        int serviceCode = 3;
        List<String> ProfessionalList = professionalDetailService.getCities(serviceCode,term);
        model.addAttribute("search",ProfessionalList);
        return ProfessionalList;
    }
}
