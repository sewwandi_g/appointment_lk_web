package lk.appointment.professional.controller;

import lk.appointment.professional.dto.SelfRegister;
import lk.appointment.professional.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by udara on 5/14/2020.
 */
@Controller
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(String name,String email,String category,String qualification,String sub,String nic,String addr,String contact)
    {
        //System.out.println("here");

        SelfRegister reg = new SelfRegister();
        reg.setName(name);
        reg.setEmail(email);
        reg.setCategory(category);
        reg.setCategory(category);
        reg.setQualification(qualification);
        reg.setSub(sub);
        reg.setNic(nic);
        reg.setAddr(addr);
        reg.setContact(contact);

        if (!registerService.checkNicEmail(reg)) {
            registerService.selfRegister(reg);
            return "web/success";
        }else{
            return "web/exist";
        }

    }
}
