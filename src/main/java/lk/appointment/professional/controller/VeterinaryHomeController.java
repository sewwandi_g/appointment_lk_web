package lk.appointment.professional.controller;

import lk.appointment.professional.domain.Appointment;
import lk.appointment.professional.domain.Center;
import lk.appointment.professional.domain.Pet;
import lk.appointment.professional.domain.Professional;
import lk.appointment.professional.dto.AppointmentDTO;
import lk.appointment.professional.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by se-8 on 9/30/2017.
 */
@Controller
@RequestMapping(value = "/v1/")
public class VeterinaryHomeController {

    @Autowired
    private ProfessionalAppointmentService professionalAppointmentService;


    @Autowired
    private BookingService bookingService;

    @Autowired
    private CenterService centerService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private ProfessionalDetailService professionalDetailService;

    @RequestMapping(value = "/index")
    public String getIndex(){
        return "web/index";
    }

    @RequestMapping(value = "/landPage")
    public String getLandingPage(Model model) {
        Professional professional =new Professional();
        professional.setServiceCode("2");
        professional.setDate("");
        professional.setProfessionalName("");
        professional.setCenterName("");
        professional.setSpecCode("");
        List<Professional> professionals = professionalDetailService.getAstrologerWithSessions(professional);
        model.addAttribute("veterinarysessions", professionals);
        return "web/veterinary/category-veterinary";
    }

    @RequestMapping(value = "/home")
    public String getRedirectPage(Model model){
        Professional professional =new Professional();
        professional.setServiceCode("2");
        professional.setDate("");
        professional.setProfessionalName("");
        professional.setCenterName("");
        professional.setSpecCode("");
        /*List<Professional> professionals = professionalDetailService.getAstrologerWithSessions(professional);*/
        List<Center> centerList = professionalDetailService.getCentersWithAstrologersAndSessions(professional);
        /*List<ProfessionalSpecializationList> professionalSpecializationLists = specializationListService.getSpecializationAccordingToProfessionals();*/
        model.addAttribute("veterinarysessions", centerList);
        /*model.addAttribute("specializationList", professionalSpecializationLists);*/
        return "web/category";
    }
    @RequestMapping(value = "/search", method = {RequestMethod.POST,RequestMethod.GET})
    public  String getSearchResults (Model model, String date, String name, String centerName, String specCode,String serviceCode,String location){
        Professional professional = new Professional();

        professional.setDate(date);
        professional.setProfessionalName(name);
        professional.setCenterName(centerName);
        professional.setLocation(location);
        professional.setServiceCode("2");

        /*List<Professional> professionals = professionalDetailService.getAstrologerWithSessions(professional);*/

        List<Center> centerList = professionalDetailService.getCentersWithAstrologersAndSessions(professional);

        model.addAttribute("veterinarysessions", centerList);

        professional.setServiceCode("1");
        professional.setDate("");
        professional.setProfessionalName("");
        List<Center> centerList2 = professionalDetailService.getCentersWithAstrologersAndSessions(professional);
        /*List<ProfessionalSpecializationList> professionalSpecializationLists = specializationListService.getSpecializationAccordingToProfessionals();*/

        model.addAttribute("sessions", centerList2);
        model.addAttribute("serviceCode","2");
        return "web/categoryAstrology";

    }

    @RequestMapping(value = "/client-details", method = RequestMethod.POST)
    public String getClientDetails(Model model, String sessionId, String specCode, String veterinaryName, String month, String startTime, String finishTime, String appNo
            ,String centerName,String city,String address, String total,String professionalCode,String year,String day,String speciality,String centerCharge, String professionalCharge,String serviceCode
    ,String qualification,String specialNote,String weekDay,String centerCode,String profNo) {

        model.addAttribute("sessionId",sessionId);
        model.addAttribute("specCode",specCode);
        model.addAttribute("name",veterinaryName);
        model.addAttribute("month",month);
        model.addAttribute("startTime",startTime);
        model.addAttribute("finishTime",finishTime);
        model.addAttribute("appNo",appNo);
        model.addAttribute("centerName",centerName);
        model.addAttribute("city",city);
        model.addAttribute("address",address);
        model.addAttribute("total",bookingService.getShowFrontEndBookingFee(total));
        model.addAttribute("professionalCode",professionalCode);
        model.addAttribute("year",year);
        model.addAttribute("day",day);
        model.addAttribute("speciality",speciality);
        model.addAttribute("centerCharge",centerCharge);
        model.addAttribute("professionalCharge",professionalCharge);
        model.addAttribute("echFee", centerService.getEchFee(centerCode));
        model.addAttribute("serviceCode","2");
        model.addAttribute("qualification", qualification);
        model.addAttribute("specialNote", specialNote);
        model.addAttribute("weekDay", weekDay);
        model.addAttribute("profNo",profNo);
        return "web/checkout";
    }

    @RequestMapping(value = "/payment-redirect", method = RequestMethod.POST)
    public String getPaymentLink(Model model, String title, String customerName, String telNo, String email,
                                 String nic, String paymentType,String paymentMode,String nationality,String channelFrom,
                                 String sessionId,String dob, String petName,String petType,String regNo,
                                 String appointmentType, String note,String specCode,String serviceCode,
                                 String address,String centerName,String city,String profNo){

        Appointment appointment = new Appointment();
        Pet pet = new Pet();

        appointment.setTitle(title);
        appointment.setCustomerName(customerName);
        appointment.setTelNo(telNo);
        appointment.setEmail(email);
        appointment.setNic(nic);
        appointment.setPaymentType("W");
        appointment.setPaymentMode(paymentMode);
        appointment.setNationality(nationality);
        appointment.setChannelFrom("W");
        appointment.setSessionId(sessionId);
        /*appointment.setB_date(b_date);
        appointment.setB_time(b_time);
        appointment.setB_country(b_country);
        appointment.setB_town(b_town);*/
        pet.setPetName(petName);
        pet.setPetType(petType);
        pet.setRegNo(regNo);
        pet.setDob(dob);
        appointment.setPet(pet);
        appointment.setAppointmentType(appointmentType);
        appointment.setNote(note);
        appointment.setSpecId(specCode);
        appointment.setServiceCode(serviceCode);
        AppointmentDTO tempInsertSuccess = professionalAppointmentService.doTempAppointment(appointment);
        model.addAttribute("referenceNo",appointment.getRefNo());
        model.addAttribute("key",appointment.getSecurityKey());
        model.addAttribute("PaymentChannel",appointment.getPaymentMode());
        if (tempInsertSuccess.getSecurityKey() != null) {
            String sms_message = "Your OTP Number is "+tempInsertSuccess.getSecurityKey();
            boolean success = smsService.sendViaMobitel(appointment.getTelNo(),appointment,sms_message,"");
            model.addAttribute("mobile",appointment.getTelNo());
            model.addAttribute("astrologerName",appointment.getProfessionalName());
            model.addAttribute("centerName",centerName);
            model.addAttribute("address",address + " "+city);
            model.addAttribute("AppDate",appointment.getAppDate());
            model.addAttribute("appTime",appointment.getGivenTime());
            model.addAttribute("appNo",appointment.getAppNo());
            model.addAttribute("customerName",appointment.getCustomerName());
            model.addAttribute("profNo",profNo);
            model.addAttribute("customerNo",appointment.getMobileNo());
            model.addAttribute("email",appointment.getEmail());
            model.addAttribute("success",success);
            return "otpGeneration";
        }
        else {
            return "FailPage";
        }
    }
}
