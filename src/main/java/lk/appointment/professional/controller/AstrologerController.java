package lk.appointment.professional.controller;

import lk.appointment.professional.domain.*;
import lk.appointment.professional.dto.AppointmentDTO;
import lk.appointment.professional.dto.CenterDTO;
import lk.appointment.professional.service.*;
import lk.appointment.professional.service.Impl.CenterServiceImpl;
import lk.appointment.professional.service.Impl.PaymentDetailServiceImpl;
import lk.appointment.professional.service.Impl.SpecializationListServiceImpl;
import lk.appointment.professional.utilities.BaseController;
import lk.appointment.professional.utilities.JsonResponse;
import lk.appointment.professional.validator.AppointmentValidator;
import lk.appointment.professional.validator.SearchValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by se-7 on 5/15/2017.
 */
@Controller
@RequestMapping("/api")
public class AstrologerController {


    /*private ProfessionalDetailService professionalDetailService;*/

    @Autowired
    private SearchValidator searchValidator;
    @Autowired
    private AppointmentValidator appointmentValidator;


    @InitBinder("sessions")
    protected void searchInitBinder(WebDataBinder binder) {
        binder.addValidators(searchValidator);
    }
    @InitBinder("appointment")
    protected void appInitBinder(WebDataBinder binder) {
        binder.addValidators(appointmentValidator);
    }


    @Autowired
    private ProfessionalDetailService professionalDetailService;

    @Autowired
    private SessionDetailsService sessionDetailsService;

    @Autowired
    private CenterService centerService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private ProfessionalAppointmentService professionalAppointmentService;

    /*public AstrologerController(ProfessionalDetailService professionalDetailService){
        this.professionalDetailService = professionalDetailService;
    }*/

    private static final Logger LOG = Logger.getLogger(AstrologerController.class);

    @RequestMapping(value = "/sessions", method = RequestMethod.POST, headers = "Accept=application/json", produces = {"application/json"})
    @ResponseBody
    public JsonResponse getSession(@RequestBody @Validated Professional professional) {

        BaseController controller = new BaseController();

        List<Center> centerList = professionalDetailService.getCentersWithAstrologersAndSessions(professional);
        if (centerList != null && !centerList.isEmpty()) {
            controller.addJsonData("center", centerList);
            controller.getJsonResponse().setCode(HttpStatus.OK.value());
            controller.getJsonResponse().setMessage(HttpStatus.OK);
            controller.getJsonResponse().setStatus(true);
        }
        else {
            controller.getJsonResponse().setStatus(false);
            controller.getJsonResponse().setCode(HttpStatus.NO_CONTENT.value());
            controller.getJsonResponse().setMessage(HttpStatus.NO_CONTENT);
            controller.addJsonData("center" , centerList);
        }
        return controller.getJsonResponse();
    }

    @RequestMapping(value = "/complete", method = RequestMethod.POST,headers = "Accept=application/json")
    @ResponseBody
    public JsonResponse doTemChannel(@RequestBody @Validated Appointment appointment) {


        LOG.info("<<<<<<<<<<<<<<<<<<<</**/<<<<<<<<<<<<<<<<final appointment in doTempAppointRECEVEDDDDDDD------------------->>>>> "+appointment.toString());
        BaseController tempController = new BaseController();

        AppointmentDTO tempAppointment = professionalAppointmentService.doTempAppointment(appointment);

        if (appointment.getSecurityKey() != null){
            tempController.getJsonResponse().setStatus(true);
            tempController.getJsonResponse().setCode(HttpStatus.OK.value());
            tempController.getJsonResponse().setMessage(HttpStatus.OK);
            tempController.addJsonData("paymentDetail",tempAppointment);

            /*System.out.println("final appointment ------------------->>>>> " + appointment.toString());*/
            LOG.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<final appointment in doTempAppoint------------------->>>>> "+appointment.toString());
        }
        else {
            tempController.getJsonResponse().setStatus(false);
            tempController.getJsonResponse().setCode(HttpStatus.BAD_REQUEST.value());
            tempController.getJsonResponse().setMessage(HttpStatus.BAD_REQUEST);
            tempController.addJsonData("paymentDetail",tempAppointment);

        }

        return tempController.getJsonResponse();
    }

    @RequestMapping(value = "/sessionDetails", method = RequestMethod.POST,headers = "Accept=application/json")
    @ResponseBody
    public SessionDetails getSessionDetails(@RequestBody @Validated Appointment appointment) {

        SessionDetails sessionDetails = sessionDetailsService.getSessionDetails(appointment);
        sessionDetails.setSessionId(appointment.getSessionId());
        return sessionDetails;

    }

    @RequestMapping(value = "/getPaymentMode",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getPaymentMode() {
        PaymentGateway paymentGateway = new PaymentGateway();
        BaseController baseController = new BaseController();
        PaymentDetailService paymentDetailService = new PaymentDetailServiceImpl();
        List<PaymentGateway> paymentGatewaysList = paymentDetailService.getPaymentChannel(paymentGateway);
        if (paymentGatewaysList != null){
            baseController.addJsonData("paymentGateways", paymentGatewaysList);
        } else {
            baseController.getJsonResponse().setStatus(false);
        }
        return baseController.getJsonResponse();
    }

    //TODO check method
    @RequestMapping(value = "/getAstrologers",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getAstrologers(HttpServletRequest request) {

        /*AstrologerDetailService astrologerDetailService = new AstrologerDetailServiceImpl();*/

        Professional professional =new Professional();

        String centerCode = request.getParameter("centerCode");
        professional.setCenterCode(centerCode);

        BaseController baseController = new BaseController();

        List<Professional> professionalList = professionalDetailService.getAstrologerData(professional);

        if (professionalList != null){
            baseController.addJsonData("astrologers", professionalList);
        }
        return baseController.getJsonResponse();
    }


    @RequestMapping(value = "/getSpecList/{serviceCode}",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getSpecList (@PathVariable("serviceCode") int serviceCode) {
        SpecializationListService specializationListService = new SpecializationListServiceImpl();
        BaseController baseController = new BaseController();
        List<ProfessionalSpecialization> astroSpecList = specializationListService.getSpecList(serviceCode);

        if ((astroSpecList != null) && !(astroSpecList.isEmpty())) {
            baseController.getJsonResponse().setStatus(true);
            baseController.addJsonData("specialization",astroSpecList);
        }
        else {
            baseController.getJsonResponse().setStatus(false);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "/getCenterList/{serviceCode}",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getCenterList(@PathVariable("serviceCode") int serviceCode) {
        CenterService centerService = new CenterServiceImpl();
        BaseController baseController = new BaseController();
        List<Center> centerList = centerService.getCenterList(serviceCode);
        if ((centerList != null) && !(centerList.isEmpty())) {
            baseController.addJsonData("centerList",centerList);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "/getProfessionals/{serviceCode}",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getAllProfessionalList(@PathVariable("serviceCode") int serviceCode){
        BaseController baseController = new BaseController();
        List<Professional> professionalList = professionalDetailService.getAstrologerList(serviceCode);
        if (professionalList != null && !professionalList.isEmpty()) {
            baseController.addJsonData("astrologers", professionalList);
        }
        else {
            baseController.getJsonResponse().setStatus(false);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "/getProfessionalName/{id}",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getAstrologer(@PathVariable("id") int id) {
        BaseController baseController = new BaseController();
        lk.appointment.professional.dto.Professional professional = professionalDetailService.getAstrologer(id);
        if (professional != null) {
            baseController.getJsonResponse().addData("professionalName",professional);
        }
        else {
            baseController.getJsonResponse().setStatus(false);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "getNote/professional/{astroId}/center/{centerId}",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getSpecNote(@PathVariable("astroId") int astroId,@PathVariable("centerId") int centerId) {
        /*AstrologerDetailService astrologerDetailService = new AstrologerDetailServiceImpl();*/
        SpecialNote specialNote= professionalDetailService.getAstroSpecNote(astroId, centerId);
        BaseController baseController = new BaseController();
        if (specialNote.getNoteId() != null) {
            baseController.addJsonData("specialNote",specialNote);
        }
        else {
            baseController.getJsonResponse().setStatus(false);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "checkAstrologer/astrologer/{astroId}/center/{centerId}",method = RequestMethod.GET)
    @ResponseBody
    public AllocateAstrologer isCheckAstrologerCenter(@PathVariable("astroId") int astroId,@PathVariable("centerId") int centerId) {
        /*AstrologerDetailService astrologerDetailService = new AstrologerDetailServiceImpl();*/
        return professionalDetailService.isCheckAstrologerCenter(astroId,centerId);
    }

    @RequestMapping(value="getSpeciality/{professionalCode}",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getSpeciality(@PathVariable("professionalCode") int professionalCode) {
        /*AstrologerDetailService astrologerDetailService = new AstrologerDetailServiceImpl();*/
        BaseController baseController = new BaseController();
        List<ProfessionalSpecialization> professionalSpecializations = professionalDetailService.getSpeciality(professionalCode);
        baseController.addJsonData("specialityList", professionalSpecializations);

        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "getTermsAndConditions/{serviceCode}",method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getTermsAndConditions(@PathVariable("serviceCode") int serviceCode) {
        List<ServiceType> serviceTypeList= professionalDetailService.getTermAndConditions(serviceCode);
        BaseController baseController = new BaseController();
        if (!serviceTypeList.isEmpty() && serviceTypeList.size()!=0) {
            baseController.addJsonData("termAndCondition",serviceTypeList);
        }
        else {
            baseController.getJsonResponse().setStatus(false);
        }
        return baseController.getJsonResponse();
    }


    @RequestMapping(value = "/fillName",method = RequestMethod.GET,headers="Accept=*/*")
    @ResponseBody
    public List<lk.appointment.professional.dto.Professional> fillName(@RequestParam("term") String nameLetters) {
        BaseController baseController = new BaseController();
        int serviceCode=1;
        /*List<Professional> professionalList = astrologerDetailService.getFillName(serviceCode,nameLetters);*/
        /*if (!professionalList.isEmpty() && professionalList.size()!=0) {
            baseController.addJsonData("nameList",professionalList);
        }
        else {
            baseController.getJsonResponse().setStatus(false);
        }
        return baseController.getJsonResponse();*/
        return null;
    }

    @RequestMapping(value = "fillCenterName/{serviceCode}",headers = "Accept=application/json",method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse fillCenterName(@PathVariable("serviceCode") int serviceCode,HttpServletRequest request) {
        String centerName = request.getParameter("term");
        BaseController baseController = new BaseController();
        /*List<ProfessionalCenter> professionalCenterList= astrologerDetailService.getCenterName(serviceCode,centerName);
        if (!professionalCenterList.isEmpty() && professionalCenterList.size()!=0) {
            baseController.getJsonResponse().addData("centerList",professionalCenterList);
        }
        else {
            baseController.getJsonResponse().setStatus(false);
        }*/
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "getCenterName/{centerCode}",method = RequestMethod.GET)
    @ResponseBody
    public CenterDTO getCenterName(@PathVariable("centerCode") String centerCode) {
        return centerService.getCenterName(centerCode);
    }

    @RequestMapping(value = "/getNames/{serviceCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public JsonResponse getNameList(@RequestParam("term") String term,@PathVariable("serviceCode") int serviceCode) {
        BaseController baseController = new BaseController();
        List<String> ProfessionalList = professionalDetailService.getFillName(serviceCode,term);
        if (ProfessionalList != null && !ProfessionalList.isEmpty()) {
            baseController.addJsonData("list",ProfessionalList);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "/getCenters/{serviceCode}", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public JsonResponse getCenterList(@RequestParam("term") String term,@PathVariable("serviceCode") int serviceCode) {
        BaseController baseController = new BaseController();
        List<String> centerList = professionalDetailService.getCenterName(serviceCode, term);
        if (centerList != null && !centerList.isEmpty()) {
            baseController.addJsonData("list",centerList);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "/getCitiesOfveterinary/{serviceCode}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public JsonResponse getCityListOfveterinary(@RequestParam("term") String term,@PathVariable("serviceCode") int serviceCode) {
        BaseController baseController = new BaseController();
        List<String> cityList = professionalDetailService.getCities(serviceCode,term);
        if (cityList != null && !cityList.isEmpty()) {
            baseController.addJsonData("list",cityList);
        }
        return baseController.getJsonResponse();
    }

    @RequestMapping(value = "/getTempAppointment/{refNo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Appointment getTempAppointmentDetails(@PathVariable("refNo") String refNo) {
        Appointment appointment = bookingService.getPaymentDetails(refNo);
        return appointment;
    }

}
