package lk.appointment.professional.controller;

import lk.appointment.professional.domain.*;
import lk.appointment.professional.dto.PaymentDetail;
import lk.appointment.professional.service.BookingService;
import lk.appointment.professional.service.Impl.BookingServiceImpl;
import lk.appointment.professional.service.Impl.SmsServiceImpl;
import lk.appointment.professional.service.PaymentService;
import lk.appointment.professional.service.SmsService;
import lk.appointment.professional.service.ValidateReqService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static lk.appointment.professional.utilities.Utility.getTimeFormat;

/**
 * Created by se-7 on 5/16/2017.
 */
@Controller
public class PaymentController {

    private static final Logger LOG = Logger.getLogger(PaymentController.class);

    private ValidateReqService validateReqService;
    private PaymentService payment;
    BookingService bookingService ; //= new BookingServiceImpl();

    @Autowired
    private SmsService smsService;

    @Autowired
    public PaymentController(ValidateReqService validateReqService,PaymentService payment,BookingService bookingService) {
        this.validateReqService=validateReqService;
        this.payment = payment;
        this.bookingService = bookingService;
    }

    @RequestMapping(value = "/payNow", method = {RequestMethod.POST,RequestMethod.GET})
    public String doPayment (ModelMap map, HttpServletRequest request) {
        String key = request.getParameter("key");
        String paymentChannel = request.getParameter("PaymentChannel");
        PaymentRequest paymentRequest = null;

        try {
            if (validateReqService.isRequestValidate(key)) {
                Appointment appointment = bookingService.validateTemporyBooking(key,paymentChannel);

                if (appointment != null) {
                    paymentRequest = bookingService.generatePaymentGatewayRequest(appointment);
                }

                if (paymentRequest != null) {
                    map.addAttribute("redirect",paymentRequest);
                    return "PaymentRedirect";
                }
                else {
                    map.addAttribute("status","fail");
                    map.addAttribute("serviceCode",appointment.getServiceCode());
                    return "FailPage";
                }
            }
            else {
                map.addAttribute("status","fail");
                return "FailPage";
            }
        }
        catch(Exception e) {
            //LOG.error("/PayNow",e);
            return "FailPage";
        }
    }


    @RequestMapping(value = "/paymentgateways", method = {RequestMethod.GET, RequestMethod.POST})
    public String connectPayment(HttpServletRequest request) {


        HttpSession session = request.getSession();
        ModelMap model = new ModelMap();
        String refNo;
        String totalfee;
        String AppNo;
        String customerName;
        String centerName;
        String paymentMode;
        String roomNo;
        String AppDate = "";
        String email ="";
        String nic = "";
        String appTime = "";
        String appNo = "";
        String transDate = "";
        String transDateNotFormt = "";
        String astrologerName = "";
        String mobileNo = "";
        String address = "";
        String advncePay = "";

        String payMode = request.getParameter("paymentMode");
        Appointment appointment = new Appointment();
        try {
            appointment.setPaymentType(payMode);

            BookingService bookingService = new BookingServiceImpl();
            SmsService smsService = new SmsServiceImpl();

            ServiceResponse serviceResponse = bookingService.confirmTemporaryBooking(appointment, request);
            LOG.info("<<<>><>><><><><><><><><><><>/responsePaymentGateway - appointment<><><<><><<><><><><><><><><<><>" + appointment.toString());
            if (serviceResponse.isStatus()) {
                refNo = serviceResponse.getDataList().get(0).toString();
                Appointment appointment1 = bookingService.getPaymentDetails(refNo);
                if (appointment1 != null) {
                    AppDate = appointment1.getAppDate();
                    customerName = appointment1.getCustomerName();
                    roomNo = appointment1.getRoomNo();
                    totalfee = appointment1.getAmount();
                    nic = appointment1.getNic();
                    appTime = getTimeFormat(appointment1.getGivenTime());
                    appNo = appointment1.getAppNo();
                    transDateNotFormt = appointment1.getTransDate();
                    transDate = transDateNotFormt.substring(0, transDateNotFormt.length() - 2);
                    astrologerName = appointment1.getTitle() + " " + appointment1.getFirstName() + " " + appointment1.getSurName();
                    mobileNo = appointment1.getMobileNo();
                    email = appointment1.getEmail();
                    centerName = appointment1.getCenterName();
                    address = appointment1.getAddress1() + "," + appointment.getAddress2() + "," + appointment.getAddress3() + "," + appointment1.getCity();
                    advncePay = appointment1.getEchFee();
                    String cusMobNo = customerName.substring((customerName.length() - 10), customerName.length());
                    String sms_message = ""+ appointment1.getTitle()+" "+appointment1.getFirstName()+" "+appointment1.getSurName() +" on "+ appointment1.getAppDate() +" at "+ getTimeFormat(appointment1.getGivenTime())+". Time allocated is subject to changed. RefNo."+refNo+ "." + "AppNo." +appointment1.getAppNo()+ ". "+ "Address." + appointment1.getAddress1()+" "+appointment1.getAddress2()+" "+appointment1.getAddress3()+"," +appointment1.getCity()+ ".";
                    String smsProf = "You have an appointment on "+ appointment1.getAppDate() +" at "+ getTimeFormat(appointment1.getGivenTime())+"";
                    boolean isSmsSuccessForCus = smsService.sendViaMobitel(cusMobNo, appointment1,sms_message,smsProf);
                    boolean isSmsSuccessForProf = smsService.sendViaMobitel(mobileNo, appointment1,sms_message,smsProf);
                    model.addAttribute("refNo", refNo);
                    session.setAttribute("refNo", refNo);
                    model.addAttribute("advncePay", advncePay);
                    session.setAttribute("advncePay", advncePay);
                    model.addAttribute("AppDate", AppDate);
                    session.setAttribute("AppDate", AppDate);
                    model.addAttribute("customerName", customerName.split("-")[0]);
                    session.setAttribute("customerName", customerName.split("-")[0]);
                    model.addAttribute("customerNo", customerName.split("-")[1]);
                    session.setAttribute("customerNo", customerName.split("-")[1]);
                    model.addAttribute("transDate", transDate);
                    session.setAttribute("transDate", transDate);
                    model.addAttribute("astrologerName", astrologerName);
                    session.setAttribute("astrologerName", astrologerName);
                    model.addAttribute("mobileNo", mobileNo);
                    session.setAttribute("mobileNo", mobileNo);
                    model.addAttribute("centerName", centerName);
                    session.setAttribute("centerName", centerName);
                    model.addAttribute("address", address);
                    session.setAttribute("address", address);
                    model.addAttribute("appNo", appNo);
                    session.setAttribute("appNo", appNo);
                    model.addAttribute("roomNo", roomNo);
                    session.setAttribute("roomNo", roomNo);
                    model.addAttribute("totalfee", totalfee);
                    session.setAttribute("totalfee", totalfee);
                    model.addAttribute("nic", nic);
                    session.setAttribute("nic", nic);
                    model.addAttribute("appTime", appTime);
                    session.setAttribute("appTime", appTime);
                    session.setAttribute("email", appointment1.getEmail());
                    session.setAttribute("serviceCode", appointment1.getServiceCode());
                    if ("2".equals(appointment1.getServiceCode())) {
                        session.setAttribute("petName", appointment1.getPet().getPetName());
                        session.setAttribute("petType", appointment1.getPet().getPetType());
                        session.setAttribute("petDob", appointment1.getPet().getDob());
                        session.setAttribute("petRegNo", appointment1.getPet().getRegNo());
                    } else if ("1".equals(appointment1.getServiceCode())) {
                        session.setAttribute("bDate", appointment1.getB_date());
                        session.setAttribute("bTime", appointment1.getB_time());
                        session.setAttribute("bCountry", appointment1.getB_country());
                        session.setAttribute("bTown", appointment1.getB_town());
                    }
                }
                return "paymentConfirm";
                } else {
                    model.addAttribute("errMessage", serviceResponse.getErrors());
                    session.setAttribute("errMessage", serviceResponse.getErrors());
                    LOG.error("<<<<<<<<<<<<<<><><><><><><><><><<<</responsePaymentGateway>>>>>>>>>>>>>><<>>><><>"+ serviceResponse.getErrors());
                    return "FailPage";
            }
        } catch (Exception e) {
            LOG.error("<<<<<<<<<<<<<<><><><><><><><><><<<</responsePaymentGateway>>>>>>>>>>>>>><<>>><><>"+ e);
            return "FailPage";
        }
    }

    @RequestMapping(value = "/checkBalance", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public PaymentDetail checkBalance(@RequestBody BalanceInquiry balanceInquiry ) {
        return payment.checkBalance(balanceInquiry.getPhoneNumber(),balanceInquiry.getFullAmt(),balanceInquiry.getAgent());
    }
    @RequestMapping(value = "/addToBill", method = {RequestMethod.GET, RequestMethod.POST})/*/{securityKey}/{paymentChannel}*/
    @ResponseBody
    public PaymentRequest addToBill( @RequestBody Confirmation confirmation) {
        Appointment appointment = bookingService.validateTemporyBooking(confirmation.getKey(), confirmation.getPaymentChannel());
        if(appointment!=null){
            appointment.setTelNo(confirmation.getPhoneNumber());
            appointment.setAmount(confirmation.getTotAmt());
        }
        return payment.getPaymentRequest(appointment);
    }

    @RequestMapping(value = "/otp_confirmation", method = {RequestMethod.GET, RequestMethod.POST})
    public String otpConfirm(Model model,String otp1,String otp2,String otp3,String otp4,String otp5,String otp6,String refNo,
                             String astrologerName,String centerName,String address,String AppDate,
                             String appTime,String appNo,String customerName,String profNo,String email,String mobile){
        boolean confirm=false;
        Appointment app= new Appointment();
        String otp=otp1+otp2+otp3+otp4+otp5+otp6;

        boolean resultCode = validateReqService.ConfirmOTPComparison(refNo, otp);
        if(resultCode){
            confirm = bookingService.confirmOTP(refNo);
        }
        if(confirm){
            app.setRefNo(refNo);
            app.setMobileNo(profNo);
            model.addAttribute("astrologerName",astrologerName);
            model.addAttribute("centerName",centerName);
            model.addAttribute("address",address);
            model.addAttribute("AppDate",AppDate);
            model.addAttribute("appTime",appTime);
            model.addAttribute("appNo",appNo);
            model.addAttribute("customerName",customerName);
            model.addAttribute("mobileNo",mobile);
            model.addAttribute("profNo",profNo);
            model.addAttribute("email",email);
            String sms_message = ""+ astrologerName +" on "+ AppDate +" at "+ getTimeFormat(appTime)+". Time allocated is subject to changed. RefNo."+refNo+ " ," + "AppNo." +appNo+ ". https://vchat.effectivesolutions.xyz/e-channelling?patient="+refNo;
            String smsProf = "You have an appointment on "+ AppDate +" at "+ getTimeFormat(appTime)+"";
            smsService.sendViaMobitel(mobile, app,sms_message,smsProf);
            return  "paymentConfirm";
        }
        return "FailPage";
    }

    @RequestMapping(value = "/otp-resend/{id}", method = RequestMethod.GET)
    public String otpResend(@PathVariable("id") int id,ModelMap model) {
        Appointment appointment= bookingService.resendOtp(id);
        String sms_message = "Your OTP Number is "+appointment.getSecurityKey();
        boolean success = smsService.sendViaMobitel(appointment.getTelNo(),appointment,sms_message,"");
        model.addAttribute("ajaxResponse",success);
        return  "ajax-response";
    }

}
